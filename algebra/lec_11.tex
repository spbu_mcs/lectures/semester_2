\lecture{11}{27.03.2024, 13:40}{}

\begin{proof}
  Идея заключается в отражении относительно гиперплоскости \(v - v'\) (\(v, v'\) --- некоторые
  векторы). Разберем несколько случаев.

  \begin{pfparts}
    \item[\(\boxed{Q(v - v') \neq 0}\)] \(S_{v - v'}(v) = v - \frac{2B(v - v', v)}{Q(v - v')}(v -
      v')\):
      \begin{itemize}
        \item \(B(v - v', v) = B(v, v) - B(v', v) = Q(v) - B(v', v)\);
        \item \(Q(v - v') = B(v - v', v - v') = \underbrace{B(v, v)}_{Q(v)} - B(v', v) - B(v, v') +
          \underbrace{B(v', v')}_{Q(v') = Q(v)} = 2Q(v) - 2B(v', v)\).
      \end{itemize}

      Получилось, что \(S_{v - v'}(v) = v - (v - v') = v'\) --- нашлась искомая изометрия.

    \item[\(\boxed{Q(v - v') = 0}\)] Пусть \(B(v', v) = \alpha\): \(Q(v + v') = B(v, v) + 2B(v', v)
      + B(v', v') = 4\alpha \neq 0\).

      \mnote{или же можно отразить относительно гиперплоскости: \(S_{v'}(v') = -v'\)}
      Заметим, что \(Q(-v') = B(-v', -v') = Q(v)\): применяя предыдущий случай к \(v' \mapsto -v'\),
      получается \(S_{v + v'}(v) = -v'\). Как тривиально это бы не казалось, чтобы получить \(v'\)
      достаточно домножить на \(-1\) (применить отображение \(-\id\), ведь \(-\id \in O(V, Q)\)).
  \end{pfparts}
\end{proof}

\begin{remark}
  Для \(\alpha = 0\) лемма тоже верна, но доказательство сложнее, а надобности в этом нет.
\end{remark}

Теперь можно приступить к доказательствам теорем Витта. Для удобства продублируем их формулировки.

\begin{quote}
  \small
  Пусть есть квадратичные пространства \((V, Q)\), \((U_1, Q_1)\) и \((U_2, Q_2)\) (\textit{полностью
  произвольные}).

  \begin{theorem}[Витта о сокращении]
    Если \((U_1, Q_1) \boxplus (V, Q) \cong_i (U_2, Q_2) \boxplus (V, Q)\), то можно ``сократить'':
    \((U_1, Q_1) \cong_i (U_2, Q_2)\).
  \end{theorem}
\end{quote}

\begin{proof}
  \hfill
  \begin{enumerate}
    \item Сведем всё к случаю, когда все \(3\) пространства невырожденные.

      Поймем, как выглядит \(\Rad\left( (V, Q) \boxplus (U_1, Q_1) \right)\): если \((u, v) \in
      \Rad(\ldots)\), то \(\forall (x, y) \quad B(v, x) + B_1(u, y) = 0\); взяв \(x\) или \(y\) за
      \(0\), получится, что \(v \in \Rad(V, Q)\) и \(u \in \Rad(U, Q_1)\) соответственно, и
      наоборот: \(\Rad\left( (V, Q) \boxplus (U_1, Q_1) \right) \cong_i \Rad(V, Q) \boxplus
      \Rad(U_1, Q_1)\).

      \begin{statement}
        Если два пространства изоморфны изометрично, то их радикалы тоже.
      \end{statement}
      \begin{proof}
        Пусть есть пространства \((W, B)\) и \((W', B')\) с изометрией \(\psi\colon W \to W'\).

        Возьмем \(w \in \Rad(W, B)\): \(\psi(w) \in \Rad(W', B')\), ведь \(B'(\psi(w), x) =
        B'(\psi(w), \psi(\psi^{-1}(x))) = B(w, \psi^{-1}(x)) = 0\) по сохранению билинейной формы.
      \end{proof}

      Получается, что \(\Rad(V, Q) \boxplus \Rad(U_1, Q_1) \cong_i \Rad(V, Q) \boxplus \Rad(U_2,
      Q_2)\): формы у таких пространств тождественна \(0\), то есть этот изоморфизм указывает лишь
      на равенство размерностей и сократить можно: \(\Rad(U_1, Q_1) \cong_i \Rad(U_2, Q_2)\).

      \begin{statement}
        Если пространства изоморфны изометрично, то они изоморфно изометрично и как факторы по их
        радикалам.
      \end{statement}
      \begin{proof}
        Пусть \((W, B) \cong_i (W', B')\): чтобы построить \(\cong_i\) для факторпространств нужно
        воспользоваться универсальным свойством и показать корректность (которая уже предоставлена в
        предыдущем утверждении): \[
        \begin{tikzcd}
          \qfrac{W}{\Rad{W}} \arrow[r] & \qfrac{W'}{\Rad{W'}} \\
          W \arrow[u] \arrow[ur] \arrow[r] & W'. \arrow[u]
        \end{tikzcd}
        \]
      \end{proof}

      По лемме, которая когда-то уже встречалась:
      \begin{itemize}
        \item \((V, Q) \cong_i \left( \qfrac{V}{\Rad{V}}, \bar{Q} \right) \boxplus \left( \Rad{V}, 0
          \right)\);
        \item \((U_1, Q_1) \cong_i \left( \qfrac{U_1}{\Rad{U_1}}, \bar{Q}_1 \right) \boxplus \left(
          \Rad{U_1}, 0 \right)\).
      \end{itemize}

      И, как только что было получено, \(\Rad\left( (V, Q) \boxplus (U_1, Q_1) \right) \cong_i
      \left( \Rad{V}, 0 \right) \boxplus \left( \Rad{U}, 0 \right)\).

      Профактуризуем исходный изоморфизм: \[
        \left( \qfrac{V}{\Rad{V}}, \bar{Q} \right) \boxplus \left( \qfrac{U_1}{\Rad{U_1}}, \bar{Q}_1
        \right) \cong_i \left( \qfrac{V}{\Rad{V}}, Q \right) \boxplus \left( \qfrac{U_2}{\Rad{U_2}},
        \bar{Q}_2 \right),
      \] --- тут все формы уже невырожденные.

      Если уже известно, что теорема Витта верна для невырожденных форм, то, сокращая, будет
      \(\left( \qfrac{U_1}{\Rad{U_1}}, \bar{Q}_1 \right) \cong_i \left( \qfrac{U_2}{\Rad{U_2}},
      \bar{Q}_2 \right)\).

      С другой стороны, опять вспоминая лемму:
      \begin{itemize}
        \item \((U_1, Q_1) \cong_i \left( \qfrac{U_1}{\Rad{U_1}}, \bar{Q}_1 \right) \boxplus \left(
          \Rad{U_1}, 0 \right)\);
        \item \((U_2, Q_2) \cong_i \left( \qfrac{U_2}{\Rad{U_2}}, \bar{Q}_2 \right) \boxplus \left(
          \Rad{U_2}, 0 \right)\).
      \end{itemize}

      Как видно, \(\left( \Rad{U_1}, 0 \right) \cong_i \left( \Rad{U_2}, 0 \right)\) по равенству
      размерностей; \(\left( \qfrac{U_2}{\Rad{U_2}}, \bar{Q}_2 \right) \cong_i \left(
      \qfrac{U_2}{\Rad{U_2}}, \bar{Q}_2 \right)\).

      Значит теорему Витта достаточно рассматривать только для невырожденных форм.

    \item Применяя теорему Лагранжа, теорему Витта достаточно доказать только для случая \(\dim{V} =
      1\), считая, что форма на нем \(\langle \alpha \rangle\), где \(\alpha \neq 0\) по
      невырожденности (для размерности больше \(1\) --- по индукции).

      Имеется изометрия \(\varphi\): \(\underbrace{\langle \alpha \rangle}_{e_1} \boxplus (U_1, Q_1)
      \cong_i \underbrace{\langle \alpha \rangle}_{e_2} \boxplus (U_2, Q_2)\); взяли слева и справа
      образующие \(e_1\) и \(e_2\). Для удобства положим, что форма в правой части изоморфизма ---
      \(Q\).

      \(Q(\varphi(e_1, 0)) = Q(e_1, 0) = \alpha\) и \(Q(e_2, 0) = \alpha\) --- \(e_1\) и \(e_2\)
      лежат на одной аффинной квадрике, а значит (по соответствующей лемме) \(\exists \psi\colon
      \langle \alpha \rangle \boxplus (U_1, Q_1) \to \langle \alpha \rangle \boxplus (U_2, Q_2)\)
      такая, что \(\psi \circ \varphi(e_1, 0) = (e_2, 0)\).

      Тогда заменим \(\varphi \mapsto \psi \circ \varphi\) (композиция изометрий --- изометрия):
      \(\varphi(e_1, 0) = (e_2, 0)\).

      Осталось придумать изометрии для \((U_1, Q_1)\) и \((U_2, Q_2)\). Посмотрим на ортогональное
      дополнение к \(\langle (e_1, 0) \rangle\) (прямая, натянутая на \((e_1, 0)\)) --- оно равно
      \(\langle (U_1, Q_1) \rangle\), ведь \(0 = B((e_1, 0), (\beta e_1, u)) = \alpha\beta, \alpha
      \neq 0 \implies \beta = 0\).

      Аналогично, ортогональное дополнение к \(\langle (e_2, 0)\rangle\) --- \((U_2, Q_2)\). Тогда,
      раз \(\varphi\) переводит ортогональное дополнение в оное, то это и есть искомая изометрия.
  \end{enumerate}
\end{proof}

\begin{quote}
  \small
  А теперь пусть \(U_1, U_2 \leqslant V\) и \(Q\restriction_{U_1}\) невырожденная.

  \begin{theorem}[Витта о продолжении]
    Если \(Q\restriction_{U_1} \cong_i Q\restriction_{U_2}\) зафиксированной \(\varphi\), то
    существует изометрия всего пространства, продолжающее заданную изометрию двух подпространств:
    \(\tilde{\varphi}\) такое, что \(\tilde{\varphi}\restriction_{U_1} = \varphi\).
  \end{theorem}
\end{quote}

\begin{proof}
  Уже известно (по одной из лемм), что:
  \begin{itemize}
    \item \((V, Q) \cong_i \left( U_1, Q\restriction_{U_1} \right) \boxplus \left( U_1^\perp,
      Q\restriction_{U_1^\perp} \right)\);
    \item \((V, Q) \cong_i \left( U_2, Q\restriction_{U_2} \right) \boxplus \left( U_2^\perp,
      Q\restriction_{U_2^\perp} \right)\).
  \end{itemize}

  По теореме Витта о сокращении есть изометрия \(\psi\): \(\left( U_1^\perp,
  Q\restriction_{U_1^\perp} \right) \cong_i \left( U_2^\perp, Q\restriction_{U_2^\perp} \right)\).

  Зададим \(\tilde{\varphi}(u, v) \coloneqq (\varphi(u), \psi(v))\) --- сразу видно, что она
  продолжает исходную, ведь \(\tilde{\varphi}(u, 0) = (\varphi(u), 0)\). Проверим, что
  \(\tilde{\varphi}\) и впрямь изометрия: \(Q\left( \tilde{\varphi}(u, v) \right) = Q(\varphi(u),
  \psi(v)) = Q(\varphi(u)) + Q(\psi(v)) = Q(u) + Q(v) = Q((u, v))\) \mnote{\(B((u, v), (u, v)) =
  B(u, u) + B(v, v)\)} --- всё так.
\end{proof}

\begin{remark}
  Случай \(\dim{U_1} = 1\) был доказан в начале лекции.
\end{remark}

Только этими теоремами Витта мы не ограничимся, но перед следующей надо запастись еще некоторыми
понятиями.

Понятно, что одномерные пространства (с квадратичной формой) задаются \(\langle \alpha \rangle\). А
как выглядят двумерные, с невырожденной формой и \textit{изотропные}?

\begin{definition}
  Пространство \((V, Q)\) называются анизотропным, если \(\forall v \neq 0 \quad Q(v) \neq 0\).
\end{definition}

\begin{remark}
  \(Q(v)\) --- непрерывная функция, поэтому если она никогда не равна \(0\), то она знакопостоянна.
\end{remark}

\begin{definition}
  Пространство \((V, Q)\) называются изотропным, если оно не анизотропное.
\end{definition}

Возьмем \(e \neq 0\) такое, что \(Q(e) = 0\). Раз форма невырожденная, то \(\exists v\colon B(e, v)
= 1\). Если заменить \(v \mapsto v + te\), то \(B(e, v + te) = B(e, v) + tQ(e) = 1\) --- с \(B\)
ничего не поменяется; \(Q(v + te) = Q(v) + 2tB(v, e) + t^2\underbrace{Q(e)}_0\).

\(t\) можно выбрать такое (решив линейную систему уравнений),\mnote{то есть \(t = -\frac{Q(v)}{2}\)}
что \(Q(v + te) = 0\); обозначим \(f \coloneqq v + te\).

Тогда матрица Грама (в базисе \((e, f)\)) такого двумерного пространства выглядит так (любое можно
привести к такому виду): \[
  \Gamma = \begin{pmatrix}
    0 & 1 \\
    1 & 0
  \end{pmatrix}
.\]

\begin{definition}
  Такое квадратичное пространство называется гиперболической плоскостью.
\end{definition}

\begin{notation}
  \(\mathcal{H}\) --- гиперболическая плоскость.
\end{notation}

Этого хватит для еще одной теоремы Витта.

Пусть \((V, Q)\) --- пространство с невырожденной (раннее было показано, как произвольную форму
свести к такой) квадратичной формой.

\begin{theorem}[Витта о структуре квадратичной формы]
  \[
    (V, Q) \cong_i \underbrace{\mathcal{H} \boxplus \ldots \boxplus \mathcal{H}}_r \boxplus (V_0, Q_0),
  \] где \((V_0, Q_0)\) анизотропно, причем \(r\) определно однозначно, а \((V_0, Q_0)\) 
  однозначно с точностью до изометрии.
\end{theorem}

\begin{definition}
  Такой \(r\) называется индексом Витта.
\end{definition}

\begin{proof}
  Будем считать, что само \((V, Q)\) изотропно, (иначе \(r = 0\), так как в \(\mathcal{H}\) нашелся
  бы изотропный вектор).

  Рассмотрим изотропный вектор \(e\): \(Q(e) = 0\). Чуть выше почти в общем случае (используя только
  невырожденность) было показано, что \(\exists f\colon B(e, f) = 1, Q(f) = 0\).

  Понятно, что \(Q\restriction_{\langle e, f \rangle} \cong_i \mathcal{H}\). Перейдем к
  ортогональному дополнению: \((V, Q) \cong_i \mathcal{H} \boxplus (V', Q')\); \(\dim{V'} <
  \dim{V}\), поэтому можно воспользоваться индукцией --- \((V', Q') \cong_i \underbrace{\mathcal{H}
  \boxplus \ldots \boxplus \mathcal{H}}_{r - 1} \boxplus (V_0, Q_0)\).

  По теореме Витта о сокращении, \(r\) и \((V_0, Q_0)\) определены однозначно.
\end{proof}

Напоследок можно, используя всё это, доказать закон инерции Сильвестра.

\begin{proof}
  Пусть есть два разных \(\langle 1, \ldots, 1, -1, \ldots, -1, 0, \ldots 0 \rangle\) с \((p, q)\) и
  \((p', q')\) (кол-во \(1\) и \(-1\) соответственно); количество нулей ---  \(\dim\Rad{B}\).

  Факторизуя по \(\Rad\), избавимся от всех \(0\).

  \(\langle 1, -1 \rangle \cong_i \mathcal{H}\); тогда, ``сокращая'' так эти плоскости, получится
  \(\mathcal{H}^{\min(p, q)} \boxplus \langle 1, \ldots, 1 \rangle\) и аналогично для штрих-версии.

  Но тогда они обязаны быть равными, ведь иначе можно было бы вытащить еще одну плоскость и/или
  сократить по теореме Витта.
\end{proof}

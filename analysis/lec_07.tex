\lecture{7}{07.03.2024, 15:35}{}

\begin{statement}
  \(\sum_{j = 1}^\infty \sum_{p + q = j} a_pb_q = \sum_{j = 1}^\infty a_{\varphi(j)}b_{\psi(j)} =
  \sum_{k = 1}^\infty a_k \sum_{k = 1}^\infty b_k\) при условии, что \(\sum_{j = 1}^\infty \sum_{p +
  q = j} a_pb_q\) сходится абсолютно.
\end{statement}
\begin{proof}
  Рассмотрим матрицу (таблицу): \[
  \begin{pmatrix}
    a_1b_1 & \textcolor{blue}{a_1b_2} & \textcolor{red}{a_1b_3} & \ldots \\
    \textcolor{blue}{a_2b_1} & \textcolor{blue}{a_2b_2} & \textcolor{red}{a_2b_3} & \ldots \\
    \textcolor{red}{a_3b_1} & \textcolor{red}{a_3b_2} & \textcolor{red}{a_3b_3} & \ldots \\
    \vdots & \vdots & \vdots & \ddots
  \end{pmatrix}
  ,\] --- если получится придумать обход по ней, то искомое равенство будет верным.

  Рассмотрим произведение \(\sum_{k = 1}^n a_k \sum_{k = 1}^n b_k\) в форме \(\sum_{i, j}^n
  a_ib_j\) и начнем обход по таблице: \[
  \begin{array}{cl}
    & a_1b_1 \\
    + & a_1b_2 + a_2b_2 + a_2b_1 \\
    + & a_1b_3 + a_2b_3 + a_3b_3 + a_3b_2 + a_3b_1 \\
    + & \ldots \\
    \vdots & \ddots
  \end{array}
  \]

  Тогда видно, что \(\lim_{n \to \infty} \sum_{i, j}^n a_ib_j = \sum_{j = 1}^\infty
  a_{\varphi(j)}b_{\psi(j)}\), а абсолютная сходимость позволяет упорядочивать члены как угодно.
\end{proof}

\begin{example}
  Пусть \(f(x) = e^{x_1 + x_2 + \ldots + x_n}\), \(f\colon \R^n \to \R\).

  Распишем \(f\): \[
    \begin{aligned}
      f(x) &= \left( \sum_{k_1 = 0}^\infty \frac{x_1^{k_1}}{k_1!} \right)\left( \sum_{k_2 =
      0}^\infty \frac{x_2^{k_2}}{k_2!} \right) \ldots \left( \sum_{k_n = 0}^\infty
      \frac{x_n^{k_n}}{k_n!} \right) = \sum_{k \in \left( \N_0 \right)^n} \frac{x^k}{k!},
    \end{aligned}
  \] где \(k\) --- мультииндекс; стоит заметить, что \(k! = k_1! \ldots k_n!\), \(|k| = k_1 + \ldots
  + k_n\) и \(x^k = x_1^{k_1} \ldots x_n^{k_n}\).

  В, может быть, более понятном виде: \[
    f(x) = \sum_{j = 0}^\infty \sum_{|k| = j} \frac{x^k}{k!}
  .\]

  На самом деле оказывается, что \[
    f(x) = \sum_{j = 0}^\infty \frac{(x_1 + \ldots + x_n)^j}{j!} = \sum_{j = 0}^\infty \sum_{|k| =
    j} \frac{x^k}{k!}
  .\]
\end{example}

Это наводит на следующее обобщение бинома Ньютона:
\begin{statement}
  \[
    \frac{(x_1 + \ldots + x_n)^j}{j!} = \sum_{|k| = j} \frac{x^k}{k!}
  .\]
\end{statement}

Произведем первое знакомство с многомерным рядом Тейлора! Это понадобится для выработки интуиции о
нем.

\begin{definition}
  Частная производная \(\frac{\partial f}{\partial x_1} \coloneqq \lim_{\Delta x \to 0} \frac{f(x_1
  + \Delta{x}, x_2, \ldots, x_n) - f(x_1, \ldots, x_nn)}{\Delta{x}}\).
\end{definition}

\begin{definition}
  \(f^{(k)}(x) = f^{(k_1, \ldots, k_n)}(x) \coloneqq \frac{\partial^{|k|} f}{\partial x_1^{k_1}
  \partial x_2^{k_2} \ldots \partial x_n^{k_n}}\).
\end{definition}

Далее будет показано, что порядок не важен.

\begin{example}
  \[
    f(x) = e^{x_1 + \ldots + x_n} \implies f(x) = \sum_{j = 0}^\infty \sum_{|k| = j}
    \frac{f^{(j)}(0)}{k!} x^k
  .\]
\end{example}

В будущем мы еще вернемся к этому и обобщим.

\subsection{Двойные ряды}

\begin{definition}
  Двойной ряд --- \(\sum_{i, j = 1}^\infty a_{ij}\).
\end{definition}

\begin{notation}
  \(S_{mn} \coloneqq \sum_{i = 1}^m \sum_{j = 1}^n a_{ij}\).
\end{notation}

\begin{definition}
  Говорят, что двойной ряд \(\sum_{i, j} a_{ij}\) сходится к \(S\), если \[
    \forall \varepsilon > 0
    \exists N = N(\varepsilon)\colon m, n > N \implies |S_{mn} - S| < \varepsilon
  .\]
\end{definition}

Графически, это сумма по прямоугольникам: \[
\begin{pmatrix}
  a_{11} & a_{12} & \ldots & a_{1n} & \ldots \\
  a_{21} & a_{22} & \ldots & a_{2n} & \ldots \\
  \vdots & \vdots & & \vdots & \\
  a_{m1} & a_{m2} & \ldots & a_{mn} & \ldots \\
  \vdots & \vdots & & \vdots & \ddots \\
\end{pmatrix}
.\]

\begin{theorem}[критерий Коши для сходимости двойного ряда]
  Двойной ряд \(\sum a_{ij}\) сходится тогда и только тогда, когда \[
    \forall \varepsilon > 0 \exists N = N(\varepsilon)\colon \forall m, n, p, q > N \quad |S_{mn} -
    S_{pq}| < \varepsilon
  .\]
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\implies}\)] Очень очевидно.

    \item[\(\boxed{\impliedby}\)] Последовательность \(S_{nn}\) --- фундаментальная в \(\R\), а
      значит, существует предел, равный \(S\), а \(S_{mn}\) --- всего лишь одна из
      подпоследовательностей.
  \end{pfparts}
\end{proof}

Продолжим обобщать понятия одномерных рядов на двойные.

\begin{definition}
  Ряд \(\sum_{i, j} a_{ij}\) сходится абсолютно, если сходится ряд \(\sum_{i, j} |a_{ij}|\).
\end{definition}

\begin{statement}
  Если двойной ряд сходится абсолютно, то он сходится.
\end{statement}

Достаточно применить критерий Коши, или, например, воспользоваться перестановками и путями в
матрице.

\subsection{Повторные ряды}

\begin{definition}
  Повторный ряд --- \(\sum_{i = 1}^\infty \sum_{j = 1}^\infty a_{ij}\) , \(\sum_{j = 1}^\infty
  \sum_{i = 1}^\infty a_{ij}\).
\end{definition}

То есть суммирование происходит сначала по строкам (столбцам), и лишь затем по столбцам (строкам).

\begin{theorem}
  Если ряд \(\sum_{i, j} a_{ij}\) сходится \textit{абсолютно} (как двойной), то \[
    \sum_{i = 1}^\infty \sum_{j
    = 1}^\infty a_{ij} = \sum_{j = 1}^\infty \sum_{i = 1}^\infty a_{ij} = \sum_{i, j} a_{ij}
  .\]
\end{theorem}
\begin{proof}
  Из сходимости двойного ряда имеется, что \[
    \left| \sum_{i = 1}^m \sum_{j = 1}^n a_{ij} - S \right| < \varepsilon \iff |S_{mn} - S| <
    \varepsilon \quad m, n > N
  .\]

  Сделаем предельный переход при \(n \to \infty\) (\(\sum_{i, j} |a_{ij}| < \infty\), поэтому предел
  будет существовать): \[
    \left| \sum_{i = 1}^m \sum_{j = 1}^\infty a_{ij} - S \right| \le \varepsilon
  ,\] --- а это то, что нужно; аналогично можно проделать и с другой суммой.

  В обратную сторону предлагается доказать самостоятельно.
\end{proof}

\begin{remark}
  Не обязательно, что внутренние ряды повторных будут сходящимися.
\end{remark}

\begin{remark}
  При произведении рядов повторных не возникает: \(\sum_i \sum_j a_ib_j = \sum_i a_i \sum_j b_j\).
\end{remark}

\begin{exercise}
  Существует ли биекция \(\varphi\colon \N \to \N\) такая, что \(\sum \frac{\varphi(n)}{n^2}\)
  сходится?
\end{exercise}

\begin{exercise}
  Верно ли, что если \(\sum_n \sin{a_n}\) сходится, то и \(\sum_n \sin{2a_n}\) тоже сходится?
\end{exercise}

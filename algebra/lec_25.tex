\lecture{25}{21.05.2024, 13:40}{}

Посмотрим, что будет в случае более чем \(2\)-ух групп.

Пусть \((G_i)_{i \in I}\) --- некоторое множество групп.

Прямое произведение задается как обычно: \(\prod_{i \in I} G_i = \left\{ (g_i)_{i \in I} \mid g_i
\in G_i \right\}\) со следующей диаграммой ...., то есть \(\forall f_j \exists! f\colon f_j =
\mathrm{pr}_{j} \circ f\).

Свободное произведение задается ``переворачиванием всех стрелок''.

\mnote{правильнее задать отдельно \((i_1, \ldots, i_k)\) и \((g_1, \ldots, g_k)\)}
Построение почти такое же, как и для двух групп: рассматриваются наборы конечной длины \(\left(
g_{i_1}, \ldots, g_{i_k} \right)\), где \(g_{i_j} \in G_{i_j}\) и \(i_j \neq i_{j + 1}\), причем
\(g_{i_j} \neq 1\); ко всему этому добавляется пустой список \(()\) и всё это кладется в \(X\).

После этого задаем действие каждой \(G_i\) на \(X\):
\begin{enumerate} \mnote{случай пустого множества сокрыт в пункте 2.1}
  \item \(1 \cdot x = x\);
  \item \(g \cdot \left( g_{i_1}, \ldots, g_{i_k} \right) = \begin{cases}
      \left( g_1, g_{i_1}, \ldots, g_{i_k} \right), &i_1 \neq i, \\
      \left( gg_{i_1}, \ldots, g_{i_k} \right), &i_1 = i, gg_{i_1} \neq 1, \\
      \left( g_{i_2}, \ldots, g_{i_k} \right), &i_1 = i, gg_{i_1} = 1.
  \end{cases}\)
\end{enumerate}

\begin{definition}
  \(*_{i \in I} G_i = \langle\text{образы } G_i \text{ в } \Sym(X) \rangle\).
\end{definition}

\begin{notation} \mnote{\(C_\infty \cong (\Z, +)\)}
  \(F(I) \coloneqq *_{i \in I} C_{\infty}\) --- свободная группа на алфавите \(I\).
\end{notation}

Зафиксировав для каждой \(C_\infty\) образующую \(\langle x_i \rangle\), слово в \(F(I)\) выглядит
как \(x_{i_1}^{n_1} x_{i_2}^{n_2} \ldots\), где \(n_1, \ldots \in \Z \setminus \left\{ 0 \right\}\).

\begin{example}
  \(x_1^{10}x_3^{5}x_2^{-3} \cdot x_2^{3}x_3^{-5}x_1^2 = x_1^{12}\).
\end{example}

Вспомним, что для задания гомоморфизма \(C_\infty \to K\) (\(K\) --- группа) достаточно задать \(x
\mapsto k\), где \(x\) --- образующая; то есть \(\Hom(C_\infty, K) \simeq K\).

Теперь сформулируем
\begin{statement}[универсальное свойство свободного произведения]
  \(\Hom(F(I), K) \simeq \Hom(I, K)\), где справа \(I\) и \(K\) воспринимаются как множества.
\end{statement}

Тут проглядывается некоторое сходства с заданием гомоморфизмов на кольцах многочленах (\(x_i \mapsto
k_i\))

Теперь можно формальнее определить задание группы соотношениями.

Зафиксируем множество ``образующих'' \(I\) и \(R \subset F(I)\) --- множество ``соотношений'' (набор
слов в алфавите из образующих и обратных к образующим). Эти слова мы хотим приравнять к \(1\).

Пусть \(X \subseteq G\); как известно, \(\langle X \rangle\) --- пересечение всех (произвольных)
подгрупп \(G\), содержащих \(X\), и по такому совсем не обязательно можно факторизовать.

\begin{definition}
  Нормальное замыкание  \(\langle X \rangle^G\) --- пересечение всех \textit{нормальных} подгрупп,
  содержащих \(G\).
\end{definition}

\begin{statement}
  Раз пересечение нормальных подгрупп --- нормальная подгруппа, то \(\langle X \rangle^G\) тоже
  является таковой.
\end{statement}

\begin{statement}
  \(\langle X \rangle \leqslant \langle X \rangle^G\).
\end{statement}

\begin{definition}
  \(\langle I \mid R \rangle \coloneqq \qfrac{F(I)}{\langle R \rangle^{F(I)}}\).
\end{definition}

Как конструктивно задать \(\langle X \rangle^G\)?

Как известно, \(\langle X \rangle\) --- множество всех слов в алфавите \(X \cup X^{-1}\),
вычисленные внутри \(G\).

\begin{notation}
  \(x^g \coloneqq g^{-1}xg\), где \(x \in X\), \(g \in G\).
\end{notation}

\begin{example}
  \(\left(x^g\right)^h = x^{gh}\).
\end{example}

\begin{example}
  \((xy)^g = x^gy^g\).
\end{example}

\begin{statement}
  \(\langle X \rangle^G = \langle X^G \rangle\), где \(X^G \coloneqq \left\{ x^g \mid x \in X, g \in
  G\right\}\).
\end{statement}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\subseteq}\)] \(\langle X^G \rangle\) --- нормальная подгруппа.

    \item[\(\boxed{\supseteq}\)] Очевидно, ведь оно содержится в каждой нормальной подгруппе,
      содержащей \(X\).
  \end{pfparts}
\end{proof}

\begin{theorem}
  Задать отображение \(\langle I \mid R \rangle \xrightarrow{\varphi} K\) --- всё равно, что задать
  отображение множеств \(f\colon I \to K\) такое, что \(\tilde{f}(R) = 1\) внутри \(K\), где
  \(\tilde{f}\colon F(I) \to K\)
\end{theorem}
\begin{proof}
  Из универсального свойства фактор-группы следует, что такие гомоморфизмы \(\langle I \mid R
  \rangle \xrightarrow{\varphi} K\) в биекции с гомоморфизмами \(\tilde{\varphi}\colon F(I) \to K\) 
  такими, что \(\tilde{\varphi}\restriction_{\langle R \rangle^{F(I)}} = 1\), но это условие
  эквивалентно тому, что \(\tilde{\varphi}(R) = 1\), ведь \(\ker{\tilde{\varphi}}\) --- нормальная
  подгруппа, и по определению следует, что \(\ker\tilde{\varphi} \supseteq R \iff
  \ker\tilde{\varphi} \geqslant \langle R \rangle^{F(I)}\).

  Теперь применим универсальное свойство свободной группы: все гомоморфизмы \(F(I) \to K\) в биекции
  с отображениями множеств \(f\colon I \to K\).
\end{proof}

Цель достигнута: мы показали, что всегда существуют группы, заданные их соотношениями.

\subsection{Полупрямое произведение групп}

\begin{definition}
  Внутреннее полупрямое произведение: \(G = H \rtimes K\), если \(K \leqslant G\), \(H
  \trianglelefteq G\), \(\langle K, H \rangle = G\) и \(K \cap H = \left\{ 1 \right\}\).
\end{definition}

\begin{remark}
  Раз \(H \trianglelefteq G\), то \(\langle K, H \rangle = HK\) по Минковскому.
\end{remark}

\begin{example}
  \(hk \cdot (h'k') = \left(h \cdot\ ^kh'\right) kk'\), где \(^kh' \coloneqq kh'k^{-1} = \left( h'
  \right)^{k^{-1}}\).
\end{example}

\begin{remark}
  \(K \cap H = \left\{ 1 \right\}\) означает, что любой элемент \(G\) однозначно записывается в виде
  \(hk\).
\end{remark}

\begin{example}
  Группы Диэдра \(D_n = C_n \rtimes C_2\).
\end{example}

\begin{example} \mnote{\(A_n\) --- четные перестановки}
  Все перестановки \(S_n = A_n \rtimes C_2\), где из \(C_2\) берется любая транспозиция (например,
  \(\langle (12) \rangle\)).
\end{example}

\begin{example}
  Рассмотрим движения в пространстве (\(\mathrm{Iso}(\R^3)\)): \(\mathrm{Iso}(\R^3) = O_3(\R)
  \rtimes \R^3\), где \(O_3\) --- ортогональные преобразования, и под \(\R^3\) подразумеваются
  параллельные переносы.
\end{example}

\begin{example}
  \(\mathrm{Aff}(\R^3) = \GL_3(\R) \rtimes \R^3\).
\end{example}

\begin{example} \mnote{здесь \(C_2^n\) --- простая подгруппа}
  Группа симметрий гипероктаэдра (выпуклой оболочки \(\left\{ \pm e_i \right\}\) в \(\R^n\))
  \(\mathrm{Oct}_n = C_2^n \rtimes S_n\) (на это намекает то, что порядок этой группы --- \(2^n
  n!\)).
\end{example}

\lecture{6}{04.03.2024, 11:15}{}

\begin{consequence}
  Если \(f(x) = \frac{1}{x^p}\), \(p > 1\), то \(\int_1^{+\infty} \frac{\df{x}}{x^p} < +\infty\),
  причем при \(p = 1\) он расходится.
\end{consequence}

\begin{theorem}[признак Раабе]
  Ряд \(\sum a_k\) сходится, если \(R = \lim_{k \to \infty} R_k > 1\) (а при \(R < 1\) расходится),
  где \(R_k \coloneqq k\left( \frac{a_k}{a_{k + 1}} - 1 \right)\).
\end{theorem}

Доказать предлагается самостоятельно, основываясь на признаке сравнения.

\begin{remark}
  \(R = 1\) ничего не говорит о сходимости ряда.
\end{remark}

\begin{example}
  \(\ln{n} - \sum_{k = 1}^n \frac{1}{k} \xrightarrow{n \to \infty} C\) (постоянная Эйлера).
\end{example}

\subsection{Знакопеременные ряды}

\begin{definition}
  Ряд \(\sum a_k\) сходится абсолютно, если сходится ряд \(\sum |a_k|\).
\end{definition}

\begin{theorem}
  Если ряд сходится абсолютно, то он сходится.
\end{theorem}

Доказательству по критерию Коши.

\begin{definition}
  Ряд \(\sum a_k\) сходится условно, если он сходится, но не абсолютно.
\end{definition}

\begin{theorem}[радикальный признак Коши]
  Если \(\limsup_{k \to \infty} \sqrt{|a_k|} < 1\), то ряд сходится абсолютно, если \(< 1\), то ряд
  расходится.
\end{theorem}

\begin{theorem}[признак сходимости Дирихле]
  Ряд \(\sum a_kb_k\) сходится (условно), если \(A_k \coloneqq \sum_{j = 1}^k a_j\) ограничена, а
  последовательность \(b_k \xrightarrow{k \to \infty} 0\) монотонна.
\end{theorem}
\begin{proof}
  Распишем сумму для критерия Коши: \[
    \begin{aligned}
      \left|\sum_{k = m}^n a_kb_k\right| &= \left|\sum_{k = m}^n \left( A_k - A_{k - 1} \right) b_k
      \right| = \left|\sum_{k = m}^n A_kb_k - \sum_{k = m}^n A_{k - 1}b_k\right| \\
                            &= \left|\sum_{k = m}^n A_k b_k - \sum_{k = m - 1}^{n - 1} A_kb_{k + 1}
                            \right| = \left|\sum_{k = m}^{n - 1} A_k(b_k - b_{k + 1}) + A_nb_n -
                            A_{m - 1}b_m\right| \\
                            &\le \sum_{k = m}^{n - 1} C|b_k - b_{k - 1}| + |A_n||b_n| + |A_{m -
                            1}||b_m| \le C|b_n - b_m| + C|b_n| + C|b_m| \\
                            &\le 2C\left( |b_n| + |b_m| \right) < \varepsilon \quad \forall m, n >
                            N(\varepsilon),
    \end{aligned}
  \] ведь \(b_n \to 0\).
\end{proof}

\begin{theorem}[признак Абеля]
  Если ряд \(\sum a_k\) сходится, а \(b_n\) --- монотонная ограниченная последовательность, то ряд
  \(\sum a_kb_k\) сходится.
\end{theorem}
\begin{proof}
  \(\sum a_k(b_k - b)\) сходится (\(b = \lim_{k \to \infty} b_k\)) по признаку Дирихле, но тогда,
  раскрывая скобки, \(\sum a_kb_k\) тоже сходится.
\end{proof}

\begin{remark}
  В признаках Абеля и Дирихле монотонность существенна.
\end{remark}

Из признака Дирихле можно вывести:
\begin{theorem}[признак Лейбница]
  Если \(b_k \xrightarrow{k \to \infty} 0\) монотонно, то \(\sum (-1)^kb_k\) сходится.
\end{theorem}

\begin{example}
  \(\sum (-1)^k \frac{2 + (-1)^k}{k}\) расходится --- монотонность \(b_k\) \textit{необходима}.
\end{example}

\begin{exercise}
  Показать или опровергнуть, что если \(a_n \xrightarrow{n \to \infty} 0\) монотонно, \(b_n
  \xrightarrow{n \to \infty} 0\) монотонно, \(a_n, b_n \ge 0\) и \(\sum a_n = \sum b_n = +\infty\),
  то \(\sum \min(a_n, b_n) = +\infty\).
\end{exercise}

\begin{theorem}
  Если ряд \(\sum a_k\) сходится \textit{абсолютно}, то \(\sum a_{\varphi(k)} = \sum a_k\), где
  \(\varphi\) --- некоторая перестановка.
\end{theorem}
\begin{proof}
  Без потери общности, \(a_k \in \R\) (комплексный ряд можно разбить на два). Положим, что \(S
  \coloneqq \sum a_k = \sum a_k^+ + \sum a_k^-\), где \(a_k^+ = \begin{cases}
    a_k, &a_k \ge 0 \\
    0, &a_k < 0
  \end{cases}\), \(a_k^-\) аналогично.

  Понятно, что раз \(\sum |a_k|\) сходится, то и \(\sum a_k^+\) сходится. По признаку сравнения,
  \(\sum a_k^-\) тоже сходится --- значит, можно переставлять слагаемые как угодно.
\end{proof}

\begin{theorem}[Римана о перестановке слагаемых]
  Если ряд \(\sum c_k\) (\(c_k \in \R\)) сходится условно (\(\sum |c_k| = +\infty\)), то \(\forall S
  \in \R \exists \varphi\colon \sum c_{\varphi(k)} = S\), где \(\varphi\) --- перестановка.
\end{theorem}
\begin{proof}
  Положим, что \(c_1, c_2, c_3, \ldots = \alpha_1, \alpha_2, \ldots, \alpha_m, \beta_1, ...,
  \beta_n, \alpha_{m_1 + 1}, \alpha_{m_1 + 2}, \ldots\), где \(\alpha_j \ge 0\), \(\beta_k \le 0\).

  Заметим, что:
  \begin{enumerate}
    \item \(\alpha_j \xrightarrow{j \to \infty} 0\), \(\beta_k \xrightarrow{k \to \infty} 0\), ведь
      ряд сходится условно;
    \item \(\sum a_j = +\infty\), \(\sum b_k = -\infty\) --- иначе исходный ряд не сошелся.
  \end{enumerate}

  Построим последовательность \(a_n\): \(a_1 \coloneqq \alpha_1\), \(j \coloneqq 2\), \(k \coloneqq
  1\), \(n \coloneqq 1\) (присваивание). Запустим цикл:
  \begin{enumerate}
    \item Если \(a_1 + \ldots + a_n < S\), то \(a_{n + 1} \coloneqq \alpha_j\); \(j \coloneqq j +
      1\);
    \item Иначе, \(a_{n + 1} \coloneqq \beta_k\); \(k \coloneqq k + 1\);
    \item \(n \coloneqq n + 1\); вернуться к шагу \(1\).
  \end{enumerate}

  В ``режиме \(\alpha\)'', \(|S - (a_1 + \ldots + a_n)|\) уменьшается, но и в ``режиме \(\beta\)''
  тоже; при ``переключении'', погрешность оценивается либо \(|\alpha_j|\), либо \(|\beta_k|\).

  Из-за сходимости ряда, \(j \to \infty\) и \(k \to \infty\), а значит, \(\left| S - (a_1 + \ldots +
  a_n)\right| \xrightarrow{n \to \infty} 0\).
\end{proof}

\begin{remark}
  Таким образом можно получить и \(S = \pm \infty\).
\end{remark}

\begin{remark}
  Для комплексных чисел это, в общем случае, неверно.
\end{remark}

\subsection{Перемножение рядов}

Как можно перемножить \(\sum a_k \cdot \sum b_j\)?
\begin{enumerate}
  \item \(\sum_{j = 1}^n \sum_{k = 1}^n a_kb_j \to AB\) (прямое перемножение частичных сумм);
  \item по Коши --- \(\sum_{n = 1}^\infty \left( \sum_{k + j = n} a_kb_j \right)\).
\end{enumerate}

Но когда может быть так, что умножение по Коши дает \(AB\)?

\begin{theorem}
  Если ряды \(\sum a_k\) и \(\sum b_k\) сходятся абсолютно, а \(\varphi\colon \N \to \N \times \N\)
  --- биекция, то \(\sum_l a_{\varphi(l)}b_{\varphi(l)}\) сходится к \(AB\).
\end{theorem}
\begin{proof}
  \[
    \sum_{l = 1}^N \left| a_{\varphi(l)} \right| \left| b_{\varphi(l)} \right| \le \left(\sum_{j =
    1}^M |a_j|\right)\left( \sum_{j = 1}^P |b_j| \right) \le \left( \sum_j \left| a_j \right|
    \right)\left( \sum_j \left| b_j \right| \right).
  \]
\end{proof}

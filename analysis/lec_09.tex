\lecture{9}{14.03.2024, 15:35}{}

Посмотрим на разные примеры норм в конечном и бесконечномерном случаях.

\begin{example}
  \(x \in \R^n\), \(\|x\|_p \coloneqq \left( \sum_{k = 1}^n |x_k|^p \right)^{\frac{1}{p}}\), \(p \ge
  1\) --- семейство норм на \(\R^n\).

  Очевидно, что \(\|\lambda x\| = \lambda\|x|\), а неравенство Минковского дает неравенство
  треугольника.
\end{example}

\begin{example}
  \(\|x\|_\infty = \sup_k |x_k|\).
\end{example}

В частности, \(\|x\|_\infty \le \|x\|_p \le n^{\frac{1}{p}}\|x\|_\infty\).

\begin{example}
  Скалярное произведение \(\inner{x}{y}\) --- линейный функционал (по \(x\) или \(y\)).
\end{example}

Тут уже применяется неравенство Гельдера: \(\inner{x}{y} \le \|x\|_p\|y\|_q\), где \(\frac{1}{p} +
\frac{1}{q} = 1\). Это наводит на возможное переопределение нормы: \(\|x\|_p \coloneqq \sup_{\|y\|_q
= 1}\inner{x}{y}\). \mnote{предлагается доказать эквивалентность}

\begin{example}
  \(l_p\)-пространство --- \(\left\{ x_1, \ldots, x_n, \ldots \mid \|x\|_p < +\infty \right\}\).
\end{example}
\begin{remark}
  Все \(l_p\)-пространства разные.
\end{remark}

\begin{statement}
  \(l_2\) --- Гильбертово пространство.
\end{statement}

Оно состоит из всех таких \(x\), что \(\sum_{k = 1}^\infty x_k^2 < +\infty\).

\begin{example}
  Рассмотрим \(\R^2\) и \(\left\{ x \mid \|x\|_p = 1 \right\}\) --- окружность.

  Тогда \(p = 1\) --- ромб, \(p = 2\) --- окружность, \(p \to \infty\) --- квадрат.
\end{example}

\section{Дифференциальное исчисление}

\begin{definition}
  Отображение \(A\colon \R^n \to \R^m\) называется линейным, если \(A(\lambda x + \mu y) = \lambda
  A(x) + \mu A(y)\), где \(\lambda \in \R\) (или \(\C\)).
\end{definition}

\begin{definition}
  \((A + B)x \coloneqq Ax + Bx\).
\end{definition}

\begin{definition}
  \((\lambda A)x \coloneqq \lambda Ax\).
\end{definition}

\begin{definition}
  \((BA)x \coloneqq BAx\), где \(A\colon \R^n \to \R^m\), \(B\colon \R^m \to \R^s\).
\end{definition}

\begin{remark}
  \(AB \neq BA\).
\end{remark}

Смотри курс алгебры для основ действий над матрицами и их свойств.

\begin{statement}
  \(x, y \in \R^n \implies \inner{x}{y} = y\top x\).
\end{statement}

\begin{theorem}
  Если \(A\colon \R^n \to \R^n\), то следующие утверждения эквивалентны:
  \begin{enumerate}
    \item \(A\) обратим;
    \item \(\Imf{A} = \R^n\);
    \item \(\det{A} \neq 0\).
  \end{enumerate}
\end{theorem}

\begin{definition}
  \(L\left( \R^n \to \R^m \right)\) --- пространство линейных операторов.
\end{definition}

\begin{definition}
  \(\|A\| \coloneqq \sup_{|x| \le 1} |Ax|\).
\end{definition}

Стоит проверить самому, что это --- норма.

\begin{definition}
  Оператор \(A\) называется ограниченным, если \(\|A\| < +\infty\).
\end{definition}

\begin{theorem}[об операторной норме]
  \[
      \begin{aligned}
        \|A\| &= \sup_{|x| \le 1} |Ax| = \sup_{|x| < 1} |Ax| = \sup_{|x| = 1} |Ax| \\
        &= \sup_{x \neq 0} \frac{|Ax|}{|x|} = \inf\left\{ C \mid |Ax| \le C|x| \quad \forall x \in
        \R^n \right\}.
    \end{aligned}
  \]
\end{theorem}

\begin{consequence}
  \(|Ax| \le \|A\| \cdot |x|\).
\end{consequence}

\begin{statement}
  \(\|AB\| \le \|A\| \cdot \|B\|\); \(|ABx| \le \|A\|\|B\||x|\).
\end{statement}

\begin{theorem}[об оценке нормы]
  \[
    \|A\| \le \sqrt{\sum_{i = 1, j = 1}^{m, n} |a_{ij}|^2}
  .\]
\end{theorem}
\begin{proof}
  \[
    |Ax| = \sqrt{\sum_{i = 1}^n \left( \sum_{j = 1}^n a_{ij}x_j \right)^2} \oversym{\le}{\text{КБШ}}
    \sqrt{\left(\sum_{i = 1}^m\sum_{j = 1}^n |a_{ij}|^2\right) \sum_{j = 1}^n x_j^2} =
    |x|\sqrt{\sum_{i = 1, j = 1}^{m, n} |a_{ij}|^2}.
  \]
\end{proof}

Пусть \(f\colon \mathcal{D} \to \R^m\), где \(\mathcal{D} \subset \R^n\), а \(a \in
\mathrm{Int}\mathcal{D}\).

\begin{definition}
  Производной функции \(f\) в точке \(a\) называется линейный оператор \(A\colon \R^m \to
  \R^n\), если такой существует, где \(f(a + h) = f(a) + Ah + \smallO(|h|)\).
\end{definition}

\begin{definition}
  Такое \(f(a + h)\) называется формулой линеаризации.
\end{definition}

\begin{notation}
  \(f'(a) \coloneqq A\).
\end{notation}

По-другому говоря, \(f(x) - f(x^0) = f'(x^0)(x - x^0) + \smallO(|x - x^0|)\) при \(x \to x^0\).

\begin{example}
  \((Ax)' = A\).
\end{example}

Пусть \(f = \begin{pmatrix} f_1(x_1, \ldots, x_n) \\ \vdots \\ f_m(x_1, \ldots, x_n) \end{pmatrix}
\).

\begin{definition}
  Матрица Якоби --- матрица, где \(a_{ij} \coloneqq \frac{\partial f_i}{\partial x_j}\).
\end{definition}

\[
A = \begin{pmatrix}
  \frac{\partial f_1}{\partial x_1} & \ldots & \frac{\partial f_1}{\partial x_n} \\
  \vdots & & \vdots \\
  \frac{\partial f_m}{\partial x_1} & \ldots & \frac{\partial f_m}{\partial x_n} \\
\end{pmatrix}
.\]

В ``хорошем'' случае, производная оператора и есть матрица Якоби, однако, например, такая матрица
может существовать без существования каких-то частичных производных.

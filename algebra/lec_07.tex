\lecture{7}{13.03.2024, 13:40}{}

\section{Комплексификация и овеществление (спуск Галуа)}

Пусть \(V\) --- ВП над \(\R\). Возьмем две его копии \(V\) и \(iV\) (\(i\) это пока что просто
обозначение).

Тогда \(V \oplus iV\) --- пространство над \(\R\); но его можно наделить структурой ВП над \(\C\):
\((u, v) \mapsto u + iv\) (\(i \in \C\)).

Понятно тогда, что \((a + bi)(u + iv) = au - bv + i(bu + av)\); элементарно проверятся соблюдение
ассоциативности (чтобы показать, что \(V \oplus iV\) --- ВП над \(\C\)).

\begin{notation}
  \(V_{\C} \coloneqq V \oplus iV\).
\end{notation}

Рассмотрим, что происходит с базисами. Пусть \(e_1, \ldots, e_n\) --- базис \(V\) над \(\R\).

\begin{statement}
  \(e_1, \ldots, e_n\) --- базис \(V_{\C}\) над \(\C\).
\end{statement}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[Система образующих] Любой вектор \(V_{\C}\) представляется как \(u + iv\), но \(iv = i
      \cdot v\) --- достаточно представить только \(u\): \(u = \sum_j \alpha_j e_j\).

    \item[Линейная независимость] Предположим обратное: \(\sum_j \left( \alpha_j + i\beta_j
      \right)e_j = 0 \iff \sum_j \underbrace{\alpha_je_j}_{\in V} + i\underbrace{\left( \sum_j
      \beta_je_j \right)}_{\in V} = 0 \implies \alpha_j, \beta_j = 0\) как базис в \(V\).
  \end{pfparts}
\end{proof}

\begin{consequence}
  \(\dim{V_{\C}} = \dim{V}\).
\end{consequence}

После базисов закономерно рассмотрим, как ведут себя линейные отображения.

Если \(\varphi\colon U \to V\) --- линейное, то
\begin{statement}
  \(\varphi_{\C}\colon U_{\C} \xrightarrow{u + iv \mapsto \varphi(u) + i\varphi(v)} V_{\C}\) ---
  линейное (над \(\C\)).
\end{statement}

\begin{statement}
  \(\End{V} \to \End{V_{\C}}\) --- гомоморфизм колец.
\end{statement}

В частности, если зафиксировать базисы, \(M_n(\R) \to M_n(\C)\) --- коэффициенты те же самые.

А как можно восстановить \(V\) по \(V_{\C}\)? Понятно, что в общем случае никак, но при введении
дополнительной структуры это осуществимо.

\begin{definition}
  \(\conj{u + iv} \coloneqq u - iv\) --- сопряжение.
\end{definition}

В частности, на \(V\) сопряжение действует как \(\id\), а на \(iV\) --- как \(-\id\).

\begin{statement}
  \(\conj{\conj{u + iv}} = u + iv\).
\end{statement}

\begin{statement}
  \(\conj{(a + bi)(u + iv)} = (a - bi)(u - iv)\).
\end{statement}

Для дальнейшего удобства,
\begin{notation}
  Греческими буквами обозначаются комплексными числами вида \(a + bi\).
\end{notation}

\begin{statement}
  Сопряжение --- полулинейное отображение.
\end{statement}

Этой операции будет достаточно, чтобы построить биекцию \(V_{\C}\) и \(V\): \(V = \{ w \in
V_{\C} \mid \conj{w} = w \}\). Это позволяет ``забыть'' о пространстве вещественных чисел и
изучать только пространство комплексных --- иногда это оказывается удобнее и практичнее.

А что происходит с матрицами при сопряжении? План:
\begin{enumerate}
  \item рассмотреть сопряжение над \(M_n(\C)\);
  \item привести к Жордановой форме  \(M_n(\C)\);
  \item как-то перевести \(M_n(\C)\) в \(M_n(\R)\).
\end{enumerate}

Начнем. Пусть \(V\) --- ВП над \(\R\); \(A\colon V \to V\) --- линейный оператор. Рассмотрим
\(V_{\C}\) и \(A_{\C}\).

Для начала посмотрим на собственные векторы: \(A_{\C}w = \lambda w\).

Раз изначально \(A\) имела лишь вещественные элементы,
\begin{statement}
  \(\conj{\lambda}\ \conj{w} = \conj{A_{\C}w} = A_{\C}\conj{w}\).
\end{statement}

\begin{consequence}
  \(\conj{w}\) --- собственный вектор, отвечающий собственному числу \(\conj{\lambda}\).
\end{consequence}

Зная это, перейдем на уровень выше: рассмотрим корневые подпространства.

\begin{statement}
  \(\conj{A_{\C} - \lambda I} = A_{\C} - \conj{\lambda} I\).
\end{statement}

\begin{consequence}
  \(\conj{V_{\C}^{(\lambda)}} = V_{\C}^{(\conj{\lambda})}\).
\end{consequence}

\begin{statement}
  Если \(\lambda \neq \conj{\lambda}\), то \(V_{\C}^{(\lambda)} \cap V_{\C}^{(\conj{\lambda})} =
  0\).
\end{statement}

\begin{statement}
  В \(V_{\C}^{(\lambda)}\) есть жорданов базис, который при сопряжении переходит в
  \(V_{\C}^{(\conj{\lambda})}\).
\end{statement}

% FIXME
Пусть \(\lambda \neq \conj{\lambda}\): получен базис \(w_1, \ldots, w_m, \conj{w_1}, \ldots,
\conj{w}_m\) в \(V_{\C}\) (такой не обязательно найдется, поэтому пока рассматриваем частный
случай). Как из него ``изготовить'' базис \(V\)?

Хочется инвариантных элементов: например, \(w_1 + \conj{w_1} \in V\). Или же, \(\conj{i(w_1 +
\conj{w}_1)} = -i(w_1 - \conj{w_1}) \in V\).

\begin{lemma}
  Если \(w_1, \ldots, w_m, \conj{w_1}, \ldots, \conj{w}_m\) --- базис \(V_{\C}\), то \(\left\{ w_j +
  \conj{w}_j\right\}\) и \(\left\{ i\left(w_j - \conj{w}_j\right) \right\}\) образуют базис \(V\).
\end{lemma}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[Линейная независимость] Для этого покажем, что это --- базис над \(\C\).

      На примере \(w_1\): \(\left(w_1, \conj{w_1}\right) \mapsto \left( w_1 + \conj{w_1}, i\left(
      w_1 - \conj{w_1} \right) \right)\), и тогда \(\conj{w_1} = \frac{1}{2}\left( \left( w_1 +
      \conj{w_1} \right) + i\left( i\left( w_1 - \conj{w_1} \right) \right) \right)\).

      С остальными аналогично, и такое преобразование биективно; тогда матрица перехода --- блочная,
      блоки размера \(2 \times 2\) и обратимы.

    \item[Система образующих] По равенству размерностей, это --- базис.
  \end{pfparts}
\end{proof}

Поднимемся еще на уровень выше: рассмотрим Жордановы клетки.

Пусть \(w_1, \left( A_{\C} - \lambda I \right)w_1, \ldots, \left( A_{\C} - \lambda I\right)w_1\) ---
жорданов базис, где \(\conj{\lambda} \in \C\) --- некоторое собственное число. Ясно, что тогда
\(w_1, \left( A_{\C} - \conj{\lambda} I \right)w_1, \ldots, \left( A_{\C} - \conj{\lambda}
I\right)w_1\) --- тоже жорданов базис.

Посмотрим как \(A\) действует в базисе \(e_1 = w_1 + \conj{w_1}\) и \(e_2 = i\left( w_1 - \conj{w_1}
\right)\) из предыдущей леммы: \[
  \begin{aligned}
    &\left( A_{\C} - \lambda I \right)w_1 + \left( A_{C} - \conj{\lambda} I \right)\conj{w_1}, \\
    &i\left( \left( A_{\C} - \lambda I \right)w_1 - \left( A_{\C} - \conj{\lambda} I \right)\conj{w_1}
  \right).
  \end{aligned}
\]

Вспомним, что \(\lambda = a + bi\): \[
  \begin{aligned}
    e_3 &= A_{\C}\left( w_1 + \conj{w_1} \right) - biw_1 + bi\conj{w_1} - a\left( w_1 + \conj{w_1}
    \right) - bi\left( w_1 - \conj{w_1} \right), \\
    e_4 &= A_{\C} i\left( w_1 - \conj{w_1} \right) - ai\left( w_1 - \conj{w_1} \right) + b\left( w_1
    + \conj{w_1} \right).
  \end{aligned}
\]

Теперь, наконец-то, можно сказать, как действует \(A\): \[
  \begin{aligned}
    e_1 &\mapsto e_3 + ae_1 + be_2, \\
    e_2 &\mapsto e_4 - be_1 + ae_2.
  \end{aligned}
\]

Получился первый блок:
\[
A = \begin{pmatrix}
  a & -b & & & & & \\
  b & a  & & & & & \\
  1 & 0  & & & & & \\
  0 & 1  & & & & &
  \\
  \\
  \\
\end{pmatrix}
.\]

В этом новом базисе \(A\) записывается в следующей форме: \[
  \begin{pmatrix} & \end{pmatrix} 
.\]

\begin{definition}
  Такая форма называется вещественной Жордановой нормальной формой.
\end{definition}

На самом деле такая форма очень логична, ведь \(\C\) вкладывается в \(M_2(\R)\) как
\(a + bi \mapsto \begin{pmatrix} a & -b \\ b & a \end{pmatrix}\).

\begin{remark}
  Вещественное пространство не разбивается в сумму корневых.
\end{remark}

Вспомним, что всё это рассматривалось, если \(\lambda \neq \conj{\lambda}\); а что если равно?

Наберем в \(V_{\C}\) наборы \(w_1, \left( A - \lambda I \right)w_1, \ldots, \left( A - \lambda I
\right)^{k - 1} w_1\) и \(\conj{w_1}, \left( A - \lambda I \right)\conj{w_1}, \ldots, \left( A -
\lambda I \right)^{k - 1} \conj{w_1}\).

Если \(\langle w_1 \rangle \neq \langle \conj{w_1} \rangle\), то сделаем то же самое, а потом
переупорядочим (переставим) по четным и нечетным позициям.

Пусть теперь \(\langle w_1 \rangle = \langle \conj{w_1} \rangle\).

Если бы \(w_1 = \conj{w_1}\), то это был бы элемент \(V\), и набор из \(V_{\C}\) выше --- базис для
одной жордановой клетки.

Иначе, понятно, что \(\conj{w_1} = \alpha w_1\): \(w_1 = \conj{\alpha}\ \conj{w_1} =
\conj{\alpha}\alpha w_1 \iff \alpha\conj{\alpha} = 1\) --- \(\alpha\) лежит на единичной окружности.

\begin{theorem}[Гильберта 90]
  Если \(\alpha\conj{\alpha} = 1\), то \(\exists\beta\colon \alpha = \beta/\conj{\beta}\).
\end{theorem}

Осталось заменить \(w_1\) на \(\beta w_1\), ведь \(\conj{\beta}\ \conj{w_1} = \beta w_1\) ---
полученный результат инвариантен и лежит в \(V\).

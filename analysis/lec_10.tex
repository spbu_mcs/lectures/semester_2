\lecture{10}{18.03.2024, 11:15}{}

\begin{definition}
  Дифференциал \(\df{f(x, h)} \coloneqq f'(x)h\).
\end{definition}

\begin{example}
  Рассмотрим отображение некоторой области в \(\R^2\) куда-то в \(\R^3\). Тогда \[
  J = f'(x) = \begin{pmatrix}
    \frac{\partial f_1}{\partial x_1} & \frac{\partial f_1}{\partial x_2} \\
    \frac{\partial f_2}{\partial x_1} & \frac{\partial f_3}{\partial x_2} \\
    \frac{\partial f_3}{\partial x_1} & \frac{\partial f_3}{\partial x_2}
  \end{pmatrix}
  .\]

  Понятно, что область в \(\R^2\) перейдет в какую-то кривую в \(\R^3\): каждый столбец --- вектор
  скорости этой кривой по каждой из координат.
\end{example}

\begin{statement}
  Если \(f\) дифференцируема, то она непрерывна.
\end{statement}
\begin{proof}
  По дифференцируемости, \(f(x_0 + h) = f(x_0) + \df_{x_0}h + \smallO(|h|)\).

  Тогда \(\lim_{h \to 0} f(x_0 + h) = f(x_0) + 0 + 0\), ведь \(\df\) непрерывна и линейна, а
  \(\lim_{h \to 0} \smallO(|h|) = 0\).
\end{proof}

\section{Градиент функции}

Пусть \(\mathcal{D} \subset \R^n\).

\begin{definition}
  Если \(f\) дифференцируема в точке \(x\), то \(\grad{f(x)} \coloneqq f'(x)\) --- вектор-строка.
\end{definition}

\begin{statement}
  \(f(x + h) = f(x) + \inner{\grad{f(x)}}{h} + \smallO(h)\) при \(h \to 0\).
\end{statement}

\begin{lemma}
  Функция \(f\colon \mathcal{D} \to \R^n\) дифференцируема в точке \(x\) тогда и только тогда, когда
  все \(f_i(x)\) дифференцируемы в точке \(x\).
\end{lemma}
\begin{proof}
  Докажем ``справа налево''. \(f_i(x + h) - f_i(x) = A_ih + \smallO_i(h)\) при \(i = 1, \ldots, n\).

  Тогда \[
    f(x + h) - f(x) = Ah + \underbrace{\begin{pmatrix}
      \smallO_1(h) \\
      \vdots \\
      \smallO_n(h)
    \end{pmatrix}}_{\smallO(h)}.
  \]

  С другой стороны, \[
    \left| \begin{pmatrix}
      \smallO_1(h) \\
      \vdots \\
      \smallO_n(h)
    \end{pmatrix}  \right| = \sqrt{\sum_{k = 1}^n \smallO_k^2(h)} \le \sum_{k = 1}^n \left|
    \smallO_k(h) \right| = \smallO(h)
  .\]
\end{proof}

\begin{definition}
  Такие \(f_i(x)\) называются координатными функциями.
\end{definition}

Посмотрим на свойства дифференцирования.

\begin{statement}
  Если \(f\) и \(g\) дифференцируемы, а \(\lambda, \mu \in \R\), то \(\left( \lambda f(x) + \mu g(x)
  \right)' = \lambda f'(x) + \mu g'(x)\).
\end{statement}

\begin{statement}
  \(f(g)'(x) = f'(g(x))g'(x)\), где \(\R^p \xrightarrow{g} \R^n \xrightarrow{f} \R^m\).
\end{statement}
\begin{proof}
  По определению (в точке \(g(x)\)), \(f(g(x + h)) - f(g(x)) = f'(g(x))(g(x + h) - g(x)) +
  \smallO(g(x + h) - g(x))\). Продолжая уже для \(g(x)\), \(g(x + h) - g(x) = g'(x)h + \smallO(h)\).

  Тогда \(f(g(x + h)) - f(g(x)) = f'(g(x))(g'(x)h + \smallO(h)) + \smallO(g'(x)h - \smallO(h)) =
  f'(g(x))g'(x)h + \smallO(h)\) --- проверить самостоятельно.
\end{proof}

\begin{statement}
  Если \(\lambda(x)\colon \R^n \to \R\), \(f(x)\colon \R^n \to \R^m\) дифференцируемы, то \(\left(
  \lambda(x)f(x) \right)'h = \lambda'(x)hf(x) + \lambda(x)f'(x)h\).
\end{statement}
\begin{proof}
  \(\lambda(x + h)f(x + h) - \lambda(x)f(x) = \left( \lambda(x + h) - \lambda(x) + \lambda(x)
  \right)f(x + h) - \lambda(x)f(x) = \left( \lambda'(x) + \smallO(h) \right)f(x + h) +
  \lambda(x)\left( f(x + h) - f(x) \right) = \lambda'(x)hf(x) + \smallO(h) + \lambda(x)( f'(x)h +
  \smallO(h))\).
\end{proof}

Ну и как упражнение предлагается доказать
\begin{statement}
  Если \(f, g\colon \mathcal{D} \to \R^m\), то \(\inner{f(x)}{g(x)}'(x) = \inner{f'(x)h}{g(x)} +
  \inner{f(x)}{g'(x)h}\).
\end{statement}

\begin{theorem}[оценка Лагранжа]
  Если \(f\colon \R \supset [a, b] \to \R^m\), \(f\) дифференцируема на \((a, b)\) и непрерывна на \([a, b]\),
  то \[
    \exists c \in (a, b)\colon |f(b) - f(a)| \le |f'(c)|(b - a)
  .\]
\end{theorem}
\begin{proof}
  Положим, что \(\varphi(x) \coloneqq \inner{f(x)}{f(b) - f(a)}\). По одномерной теореме Лагранжа,
  \[ \exists c \in (a, b)\colon \varphi(b) - \varphi(a) = \varphi'(c)(b - a) .\]

  С другой стороны, \(\varphi'(x) = \sum_{k = 1}^m f_k'(x)\left( f_k(b) - f_k(a) \right)\); \[
  \begin{aligned}
    |\varphi'(c)| &= \left| \sum_{k = 1}^m f_k'(c)\left( f_k(b) - f_k(a) \right)
    \right| \\
    &\oversym{\le}{\text{КБШ}} \sqrt{\sum_{k = 1}^m f_k'^2(c)}\sqrt{\sum_{k = 1}^m \left(f_k(b) -
    f_k(a)\right)^2} \eqqcolon \left| f'(c) \right|\left| f(b) - f(a) \right|.
  \end{aligned}
  \]

  Возвращаясь к первому равенству, \[
  \begin{aligned}
    \inner{f(b) - f(a)}{f(b) - f(a)} = \left| \varphi(b) - \varphi(a) \right| = \left|
    \varphi'(c)\right |(b - a) \le \left| f'(c) \right|\left| f(b) - f(a) \right|(b - a).
  \end{aligned}
  \]

  Возведем всё в квадрат: \[
  \left| f(b) - f(a) \right|^2 \le \left| f'(c) \right|\left| f(b) - f(a) \right|(b - a)
  .\]
\end{proof}

Чаще всего используется следующее ``многомерное ''следствие, которое, пожалуй, одно из самых
используемых в многомерном анализе.

Пусть \(f\) дифференцируема в \(\mathcal{D}\) --- открытом множестве \(\R^n\); \([a, b] \subset
\mathcal{D}\), где \(a, b \in \R^n\). То есть теперь \(f\colon \R^n \to \R^m\).

\begin{consequence}
  \[
  \exists \Theta \in (0, 1)\colon \left| f(b) - f(a) \right| \le \|f'\left( a + \Theta(b - a)
  \right)\||b - a|
  .\]
\end{consequence}
\begin{proof}
  Возьмем \(g(x) = f\left( a + x(b - a) \right)\): \[
    \left| g(1) - g(0) \right| \le \left| g'(\Theta) \right|(1 - 0)
  .\]

  Дифференцируя композицию, \mnote{\(|Ax| \le \|A\||x|\)} \(g'(x) = f'(a + x(b - a))(b - a)\): \[
    \left| f(b) - f(a) \right| \le \left| f'(a + \Theta(b - a))(b - a) \right| \le \|f'(a + \Theta(b
    - a))\||b - a|
  .\]
\end{proof}

Сконцентрируемся на изучении скалярных функций \(f\colon \R^n \to \R\).

Пусть вектор \(h \neq 0_n\).

\begin{definition}
  Производной функции \(f\colon \mathcal{D} \to \R\) по вектору \(h\) называется \(D_hf(x) \coloneqq
  \lim_{t \to 0} \frac{f(x + th) - f(x)}{t}\).
\end{definition}

\begin{definition}
  Если такой предел существует, то \(f\) называется дифференцируемой по вектору \(h\).
\end{definition}

\begin{notation}
  \(D_hf = \frac{\partial f}{\partial h}\).
\end{notation}

\begin{definition}
  Если \(|h| = 1\), то \(D_hf\) называется производной по направлению.
\end{definition}

\begin{example}
  Если \(h = \begin{pmatrix} 0 \\ \vdots \\ 1 \\ \vdots \\ 0 \end{pmatrix} \), то \(D_kf(x) =
  \frac{\partial f}{\partial x_k}\) (\(k\) --- позиция \(1\)).
\end{example}


\lecture{3}{21.02.2024, 13:40}{}

Напомним, что \(\Tors{M} = \qfrac{R}{a_1R} \oplus \ldots \oplus \qfrac{R}{a_kR}\), а \(\Tors(R^n) =
0\) (за счет свободности модуля и PID); \(a_1 \divs a_2 \divs \ldots \divs a_k\) (см. раннее).

Но можно (и нужно ли) выбирать \(a_i\) по-другому? Например, \(\qfrac{\Z}{6\Z} \oplus
\qfrac{\Z}{18\Z} \oplus \qfrac{\Z}{36\Z} = \left( \qfrac{\Z}{2\Z} \oplus \qfrac{\Z}{2\Z} \oplus
\qfrac{\Z}{4\Z} \right) \oplus \left( \qfrac{\Z}{3\Z} \oplus \qfrac{\Z}{9\Z} \oplus \qfrac{\Z}{9
\Z}\right)\) --- первое кручение обнуляется степенями \(2\), второе --- степенями \(3\).

\begin{definition}
  Если \(M_1\), \(M_2\) --- подмодули \(Ь\) (\(M\) --- \(R\)-модуль), то говорят, что \(M\)
  раскалывается в внутреннюю прямую сумму \(M_1\) и \(M_2\), если
  \begin{enumerate}
    \item \(M = \linspan{M_1, M_2}\): \(\forall m \in M \exists m_1 \in M_1, m_2 \in M_2\colon
      m = m_1 + m_2\);
    \item \(M_1 \cap M_2 = \left\{ 0 \right\}\).
  \end{enumerate}
\end{definition}

\begin{statement}
  \(M \cong M_1 \oplus M_2\).
\end{statement}
\begin{proof}
  Построим такой изоморфизм: \((m_1, m_2) \mapsto m_1 + m_2\). Понятно, что это --- линейное
  отображение.

  \begin{itemize}
    \item \(\Imf = M\) по свойству 1 (определения);
    \item \((m_1, m_2) \mapsto 0 \iff m_1 = -m_2 \iff m_1 = 0 \land m_2 = 0\), --- \(\ker = 0\).
  \end{itemize}
\end{proof}

Данное понятие можно обобщить:
\begin{definition}
  Если \(M_1, \ldots, M_k\) --- подмодули \(M\), то \(M\) раскладывается во внутреннюю прямую сумму
  \(M\), если
  \begin{enumerate}
    \item \(M = \linspan{M_1, \ldots, M_k}\);
    \item \(\forall i \quad M_i \cap \linspan{M_1, \ldots, \hat{M_i}, \ldots, M_k} = 0\)
  \end{enumerate}
\end{definition}

Приведем пример, почему попарности во втором условии не достаточно:
\begin{example}
  Рассмотрим \(M = F^2\) (\(F\) --- поле); в \(M\) рассмотрим прямые \(M_1\), \(M_2\), \(M_3\),
  проходящие через \(0\) (попарно различные).

  Тогда \(\forall i \quad M_i \cong F\), но \(F^2 \not\cong M_1 \oplus M_2 \oplus M_3\).
\end{example}

\begin{statement}
  \(M \cong \bigoplus_i M_i\).
\end{statement}
\begin{proof}
  Аналогично, \((m_i) \mapsto \sum_i m_i\).
\end{proof}

В определениях выше \(R\) было любым кольцом; пусть теперь \(R\) --- PID (!).

\begin{notation}
  \(M_a \coloneqq \left\{ m \in M \mid am = 0 \right\}\) (\(a \in R, a \neq 0\)).
\end{notation}

Понятно, что такой \(M_a \leqslant M\).

\begin{lemma}
  Если \(\gcd(a, b) = 1\), то \(M_{ab} \cong M_a \oplus M_b\) (внутренняя прямая сумма).
\end{lemma}
\begin{proof}
  Понятно, что \(M_a \leqslant M_{ab}\), \(M_b \leqslant M_{ab}\). Надо проверить два свойства.

  \begin{enumerate}
    \item Почему \(M_{ab} = \linspan{M_a, M_b}\)?

      Воспользуемся линейным разложением: \(\exists m\colon abm = 0\), \(\exists x, y\colon ax + by
      = 1\). Тогда \(m = (ax + by)m = \underbrace{axm}_{\in M_b} + \underbrace{bym}_{\in M_a}\);
      следовательно, \(b(axm) = xabm = 0\), \(a(bym) = yabm = 0\).

    \item Почему \(M_a \cap M_b = \left\{ 0 \right\}\)?

      Рассмотрим \(a \in M_a\) и \(b \in M_m\): \(am = 0\), \(bm = 0\). Тогда \(m = (ax + by)m = xam
      + ybm = 0\).
  \end{enumerate}
\end{proof}

Аналогично это можно обобщить и на случай \(k \ge 2\):
\begin{lemma}
  Если \(a_i\) --- попарно \mnote{попарности достаточно} взаимно простые элементы, то \(M_{a_1
  \ldots a_k} \cong M_{a_1} \oplus \ldots \oplus M_{a_k}\) (внутренняя прямая сумма).
\end{lemma}
\begin{proof}
  Индукция по \(k\):
  \begin{pfparts}
    \item[База] \(k = 2\) --- см. выше.

    \item[Шаг индукции] Почему \(M_{a_1 \ldots a_k} \leqslant \linspan{M_{a_1}, \ldots, M_{a_k} }\)?
      \mnote{в другую сторону включение очевидно} По индукционному переходу известно, что \(M_{a_1
      \ldots a_{k - 1}} \leqslant \linspan{M_{a_1}, \ldots, M_{a_{k - 1}}}\).

      Тогда, по лемме для 2-ух элементов и того, что \(\gcd(a_k, a_1, \ldots, a_{k - 1}) = 1\),
      получается искомое.

      Осталось проверить пересечение: \(M_{a_i} \cap \linspan{M_{a_1}, \ldots, \hat{M_{a_i}},
      \ldots, M_k}= 0\)? Эта линейная оболочка --- в точности \(M_{\prod_{j \neq i} a_j}\) (по
      индукционному предположению); но \(\gcd\left( a_i, \prod_{j \neq i} a_j \right) = 1\) ---
      тогда, по лемме, пересечение и впрямь тривиальное.
  \end{pfparts}
\end{proof}

Введем полезное обозначение из теории чисел.

\begin{notation}
  \(M[p] \coloneqq \bigcup_{i \ge 1} M_{p^i}\).
\end{notation}

Понятно, что это подмодуль, так как \(M_p \leqslant M_{p^2} \leqslant \ldots\) --- если \(M\)
конечнопорожденный, то эта цепочка стабилизируется.

Напомним, что модуль называется модулем кручения, если \(M = \Tors(M)\).

\begin{theorem} \mnote{Structure th. for finitely generated modules over a PID}
  Если \(M\) --- КП модуль \textit{кручения}, то
  \begin{enumerate}
    \item \(M \cong \bigoplus M[p]\) (внутренняя прямая сумма) для некоторого конечного набора
      простых чисел \(p\), определенного однозначно;

    \item Если \(q\) --- простое число, не фигурирующее в пункте 1, то \(M[q] = \left\{ 0
      \right\}\);

    \item Если \(p\) --- простое число, фигурирующее в пункте \(1\), то \(\exists n\colon M[p] =
      M_{p^n}\).
  \end{enumerate}
\end{theorem}
\begin{proof}
  По большому счету все уже почти доказано: \(M \cong \bigoplus \qfrac{R}{a_iR}\) (свободного
  слагаемого нет), а по КТО, \(M \cong \bigoplus_p \left( \bigoplus \qfrac{R}{p^{k_i}R}
  \right)\)\mnote{чтобы \(p\) не повторялись}, где \(p_i\) --- простое число.

  \begin{pfparts}
    \item[Внутренняя прямая сумма] По лемме выше: \(M = M_{\prod_p p^{\max{k_i}}} \cong \bigoplus_p
      M_{p^{\max{k_i}}}\) --- первое равенство верно по определению \(M_a\) и изоморфизму
      \(\bigoplus \qfrac{R}{a_iR}\) (см. начало доказательства) --- любой элемент \(M\) зануляется
      при умножении на \(\prod_p p^{\max{k_i}}\).

    \item[Однозначность \(\left\{ p \right\}\) ] Рассмотрим инвариант \(\Ann(M) \coloneqq
      \left\{ r \in R \mid \forall m \in M \quad rm = 0\right\}\) (\textit{аннулятор}).

      Понятно, что \(\Ann(M)\) --- идеал в \(R\): \mnote{даже если \(R\) не PID} \(rm = 0 \implies
      srm = 0\), \(rm = 0 \land sm = 0 \implies (r + s)m = 0\).

      Тогда \(\Ann\left( \bigoplus_p \qfrac{R}{p^{k_i}R} \right) = \left(\prod_p
      p^{\max{k_i}}\right)\) (идеал) --- всё однозначно, ведь аннулятор инвариантен, и по нему можно
      восстановить все \(p\).

    \item[Для \(q\)] Если \(q\) не лежит в этом наборе, то \(M[q] = 0\) --- очевидно (никакой
      элемент не зануляется на любую степень \(q\)).

    \item[Для \(p\)] Предъявим: \(M[p] \cong \bigoplus \qfrac{R}{p^{k_i}R} \implies M[p] =
      M_{p^{\max{k_i}}}\).
  \end{pfparts}
\end{proof}

\subsection{Модули многочленов}

Пусть \(F\) --- поле, \(R = F[t]\). Понятно, что \(F[t]\)-модуль \(= V\) --- векторное пространство
над \(F\) вместе с линейным оператором \(A\colon V \xrightarrow{v \mapsto tv} V\), \(A(v) = tv \quad
\forall v \in V\) (см. лекцию 1).

Как и с модулями над кольцом, хотим классифицировать модули над кольцом многочленов.

Поймем, что вообще представляет из себя линейное отображение.

Пусть \(U\) и \(V\) --- \(F[t]\)-модули, то есть пары из векторного пространства и оператора: \((U,
A\colon U \to U)\), \((V, B\colon V \to V)\). Положим, что \(\varphi\colon U \to V\) --- линейное
отображение \(F[t]\)-модулей --- как ``расшифровать'', что оно линейное (ведь для просто
\(F\)-модулей это понятно)?

Пусть \(F\) конечномерно. Рассматривая \(\varphi\) как отображение \(F\)-модулей, понятно, что это
линейное отображение векторных пространств. Но для \(F[t]\)-модулей есть дополнительное условие о
сохранении умножения на \(t\): \(\varphi(tu) = t\varphi(u)\), но \(\varphi(tu) = \left( \varphi
\circ A \right)(u)\), а \(t\varphi(u) = \left( B \circ \varphi \right)(u)\). Получается, что
следующая диаграмма должна коммутировать (результат одинаков, если идти по любому цвету): \[
  \begin{tikzcd}
    U \arrow[d, "\varphi"', magenta] \arrow[r, "A", blue] & U \arrow[d, "\varphi", blue] \\
    V \arrow[r, "B", magenta] & V.
  \end{tikzcd}
\]

Прояснив это, перейдем к другому вопрос: в чем состоит описание \(F[t]\)-модуля с точность до
изоморфизма? Такой изоморфизм можно понимать как коммутативность следующей диаграммы: \[
  \begin{tikzcd}
    V \arrow[d, "\varphi"'] \arrow[r, "A"] & V \arrow[d, "\varphi"] \\
    V \arrow[r, "B"] & V,
  \end{tikzcd}
\] --- здесь \(V\) зафиксирован с двумя разными линейными операторами \(A\) и \(B\) и мы
рассматриваем, когда \(F[t]\)-модули \((V, A) \cong (V, B)\).

В частности, как изоморфизм векторных пространств (\(\varphi\) это \(F\)-линейная \mnote{линейность
зависит от кольца, биекция --- нет} биекция), \(\varphi A = B\varphi \iff \boxed{B = \varphi A
\varphi^{-1}}\).

Сродни сопряженным группам,
\begin{definition}
  Линейные операторы \(A\) и \(B\) называются сопряженными, если \(\exists \varphi\colon B = \varphi
  A \varphi^{-1}\).
\end{definition}

Резюмируя,
\begin{statement}
  \((V, A) \cong (V, B) \iff \exists \varphi\colon B = \varphi A \varphi^{-1}\): описать
  \(F[t]\)-модули с точность до изоморфизма есть то же самое, что описать линейные операторы с
  точностью до сопряженности.
\end{statement}

Продолжим изучать отношения между \(F\)-модулями и \(F[t]\)-модулями.

Пусть \(V\) --- КП \(F[t]\)-модуль, снабженный линейным оператором \(A\).

\begin{lemma}
  \(V\) --- модуль кручения (над \(F[t]\)) тогда и только тогда, когда \(V\) --- КП векторное
  пространство (над \(F\)).
\end{lemma}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\implies}\)] Раз \(V\) --- КП модуль кручения, то свободной части нет: \(V \cong
      \bigoplus_i \qfrac{F[t]}{(f_i(t))}\), где \(f_i\) --- многочлены. Понятно, что прямая сумма
      \(F[t]\)-модулей тем более прямая сумма \(F\)-модулей.

      Заметим, что \(\dim{\qfrac{F[t]}{(f_i(t))}} = \deg{f_i} \): каждый элемент
      \(\qfrac{F[t]}{(f_i(t))}\) представляется однозначно в виде \(\left[ a_0 + a_1t + \ldots +
      a_{\deg{f_i}} t^{\deg{f_i} - 1} \right]\).

      Итого: \(\dim{V} = \sum_i \deg{f_i} < \infty\) за счет конечнопорожденности \(V\) как
      \(F[t]\)-модуля.

      \item[\(\boxed{\impliedby}\)] Раз это конечномерное пространство, то есть конечная система
        образующих, а значит, \(V\) КП как \(F[t]\)-модуль.

        Почему не может быть свободной части? Если бы внутри \(V\) было слагаемое из \(F[t]\) в
        какой-то степени, то, в частности, \(F[t] \leqslant V\), но \(\dim{F[t]} = \infty\) и
        \(\dim{F[t]} \le \dim{V} < \infty\) --- противоречие.
  \end{pfparts}
\end{proof}

\lecture{5}{15.03.2024, 15:25}{}

\section{Параллельные переносы и гомотетии}

Напомним, что \(T_v\colon X \to X\) (параллельный перенос относительно \(v \in \vv{X}\)), \(\forall
x \in X \quad T_v(x) = x + v\).

\begin{theorem}
  \hfill
  \begin{enumerate}
    \item \(T_v\) --- аффинное отображение с линейной частью \(\id\);
    \item Любое аффинное отображение с линейной частью \(\id\) --- параллельный перенос.
  \end{enumerate}
\end{theorem}
\begin{proof}
  Для удобства, \(F \coloneqq T_v\).

  \begin{pfparts}
    \item[\(\boxed{\implies}\)] Надо проверить, что \(\tilde{F} = \id\).  % FIXME: picture

      \(xx + \vv{xy} + v = y + v = x + v + \tilde{F}\left( \vv{xy} \right)\), а значит, \(\vv{xy} =
      \tilde{F}\left( \vv{xy} \right)\).

    \item[\(\boxed{\impliedby}\)] \(\forall p \in X\) рассмотрим \(q \coloneqq F(p)\) и
      \(T_{\vv{pq}}\).

      \(T_{\vv{pq}}(p) = q\) --- линейная часть \(\id\) по теореме о задании аффинного отображения
      образом и линейной частью.
  \end{pfparts}
\end{proof}

\begin{definition}
  Аффинное отображение \(F\colon X \to X\) такое, что \(\tilde{F} = k \cdot \id\) (\(k \in \R
  \setminus \left\{ 0, 1 \right\}\)) называется гомотетией с коэффициентом \(k\).
\end{definition}

\begin{remark}
  \(k = 1\) --- параллельный перенос.
\end{remark}

\begin{theorem}
  Гомотетия имеет ровно одну неподвижную точку.
\end{theorem}
\begin{proof}
  Зафиксируем начало отсчета: \(p \in X\). Надо убедиться, что \(F(x) = x\) имеет единственное
  решение.

  \begin{pfparts}
    \item[\(\boxed{\exists}\)] \(F(x) = F(p + \vv{px}) = F(p) + \tilde{F}\left( \vv{px} \right) = p
      + k\vv{px} = p + \vv{pF(p)} + k\vv{px}\), а \(x = p + \vv{px}\) --- приравнивая крайние части,
      \(\vv{pF(p)} + k\vv{px} = \vv{px} \iff x = p + \frac{1}{1 - k}\vv{pF(p)} = f(k, p)\) ---
      нашлась неподвижна точка.

    \item[\(\boxed{!}\)] Пусть \(x, y\) --- неподвижные точки.

      \(\tilde{F}(\vv{xy}) = \vv{F(x)F(y)} = \vv{xy}\) по неподвижности; но тогда \(k = 1\) ---
      противоречие.
  \end{pfparts}
\end{proof}

\begin{definition}
  Неподвижная точка гомотетии называется центром гомотетии.
\end{definition}

\section{Основная теорема аффинной геометрии}

Все утверждения в этом разделе предлагается доказать самостоятельно.

\begin{statement}
  Аффинное отображение \(F\) является гомотетией или параллельным переносом тогда и только тогда,
  когда \(\forall l \quad F(l) \parallel l\) (\(l\) --- прямая).
\end{statement}

Пусть \(X\), \(Y\) --- аффинные пространства, \(\dim{X} = \dim{Y} \ge 2\).

\begin{theorem}
  Если \(F\colon X \to Y\) --- инъективное отображение такое, что \(\forall l \subset X \quad F(l)\)
  --- прямая (\(l\) --- прямая), то \(F\) --- аффинное отображение.
\end{theorem}

Введем ряд дополнительных обозначений; пусть \(a,  \in X\).

\begin{notation}
  \((ab)\) --- прямая, проходящая через точки \(a\) и \(b\).
\end{notation}

\begin{statement}
  Через любые две точки проходит прямая, и причем одна.
\end{statement}

\begin{statement}
  Любые две прямые на плоскости либо параллельны, либо пересекаются.
\end{statement}

\begin{definition}
  Упорядоченная четверка точек \((a, b, c, d)\) называется параллелограммом, если \((ab) \parallel
  (cd)\) и \((bc) \parallel (ad)\).
\end{definition}

\begin{statement}
  В параллелограмме, если \(u = \vv{ad}\) и \(u = \vv{ad}\), то \(\vv{ac} = u + v\).
\end{statement}

\begin{proof}[Доказательство теоремы]
  {\renewcommand\qedsymbol{\(\square\)}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\dim{X} = \dim{Y} = 2}\)]
      \hfill
      \begin{enumerate}
        \item \(l_1 \parallel l_2 \implies F(l_1) \parallel F(l_2)\).
          \begin{proof}
            Если бы это было не так (они пересекаются), то получится противоречие инъективности
            отображения.
          \end{proof}

        \item Если \((a, b, c, d)\) --- параллелограмм, то \((F(a), F(b), F(c), F(d))\) --- тоже
          параллелограмм.

        \item Зафиксировав \(a \in X\), рассмотрим \(\tilde{F}_a\) и докажем аддитивность: \(\forall
          u, v \in \vv{X} \quad \tilde{F}_a(u + v) = \tilde{F}_a(u) + \tilde{F}_a(v)\).

          \begin{proof}
            Рассмотрим два случая, в зависимости от зависимости \(u\) и \(v\).

            Если они независимы, то сделаем дополнительные построения: \(b = a + v\), \(d = a + u\),
            \(с = a + (u + v)\). Набор \((a, b, c, d)\) --- параллелограмм. Посмотрим на векторы
            \(\vv{F(u)F(b)}\), \(\vv{F(u)F(c)}\) и \(\vv{F(u)F(a)}\) --- за счет
            параллелограммности, получается искомая аддитивность.

            Если же они зависимы, можно считать, что среди нет них \(0\)-ых: тогда \(\exists \lambda
            \in \R\colon u = \lambda v\). Рассмотрим \(w \in \vv{X}\) такой, что \(u, w\) линейно
            независимы. Тогда пары векторов \((u + v, w)\), \((u, v + w)\) и \((v, w)\) линейно
            независимы, а значит, \(\tilde{F}_a(u + v) + \tilde{F}_a(w) = \tilde{F}_a(u + v + w) =
            \tilde{F}_a(u) + \tilde{F}_a(v + w) = \tilde{F}_a(u) + \tilde{F}_a(v) +
            \tilde{F}_a(w)\).
          \end{proof}

        \item Вспомним, что лемма о независимости выбора точки для \(\tilde{F}\) использовала только
          аддитивность.

        \item Покажем однородность над \(\Q\): \(\forall k \in \Q, u \in \vv{X} \quad \tilde{F}(ku)
          = k\tilde{F}(u)\).

          \begin{proof}
            \hfill
            \begin{itemize}
              \item \(k \in \N\): по аддитивности.

              \item \(k = \frac{1}{n}\) (\(n \in \N\)): \(\tilde{F}(ku) = \tilde{F}\left( n
                \frac{1}{n}u \right) = n\tilde{F}\left( \frac{1}{n} u \right) \iff
                \tilde{F}\left(\frac{1}{n} u\right) = \frac{1}{n}\tilde{F}(u)\).

              \item \(k = -1\): \(\tilde{F}(-v) + \tilde{F}(v) = 0 \iff \tilde{F}(-u) =
                -\tilde{F}(u)\).
            \end{itemize}
          \end{proof}

        \item \(\forall u \in \vv{X} \setminus \left\{ 0 \right\} \forall \lambda \in \R \setminus
          \left\{ 0, 1 \right\} \exists \mu \in \R \setminus \left\{ 0 \right\}\colon
          \tilde{F}\left( \lambda^2u \right) = \mu^2\tilde{F}(u)\).

          Геометрически это значит, что раз \(u \collin \lambda^2 u\), то и \(\tilde{F}(u) \collin
          \tilde{F}\left( \lambda^2 u \right)\).

          \begin{proof}
            \begin{figure}[ht]
              \centering
              \hfill
              \begin{subfigure}{.3\textwidth}
                \centering
                \resizebox{\linewidth}{!}{
                \begin{tikzpicture}
                  \tkzDefPoints{0/0/p, 2/0/x, 3/0/x_1, 4/0/x_2}

                  \tkzDefPoints{1/1/y}
                  \tkzDefPointOnLine[pos=1.5](p,y)\tkzGetPoint{y_1}

                  \tkzDrawLines(p,x_2 p,y_1)
                  \tkzLabelLine[pos = 1.2, above](p,x_2){\(l_1\)}
                  \tkzLabelLine[pos = 1.2, right](p,y_1){\(l_2\)}

                  \tkzDrawLines[blue](y,x y_1,x_1)

                  \tkzDrawPoints(p,x,x_1,x_2)
                  \tkzLabelPoints[below](p,x,x_1,x_2)

                  \tkzDrawPoints(y,y_1)
                  \tkzLabelPoints[above](y,y_1)
                \end{tikzpicture}
                }

                \caption{Построение}
              \end{subfigure}
              \hfill
              \begin{subfigure}{.3\textwidth}
                \centering
                \resizebox{\linewidth}{!}{
                \begin{tikzpicture}
                  \tkzDefPoints{0/0/p, 2/0/x, 3/0/x_1, 4/0/x_2}

                  \tkzDefPoints{1/1/y}
                  \tkzDefPointOnLine[pos=1.5](p,y)\tkzGetPoint{y_1}

                  \tkzDrawLines(p,x_2 p,y_1)
                  \tkzLabelLine[pos = 1.2, above](p,x_2){\(l_1\)}
                  \tkzLabelLine[pos = 1.2, right](p,y_1){\(l_2\)}

                  \tkzDrawLines[blue](y,x_1 y_1,x_2)

                  \tkzDrawPoints(p,x,x_1,x_2)
                  \tkzLabelPoints[below](p,x,x_1,x_2)

                  \tkzDrawPoints(y,y_1)
                  \tkzLabelPoints[above](y,y_1)
                \end{tikzpicture}
                }

                \caption{Образ построения в \(G\)}
              \end{subfigure}
              \hfill
              \begin{subfigure}{.3\textwidth}
                \centering
                \resizebox{\linewidth}{!}{
                \begin{tikzpicture}
                  \tkzDefPoints{0/0/p', 2/0/x', 3/0/x_1', 4/0/x_2'}

                  \tkzDefPoints{2/1/y'}
                  \tkzDefPointOnLine[pos=1.49](p',y')\tkzGetPoint{y_1'}

                  \tkzDrawLines(p',x_2' p',y_1')
                  \tkzLabelLine[pos = 1.2, above](p',x_2'){\(l_1'\)}
                  \tkzLabelLine[pos = 1.2, right](p',y_1'){\(l_2'\)}

                  \tkzDrawLines[red](y',x' y_1',x_1')

                  \tkzDrawPoints(p',x',x_1',x_2')
                  \tkzLabelPoints[below](p',x',x_1',x_2')

                  \tkzDrawPoints(y',y_1')
                  \tkzLabelPoints[above](y',y_1')
                \end{tikzpicture}
                }

                \caption{Образ построения в \(F\)}
              \end{subfigure}
              \hfill
            \end{figure}

            \hfill
            \begin{enumerate}[label=\roman*.]
              \item Для фиксированной \(p \in X\), возьмем \(l_1 \coloneqq p + ku\) (прямая).

              \item Выберем на ней точки \(x \coloneqq p + u\), \(x_1 \coloneqq p + \lambda u\) и
                \(x_2 \coloneqq p + \lambda^2 u\).

              \item Проведем прямую \(l_2\), проходящую через \(p\) (и отличающуюся от \(l_1)\).

              \item Выберем на \(l_2\) точки \(y \neq p\) и \(y_1 \coloneqq p + \lambda\vv{py}\).

              \item Возьмем гомотетию \(G\) с центром \(p\) и коэффициентом \(\lambda\): \(G(xy) =
                (x_1y_1)\), \(G(x_1y) = (x_2y_2)\).

                Раз \(G\) переводит одну прямую в другую, то \((xy) \parallel (x_1y_1)\) и \((x_1y)
                \parallel (x_2y_1)\).

              \item Будем обозначать \(\cdot'\) как образ \(F\) от \(\cdot\).

                Зададим \(\mu\) из условия, что \(\vv{p'x_1'} = \mu\vv{p'x'}\).

                После \(F\): \((xy) \mapsto (x'y')\), \((x_1y_1) \mapsto (x_1'y_1')\) ---
                параллельность сохраняется.

              \item Формально, теоремы Фаллеса нет, поэтому для определения коэффициента сделаем
                гомотетию с коэффициентом \(\mu\): \(\vv{p'y_1'} = \mu\vv{p'y'}\).

              \item Перенесем \((x_1y_1)\): \(\vv{p'x_2'} = \mu\vv{p'x'} \implies \boxed{\vv{p'x_2'}
                = \mu^2\vv{p'x'}}\).
            \end{enumerate}
          \end{proof}

        \item \(F\) сохраняет порядок точек на прямой.

          Перед тем как доказать это, оговорим, что такое порядок. Если \(a = p + \lambda v\), \(b =
          p + \lambda' v\) и \(c = p + \lambda'' v\), то порядок \((a, b, c)\) есть то же самое, что
          порядок \(\lambda, \lambda', \lambda'')\).

          \begin{proof}
            Прямое следствие шага 6.
          \end{proof}

        \item \(\forall \lambda \in \R_+ \setminus \Q, u \in \vv{X} \quad \tilde{F}(\lambda u) =
          \lambda\tilde{F}(u)\).

          \begin{proof}
            Возьмем \(\lambda_1, \lambda_2 \in \Q\) такие, что \(\lambda \in (\lambda_1,
            \lambda_2)\); зафиксируем \(p \in X\) и \(u \in \vv{X}\).

            Сделаем построение: \(a_1 \coloneqq p + \lambda_1u\), \(a_2 \coloneqq p + \lambda_2 u\),
            \(b \coloneqq p + \lambda u\). По пункту 7 известно, что после их отображения \(F\) 'ом
            порядок сохранится, а с рациональными точками и более: \(F(a_1) = F(p) + \lambda_1
            \tilde{F}(u)\), \(F(a_2) = F(p) + \lambda_2\tilde{F}(u)\).

            Покажем, что на самом деле и \(F(b) = F(p) + \lambda\tilde{F}(u)\). Предположим
            обратное: \(F(b) \neq \ldots\) и \(\exists q\colon q = F(b) + \lambda\tilde{F}(u)\),
            причем \(q \neq F(b)\).

            Ясно, что \(q \in (F(a_1), F(a_2))\); пусть \(F(b) = F(p) +
            \tilde{\lambda}\tilde{F}(u)\), где \(\lambda_1 < \tilde{\lambda} < \lambda_2\) (без
            потери общности). Получилось, что изменился порядок точек (беря сколь угодно близкие
            \(a_1\) и \(a_2\)), но \(F\) должен его сохранять.
          \end{proof}
      \end{enumerate}

      Это завершается доказательства для случая плоскости.

      \item[\(\boxed{\dim > 2}\)]
        \hfill
        \begin{enumerate}
          \item Образ плоскости \(\Pi\) в \(F\) --- плоскость.

            Доказать можно самому, взяв для произвольной точки прямую, перпендикулярную, или
            посмотреть запись лекции.

          \item Осталось лишь показать, что \(\tilde{F}(u + v) = \tilde{F}(u) + \tilde{F}(v)\) и
            \(\tilde{F}(\lambda u) = \lambda\tilde{F}(u)\).

            Но это элементарно, если рассмотреть плоскость, натянутую на \(u\) и \(v\).
        \end{enumerate}
  \end{pfparts}
}
\end{proof}

\begin{exercise}
  Подумать, зачем в условии теоремы условие на равенство размерностей.
\end{exercise}

\section{Евклидовы аффинные пространства и движения}

\begin{definition}
  Евклидово аффинное пространство --- тройка \(\left(X, \vv{X}, +\right)\), где \(\vv{X}\) ---
  евклидово пространство.
\end{definition}

\begin{definition}
  Расстояние \(d(x, y) \coloneqq \left| \vv{xy} \right| \quad \forall x, y \in X\).
\end{definition}

\begin{exercise}
  Проверить, является ли ЕАП метрическим.
\end{exercise}

Пусть \(X\) --- ЕАП.

\begin{definition}
  Отображение \(F\colon X \to X\) называется движением пространства \(X\), если \(F\) сохраняет
  расстояния: \(\forall p, q \in X \quad d(p, q) = d\left( F(p), F(q) \right)\).
\end{definition}

\begin{theorem}
  \hfill
  \begin{enumerate}
    \item Любое движение --- аффинное преобразование, линейная часть которого --- ортогональное
      преобразование;
    \item Любое аффинное преобразование, линейная часть которого есть ортогональное преобразование,
      --- движение.
  \end{enumerate}
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\implies}\)] Точки \(a, b, c\) лежат на одной прямой тогда и только тогда, когда
      одно из неравенств треугольника обращается в равенство; раз расстояние сохраняется, то одно из
      неравенств треугольника для \(F(a), F(b), F(c)\) обращается в равенство (\(F(a), F(b), F(c)\)
      лежат на одной прямой).

      Но \(F\) инъективно (прямая в прямую), а значит, \(F\) --- аффинное преобразование; скалярное
      произведение выражается через длины, а значит, \(F\) еще и ортогонально.

    \item[\(\boxed{\impliedby}\)] \(d(F(p), F(q)) \coloneqq \left| \vv{F(p)F(q)} \right| = \left|
      \tilde{F}\left( \vv{pq} \right) \right| = \left| \vv{pq} \right|\), ведь ортогональное
      отображение сохраняет длины.
  \end{pfparts}
\end{proof}

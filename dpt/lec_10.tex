\lecture{10}{19.04.2024, 13:40}{}

\begin{proof}[Продолжение доказательства теоремы Пойа]
  \hfill
  \begin{pfparts}
    \item[\(\boxed{d = 3}\)]  Покажем, что \(p^n(0 \leadsto 0) \le c_1\exp(-c_2n) +
      \frac{c_3}{n^{\frac{3}{2}}}\).

      Положим, что \(M_k\) --- число шагов вдоль \(k\)-ой оси. Понятно, что \(\sum_k M_k = n\) и
      \(\E{M_k} = \frac{n}{3}\).

      Рассмотрим несколько случаев.
      \begin{itemize}
        \item \(\exists j\colon M_j \le \frac{n}{6}\). Покажем, что \(\P\left(\exists j\colon M_j
          \le \frac{n}{6}\right) \le c_1\exp(-c_2n)\).

          Заметим, что \[
          \begin{aligned}
            \P\left(M_1 \le \frac{n}{6}\right) &= \P\left( M_1 - \frac{n}{3} \le -\frac{n}{2}
            \right) \\
            &\le \P\left( |M_1 - \E{M_1}| \ge c_rn \right) \le \frac{\D{M_1}}{\left(
            c_rn \right)^2}
          \end{aligned}
          \] по неравенству Чебышева.

          Положим, что \(V_j\) --- н.о.р случайные величины, где \(\P(V_j = 1) = \frac{1}{3}\) и
          \(\P(V_j = 0) = \frac{2}{3}\): тогда \(M_1 = \sum_{j = 1}^n V_j\) и
          \(\frac{\D{M_1}}{c_rn}^2 \le \frac{\D{V_1}n}{c_r^2n^2} \sim \frac{1}{n}\) --- слишком
          грубо.

          Замечая, что \(\P\left(M_1 \le \frac{n}{6}\right) = \P\left( -M_1 \ge -\frac{n}{6}
          \right)\), применим к последнему экспоненциальное неравенство Чебышева: \[
            \P\left( -M_1 \ge -\frac{n}{6} \right) \le \frac{\E\exp(-\lambda M_1)}{\exp\left(
            -\lambda \frac{n}{6} \right)}
          .\]

          \[
          \begin{aligned}
            \E e^{-\lambda M_1} &= \E e^{-\lambda \sum V_j} = \prod_j \E e^{-\lambda V_j} \\
            &= \left( \E e^{-\lambda V_1} \right)^n = \left( \frac{2}{3} + \frac{1}{3}e^{-\lambda}
              \right)^n.
          \end{aligned}
          \]

          Возвращаясь к предыдущему неравенству, \[
          \begin{aligned}
            \ldots &= \left( \left( \frac{2}{3} + \frac{1}{3}e^{-\lambda}
            \right)e^{\frac{\lambda}{6}} \right)^n = g^n(\lambda).
          \end{aligned}
          \]

          \(g(0) = 1\), что не сильно подходит, однако \(g'(0) < 0\) --- при достаточно малых
          \(\lambda\), \(g(\lambda) < 1\).

          Тогда \[
          \begin{aligned}
            \P\left( \left( M_1 \le \frac{n}{6} \right) \cup \left( M_2 \le \frac{n}{6} \right)
            \cup \left( M_3 \le \frac{n}{6} \right) \right) &\le 3\P\left(M_1 \le \frac{n}{6}\right)
            \\
            &= 3g^n(\lambda) = 3\exp\left( \ln{g(\lambda)} n \right) = 3\exp\left(
            -|\ln{g(\lambda)}| n \right).
          \end{aligned}
          \]

        \item \(\forall j\colon M_j \ge \frac{n}{6}\). Покажем, что \(p^n\left(0 \leadsto 0, M_j \ge
          \frac{n}{6}\right) \le \frac{c_3}{n^{\frac{3}{2}}}\).

          Положим, что событие \(A_{m_1, m_2, m_3} \coloneqq \left\{ M_1 = m_1, M_2 = m_2, M_3 =
          m_3 \right\}\), причем \(m_1, m_2, m_3 \ge \frac{n}{6}\).

          Заметим, что \(\P\left( X_0 = 0, X_n = 0 \mid A_{m_1, m_2, m_3} \right) \le
          \frac{c_1}{\sqrt{m_1}}\frac{c_1}{\sqrt{m_2}}\frac{c_1}{\sqrt{m_3}} \le
          \frac{c}{n^{\frac{3}{2}}}\) как независимые одномерные блуждания.

          Тогда
          \[
          \begin{aligned}
            \P\left( (X_0 = 0, X_n = 0) \cup A_{m_1, m_2, m_3} \right) &= \sum_{m_1, m_2, m_3}
            \P\left( (X_0 = 0, X_n = 0) \cap A_{m_1, m_2, m_3} \right) \\
            &= \sum_{m_1, m_2, m_3} \P\left( (X_0 = 0, X_n = 0) \mid A_{m_1, m_2, m_3} \right)
            \P\left(A_{m_1, m_2, m_3}\right) \\
            &\le \frac{c}{n^{\frac{3}{2}}}.
          \end{aligned}
          \]
      \end{itemize}
  \end{pfparts}
\end{proof}

\subsection{Свойства одномерного случайного блуждания}

Здесь \(\mathcal{X} = \Z\), \(S_0 = 0\), \(S_n = \sum_{j = 1}^n X_j\), где \(X_j\) независимые и
\(\P(X_j = +1) = p\), \(\P(X_j = -1) = q\).

Заметим сразу, что \(\E{X_j} = p - q\) и \(\D{X_j} = 1 - (p - q)^2 = 1 - (2p - 1)^2 = 4p - (4p)^2 =
4pq\).

Найдем приближенное распределение \(S_n\).

Положим, что \(\tilde{X_j} \coloneqq \frac{X_j + 1}{2}\) и \(\tilde{S_n} = \sum \tilde{X_j}\):
\(\P(\tilde{X_j} = 1) = p\), \(\P(\tilde{X_j} = 0) = q\) и \(\tilde{S_n} = \frac{S_n + n}{2}\).

По теореме Муавра-Лапласа,
\[
  \forall \tilde{c} < \tilde{d} \quad \P\left( \frac{\tilde{S_n} - np}{\sqrt{n}} \in [\tilde{c},
  \tilde{d}] \right) \xrightarrow{n \to \infty} \frac{1}{\sqrt{2\pi pq}}
  \int_{\tilde{c}}^{\tilde{d}} \exp\left( -\frac{u^2}{2pq} \right) \df{u}.
\]

Преобразуем подвероятностное выражение: \[
\begin{aligned}
  \P\left( \frac{\tilde{S_n} - np}{\sqrt{n}} \in [\tilde{c}, \tilde{d}] \right) &= \P\left(
  \frac{\frac{S_n}{2} + \frac{n}{2} - np}{\sqrt{n}} \in [\tilde{c}, \tilde{d}] \right) \\
  &= \P\left( \frac{S_n + n - 2np}{\sqrt{n}} \in [2\tilde{c}, 2\tilde{d}] \right) \\
  &= \P\left( \frac{S_n - n(p - q)}{\sqrt{n}} \in [2\tilde{c}, 2\tilde{d}] \right).
\end{aligned}
\]

Обозначив \(c \coloneqq 2\tilde{c}\) и \(d \coloneqq 2\tilde{d}\), \[
\begin{aligned}
  \P\left( \frac{S_n - n(p - q)}{\sqrt{n}} \in [c, d] \right) \xrightarrow{n \to \infty}
  \frac{1}{\sqrt{2\pi pq}} &\int_{\frac{c}{2}}^{\frac{d}{2}} \exp\left( -\frac{u^2}{2pq} \right)
  \df{u} \\
  = &\frac{1}{\sqrt{2\pi (4pq)}} \int_c^d \exp\left( -\frac{v^2}{2(4pq)} \right) \df{v}.
\end{aligned}
\]

Заметим, что если \(p = q\), то \[
\begin{aligned}
  \P\left( \frac{S_n}{\sqrt{n}} \in [c, d] \right) \xrightarrow{n \to \infty} \frac{1}{\sqrt{2\pi}}
  \int_c^d \exp\left( -\frac{v^2}{2} \right) \df{v}.
\end{aligned}
\]

\subsubsection{Распределение максимума симметричного случайного блуждания}

Положим, что \(p = q = \frac{1}{2}\), \(M_n \coloneqq \max_{0 \le j \le n} S_j\); понятно, что \(M_n
\ge S_0 = 0\).

Разобьем событие \(M_n \ge r\): \((M_n \ge r) = \underbrace{(M_n \ge r, S_n > r)}_{\P(S_n > r)} \cup
(M_n \ge r, S_n < r) \cup (M_n \ge r, S_n = r)\). За счет того, что блуждание симметричное, первое и
второе события эквивалентны: после достижения максимума все пути можно отразить.

Последнее событие легко оценить: \(\P(M_n \ge r, S_n = r) \le \P(S_n = r) = \bigO\left(
\frac{1}{\sqrt{n}} \right) \to 0\).

\[
  \forall c \ge 0 \quad \P\left( \frac{M_n}{\sqrt{n}} \ge c \right) \to \frac{2}{\sqrt{2\pi}}
  \int_c^\infty \exp\left( -\frac{v^2}{2} \right) \df{v}
.\]

\subsubsection{Задача о разорении}

Положим, что два игрока играют серию независимых матчей: \(p\) и \(q\) --- вероятности выигрыша
первого и второго соответственно. Пусть \(B\) --- начальный капитал второго игрока; \(|A|\) ---
начальный капитал первого (\(A < 0\)). Кто разорится первый?

Пусть \(T_n\) --- время до выхода, если это время не превосходит \(n\); \(n\), если выход не
состоялся.

До сих пор случайное блуждание начиналось из \(0\); введем случайный параметр \(x \in \Z\), где \(A
\le x \le B\), и будем начинать игру с него. Введем \(m_n(x) \coloneqq \E_x T_n\) --- мат. ожидание
\(T_n\) в предположении начала в точке \(x\).

Заметим, что \(m_n(x) = 1 + pm_{n - 1}(x + 1) + qm_{n - 1}(x - 1)\); на краях \(m(A) = M(B) = 0\).

Случайная величина \(T_n\) поточечно возрастает по \(n\); \(m_n(x) = E_x T_n \to m(x)\).

\begin{pfparts}
  \item[\(\boxed{p = q = \frac{1}{2}}\)] \(m(x) = 1 + \frac{1}{2}m(x + 1) - \frac{1}{2}m(x - 1) \iff
    m(x) - m(x + 1) = 2 + (m(x - 1) - m(x))\); понятно, что решение представляет собою квадратичный
    полином.

    Получается, что \(m(x) = -(x - A)(x - B) = (x - A)(B - x)\); в частности, \(m(0) = -AB = |A|B\).
\end{pfparts}

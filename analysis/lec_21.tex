\lecture{21}{29.04.2024, 11:15}{}

Пусть \(X\) --- метрическое пространство, \(D \subset X\), \(x_0\) --- предельная точка \(X\);
\(f_n\colon D \to \R\) или \(\C\).

\begin{theorem}[о перестановке пределов]
  Если \(f_n \unito f\) на \(D\) и \(\exists \lim_{x \to x_0} f_n(x) = A_n\), то \(\exists \lim_{x
  \to x_0} f(x) = \underbrace{\lim_{n \to \infty} A_n}_A\).
\end{theorem}
\begin{proof}
  Из условия \(1\), \[
  \begin{aligned}
    \forall \varepsilon > 0 \exists N = N(\varepsilon)\colon \forall n, m > N \quad |f_m(x) -
    f_n(x)| < \varepsilon;
  \end{aligned}
  \] применяя еще и условие \(2\) с переходом к пределу \(x \to x_0\), \[
    \forall n, m > N \quad |A_m - A_n| < \varepsilon
  .\]

  Существование предела показано (критерий Коши); осталось проверить равенство.

  Возьмем некоторый \(N\) такой, что \(|A - A_N| < \varepsilon/3\); раз \(f_n \unito f\), то
  \(|f_N(x) - f(x)| < \frac{\varepsilon}{3}\). \mnote{``А зачем я здесь такой написал, а здесь
  такой...''} Подберем еще и такой \(\delta\), что \(|f_N(x) - A_N| < \frac{\varepsilon}{3}\) при
  \(|x - x_0| < \delta\): получилось \[
    |x - x_0| < \delta \implies |f(x) - A| < \varepsilon
  .\]
\end{proof} \mnote{``Видите, утро хотя и солнечное, но провоцирует некоторую неточность''}

\begin{theorem}[о перестановке пределов для рядов]
  Если \(\sum_n f_n \unito f\) на \(D\) и \(\exists \lim_{x \to x_0} f_n(x) = A_n\), то \(\lim_{x
  \to x_0} f(x) = \lim_{n \to \infty} \sum_n A_n\), причем такой предел существует.
\end{theorem}

Пусть \(D \subset \C\) открыто и \(z_0 \in D\); \(f\colon D \to \C\).

\begin{definition}
  Функция \(f\) называется \(\C\)-дифференцируемой в точке \(z_0\), если существует предел \(\lim_{z
  \to z_0} \frac{f(z) - f(z_0)}{z - z_0} \eqqcolon f'(z_0)\).
\end{definition}

\begin{theorem}[Коши-Римана]
  Функция \(f(z) = u(x, y) + iv(x, y)\) \(\C\)-дифференцируема в точке \(z_0 = x_0 + iy_0\) тогда и
  только тогда, когда \(u\) и \(v\) дифференцируемы в точке \(z_0\), причем \(u_x = v_y\) и \(u_y =
  -v_x\) (\textit{уравнения Коши-Римана}).
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\implies}\)] \(\C\)-дифференцируемость эквивалентна тому, что \(f(z) - f(z_0) =
      A(z - z_0) + \smallO(|z - z_0|)\) при \(z \to z_0\), где \(A = f'(z_0)\).

      \(z \to z_0\) может по-разному; например, через \((x_0 + \Delta{x}, y_0)\): \[
        \frac{f(x_0 + \Delta x, y_0) - f(x_0, y_0)}{\Delta x} \to f'(z_0), \quad \Delta x \to 0
      .\]

      Но это и значит, что \(\frac{\partial f}{\partial x}\Big|_{z_0} = f'(z_0)\).

      Теперь пусть \(\Delta x = 0\): пойдем к \(z_0\) только по \(\Delta y\). Тогда \[
        \frac{f(x_0, y_0 + \Delta y) - f(x_0, y_0)}{i\Delta y} \to f'(z_0), \quad \Delta y \to 0
      ,\] --- \(\frac{\partial f}{\partial y}\Big|_{z_0} = if'(z_0)\) 

      И всё, ведь \(\frac{\partial f}{\partial x} = u_x + v_x\) и аналогично для \(y\).

    \item[\(\boxed{\impliedby}\)] Нужно доказать, что \(f(z) - f(z_0) = A(z - z_0) + \smallO(|z -
      z_0|)\) при \(z \to z_0\).

      Известно, что \[
      \begin{aligned}
        u(x, y) &= u(x_0, y_0) + (u_x(x_0)\ u_y(y_0)) \begin{pmatrix} x - x_0 \\ y -
        y_0 \end{pmatrix} + \smallO(|z - z_0|), \\
        v(x, y) &= v(x_0, y_0) + (v_x(x_0)\ v_y(y_0)) \begin{pmatrix} x - x_0 \\ y -
        y_0 \end{pmatrix} + \smallO(|z - z_0|).
      \end{aligned}
      \]

      Тогда \[
        f(z) = u(x, y) + iv(x, y) = f(z_0) + (u_x + iv_x)((x - x_0) + i(y - y_0)) + \smallO(|z -
        z_0|)
      ,\] --- достаточно положить \(A = (u_x + iv_x)\).
  \end{pfparts}
\end{proof}

\begin{consequence}
  \(\frac{\df{f}}{\df{z}} = \frac{\partial f}{\partial x} = -i\frac{\partial f}{\partial y}\).
\end{consequence}

\begin{definition}
  Ряд вида \(\sum_{n = 0}^\infty c_n(z - a)^n\), где \(c_n, z, a \in \C\), называется степенным.
\end{definition}

\begin{theorem}[Абеля I]
  Если степенной ряд сходится для некоторого \(z \neq a\), то \(\exists R > 0\) такое, что этот
  степенной ряд сходится локально равномерно и абсолютно в круге \(|z - a| < R\) и расходится в
  круге \(|z - a| > R\).
\end{theorem}

Это \(R\) можно вычислить по формуле \textit{Коши-Адамара}: \[
  \frac{1}{R} = \limsup_{n \to \infty} \sqrt[n]{|c_n|}
.\]

\begin{definition}
  Это \(R\) называется радиусом сходимости.
\end{definition}

\begin{definition}
  \(|z - a| < R\) называется кругом сходимости.
\end{definition}

\begin{proof}
  Пусть \(a = 0\) а \(R = 1\).

  Покажем, что тогда \(\sum_{n = 0}^\infty c_nz^n\) сходится локально равномерно и абсолютно в круге
  \(|z| < 1\) по признаку радикальному Коши. Это очевидно.

  Если же \(|z| > 1\), то не выполнится необходимый признак сходимости.
\end{proof}

\begin{theorem}[о дифференцируемости рядов в круге сходимости]
  \(\left( \sum_{n = 0}^\infty c_n(z - a)^n \right)' = \sum_{n = 0}^\infty nc_n(z - a)^{n - 1}\),
  причем этот ряд сходится локально равномерно и абсолютно там же, где сходится исходный (\(|z - a|
  < R\)).
\end{theorem}
\begin{proof}
  Пусть \(a = 0\).

  Положим, что \(f(z) = \sum_{n = 0}^\infty c_nz^n\). Найдем комплексную производную: \[
    \frac{f(z) - f(z_0)}{z - z_0} = \sum_{n = 0}^\infty c_n\frac{z^n - z_0^n}{z - z_0}
  ,\] --- нужен только предельный переход.

  Применяя теорему о перестановке пределов, \[
  \begin{aligned}
    \lim_{z \to z_0} \frac{f(z) - f(z_0)}{z - z_0} &= \sum_{n = 0}^\infty c_n \lim_{z \to z_0}
    \frac{(z - z_0)(z^{n - 1} + \ldots + z^{n - 1})}{z - z_0} \\
    &= \sum_{n = 1}^\infty c_nnz_0^{n - 1}.
  \end{aligned}
  \]
\end{proof}

Интегрировать тоже можно, но мы этим пока не будем заниматься.

\begin{theorem}[Абеля II]
  Если ряд \(\sum_{n = 0}^\infty c_n(z - a)^n\) сходится в точке \(\zeta\), то он сходится
  равномерно на \([a, \zeta]\).
\end{theorem}
\begin{proof}
  Пусть \(a = 0\) и \(\zeta = 1\).

  Надо проверить сходимость ряда \(\sum_n c_n r^n\), но проверять тут нечего, ведь это верно по
  признаку Абеля. \mnote{хоть он и для \(\R\), достаточно разбить ряд на вещественную и мнимую
  части}
\end{proof}

\begin{example}
  Рассмотрим \(\ln(1 + z) = \sum_{n = 1}^\infty (-1)^{n + 1} \frac{z^n}{n}\) --- по признаку Абеля он
  сходится равномерно, и вообще, он сходится равномерно на \([0, 1]\).
\end{example}

\begin{example}
  Рассмотрим \(\lim_{z \to -1} \sum_{k = 0}^\infty z^k\) (\(z \in \R, z > -1\)).

  Понятно, что этот предел \(= \lim_{z \to -1} \frac{1}{1 - z} = \frac{1}{2}\), хотя ряд \(1 - 1 + 1
  - 1 \ldots\) расходится.
\end{example}

\lecture{6}{22.03.2024, 15:25}{}

\subsection{Классификация движений}

Классификации аффинных отображений не было, потому что это вне рамок данного курса: классификацией
линейных отображений (к котором сводятся аффинные) занимается алгебра (ЖНФ, ...).

\begin{definition}
  Точка \(p \in X\) называется неподвижной относительно \(F\), если \(F(p) = p\).
\end{definition}

\begin{notation}
  \(\Fix{F}\) --- множество всех неподвижных точек \(F\).
\end{notation}

Чуть позже станет видна значимость неподвижных точек для классификации движений.

\begin{lemma}
  Если \(F\) --- аффинное отображение и \(\Fix{F} \neq \O\), то \(\Fix{F}\) --- аффинное
  подпространство с направлением \(\ker\left( \tilde{F} - \id \right)\).
\end{lemma}
\begin{proof}
  Пусть \(p \in \Fix{F}\). Возьмем другую \(q \in \Fix{F}\):
  \[
  \begin{aligned}
    q \in \Fix{F} &\iff F(q) = q \iff F(p) + \tilde{F}\left( \vv{pq} \right) = p + \vv{pq} \iff
    \tilde{F}\left( \vv{pq} \right) = \vv{pq} \\
    &\iff \vv{pq} \in \ker\left( \tilde{F} - \id \right).
  \end{aligned}
  \]
\end{proof}

\begin{remark}
  Само ядро о неподвижных точках ничего не говорит: например, параллельный перенос таких точек не
  имеет, но \(\ker\left( \tilde{F} - \id \right)\) совпадает со всем пространством.
\end{remark}

\begin{remark}
  Если такое ядро отсутствует, то неподвижные точки все равно могут быть: например, вращение
  плоскости.
\end{remark}

Пусть \(F\) --- аффинное отображение.

\begin{theorem}
  \(\left| \Fix{F} = 1 \right| \iff \ker\left( \tilde{F} - \id \right) = 0\).
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\implies}\)] Смотри предыдущую лемму.

    \item[\(\boxed{\impliedby}\)] Решим уравнение \(F(q) = q\): зафиксировав \(p \in X\), \[
      \begin{aligned}
        F(q) = q &\iff F(p) + \tilde{F}\left( \vv{pq} \right) = p + \vv{pq} \iff p + \vv{pF(p)} +
        \tilde{F}\left( \vv{pq} \right) = p + \vv{pq} \\
        &\iff \left( F - \id \right)\left( \vv{pq}
        \right) = -\vv{pF(p)},
      \end{aligned}
      \] --- по тривиальности ядра следует инъективность, сюръективность есть, а значит, неподвижная
      точка найдется (\(\Fix{F} \neq \O\)).

      Но будь таких точек хотя бы \(2\), по предыдущей лемме ядро было бы нетривиально.
  \end{pfparts}
\end{proof}

Если движение имеет неподвижную точку, то, по сути, \(F = \tilde{F}\), а \(\tilde{F}\) ---
ортогональное преобразование, классификация которых уже известна.

Теперь будем считать, что неподвижных точек у преобразования \textbf{нет}; из теоремы следует, что
ядро будет нетривиально (\textit{\(1\) --- собственное значение оператора}).

Наведем обозначения: \(X\) --- ЕАП, \(F\) --- движение \(X\) без неподвижных точек.

Тогда \(\vv{X} = \ker\left( \tilde{F} - \id \right) \oplus \left(\ker\left( \tilde{F} - \id
\right)\right)^\perp\). Рассмотрим \(p \in X\): \[
  \vv{pF(p)} = u_F + u \quad u_F \in \underbrace{\ker\left( \tilde{F} - \id \right)}_V, u \in
  \underbrace{\left( \ker\left( \tilde{F} - \id \right) \right)^\perp}_{V^\perp}
.\]

\begin{statement}
  \(V\) инвариантно относительно \(\tilde{F}\) (\(\tilde{F}(V) \subseteq V\)).
\end{statement}

\begin{statement}
  \(V^\perp\) инвариантно относительно \(\tilde{F}\).
\end{statement}
\begin{proof}
  Пусть \(w \in V^\perp\): \[
  \begin{aligned}
    \forall v \in V \quad \inner{\tilde{F}(w)}{v} &= \inner{\tilde{F}(w)}{\tilde{F}(v)} =
    \inner{w}{v} = 0 \\
    &\implies \tilde{F}(w) \perp v.
  \end{aligned}
  \]
\end{proof}

\begin{statement}
  \(\Imf\left( \tilde{F} - \id \right) = V^\perp\).
\end{statement}
\begin{proof}
  Пусть \(v \in V\), \(w \in V^\perp\): \[
  \begin{aligned}
    \left( \tilde{F} - \id \right)(u) &= \left( \tilde{F} - \id \right)(v + w) =
    \underbrace{\tilde{F}(v) - v}_0 +
    \underbrace{\tilde{F}(w) - w}_{\in V^\perp} \\
    &\implies \Imf\left( \tilde{F} - \id \right) \subseteq V^\perp;
  \end{aligned}
  \] в другую сторону верно за счет совпадения размерностей.
\end{proof}

\begin{statement}
  Вектор \(u_F\) не зависит от точки \(p\): \(\forall q \in X \quad \vv{qF(q)} = u_F + \tilde{u}\),
  где \(\tilde{u} \in \left( \ker\left( \tilde{F} - \id \right) \right)^\perp\).
\end{statement}

По-другому говоря, \(\forall p \in X\) рассматривая \(\vv{pF(p)}\), \(\Pr_{\ker\left( \tilde{F} -
\id \right)}{\vv{pF(p)}}\) не зависит от точки \(p\) и всё время будет одна и та же.

\begin{proof}
  \[
  \begin{aligned}
    \vv{qF(q)} &= \vv{qp} + \vv{pF(p)} + \vv{F(p)F(q)} = u_F + u + \left( \vv{F(p)F(q)} - \vv{pq}
    \right) \\
    &= u_F + \underbrace{u}_{\in V^\perp} + \underbrace{\left( \tilde{F}\left( \vv{pq} \right) -
    \vv{pq} \right)}_{\in V^\perp},
  \end{aligned}
  \] --- получили искомое разложение.
\end{proof}

\begin{theorem}
  \(G = F \circ T_{-u_F}\) --- движение с неподвижной точкой (\(T\) --- параллельный перенос).
\end{theorem}

Эта теорема и является классификацией всех движений, ведь оно сводит параллельным переносом любое
движение к движению с неподвижной точкой, чья классификация уже известна.

\begin{proof}
  Зафиксируем \(p \in X\): \(\vv{pF(p)} = u_F + u\), где \(u_F \in V\), \(u \in V^\perp\) (см.
  раннее).

  Раннее также было показано, что \(\exists w \in \vv{X}\colon \left( \tilde{F} - \id \right)(w) =
  u\). Тогда утверждается, что \(q = p - w\) --- неподвижная точка \(G\): \[
  \begin{aligned}
    G(q) &= F(p - w - u_F) = \underbrace{F(p)}_{p + \vv{pF(p)}} - \tilde{F}(u + u_F) = q.
  \end{aligned}
  \]
\end{proof}

\begin{theorem}[Шале]
  Любое движение плоскости --- одно из
  \begin{enumerate}
    \item параллельный перенос;
    \item поворот относительно некоторой точке;
    \item скользящая симметрия (композиция осевой симметрии и параллельного переноса).
  \end{enumerate}
\end{theorem}

Доказать предлагается на семинарах (или самостоятельно).


\chapter{Проективные пространства}

\epigraph{Мы говорим Ленин, \\
подразумеваем --- \\
партия, \\
мы говорим \\
партия, \\
подразумеваем --- \\
Ленин.}{В. Маяковский}

Пусть \(V\) --- ВП над полем \(K\) (а вообще, почти всегда, \(K = \R\)).

На \(V \setminus \left\{ 0 \right\}\) введем отношение эквивалентности: \(u \sim v \iff \exists ;l
\in K \quad u = \lambda v\).

\begin{definition}
  \(\P(V) \coloneqq \qfrac{\left( V \setminus \left\{ 0 \right\} \right)}{\sim}\) --- проективное
  пространство, порожденное \(V\).
\end{definition}

\begin{notation}
  Порой обозначают как \(\R P^{n - 1}\).
\end{notation}

\begin{definition}
  Отображение \(P\colon V \setminus \left\{ 0 \right\} \to \P(V)\), где \(P(v) \coloneqq [v]\) ---
  каноническая проекция (\textit{проективизация}).
\end{definition}

Раз пространства конечномерные, мыслить об этом можно так: точка проективного пространства ---
прямая, проходящая через эту точку и начало координат (ВП размерности \(1\)).

\begin{definition}
  \(\dim{\P(V)} \coloneqq \dim{V} - 1\).
\end{definition}

Немного можно отвлечься на топологию: проще всего ее получить как топологию факторпространства из
топологии подпространства \(\R^{n + 1} \setminus \left\{ 0 \right\}\) (со стандартной топологией
\(\R^{n + 1}\)); открытые множества представляют собою всё между любыми парами прямых.

Пусть \(W \leqslant V\); \(W \neq 0\).

\begin{definition}
  \(\P(W)\) называется проективным подпространством в \(\P(V)\).
\end{definition}

Данное определение является, по сути, ключевым во всем разделе:
\begin{example}
  О \(\R P^2\) стоит мыслить как об \(\R^3\), в котором точки --- прямые проходящие через \(0\);
  взяв некоторое векторное подпространство в \(\R^3\) размерности \(2\) (случай \(1\) не интересен),
  получится проективная прямая (подпространство \(\R P^2\)) --- все прямые, лежащие в этой плоскости
  будут образовывать конкретную проективную прямую.
\end{example}

\begin{example}
  В проективной плоскости любые две прямые пересекаются.
\end{example}

Прямиком из линейной алгебры:
\begin{theorem}
  Если \(X = \P(V)\), а \(Y, Z \leqslant X\), причем \(\dim{Y} + \dim{Z} \ge \dim{X}\), то
  \begin{enumerate}
    \item \(Y \cap Z \neq \O\);
    \item \(Y \cap Z \leqslant X\);
    \item \(\dim\left( Y \cap Z \right) \ge \dim{Y} + \dim{Z} - \dim{X}\).
  \end{enumerate}
\end{theorem}

\begin{example}
  Из этой теоремы сразу следует пересечение любых прямых проективной плоскости.
\end{example}

\paragraph{Однородные координаты}

Выберем \(e_1, \ldots, e_n\) --- базис \(V\); тогда \(\forall v \in V \quad v = \alpha_1e_1 + \ldots
+ \alpha_ne_n\). Если двигать это точку по прямой, то все ее координаты претерпевают только
умножение на соответствующий (одинаковый для всех координат) коэффициент.

\begin{definition}
  \([\alpha_1 : \alpha_2 : \ldots : \alpha_n]\) --- однородные координаты \(\P\left( \lin{v}
  \right)\).
\end{definition}

\section{Аффинные карты и проективное пополнение}

Пусть \(A\) --- аффинное пространство. Как его превратить в проективное?

\begin{definition} \mnote{точки ``\(A\) бесконечность''}
  \(A_\infty \coloneqq \P\left(\vv{A}\right)\).
\end{definition}

\begin{definition}
  Проективное пополнение \(\hat{A} \coloneqq A \sqcup A_\infty\).
\end{definition}

Построим проективную структуру на \(\hat{A}\).

\begin{enumerate}
  \item Вложим \(A\) в аффинное пространство \(V\) (\(V = \vv{V}\)) такое, что \(\dim{V} = \dim{A} +
    1\), как гиперплоскость, не проходящую через \(0\), отождествляя при этом \(\vv{A}\) с
    векторной гиперплоскостью \(\vv{V}\).

  \item Построим биекцию \(i\colon \hat{A} \to \P(V)\):
    \begin{itemize}
      \item \(x \in A \implies i(x) \coloneqq \P(0x)\);
      \item \(x \in A_\infty \implies i(x) \coloneqq x \in \P\left( \vv{A} \right) \subset \P(V)\).
    \end{itemize}
\end{enumerate}

А обратно? Выкинув из проективного пространства любую гиперплоскость получится аффинное
пространство.

\section{Проективные отображения}

Пусть есть векторные пространства с соответствующими проективизациями: как связать их проективные
пространства? Как сделать это удобнее всего?

\[
\begin{tikzcd}
  V \setminus \left\{ 0 \right\} \arrow[d, "P_V"'] \arrow[r, dashed, "L"] & W \setminus \left\{ 0
  \right\} \arrow[d, "P_W"] \\
  \P(V) \arrow[r, "F?"] & \P(W)
\end{tikzcd}
\]

Можно построить линейное отображение \(L\) и как-то его связать с \(F\).

Пусть \(V\) и \(W\) --- векторные пространства.

\begin{lemma}
  Если \(L\colon V \to W\) --- линейное \textit{инъективное} отображение, то существует и
  единственно отображение \(F\colon \P(V) \to \P(W)\) такое, что диаграмма коммутирует: \(P_W \circ
  L = F \circ P_V\).
\end{lemma}

Условие не линейность естественно --- прямые должны переходить в прямые. Если же отбросить
инъективность, то ядро было бы нетривиально, прямая перешла бы в точку и непонятно, чему это бы
соответствовало --- коммутативность диаграммы нарушится.

\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\exists}\)] Пусть \(a \in \P(V)\): существует \(v \in V \setminus \left\{ 0
      \right\}\colon P_V(v) = a\) (вектор ``поднялся'').

      За счет инъективности, \(L(v) \in W \setminus 0\): тогда \(F(a) \coloneqq P_W(L(v))\).

    \item[Корректность] Возьмем \(\lambda v\) такой, что \(\P(\lambda v) = a\).

      \(P_W(L(\lambda v)) = P_W(\lambda L(v)) = F(a)\).

    \item[\(\boxed{!}\)] Очевидно в силу коммутативности.
  \end{pfparts}
\end{proof}

\begin{definition}
  Такое \(F\) называется проективизацией отображения \(L\).
\end{definition}

\begin{definition}
  Отображение \(F\colon \P(V) \to \P(W)\) называется проективным, если оно есть проективизация
  некоторого инъективного линейного отображения \(L\colon V \to W\).
\end{definition}

\begin{remark}
  Проективное отображение тоже инъективно.
\end{remark}

\begin{remark}
  Проективное отображение переводит проективные подпространства в проективные подпространства.
\end{remark}

\section{Продолжение аффинных отображений}

Раз проективное пространства можно представить как пополнение аффинного, можно ли что-то говорить о
проективных отображениях, зная о соответствующих аффинных?

Пусть \(X\) и \(Y\) --- аффинные пространства, а \(\hat{X}\) и \(\hat{Y}\) --- соответствующие
проективные пополнения.

\begin{theorem}
  Если \(F\colon X \to Y\) --- инъективное аффинное отображение, то
  \begin{enumerate}
    \item Существует и единственно проективное отображение \(\hat{F}\colon \hat{X} \to \hat{Y}\)
      такое, что \(\hat{F}\restriction_X = F\);
    \item \mnote{обладает таким свойством} \(\hat{F}(X_\infty) \subseteq Y_\infty\).
  \end{enumerate}
\end{theorem}

\lecture{21}{07.05.2024, 13:40}{}

\begin{statement}
  Отображение \(\Lambda^k(M) \to P\) можно задать на поливекторах \(m_1 \wedge \ldots \wedge m_k
  \mapsto \varphi(m_1, \ldots, m_k)\), где \(\varphi\) полилинейно и антисимметрично.
\end{statement}

По факту это просто ``расшифровка'' универсального свойства.

\begin{consequence}[формулы Уитни]
  Если \(M \cong R^n\) (свободный модуль) с зафиксированным базисом \(e_1, \ldots, e_n\), то
  \(\Lambda^k(M) \cong R^{C_n^k}\), где в качестве базиса можно взять поливекторы вида \(e_{i_1}
  \wedge \ldots \wedge e_{i_k}\), где \(i_1 < \ldots < i_k\).
\end{consequence}
\begin{proof}
  Индукция по \(n\).
  \begin{pfparts}
    \item[База] Тривиально (например, когда \(k > n\)).

    \item[Шаг индукции] Применяя формулу Уитни, почти все \(\Lambda^{k - i}\) обнулятся кроме как
      когда \(k - i \le 1\):\(\Lambda^k(R^{n - 1} \oplus R) \cong \Lambda^{k - 1}(R^{n - 1}) \otimes
      R \oplus \Lambda^k(R^{n - 1})\). \mnote{тоже самое, что и \(C_n^k = C_{n - 1}^{k - 1} + C_{n
      - 1}^k\)}

      В качестве базиса тогда можно взять \((\underbrace{e_{i_1} \wedge \ldots \wedge e_{i_{k - 1}}}
      \wedge e_n, e_{i_1} \wedge \ldots \wedge e_{i_k})\), где \(i_k < n\).
  \end{pfparts}
\end{proof}

\begin{notation}
  \(e_{i_1} \wedge \ldots \wedge e_{i_k} \eqqcolon e_I\), где \(I = \left\{ i_1, \ldots, i_k
  \right\}\) (\(|I| = k\)).
\end{notation}

А зачем вообще всё это нужно?

Рассмотрим гомоморфизм модулей \(\varphi\colon M \to N\). Естественным образом структура
индуцируется на гомоморфизм: \(\Lambda^k\varphi\colon \Lambda^kM \to \Lambda^kN\), где \(m_1 \wedge
\ldots \wedge m_k \mapsto \varphi(m_1) \wedge \ldots \wedge \varphi(m_k)\). 

\begin{example}
  Рассмотрим \(\varphi\colon R^n \to R^n\) и его старшую внешнюю степень \(\Lambda^n\varphi\colon
  \Lambda^n(R^n) \to \Lambda^n(R^n)\), где \(e_1 \wedge \ldots \wedge e_n \mapsto \varphi(e_1)
  \wedge \ldots \wedge \varphi(e_n) = d(\varphi)e_1 \wedge \ldots \wedge e_n\), ведь
  \(\Lambda^n(R^n)\) имеет размерность \(1\).

  Пусть \(A\) --- матрица \(\varphi\) в базисе \(e_1, \ldots, e_n\).

  \(\underbrace{Ae_1}_{\text{столбец } 1} \wedge Ae_2 \wedge \ldots \wedge Ae_n\); коэффициент
  пропорциональности \(d(\varphi)\) линеен по каждому столбцу, если два столбца совпадают, то он
  равен \(0\), и \(d(\varphi) = 1\) для \(A = I\) --- это определитель по определению.

  Его можно еще и посчитать.

  Известно, что \(\varphi(e_i) = \sum_j e_j A_{ji}\); \(\left( \sum_{j_1} e_{j_1} a_{j_1 1} \right)
  \wedge \left( \sum_{j_2} e_{j_2} a_{j_1 2} \right) \wedge \ldots \wedge \left( \sum_{j_n} e_{j_n}
  a_{j_1 n} \right)\) --- раскрывая скобки одинаковые индексы сократятся; \(\sum_{\begin{pmatrix}
  1 & \ldots & n \\ j_1 & \ldots & j_n\end{pmatrix} } a_{j_1 1} a_{j_2 2}
  \ldots a_{j_n n} e_{j_1} \wedge \ldots \wedge e_{j_n} = \sum_{\sigma \in S_n} \sgn{\sigma}
  a_{\sigma(1)1} \ldots a_{\sigma(n) n} e_1 \wedge \ldots \wedge e_n\), --- знак появился из-за
  того, что \(e_{j_1} \wedge \ldots \wedge e_{j_n}\) могут стоять в неправильно порядке и меняя
  местами два из них, меняется знак.

  \begin{statement}
    \(\det(\varphi \psi) = \det{\varphi} \det{\psi}\).
  \end{statement}
  \begin{proof}
    \(\det(\varphi) = \frac{\varphi(e_1) \wedge \ldots \wedge(e_n)}{e_1 \wedge \ldots \wedge e_n}\),
    ведь эти векторы пропорциональны. Отметим, что \(e_1, \ldots, e_n\) --- любой базис.

    Аналогично, \(\det(\varphi\psi) = \frac{\varphi\psi(e_1) \wedge \ldots \wedge
    \varphi\psi(e_n)}{e_1 \wedge \ldots \wedge e_n} = \frac{\varphi\psi(e_1) \wedge \ldots \wedge
    \varphi\psi(e_1)}{\psi(e_1) \wedge \ldots \psi(e_1)} \frac{\psi(e_1) \wedge \ldots
    \psi(e_n)}{e_1 \wedge \ldots \wedge e_n}\) --- за счет того, что в ``знаменателе'' базис любой,
    получается искомое.
  \end{proof}
\end{example}

\begin{example}
  Рассмотрим отображение \(\varphi\colon R^n \to R^m\); \(\Lambda^k\varphi\colon R^{C_n^k} \to
  R^{C_m^k}\). Как выглядит матрица у \(\Lambda^k\varphi\) в базисах \(e_I\), где \(I = \left\{ i_1,
  \ldots, i_k \right\}\) и \(i_1 < \ldots < i_k\)?

  Достаточно понять, куда переходит \(\Lambda^k\varphi(e_I)\): \(\Lambda^k\varphi(e_I) =
  \varphi(e_{i_1}) \wedge \ldots \wedge \varphi(e_{i_k}) = \left( \sum_{j_1} f_{j_1} e_{j_1 i_1}
  \right) \wedge \ldots \wedge \left( \sum_{j_k} f_{j_k} a_{j_k i_k} \right)\), где \(f_J\) ---
  базис \(\Lambda^k R^m\).

  Раскроем скобки: получится \(\sum_{J} f_J \sum_{J = \left\{ j_1, \ldots, j_k \right\}} \pm
  a_{j_1i_1} \ldots a_{j_k i_k}\); с точки зрения матрицы \(A\), коэффициент при \(f_J\) --- минор,
  заданный подмножествами \(I\) и \(J\).
\end{example}

Рассмотрим матрицы \(A\colon k \times n\) и \(B\colon n \times k\), где \(AB\colon k \times k\) ---
квадратная матрица. Как посчитать ее определитель, не считая произведение матриц?

\begin{statement}
  \(\Lambda^k(\varphi \circ \psi) = \Lambda^k\varphi \circ \Lambda^k\psi\).
\end{statement}

\begin{theorem}[формула Бине-Коши]
  \(\det(AB) = \sum_{|I| = k} A_IB_I\) (миноры, порядка \(k\)).
\end{theorem}
\begin{proof}
  Пусть \(\varphi\colon R^n \to R^k\), \(\psi\colon R^k \to R^n\); \(\varphi \circ \psi\colon R^k
  \to R^k\) c базисами \(e_1, \ldots, e_k\) и \(f_1, \ldots, f_n\) соответственно.

  \(\left( \Lambda^k (\varphi \circ \psi) \right)\left( e_1 \wedge \ldots \wedge e_k\right) = \left(
  \Lambda^k\varphi\right)\left( \psi(e_1) \wedge \ldots \wedge \psi(e_k) \right) = \left(
  \Lambda^k\varphi \right) \sum_{|I| = k} f_IB_I\) по посчитанному раннее; \mnote{ранее} получается
  \(\sum_{|I| = k} \Lambda^k(\varphi)(f_I) B_I\).

  Каждое из \(\Lambda^k(\varphi)(f_I)\) кратно \(e_1 \wedge \ldots \wedge e_k\) с коэффициентом
  \(A_I\): итого получится \(\left( \sum_{|I| = k} A_IB_I \right) e_1 \wedge \ldots \wedge e_k\).
\end{proof}

В скором времени мы продолжим изучать подобные теоремы; для этого надо ввести
\begin{definition} \mnote{\(M \cong R^n\)}
  Внешняя алгебра \(\Lambda^\bullet(M) = \bigoplus_{k = 0}^n \Lambda^k(M)\) --- свободный модуль
  ранга \(2^n\).
\end{definition}

Подразумевается, что \(\Lambda^0(M) \coloneqq R\) и \(\Lambda^1(M) = M\).

Если в \(M\) был базис \(e_1, \ldots, e_n\), то в \(\Lambda^\bullet(M)\) можно выбрать базис \(e_I =
e_{i_1} \wedge \ldots \wedge e_{i_k}\), где \(I = \left\{ i_1, \ldots, i_k \right\}\) (\(i_1 <
\ldots < i_k\)); \(I \subseteq \left\{ 1, \ldots, n \right\}\).

Введем операцию умножения.

\begin{definition}
  Умножение \(\Lambda^k(M) \times \Lambda^l(M) \to \Lambda^{k + l}(M)\), где \((m_1 \wedge \ldots
  \wedge m_k, m_{k + 1} \wedge \ldots \wedge m_{k + l}) \mapsto m_1 \wedge \ldots \wedge m_k \wedge
  m_{k + 1} \wedge \ldots \wedge m_l\).
\end{definition}

Корректность проверяется так же, как в доказательстве формулы Уитни.

\begin{proof}
  Внешняя степень порождается разложимыми поливекторами, а билинейное отображение достаточно задать
  на каждой паре из таких поливекторов.

  Зададим полилинейную кососимметрическую функцию \(f\colon M^k \times M^l \to \Lambda^{k + l}(M)\),
  где \((m_1, \ldots, m_k, m_1', \ldots, m_l') \mapsto m_1 \wedge \ldots \wedge m_k \wedge m_1'
  \wedge \ldots \wedge m_l'\). По универсальному свойству внешней степени, оно единственным образом
  пропускается через \(\tilde{f}\colon \Lambda^k(M) \times M^l \to \Lambda^{k + l}(M)\) так, что
  \((m_1 \wedge \ldots \wedge m_k, m_1', \ldots, m_l') \mapsto m_1 \wedge \ldots \wedge m_k \wedge
  m_1' \wedge \ldots \wedge m_l'\) --- такое отображение полилинейно и кососимметрично в последних
  \(l\) координатах. Тогда, опять по универсальному свойству, уже оно пропускается через отображение
  \(B\colon \Lambda^k(M) \times \Lambda^l(M) \to \Lambda^{k + l}(M)\) --- оно и будет искомым.
\end{proof}

\begin{remark}
  Считается, что \(\Lambda^k(M) \leqslant \Lambda^\bullet(M)\).
\end{remark}

\begin{statement}
  \(e_I \wedge e_J = \begin{cases}
    0, &I \cap J \neq \O \\
    (-1)^{\mathrm{inv}(I, J)}e_{I \cup J}, &\text{иначе},
  \end{cases}\) где \(\mathrm{inv}(I, J) \coloneqq \#\left\{ i \in I, j \in J \mid i > j \right\}\).
  \mnote{сортировка слиянием}
\end{statement}

\begin{statement}
  Умножение внешних степеней дистрибутивно по сложению.
\end{statement}

\begin{statement}
  Умножение внешних степеней ассоциативно.
\end{statement}

\begin{statement}
  Умножение внешних степеней суперкоммутативно: \(x \wedge y = (-1)^{kl} y \wedge x\), где \(x \in
  \Lambda^k\), \(y \in \Lambda^l\).
\end{statement}

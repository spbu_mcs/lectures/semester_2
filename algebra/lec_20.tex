\lecture{20}{30.04.2024, 13:40}{}

\(R\) --- по прежнему коммутативное кольцо, \(S\) --- по прежнему \(R\)-алгебра.

\begin{statement}
  Любой (левый) \(S\)-модуль является \(R\)-модулем, где \(rx \coloneqq \varphi(r)x\).
\end{statement}

\begin{definition}
  Такая конструкция называется сужением скаляров.
\end{definition}

Сужение, потому что такое \(\varphi\) зачастую инъективно, и \(R\) является подкольцом в \(S\).

Соответственно, построение \(S\)-модуля по \(R\)-модулю --- это расширение скаляров.

\begin{remark}
  Конструкции сужения и расширения скаляров совсем не являются взаимнообратными, но являются
  \textit{сопряженными}.
\end{remark}

Пусть \(M\) --- \(R\)-модуль: рассмотрим отображение \(i\colon M \xrightarrow{m \mapsto 1 \otimes m}
S \otimes_R M\). Оно линейное как отображение \(R\)-модулей: \(rm \mapsto 1 \otimes rm = r(1 \otimes
m)\).

На самом деле, оно в некотором смысле универсально.

Рассмотрим некоторый \(N\) --- \(S\)-модуль.

\begin{theorem}[универсальное свойство расширения скаляров]
  Если \(\psi\) --- линейное отображение \(R\)-модулей, то существует единственное \(S\)-линейное
  отображение \(S \otimes_R M \to N\) такое, что следующая диаграмма коммутирует: \[
  \begin{tikzcd}
    M \arrow[r, "i"] \arrow[rd, "\psi"'] & S \otimes_R M \arrow[d, dashed] \\
    & N.
  \end{tikzcd}
  \]

  То есть \(\Hom_R(M, N) \simeq \Hom_S(S \otimes_R M, N)\). \mnote{какая-то конкретная хорошая
  биекция; слева \(N\) --- сужение скаляров}
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[Существование] Построим сначала \(R\)-линейное отображение, а потом покажем, что оно и
      \(S\)-линейное.

      Пусть \((s, m) \mapsto s\psi(m)\) (билинейное отображение \(S \times M \to N\), задающее \(S
      \otimes_R M \to N\)). Это задает \(s \otimes m \mapsto s\psi(m)\).

      Оно подходит (диаграмма коммутативна), так как
      \[
      \begin{tikzcd}
        m \arrow[r] \arrow[rd] & 1 \otimes m \arrow[d] \\
        & 1\psi(m).
      \end{tikzcd}
      \]

      Проверим, что оно и \(S\)-линейное: \(t(s \otimes m) = ts \otimes m \mapsto (ts)\psi(m)\); с
      другой стороны \(s \otimes m \mapsto s\psi(m)\); \((ts)\psi(m) = t(s\psi(m))\).

    \item[Единственность] Возьмем некоторый \(\tilde{\psi}\) с такими же свойствами, как и \(\psi\).

      По коммутативности диаграммы, \(\tilde{\psi}(1 \otimes m) = \psi(m)\); по линейности же
      \(\tilde{\psi}(s \otimes m) = \tilde{\psi}(s(1 \otimes m)) = s\psi(m)\). По универсальному
      свойство тензорного произведения вытекает единственность.
  \end{pfparts}
\end{proof}

\begin{exercise}
  Найти, где в этом доказательстве использовалась коммутируемость \(R\)-алгебры.
\end{exercise}

Наблюдения о сопряженности \(\Hom\) и \(\otimes\) можно продолжить: \(\Hom_R(M \otimes_R N, P)
\simeq \left\{ M \times N \to P \mid \text{билинейное} \right\} \simeq \Hom_R(M, \Hom_R(N, P))\).
\mnote{в ФП это называется currying}

Еще раз: \(\boxed{\Hom_R(M \otimes_R N, P) \simeq \Hom_R(M, \Hom_R(N, P))}\). Подробнее таким
занимаются на втором курсе.

Рассмотрим в том же духе свободные модули:
\begin{statement}
  \(\Hom_R(R^{(I)}, M) \simeq \mathrm{Map}(I, M)\), \mnote{\(M^I\)} где слева \(M\) воспринимается
  как множество.
\end{statement}

\begin{exercise}
  А что можно придумать в том же духе с полем частных?
\end{exercise}

\section{Внешняя степень и внешняя алгебра}

Пусть \(R\) --- коммутативное кольцо, \(M\) и \(P\) --- \(R\)-модули.

Хотим научиться параметризовать отображения вида \(\underbrace{M \times \ldots \times M}_k \to P\) с
такими свойствами:
\begin{itemize}
  \item оно полилинейное;
  \item оно знакопеременное (симметричное).
\end{itemize}

\begin{example}
  Определитель \(\det\colon R^k \times \ldots \times R^k \to R\) является таким отображением.
\end{example}

% FIXME: wedge and lambda
\begin{definition}
  \(\Lambda^k(M)\) (\(k\)-ая внешняя степень \(M\)) --- \(R\)-модуль с фиксированным полилинейным
  знакопеременным отображением \(M \times \ldots \times M \xrightarrow{(m_1, \ldots, m_k) \mapsto
  m_1 \wedge \ldots \wedge m_k} \Lambda^k(M)\) со следующим универсальным свойством: \[
  \begin{tikzcd}
    M \times \ldots \times M \arrow[r] \arrow[rd, "\varphi"'] & \Lambda^k(M) \arrow[d, dashed,
    "\tilde{\varphi}"] \\
    & P,
  \end{tikzcd}
  \] где \(\exists! \tilde{\varphi}\colon \Lambda^k(M) \to P\), где \(\tilde{\varphi}(m_1 \wedge
  \ldots \wedge m_k) = \varphi(m_1, \ldots, m_k)\).
\end{definition}

Покажем, что внешние степени существуют и единственны.

Как уже известно, \(\Hom_R(M \otimes \ldots \otimes M, P) \simeq \left\{ M \times \ldots \times M
\to P \mid \text{полилинейное} \right\}\); хочется, чтобы справа отображения были еще и
знакопеременные. Для этого можно применить факторизацию.

\begin{statement}
  \(\Lambda^k(M) \coloneqq \qfrac{M \otimes \ldots \otimes M}{A}\), где \(A\) --- подмодуль,
  порожденный элементами вида \(m_1 \otimes \ldots \otimes \hat{m} \otimes \ldots \otimes \hat{m}
  \otimes \ldots \otimes m_k\) (ровно два элемента совпадают).
\end{statement}

По универсальному свойству факторизации, \(\Hom(\Lambda^k(M), P) = \) полилинейные отображения
\(\varphi\) такие, что \(\varphi(m_1, \ldots, \hat{m}, \ldots, \hat{m}, \ldots, m_k) = 0\) --- это и
есть знакопеременные отображения.

Хочется научиться это как-то нормально считать. Зададимся вопросом нахождения базиса в
\(\Lambda^k(R^n)\) --- какая у него размерность?

\begin{example}
  Рассмотрим \(\Lambda^2(R^n)\): \(\Hom(\Lambda^2(R^n), P)\) --- знакопеременное билинейное
  отображение, задаваемая парами \((e_i, e_j)\), где \(i < j\) (за счет симметричности:
  \(\varphi(e_j, e_i) = -\varphi(e_i, e_j)\)); \(\dim \Lambda^2(R^n) = C_n^2\).
\end{example}

\begin{definition}
  Поливектор --- элемент \(\Lambda^k(M)\).
\end{definition}

\begin{statement}[универсальное свойство прямой суммы]
  \(\Hom(Q_0 \oplus \ldots \oplus Q_k, P) \simeq \Hom(Q_0, P) \times \ldots \times \Hom(Q_k, P)\).
\end{statement}

\begin{statement}[формула Уитни] \mnote{\(C_{s + t}^k = \sum_i C_s^iC_t^{k - i}\)}
  \(\Lambda^k(M \oplus N) \simeq \bigoplus_{i = 0}^k \Lambda^i(M) \otimes \Lambda^{k - i}(N)\), где
  \(\Lambda(M) = M\), а \(\Lambda^0(M) = R\) (договоренность).
\end{statement}
\begin{proof}
  Покажем, что правая часть обладает универсальным свойством как внешность \(\Lambda^k(M \oplus
  N)\).

  Рассмотрим полилинейное знакопеременное отображение \(\varphi\colon M \oplus N \times \ldots
  \times M \oplus N\); будем обозначать \((m_1, n_1) = m_1 + n_1\) (то есть \((m_1, 0) + (0,
  n_1)\)).

  У \(\varphi(m_1 + n_1, \ldots, m_k + n_k)\) всего \(2^k\) слагаемых по полилинейности; применяя
  еще и знакопеременность, получается \(\sum \sum \pm \varphi\left(m_{j_1}, \ldots, m_{j_i},
  n_{l_1}, \ldots, n_{l_{k - i}}\right)\), причем индексы можно упорядочить: \(j_1 < \ldots < j_i\),
  \(l_1 < \ldots < l_{k - i}\), а все \(\left\{ j_1, \ldots, j_i, l_1, \ldots, l_{k - i} \right\}\)
  образуют множество от \(1\) до \(k\). В \(\pm\) кроется знак перестановки \(\begin{pmatrix}
    1 & \ldots & i & i + 1 & \ldots & k \\
    j_1 & \ldots & j_i & l_1 & \ldots & l_{k - i}
  \end{pmatrix} \).

  Зафиксируем \(m_{j_1}, \ldots, m_{j_i}\): по оставшимся аргументам получается полилинейное
  знакопеременное отображение; \(\varphi\left(m_{j_1}, \ldots, m_{j_i}, -, \ldots,
  -\right)\) задает линейное отображение из \(\Lambda^{k - i}(N)\); по универсальному свойству,
  отображения такого вида находятся в биекции с этой \(\Lambda^{k - i}\).

  Получилось, что \(\varphi_i\colon \underbrace{M \times \ldots \times M}_i \to \Hom(\Lambda^{k -
  i}(N), P)\), где \(\varphi_i\left( m_{1}, \ldots, m_{i} \right)\left(n_{1} \wedge \ldots \wedge
  n_{k - i}\right) = \varphi\left(m_{j_1}, \ldots, m_{j_i}, n_{l_1}, \ldots, n_{k - i}\right)\).
  Такое \(\varphi_i\) полилинейно и знакопеременно; по универсальному свойству она задает элемент из
  \(\Hom(\Lambda^i(M), \Hom(\Lambda^{k - i}(N), P)) \simeq \Hom(\Lambda^i(M) \otimes \Lambda^{k -
  i}(N), P)\).

  Формула с \(\sum \sum \pm\) же говорит, что этот \(\varphi\) однозначно задается таким набором
  отображений.

  Покажем, как по \(\tilde{\varphi} \in \Hom(\Lambda^i(M) \otimes \Lambda^{k - i}(N),
  P)\) восстанавливается \(\varphi\): \(\varphi(m_1 + n_1, \ldots, m_k + n_k) = \sum\sum \pm
  \tilde{\varphi}_i\left( \left( m_{j_1} \wedge \ldots \wedge m_{j_i} \right) \otimes \left( n_{l_1}
  \wedge \ldots \wedge n_{l_{k - i}} \right) \right)\); предлагается проверить, что если \(\varphi\) 
  так задать, то получится полилинейная знакопеременная функция.

  Применяя универсальное свойство прямой суммы и получается, что правая часть обладает универсальным
  свойством внешности.
\end{proof}

\begin{example}
  \(\Lambda^2(M \oplus N) \simeq \Lambda^2(M) \oplus M \otimes N \oplus \Lambda^2(N)\).
\end{example}

\begin{consequence}
  \(\dim{\Lambda^k(R^s)} = C_s^k\).
\end{consequence}

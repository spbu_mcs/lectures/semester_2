\lecture{14}{01.04.2024, 11:15}{}

\section{Теорема об обратном отображении}

Пусть \(\mathcal{D} \subset \R^n\) открыто, а \(f\colon \mathcal{D} \to \R^n\) (именно \(\R^n\)).

Положим, что \(f \in C^1\left( \mathcal{D} \right)\).

Раз мы будем рассматривать обратные отображения, выберем точку \(x^0 \in \mathcal{D}\) такую, что
\(\det{f'(x^0)} \neq 0\).

\begin{theorem}[о билипшицевости]
  Существует окрестность \(U(x^0)\) и константа \(L\) такие, что \[
    \frac{|x - y|}{L} \le |f(x) - f(y)| \le L|x - y| \quad \forall x, y \in U(x^0)
  .\]
\end{theorem}

По сути эта теорема даёт ``локальную биекцию''.

\begin{proof}
  По оценке Лагранжа, найдется \(c \in (x, y)\) такая, что \[
  \begin{aligned}
    |f(x) - f(y)| &\le \|f'(c)\||x - y|.
  \end{aligned}
  \]

  Теперь снизу: \[
  \begin{aligned}
    |f(x) - f(y)| &= \left| f(x) - f(y) - f'(x^0)(x - y) + f'(x^0)(x - y) \right| \\
    &\oversym{\ge}{\triangle} \left| f'(x^0)(x - y) \right| - \left| f(x) - f'(x^0)x - (f(y) -
    f'(x^0)y) \right| \\
    &\oversym{\ge}{\text{Л-ж}} \left| f'(x^0)(x - y) \right| - \|f'(c) - f'(x^0)\||x -
    y|;
  \end{aligned}
  \] раз \(f'(x)\) непрерывна, то в \(U(x^0)\) норма \(\|f'(c) - f'(x^0)\|\) будет достаточно мала.

  \[
  \begin{aligned}
    |f'(x^0)(x - y)| &\ge \frac{|x - y|}{\|f'^{-1}(x^0)\|},
  \end{aligned}
  \] ведь если \(A\) --- невырожденный оператор, то \(|AS| \ge \frac{|S|}{\|A^{-1}\|}\) (если взять
  \(S = A^{-1}h\), то из уже известного \(|A^{-1}h| \le \|A^{-1}\||h|\) получится искомое).
\end{proof}

\textit{Пояснение}. Найденная по оценке Лагранжа константа \(c\) одна для всех \(x, y\), ведь \(f'\)
непрерывна на компакте, а значит и равномерно непрерывна: \(\forall \varepsilon > 0 \exists
\delta\colon |x^0 - c| < \delta \implies |f'(c) - f'(x^0)| < \varepsilon\) (и поэтому норма будет
``достаточно мала'').

Для теоремы об обратной функции билипшицевости не хватает: она не утверждается, например, что
область перейдет в область.

Пусть \(M \subset \R^n\) замкнуто (в \(\R^n\)), а \(f\colon M \to M\) является сжимающим.

\begin{theorem}[о неподвижной точке]
  \(f(x) = x\) имеет единственное решение (в \(M\)).
\end{theorem}
\begin{proof}
  Рассмотрим последовательность \(x_{n + 1} \coloneqq f(x_n)\): \mnote{\(n\) не из \(\R^n\)} \[
  \begin{aligned}
    |x_{n + 1} - x_n| = |f(x_n) - f(x_{n - 1}))| &\le \alpha|x_n - x_{n - 1}| \\
    &\le \ldots \le \alpha^n|x_1 - x_0|.
  \end{aligned}
  \]

  Проверим фундаментальность: \[
  \begin{aligned}
    |x_{n + m} - x_n| &= |x_{n + m} - x_{n + m - 1} + x_{n + m - 1} - x_n| \le \alpha^{n + m -
    1}|x_1 - x_0| + |x_{n + m - 1} - x_n| \\
    &\le \ldots \le \alpha^{n + m - 1}|x_1 - x_0| + \alpha^{n + m - 2}|x_1 - x_0| + \ldots +
    \alpha^{n - 1}|x_1 - x_0| < \varepsilon
  \end{aligned}
  \] при \(n > N(\varepsilon)\) (раз \(\alpha < 1\)).

  Значит \(\exists x\colon x = \lim_{n \to \infty} x_n\), являющийся решением уравнения \(f(x) =
  x\) (по непрерывности).

  Единственность очевидна, ведь если бы была другая неподвижная точка, то \(|x - x'| = |f(x) -
  f(x')| < \alpha|x - x'|\).
\end{proof}

Пусть \(f \in C^1\left( \mathcal{D} \right)\) и \(\forall x \in \mathcal{D} \quad \det{f'(x)} \neq
0\) (опять).

\begin{theorem}[об открытом отображении]
  \(f(\mathcal{D})\) открыто в \(\R^n\).
\end{theorem}
\begin{proof}
  Зафиксируем \(x^0 \in \mathcal{D}\). Цель --- решить уравнение \(f(x) = a\) в окрестности точки
  \(y^0 \coloneqq f(x^0)\).

  Положим, что \(\varphi_a(x) \coloneqq x - f'^{-1}(x^0)\left( f(x) - a \right)\).
  \begin{pfparts}
    \item[Сжимаемость] Рассмотрим \(|x - x^0| \le \delta\) (неравенство нестрогое из-за
      замкнутости); \(|y - x^0| < \delta\). По теореме Лагранжа найдется \(c \in [x, y]\) такая, что
      \[
      \begin{aligned}
        |\varphi_a(x) - \varphi_a(y)| &\le \|\varphi_a'(c)\||x - y| \\
        &\le \|I - f'^{-1}(x^0)f'(c)\||x - y| \\
        &= \|f'^{-1}(x^0)\left( f'(x^0) - f'(c) \right)\||x - y| \\
        &\le \|f'^{-1}(x^0)\|\|f'(x^0) - f'(c)\||x - y|.
      \end{aligned}
      \]

      Выберем \(\delta\) таким, что \(\|f'^{-1}(x^0)\|\|f'(x^0) - f'(c)\| \le \frac{1}{2}\)
      для \(|x - x^0| \le \delta\) (по непрерывности).

      Сжимаемость получена.

    \item[Отображение в себя] Проверим, что \(\varphi_a\) в \(|x - x^0| \le \delta\) отображает в
      себя саму.

      \[
      \begin{aligned}
        |\varphi_a(x) - x^0| &= \left| x - x^0 - f'^{-1}(x^0)(f(x) - a) \right| \\
        &= \left| x - x^0 - f'^{-1}(x^0)\left(f(x^0) + f'(x^0)(x - x^0) + \smallO(|x - x^0|) -
        a\right) \right| \\
        &= \left| -f'^{-1}(x^0)(f(x^0) - a) + \smallO(|x - x^0|) \right| \\
        &\le \|f'^{-1}(x^0)\||f(x^0) - a| + \smallO_1(|x - x^0|).
      \end{aligned}
      \]

      Хочется, чтобы \(\|f'(x^0)\||f(x^0) - a| + \smallO_1(|x - x^0|) \le \delta\) (всё еще при \(|x
      - x^0| \le \delta\)).

      Понятно, что можно выбрать \(\delta\) (хоть и немного другую), что \(\smallO_1(|x - x^0|) \le
      \frac{1}{2}|x - x^0|\): \[
      \begin{aligned}
        |f(x^0) - a| \le \frac{\delta}{2\|f'^{-1}(x^0)\|} \eqqcolon r.
      \end{aligned}
      \]

      Получилось, что \(\exists \delta\colon \left\{ a|y^0 - a| < r \right\} \subset f(|x - x^0 \le
      \delta)\).
  \end{pfparts}

  Теперь можно применить теорему о неподвижной точке: \(\varphi_a(x) = x \iff f(x) = a\).
\end{proof}

\lecture{15}{04.04.2024, 15:35}{}

Наконец-то мы добрались до центральной теоремы этого семестра.

Пусть \(f \in C^1\left(\mathcal{D}\right)\), \(\mathcal{D} \subset \R^n\) открыто, \(x^0 \in
\mathcal{D}\) и \(\det{f'(x^0)} \neq 0\).

\begin{theorem}[об обратном отображении]
  Существуют окрестности \(U(x^0)\) и \(V(y^0) = V(f(x^0))\) такие, что \(f\colon U \to V\) ---
  гомеоморфизм, \(f^{-1}(y) \in C^1\left( V \right)\) и \(\left(f^{-1}(y)\right)' = f'^{-1}(x(y))\).

  Если же еще \(f \in C^r(U)\), то и \(f^{-1} \in C^r(V)\).
\end{theorem}
\begin{proof}
  В силу непрерывности, \(f(x) - f(x^0) = f'(x^0)(x - x^0) + \smallO(|x - x^0|)\).

  По теореме об открытом отображении найдется \(x = f^{-1}(y)\) в некоторой окрестности \(y^0\)
  (то есть \(f(x^0)\)).

  Получается, что \[
  \begin{aligned}
    y - y^0 &= f'(x^0)(x(y) - x(y^0)) + \smallO\left( |x(y) - x(y^0)| \right) \\
    &\oversym{=}{\text{Лип-ость}} f'(x^0)(x - x^0) + \smallO\left(|y - y^0|\right).
  \end{aligned}
  \]

  Видно, что тогда \[
  \begin{aligned}
    x - x^0 &= f'(x^0)^{-1}(y - y^0) + f'(x^0)^{-1}\smallO\left(|y - y^0|\right) \\
    &= f'(x^0)^{-1}(y - y^0) + \smallO\left(|y - y^0|\right).
  \end{aligned}
  \]

  Значит \(x(y)\) дифференцируема в точке \(y^0\) и ее окрестности \(V(y^0)\).

  Из дифференцируемости следует непрерывность \(x(y)\), из чего следует непрерывность и \(x'(y) =
  f'(x(y))^{-1}\): \(x(y) \in C^1\left( V \right)\).
\end{proof}

\begin{example}
  Рассмотрим функцию \(f\colon \R^2 \to \R^2\).

  \[
    f'(x) = \begin{pmatrix} a(x) & b(x) \\ c(x) & d(x) \end{pmatrix}
  .\]

  \[
    x'(y) = f'(x)^{-1} = \begin{pmatrix}
      \frac{d(x(y))}{\det{f'(x)}} & \frac{-b(x(y))}{\det{f'(x)}} \\[7pt]
    \frac{-c(x(y))}{\det{f'(x)}} & \frac{a(x(y))}{\det{f'(x)}}
  \end{pmatrix}
  ,\] --- отсюда сразу видна непрерывность и производной, ведь \(a, b, c, d\) --- непрерывные
  функции.

  И вообще, \(y(x) \in C^r(U) \implies x(y) \in C^r(V)\).
\end{example}

\section{Модифицированный метод Ньютона}

Зададимся вопросом приближенного решения уравнения \(\boxed{f(x) = 0}\).

Зафиксируем точку \(x^0\) (\textit{начальное приближение}) и положим, что \[
  x_{n + 1} \coloneqq x_n - \frac{f(x_n)}{f'(x^0)}.
\]

Пусть \(|f'(x_1) - f'(x_2)| \le L|x_1 - x_2|\) при \(|x - x_0| < r\) (\textit{липшицевая
непрерывность}).

Дополнительно положим, что \(\det{f'(x_0)} \neq 0\), \(M \coloneqq \|f'(x_0)\|\) и \(k \coloneqq
\left| f'(x_0)^{-1}f(x_0) \right|\).

\begin{theorem}
  Если \(h \coloneqq MkL < \frac{1}{4}\), то в шаре \(|x - x_0| \le kt_0\), где \(t_0\) ---
  минимальный корень \(ht_0^2 - t_0 + 1 = 0\), \(f(x) = 0\) имеет единственное решение
  \(x_{\pumpkin}\), причем \(|x_{\pumpkin} - x_n| \le \frac{q^n}{1 - q}k\), где \(q < \frac{1}{2}\).
\end{theorem}
\begin{proof}
  Положим \(\varphi(x) = x - f'(x_0)^{-1}f(x)\); надо найти неподвижную точку (\(\varphi(x) = x\)).

  \[
  \begin{aligned}
    |\varphi(x) - x_0| &= |x - x_0 - f'(x_0)^{-1}(f(x) - f(x_0)) + f'(x_0)^{-1}f(x_0)| \\
    &\oversym{\le}{\text{Л-ж}} \|I - f'(x_0)^{-1}f'(c)\||x - x_0| + k = \dots = ML(kt_0)^2 + k =
    kt_0.
  \end{aligned}
  \]

  Покажем сжимаемость: \[
  \begin{aligned}
    |\varphi(x_1) - \varphi(x_2)| &\le \|\varphi'(c)\||x_1 - x_2| = \|I - f'(x_0)^{-1}f'(c)\||x_1 -
    x_2| \\
    &\le \underbrace{MLkt_0}_{< \frac{1}{4}t_0 < \frac{1}{2}}|x_1 - x_2|.
  \end{aligned}
  \]

  Осталось оценить сходимость: \[
  \begin{aligned}
    \left| x_n - x_{\pumpkin} \right| &= \left| x_{n} - x_{n + 1} - x_{\pumpkin} + x_{n + 1} \right|
    \\
    &\le |x_{n + 1} - x_n| + |x_{n - 2} - x_{n + 1}| + \ldots < \infty.
  \end{aligned}
  \]

  \[
  \begin{aligned}
    |x_{n + 1} - x_n| &= |\varphi(x_n) - \varphi(x_{n + 1})| \le q|x_n - x_{n - 1}| \le q^2|x_{n -
    1} - x_{n - 2}| \\
    &\le \ldots \le q^n|x_1 - x_0| = kq^n.
  \end{aligned}
  \]

  Ну и получилось, что \(\left|x_n - x_{\pumpkin}\right| \le \frac{kq^n}{1 - q}\).
\end{proof}

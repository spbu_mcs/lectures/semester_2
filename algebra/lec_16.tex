\lecture{16}{16.04.2024, 13:40}{}

\section{Тензорное произведение}

Как известно, замена базиса для линейного отображения --- \(C^{-1}AC\), а замена базиса для
билинейной формы --- \(C^\top\Gamma C\). Выглядит похоже, но не совсем.

Пусть \(R\) --- коммутативное кольцо, а \(M\), \(N\) и \(P\) --- \(R\)-модули.

\begin{definition}
  Отображение \(\varphi\colon M \times N \to P\) называется билинейным, если
  \begin{enumerate}
    \item \(\varphi(m_1 + m_2, n) = \varphi(m_1, n) + \varphi(m_2, n)\);
    \item \(\varphi(m, n_1 + n_2) = \varphi(m, n_1) + \varphi(m, n_2)\);
    \item \(\varphi(\alpha m, n) = \alpha\varphi(m, n)\);
    \item \(\varphi(m, \alpha n) = \alpha\varphi(m, n)\).
  \end{enumerate}
\end{definition}

\begin{example}
  Билинейная форма \(B\colon V \times V \to F\) --- билинейное отображение.
\end{example}

\begin{example}
  Отображение \(R \times R \xrightarrow{(r, s) \mapsto rs} R\) билинейное.
\end{example}

\begin{example}
  Отображение \(R \times N \xrightarrow{(r, n) \mapsto rn} N\) билинейное.
\end{example}

Можно придумать что-нибудь похитрее.

\begin{example}
  Рассмотрим отображение \(V \times V^* \to F\), где \((v, f) \mapsto f(v)\), --- оно тоже
  билинейное.
\end{example}

\begin{example}
  Рассмотрим отображение \(\H \to \H \xrightarrow{(q, r) \mapsto q^2} \H\), принимая \(\H\) как ВП
  над \(\R\). Это --- билинейное отображение.
\end{example}

В кватернионах изначально есть и ассоциативность: ее наличие тоже не обязательно.
\begin{example}
  Отображение \(\R^3 \times \R^3 \xrightarrow{(u, v) \mapsto u \times v} \R^3\) билинейное без
  ассоциативности.
\end{example}

\begin{example}
  Рассмотрим матрицы \(n \times m\) и \(m \times k\): отображение \((A, B) \mapsto AB\) билинейное
  (и ассоциативное).
\end{example}

\begin{statement} \mnote{все буквы --- \(R\)-модули}
  Если отображение \(\varphi\colon M \times N \to P\) билинейное, а \(\alpha\colon P \to Q\)
  линейное, то \(\alpha \circ \varphi\colon M \times N \to Q\) тоже билинейно.
\end{statement}

\begin{example}
  Раз на матрицах отображение \((A, B) \mapsto AB\) билинейно, то таким будет и \((A, B) \mapsto
  \tr(AB)\).
\end{example}

Хватит примеров. Как классифицировать все билинейные отображения \(M \times N \to P\), где \(M\) и
\(N\) --- фиксированные \(R\)-модули?

Попробуем придумать \textit{универсальное} билинейное отображение такое, что любое другое получается
из него как в предыдущем утверждении (с композицией с линейным).

Формальнее, хотим придумать такой единственный \(\alpha\) (линейный), чтобы следующая диаграмма
коммутировала (\(\varphi\) --- билинейное отображение).

\[
\begin{tikzcd}
  M \times N \arrow[d] \arrow[r, "\varphi"] & Q \\
  M \otimes N \arrow[ru, dashed, "\alpha"']
\end{tikzcd}
\]

\begin{theorem}
  Для фиксированных \(M\) и \(N\) существует модуль \(M \otimes N\) и билинейное отображение \(M
  \times N \to M \otimes N\) такое, что диаграмма выше коммутирует, причем оно единственно в
  следующем смысле: если есть другой модуль \(M \otimes' N\) с таким же свойством, то существует и
  единственен изоморфизм, при котором следующая диаграмма коммутирует:
  \[
  \begin{tikzcd}
    M \times N \arrow[rd] \arrow[r] & M \otimes N \arrow[d, "\cong"] \\
    & M \otimes' N
  \end{tikzcd}
  \]
\end{theorem}

\begin{remark}
  То есть они не просто изоморфны как модули, а еще согласуются с данным билинейным отображением.
\end{remark}

\begin{proof}
  \hfill
  \begin{pfparts}
    \item[Единственность] Воспользуемся универсальным свойством \(M \otimes N\): \(\exists!
      \beta\colon M \otimes N \to M \otimes' N\), которое делает следующую диаграмму коммутативной
      \[
      \begin{tikzcd}
        M \times N \arrow[rd] \arrow[r] & M \otimes N \arrow[d, dashed, "\beta"] \\
        & M \otimes' N
      \end{tikzcd}
      .\]

      Но надо еще показать, что это изоморфизм. Проверять сюръективность и инъективность --- не наш
      метод; раз для \(M \otimes' N\) тоже выполняется универсальное свойство, то \(\exists
      \beta'\colon \ldots\) такое, что следующая диаграмма коммутативная.
      \[
      \begin{tikzcd}
        M \times N \arrow[rd] \arrow[r] & M \otimes N \arrow[d, dashed, bend left=60, "\beta"] \\
        & M \otimes' N \arrow[u, dashed, "\beta'"]
      \end{tikzcd}
      .\]

      Надо проверить, что \(\beta'\) --- обратное к \(\beta\); рассмотрим \(\beta' \circ \beta\colon
      M \otimes N \to M \otimes N \) --- оно линейно.

      Оно делает следующую диаграмма коммутативной; но для коммутативности подойдет и \(\id\).
      \[
      \begin{tikzcd}
        M \times N \arrow[rd] \arrow[r] & M \otimes N \arrow[d, "\beta' \circ \beta"] \arrow[d,
        bend left=90, "\id"] \\
        & M \otimes N
      \end{tikzcd}
      .\]

      Однако универсальное свойство говорит и про единственность: значит \(\beta' \circ \beta \equiv
      \id\), \(\beta \circ \beta' \equiv \id\) и \(\beta\) и впрямь является изоморфизмом.

    \item[Существование] Возьмем огромный свободный модуль \(R^{(M \times N)}\) (произведение как
      множеств); естественным образом есть отображение \(M \times N \xrightarrow{(m, n) \mapsto
      \delta_{(m, n)}} R^{(M \times N)}\), однако на этом преимущества заканчиваются --- нет ни
      линейности, ни билинейности.

      Сделаем его билинейным. Рассмотрим в \(R^{(M \times N)}\) подмодуль \(L\), порожденный
      следующими элементами:
      \begin{itemize}
        \item \(\delta_{(m_1 + m_2, n)} - \delta_{(m_1, n)} - \delta_{(m_2, n)}\);
        \item \(\delta_{(m, n_1 + n_2)} - \delta_{(m, n_1)} - \delta_{(m, n_2)}\);
        \item \(\delta_{(rm, n)} - r\delta_{(m, n)}\);
        \item \(\delta_{(m, rn)} - r\delta_{(m, n)}\).
      \end{itemize}

      Раз хочется билинейности, то отфакторизуем! \(\boxed{M \otimes N \coloneqq \qfrac{R^{(M \times
      N)}}{L}}\).

      Отображение \((m, n) \mapsto m \otimes n\) заведем композицией \(M \times N \to R^{(M \times
      N)} \xrightarrow{\pi} \qfrac{R^{(M \times N)}}{L}\); оно уже является билинейным.

      Осталось проверить универсальное свойство тензорного произведения: оно получается комбинацией
      универсального свойства свободного модуля и факторизации.

      Пусть \(\varphi\colon M \times N \to Q\) --- билинейное отображение. Рассмотрим его сугубо как
      отображение множеств, не обращая внимание на билинейность. По универсальному свойству
      свободного модуля \(\exists! \alpha\colon R^{(M \times N)} \to Q\) такое, что \(\delta_{(m,
      n)} \mapsto \varphi(m, n)\), где \(\alpha\) --- линейное отображение.

      Этого недостаточно, ведь цель --- отображение из фактора. Нужно проверить, что это отображение
      обнуляется на элементах \(L\):
      \begin{itemize}
        \item \(\delta_{(m_1 + m_2, n)} - \delta_{(m_1, n)} - \delta_{(m_2, n)} \mapsto \varphi(m_1
          + m_2, n) - \varphi(m_1, n) - \varphi(m_2, n) = 0\) по билинейности \(\varphi\);
        \item аналогично.
      \end{itemize}

      Раз любой элемент из \(L\) --- линейная комбинациях таких \(\delta\), то \(\varphi\) и впрямь
      обнуляется на его элементах. Тогда, по универсальному свойству факторизации \(\exists!
      \beta\colon M \otimes N \to Q\), где \(m \otimes n \mapsto \varphi(m, n)\). Это то, что
      требовалось.
  \end{pfparts}
\end{proof}

\begin{notation}
  \(M \otimes_R N\) --- тензорное умножение над кольцом \(R\).
\end{notation}

\begin{statement}
  \(R \otimes_R N \cong N\), где \((r, n) \mapsto rn\) --- \(R\) ``сократилось''.
\end{statement}
\begin{proof}
  \[
  \begin{tikzcd}
    R \times N \arrow[d] \arrow[r, "\varphi"] & Q \\
    N \arrow[ru, dashed, "\alpha"']
  \end{tikzcd}
  ,\] где \(\alpha(n) \coloneqq \varphi(1, n)\), если \(\alpha\) существует вовсе; \(\varphi\colon
  (r, n) \mapsto r(1, n)\) (оно может задаться только так); \(\varphi(1, n)\colon N \to Q\).

  \begin{itemize}
    \item \(\alpha\) линейно из-за линейности \(\varphi\) по второму аргументу;
    \item \(\alpha(rn) = \varphi(1, rn) = r\varphi(1, n) = \varphi(r, n)\).
  \end{itemize}
\end{proof}

В некотором смысле тензорное произведение коммутативно.
\begin{statement}
  \(M \otimes_R N \cong N \otimes_R M\), где \(m \otimes n \mapsto n \otimes m\).
\end{statement}
\begin{proof}
  Покажем, что \(M \otimes N\) обладает универсальным свойством для пары \(N, M\).

  \[
  \begin{tikzcd}
    N \times M \arrow[d] \arrow[r, "\varphi"] & Q \\
    M \otimes N
  \end{tikzcd}
  .\]

  Отображение \(N \times M \xrightarrow{(n, m) \mapsto m \otimes n} M \otimes N\) билинейно;
  рассмотрим \(\varphi'\colon M \times N \xrightarrow{(m, n) \mapsto \varphi(n, m)} Q\) --- оно тоже
  билинейно. Тогда, по универсальному свойству \(\exists! \alpha\colon M \otimes N \to Q\) такое,
  что \(\varphi(n, m) = \varphi'(m, n) = \alpha(m \otimes n)\).
\end{proof}

Ассоциативность тоже присутствует в некотором смысле (``в кавычках'', ведь модули не образуют
множества).

\begin{statement} \mnote{и упражнение}
  \((M \otimes N) \otimes P \cong M \otimes (N \otimes P)\), где \((m \otimes n) \otimes p \mapsto m
  \otimes (n \otimes p)\).
\end{statement}

\begin{remark}
  \(M \otimes N\) состоит из элементов не только вида \(m \otimes n\) (\textit{разложенный тензор}),
  но порождается ими (состоит из их линейных комбинацией).
\end{remark}

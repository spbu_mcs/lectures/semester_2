\chapter{Евклидовы пространства}

\lecture{1}{16.02.2024, 15:25}{}

\section{Скалярное произведение, длина}

Пусть \(X\) --- векторное пространство над \(\R\).

\begin{definition}
  Функция \(\inner{\cdot}{\cdot}\colon X \times X \to \R\) называется скалярным произведением на
  \(X\), если
  \begin{enumerate}
    \item \(\forall v \in X \quad \inner{v}{v} \ge 0\), причем \(\inner{v}{v} = 0 \iff v = 0\);
    \item \(\forall v, w \in X \quad \inner{v}{w} = \inner{w}{v}\);
    \item \(\forall v, w, z \in X \quad \inner{v + w}{z} = \inner{v}{z} + \inner{w}{z}\) и
      \(\forall \alpha \in \R \quad \inner{\alpha v}{w} = \alpha\inner{v}{w}\).
  \end{enumerate}
\end{definition}

\begin{example}
  В \(\R^n\), \(\inner{x}{y} = \sum_{i = 1}^n x_iy_i\).
\end{example}

\begin{example}
  Если \(V\) --- подпространство \(X\), то \(\inner{\cdot}{\cdot}\restriction_{V}\) --- скалярное
  произведение на \(V\).
\end{example}

\begin{example}
  Если \(X = C[0, 1]\), то, например, \(\inner{f}{g} = \int_0^1 f(x)g(x) dx\).
\end{example}

\begin{definition}
  Пара \((X, \inner{\cdot}{\cdot})\) называется евклидовым пространством.
\end{definition}

В разных источниках скалярное произведение обозначается по-разному: \((v, w)\), \(v \cdot w\) или
\(wv\) и так далее.

Пусть \(v \in X\).

\begin{definition}
  Длина \(|v| \coloneqq \sqrt{\inner{v}{v}}\).
\end{definition}

\begin{definition}
  \(\forall v, w \in X \quad d(v, w) \coloneqq |v - w|\) --- расстояние.
\end{definition}

Чуть позже будет показано, что это и впрямь метрика.

Рассмотрим различные свойства.

\begin{statement}
  \(|v| > 0\) если \(v \neq 0\).
\end{statement}

\begin{statement}
  \(|\lambda v| = \lambda|v|\) (\(\lambda \in \R\)).
\end{statement}

\begin{statement}
  \(|-v| = |v|\).
\end{statement}

\begin{statement}
  Если в \(X\) задан базис \(v_1, \ldots, v_n\), \(v = \sum_i \alpha_i v_i\) и \(w = \sum_j \beta_j
  v_j\), то
  \[
    \inner{v}{w} = \sum_{i, j} \alpha_i\beta_j\inner{v_i}{v_j}
  .\]
\end{statement}

\begin{statement}
  \(|v \pm w|^2 = |v|^2 + |w|^2 \pm 2\inner{v}{w}\).
\end{statement}

Забегая вперед, свойство выше --- заготовка теоремы косинусов.

\begin{statement}
  \(|v + w|^2 + |v - w|^2 = 2(|v|^2 + |w|^2)\) (тождество параллелограмма).
\end{statement}

\begin{statement}
  \(\inner{v}{w} = \frac{|v + w|^2 - |v - w|^2}{4}\).
\end{statement}

Это свойство позволяет вычислять скалярное произведение через длины.

\begin{statement}
  Расстояние сохраняется при параллельном переносе: \(d(v + u, w + u) = d(v, w)\).
\end{statement}

\begin{theorem}[неравенство Коши-Буняковского-Шварца]
  \hfill
  \begin{enumerate}
    \item \(\forall v, w \in X \quad |\inner{v}{w}| \le |v||w|\);
    \item Равенство достигается тогда и только тогда, когда \(v\) и \(w\) линейно зависимы.
  \end{enumerate}
\end{theorem}
\begin{proof}
  Рассмотрим несколько случаев того, как устроены векторы \(v\) и \(w\):
  \begin{pfparts}
    \item[\(v = 0\)] \(\inner{0}{w} = 0\), \(|0| = 0\);
    \item[\(v \neq 0, w \neq 0\)] Если они линейно зависимы, то \(\exists \lambda \in \R\colon v =
      \lambda w\). Тогда, по свойству, получится равенство \(|\inner{v}{w}| = |v||w|\).

      Иначе, можно взять такое \(\lambda\), что \(|v - \lambda w| > 0\). Расписывая скалярное
      произведение (длину в квадрате), получится, что \(|v|^2 + \lambda^2|w|^2 - 2\lambda\inner{v}{
      w} > 0\), а значит, \(D = |\inner{v}{w}|^2 - |v|^2|w|^2 < 0\).
  \end{pfparts}
\end{proof}

\begin{remark}
  Если \(v, w\) сонаправлены, то \(\inner{v}{w} = |v||w|\); если они противоположно направленны, то
  \(\inner{v}{w} = -|v||w|\).
\end{remark}

Выведем несколько следствий из КБШ.

\begin{consequence}
  \(\forall v, w \in X \quad |v + w| \le |v| + |w|\), причем равенство достигается тогда и только
  тогда, когда один из векторов нулевой или векторы сонаправлены.
\end{consequence}
\begin{proof}
  \[
  |v + w|^2 = |v|^2 + |w|^2 + 2\inner{v}{w} \le |v|^2 + |w|^2 + 2|\inner{v}{w}| \le |v|^2 + |w|^2
  + 2|v||w| = (|v| + |w|)^2
  .\]
\end{proof}

\begin{consequence}
  \(\forall v, w, u \in X \quad d(v, u) \le d(v, w) + d(w, u)\), причем равенство достигается тогда
  и только тогда, когда векторы \(v - u\) и \(w - u\) сонаправлены или один из них равен нулю.
\end{consequence}

\section{Углы}

Пусть \(X\) --- евклидово пространство над \(\R\).

\begin{definition}
  Углом между ненулевыми \(v, w \in X\) называется число \(\angle(v, w) \coloneqq
  \arccos\frac{\inner{v}{w}}{|v||w|}\).
\end{definition}

\begin{statement}
  Такое определение корректно.
\end{statement}
\begin{proof}
  По неравенство КШБ (область определения).
\end{proof}

\begin{statement}
  \(\angle(v, w) \in [0, \pi]\).
\end{statement}

\begin{statement}
  \(\angle(v, \lambda w) = \begin{cases}
    \angle(v, w), &\lambda > 0 \\
    \pi - \angle(v, w), &\lambda < 0
  \end{cases}\)
\end{statement}

По-другому говоря, \(\inner{v}{w} = |v||w|\cos\angle(v, w)\).

\begin{statement}[теорема косинусов]
  \(|v - w|^2 = |v|^2 + |w|^2 - 2|v||w|\cos\angle(v, w)\).
\end{statement}

\begin{theorem}[неравенство треугольника для углов]
  \[
  \forall x, y, z \quad \angle(x, z) \le \angle(x, y) + \angle(y, z)
  \] (\(x\), \(y\) и \(z\) ненулевые).
\end{theorem}
\begin{proof}
  Пусть \(\angle(x, y) = \alpha\) и \(\angle(y, z) = \beta\); можно считать, что \(\alpha + \beta <
  \pi\), ведь иначе неравенство выполняется.

  Изобразим тройку векторов \((x, y, z)\) в \(X\) произвольным образом (не обязательно в плоскости).
  Построим вектора \((x', y', z')\) в плоскости \(\R^2\):
  \begin{enumerate}
    \item \(|x| = |x'|\);
    \item \(|z| = |z'|\) и \(\angle(x', z') = \alpha + \beta\);
    \item конец вектора \(u'\) лежит на векторе \(z' - x'\) так, что \(\angle(x', u') = \alpha\).
  \end{enumerate}

  \begin{figure}[ht]
    \centering

    \hspace*{\fill}
    \begin{subfigure}[b]{.35\textwidth}
      \incfig{angle_triangle_inequality_X}
      \caption{\((x, y, z)\) в \(X\)}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{.35\textwidth}
      \incfig{angle_triangle_inequality_R2}
      \caption{\((x', y', z')\) в \(\R^2\) }
    \end{subfigure}
    \hspace*{\fill}
  \end{figure}

  Вернемся к \(X\): построим вектор \(u \collin y\) такой, что \(|u| = |u'|\). Тогда:
  \begin{enumerate}
    \item \(|x - u| = |x' - u'|\) по теореме косинусов;
    \item \(|u - z| = |u' - z'|\) по теореме косинусов;
    \item \(|x - z| \le |x - u| + |u - z| = |x' - u'| + |u' - z'| = |x' - z'|\) по неравенству
      треугольника для длин;
    \item \(\cos\angle(x, z) = \frac{|x|^2 + |z|^2 - |x - z|^2}{2|x||z|}\), \(\cos\angle(x', z') =
      \frac{|x'|^2 + |z'|^2 - |x' - z'|^2}{2|x'||z'|}\), но тогда \(\cos\angle(x, z) \ge
      \cos\angle(x', z') \iff \angle(x, z) \le \angle(x', z') = \alpha + \beta\).
  \end{enumerate}
\end{proof}

\begin{exercise}
  Проверить, когда \(\angle(x, z) = \angle(x, y) + \angle(y, z)\).
\end{exercise}

\begin{consequence}
  На сфере \(S \coloneqq \left\{ x \in X \mid |x| = 1 \right\}\) можно ввести метрику
  \(d_{\angle}(x, y) \coloneqq  \angle(x, y)\).
\end{consequence}

\begin{consequence}
  \(\forall x, y, z \in X \quad \angle(x, y) + \angle(x, z) + \angle(y, z) \le 2\pi\).
\end{consequence}
\begin{proof}
  Неравенство треугольника для углов \(x\), \(-y\), \(z\).
\end{proof}

\section{Ортогональные векторы}

Пусть \(X\) --- евклидово пространство.

\begin{definition}
  Векторы \(x, y \in X\) называются ортогональными, если \(\inner{x}{y} = 0\).
\end{definition}

\begin{notation}
  \(x \perp y\).
\end{notation}

\begin{statement}
  \(\forall x \quad 0 \perp x\).
\end{statement}

\begin{statement}
  Если \(x\) ортононален каждому из \(v_1, \ldots, v_k\), то \(\forall y \in \lin(v_1, \ldots, v_k)
  \quad x \perp y\).
\end{statement}

\begin{statement}
  \(x \perp y \implies  |x + y|^2 = |x|^2 + |y|^2\) --- теорема Пифагора.
\end{statement}

В более общем виде теорему Пифагора можно переформулировать как
\begin{statement}
  Если  \(v_1, \ldots, v_k\) попарно ортогональны, то \(\left| \sum_{i = 1}^k v_i \right|^2 =
  \sum_{i = 1}^k |v_i|^2\).
\end{statement}

\begin{definition}
  Набор \(v_1, \ldots, v_k \in X\) называется ортогональным, если \(\forall i, j \quad v_i \perp
  v_j\) и набор ортонормирован.
\end{definition}

Пусть \(v_1, \ldots, v_n\) --- ортонормированный базис.
\begin{statement}
  Если \(x = \sum \alpha_i v_i\) и \(y = \sum \beta_j v_j\), то \(\inner{x}{y} = \sum_i
  \alpha_i\beta_i\), а \(|x|^2 = \sum \alpha_i^2\).
\end{statement}

\begin{theorem}
  Любой ортогональный набор ненулевых векторов линейно независим.
\end{theorem}

Рассмотрим процесс ортогонализации по \textit{Граму-Шмидту}.

\begin{theorem}
  Для любого линейно независимого набора векторов \(v_1, \ldots, v_n\) существует и единственен
  ортонормированный набор векторов \(e_1, \ldots, e_n\) такой, что \(\forall k = 1, \ldots, n \quad
  \lin(v_1, \ldots, v_k) = \lin(e_1, \ldots, e_k)\) и \(\inner{v_k}{e_k} > 0\).
\end{theorem}

\begin{minipage}{.6\textwidth}
  Немного скажем об интуиции для условий, накладываемых на \(e_1, \ldots, e_n\). Рассмотрим векторы
  \(v_1, v_2 \in \R^2\): понятно, что \(e_1\) выбирается однозначно как \(e_1 = \frac{v_1}{|v_1|}\).

  Как теперь выбрать \(e_2\)? Он должен быть перпендикулярным \(v_1\) (\(e_1\)), быть единичной
  длины --- значит, есть только два возможных направления \(e_2\): и второе условие указывает, какое
  именно.
\end{minipage}
\hfill
\begin{minipage}{.35\textwidth}
  \centering
  \includegraphics[width=\textwidth]{gram_schmidt_intuition}
\end{minipage}
\hfill

Перейдем к доказательству.

\begin{proof}
  По индукции построим искомый набор \(e\).
  \begin{pfparts}
    \item[База] \(e_1 = \frac{v_1}{|v_1|}\).

    \item[Шаг индукции] Пусть \(e_1 \ldots, e_{k - 1}\) уже построены. Сделаем ``дополнительное
      построение'': положим, что \(w_k \coloneqq  v_k + \sum_i \lambda_i e_i\), где \(\lambda_i\)
      --- просто некоторые коэффициенты.

      Хочется, чтобы \(\forall i = 1, \ldots, k - 1 \quad w_k \perp e_i\): значит,
      \(\inner{w_k}{e_i} = \inner{v_k}{e_i} + \lambda_i = 0 \iff \lambda_i = -\inner{v_k}{e_i}\).

      Будем утверждать, что искомый \(e_k = \frac{w_k}{|w_k|}\). Проверим это:
      \begin{enumerate}
        \item \(\lin(e_1, \ldots, e_k) = \lin(e_1, \ldots, e_{k - 1}, w_k) = \lin(e_1, \ldots., e_{k
        - 1}, v_k) = \lin(v_1, \ldots, v_{k - 1}, v_k)\) по индукционному предположению;

        \item Раз \(v_k = w_k + \sum_i \underbrace{\inner{v_k}{e_i}}_{\lambda}e_i\), то
          \(\inner{v_k}{e_k} = \inner{w_k}{e_k} = \frac{1}{|w_k|}\inner{w_k}{w_k} > 0\).
      \end{enumerate}
  \end{pfparts}

  Набор \(e\) построен; осталось проверить единственность --- сделаем это опять по индукции.

  \(e_1\) выбирается однозначно; \(e_k = \alpha v_k + \sum_i \alpha_i e_i\) и \(\forall j = 1,
  \ldots, k - 1 \quad \alpha\inner{v_k}{e_j} + \alpha_j = 0\) (рассматривая \(\inner{e_k}{e_j}\)).

  Таких уравнений \(k - 1\) штука, а неизвестных ровно \(k\) --- значит, все коэффициенты
  пропорциональны; набор \(\left\{ \alpha_i \right\}_{i = 1}^{k - 1}\) определен с точность до
  умножения на скаляр.
\end{proof}

\begin{consequence}
  Если \(X\) --- конечномерное ЕП, то
  \begin{enumerate}
    \item Существует ортонормированный базис (достаточно ``исправить'' какой-нибудь базис);
    \item Любой ортономированный набор векторов можно дополнить до ортонормированного базиса
      (дополнив и исправив).
  \end{enumerate}
\end{consequence}

\section{Изоморфизм}

\begin{definition}
  Евклидовы пространства \(X\) и \(Y\) изоморфны (\(X \cong Y\)), если существует линейная биекция
  \(f\), сохраняющая скалярное произведение: \(\forall v, w \in X \quad \inner{f(v)}{f(w)}_Y =
  \inner{v}{w}_X\).
\end{definition}

Понятно, что сохраняются также размерности, углы и прочие.

\begin{theorem}
  Если \(X\) и \(Y\) --- ЕП, а \(\dim{X} = \dim{Y} < \infty\), то \(X \cong Y\).
\end{theorem}
\begin{proof}
  Как в подобной теореме из линейной алгебры, выберем \(\left\{ e_1, \ldots, e_n \right\}\) и
  \(\left\{ v_1, \ldots, v_n \right\}\) --- ортонормированные базисы в \(X\) и \(Y\).

  Построим искомый изоморфизм:
  \begin{enumerate}
    \item \(\forall i \quad f(e_i) = v_i\);
    \item По линейности предыдущее условие распространяется на всё \(X\);
    \item Скалярное произведение, очевидно, сохраняется.
  \end{enumerate}
\end{proof}

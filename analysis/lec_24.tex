\lecture{24}{25.05.2024, 11:15}{Собственные интегралы, дифференцирование функций, зависящих от
параметра}

Пусть \(I_1 \coloneqq [a, b]\), \(I_2 \coloneqq [c, d]\), где \(a, b, c, d \in \R\). Будем
рассматривать \(E \coloneqq I_1 \times I_2\).

\begin{remark}
  \(E\) --- компакт.
\end{remark}

Пусть \(f\colon E \to \R\), \(f \in C(E)\).

\begin{enumerate}
  \item Как себя ведет \(\int_a^b f(x, y) \df{x}\)?
  \item Когда можно сказать, что \(F'(y) = \int_a^b \frac{\partial}{\partial y} f(x, y) \df{x}\),
    где \(F(y) \coloneqq \int_a^b f(x, y) \df{x}\)?
  \item Когда \(\int_c^d \left( \int_a^b f(x, y) \df{x} \right) \df{y} = \int_a^b \left( \int_c^d
    f(x, y) \df{y} \right) \df{x}\)?
\end{enumerate}

\begin{remark}
  Функция от двух переменных с одной фиксированной будет интегрируемой на \(I\), ведь
  рассматривается сужение.
\end{remark}

Пусть \(\varphi_1, \varphi_2\colon [c, d] \to [a, b]\) --- непрерывные функции.

\begin{theorem}
  Функция \(F(y) \coloneqq \int_{\varphi_1(y)}^{\varphi_2(y)} f(x, y) \df{x}\) непрерывна на \([c,
  d]\).
\end{theorem}
\begin{proof}
  Зафиксируем \(y_0 \in [c, d]\) и рассмотрим \(\forall \varepsilon > 0\): нужно такое \(\delta >
  0\), что \[
    |y - y_0| < \delta \implies |F(y) - F(y_0)| < \varepsilon
  .\]

  Распишем: \[
  \begin{aligned}
    -F(y) + F(y_0) &= -\int_{\varphi_1(y)}^{\varphi_2(y)} f(x, y) \df{x} +
    \int_{\varphi_1(y_0)}^{\varphi_2(y_0)} f(x, y_0) \df{x} \\
    &= \underbrace{\int_{\varphi_1(y_0)}^{\varphi_2(y_0)} f(x, y_0) \df{x} -
    \int_{\varphi_1(y_0)}^{\varphi_2(y_0)} f(x, y) \df{x}}_I +
    \underbrace{\int_{\varphi_1(y_0)}^{\varphi_2(y_0)} f(x, y) \df{x} -
    \int_{\varphi_1(y)}^{\varphi_2(y)} f(x, y) \df{x}}_{II}.
  \end{aligned}
  \]

  \mnote{порядок верхних и нижних пределов интегралов не важен, ведь потом это будет в модулях}
  Рассмотрим сначала группу \(II\): \[
  \begin{aligned}
    \ldots &= \int_{\varphi_1(y_0)}^{\varphi_1(y)} f(x, y) \df{x} +
    \int_{\varphi_2(y)}^{\varphi_2(y_0)} f(x, y) \df{x}.
  \end{aligned}
  \]

  Оценим, например, первый (из последнего равенства) интеграл.

  \begin{itemize}
    \item Как известно, \(f\) непрерывна на компакте, а значит, достигает там наибольшего и
      наименьшего значения, и в частности, она ограничена: \(\exists m, M\colon m \le f \le M\).
    \item Функции \(\varphi_1, \varphi_2\) непрерывны в том числе в \(y_0\), то есть \(\exists
      \delta_1 > 0\colon |y - y_0| < \delta_1 \implies \left| \varphi_1(y) - \varphi_1(y_0) \right|,
      \left| \varphi_2(y) - \varphi_2(y_0) \right| < \frac{\varepsilon}{4} \frac{1}{\max\left( |m|,
      |M| \right)}\).
  \end{itemize}

  Тогда, \[
  \begin{aligned}
    \left| \int_{\varphi_1(y_0)}^{\varphi_1(y)} f(x, y) \df{x} \right| &\le \left|
    \int_{\varphi_1(y_0)}^{\varphi_1(y)} |f(x, y)| \df{x} \right| \le \max\left( |m|, |M| \right)
    \left| \varphi_1(y) - \varphi_1(y_0) \right| \\
    &< \frac{\varepsilon}{4}, \quad |y - y_0| < \delta_1.
  \end{aligned}
  \]

  Со вторым интегралом точно также.

  Рассмотрим теперь группу \(I\). Так как пределы интегрирования одни и те же, то \[
  \begin{aligned}
    |I| &= \left| \int_{\varphi_1(y_0)}^{\varphi_2(y_0)} \left( f(x, y_0) - f(x, y) \right) \df{x}
    \right| \le \left| \int_{\varphi_1(y_0)}^{\varphi_2(y_0)} \left| f(x, y_0) - f(x, y) \right|
    \df{x} \right|.
  \end{aligned}
  \]

  \mnote{\(\rho_2\) --- евклидова метрика на плоскости}
  Раз \(f\) непрерывна на компакте, то она равномерно непрерывна на нем: \[
    \exists \delta_2\colon \rho_2\left( (s, t), (s', t') \right) < \delta_2 \implies \left| f(s, t)
    - f(s', t') \right| < \frac{\varepsilon}{2(b - a)},
  \] и тогда \[
    \left| f(x, y_0) - f(x, y) \right| < \frac{\varepsilon}{2(b - a)}, \quad |y - y_0| < \delta_2.
  \]

  Получается, что \[
  \begin{aligned}
    \left| \int_{\varphi_1(y_0)}^{\varphi_2(y_0)} \left| f(x, y_0) - f(x, y) \right| \df{x} \right|
    \le \frac{\varepsilon}{2},
  \end{aligned}
  \] ведь \([\varphi_1(y_0), \varphi_2(y_0)] \subseteq [a, b]\).
\end{proof}

Рассмотрим функцию \(\psi_y\colon [a, b] \to \R\), где \(\psi_y(x) \coloneqq f(x, y)\). В процессе
доказательства предыдущей теоремы было полученное следующее
\begin{statement}
  Если \(f\) непрерывна на \(E\), то \(\psi_y \unito \psi_{y_0}\) при \(y \to y_0\).
\end{statement}

\begin{consequence}
  Если \(\varphi_1 \equiv a\) и \(\varphi_2 \equiv b\), то \(\int_a^b f(x, y) \df{x}\) непрерывна по
  \(y\) на \([c, d]\).
\end{consequence}

\(f, \varphi_1, \varphi_2\) остаются такими же; дополнительно, пусть \(\forall x \in I_1 \exists
\frac{\partial}{\partial y} f(x, y)\), непрерывная на \(E\), и \(\varphi_1, \varphi_2\)
дифференцируемы на \(I_2\).

\begin{theorem}
  Для \(\forall y \in I_2\) верно \[
    \left( \int_{\varphi_1(y)}^{\varphi_2(y)} f(x, y) \df{x} \right)'_y =
    \int_{\varphi_1(y)}^{\varphi_2(y)} \frac{\partial}{\partial y} f(x, y) \df{x} + f(\varphi_2(y),
    y) \varphi_2'(y) - f(\varphi_1(y), y) \varphi_1'(y).
  \]
\end{theorem}
\begin{proof}
  Проверим это равенство для фиксированного \(y_0 \in I_2\).

  Левая часть есть ни что иное, как \[
  \begin{aligned}
    \lim_{h \to 0} \frac{\int_{\varphi_1(y_0 + h)}^{\varphi_2(y_0 + h)} f(x, y_0 + h) \df{x} -
    \int_{\varphi_1(y_0)}^{\varphi_2(y_0)} f(x, y_0) \df{x}}{y_0 + h - y_0};
  \end{aligned}
  \] подразумевается, что \(y_0 + h \in I_2\) (иначе рассматривается односторонняя производная).

  \[
  \begin{aligned}
    & \int_{\varphi_1(y_0 + h)}^{\varphi_2(y_0 + h)} f(x, y_0 + h) \df{x} -
      \int_{\varphi_1(y_0)}^{\varphi_2(y_0)} f(x, y_0) \df{x} \\
    = & \int_{\varphi_1(y_0 + h)}^{\varphi_2(y_0 + h)} \left( f(x, y_0 + h) - f(x, y_0) \right)
      \df{x} + \int_{\varphi_1(y_0 + h)}^{\varphi_2(y_0 + h)} f(x, y_0) \df{x} - \int_{\varphi_1(y_0
      )}^{\varphi_2(y_0)} f(x, y_0) \df{x}  \\
    = & \underbrace{\int_{\varphi_1(y_0 + h)}^{\varphi_2(y_0 + h)} \left( f(x, y_0 + h) - f(x, y_0)
      \right) \df{x}}_{I} + \underbrace{\int_{\varphi_1(y_0 + h)}^{\varphi_1(y_0)} f(x, y_0)
      \df{x}}_{II} + \underbrace{\int_{\varphi_2(y_0)}^{\varphi_2(y_0 + h)} f(x, y_0) \df{x}}_{III}.
  \end{aligned}
  \]

  \begin{enumerate}[label=\Roman*.]
    \item Из-за условия на частные производные \(f\), можно применить теорему Лагранжа для
      подынтегральной функции: \[
      \begin{aligned}
        f(x, y_0 + h) - f(x, y_0) &= \frac{\partial}{\partial y} f(x, y_0 + \Theta_x) h, \quad
        \Theta_x \in (y, y_0 + h).
      \end{aligned}
      \]

      Тогда \[
      \begin{aligned}
        \int_{\varphi_1(y_0 + h)}^{\varphi_2(y_0 + h)} \left( f(x, y_0 + h) - f(x, y_0) \right)
        \df{x} &= h \int_{\varphi_1(y_0 + h)}^{\varphi_2(y_0 + h)} \frac{\partial}{\partial y} f(x,
        y_0 + \Theta_x) \df{x};
      \end{aligned}
      \] такой интеграл всё еще существует, ведь \(\Theta_x\) можно выбрать непрерывной по \(x\) по
      непрерывности частной производной. \mnote{ниже в замечании подробнее об этом}

      Продолжим: \[
      \begin{aligned}
        \ldots &= h \left( \int_{\varphi_1(y_0)}^{\varphi_2(y_0)} \frac{\partial}{\partial y}f(x,
        y_0 + \Theta_x) \df{x} + \int_{\varphi_1(y_0 + h)}^{\varphi_2(y_0)} \frac{\partial}{\partial
      y} f(x, y_0 + h) \df{x} + \int_{\varphi_2(y_0)}^{\varphi_2(y_0 + h)} \frac{\partial}{\partial
    y} f(x, y_0 + \Theta_x) \df{x} \right).
      \end{aligned}
      \]

      Раз \(h \to 0\), то последние \(2\) интеграла тоже \(\to 0\).

      Промассажируем отдельно первый интеграл из последнего равенства: \[
      \begin{aligned}
        & \int_{\varphi_1(y_0)}^{\varphi_2(y_0)} \frac{\partial}{\partial y}f(x, y_0 + \Theta_x)
        \df{x} = \\
        =& \int_{\varphi_1(y_0)}^{\varphi_2(y_0)} \frac{\partial}{\partial y} f(x, y_0) \df{x} +
        \int_{\varphi_1(y_0)}^{\varphi_2(y_0)} \left( \frac{\partial}{\partial y} f(x, y_0 +
        \Theta_x) - \frac{\partial}{\partial y} f(x, y_0) \right) \df{x},
      \end{aligned}
      \] --- второй интеграл будет стремиться к \(0\) при \(h \to 0\), ведь
      \(\frac{\partial}{\partial y} f(x, y_0 + \Theta_x) \unito \frac{\partial}{\partial y} f(x,
      y_0)\), так как \(\left| \Theta_x \right| \le |h|\).

      Итого получилось, что \[
        I \xrightarrow{h \to 0} \int_{\varphi_1(y_0)}^{\varphi_2(y_0)} \frac{\partial}{\partial
        y} f(x, y) \df{x}.
      \]

    \item Надо найти \(\lim_{h \to 0} \frac{1}{h} \int_{\varphi_1(y_0 + h)}^{\varphi_1(y_0)} f(x,
      y_0) \df{x}\).

      Раз функция \(f\) непрерывна на прямоугольнике, а \(\varphi_1\) --- в точке \(y_0\), поэтому
      \(f(x, y_0) = f(\varphi_1(y_0), y_0) + \smallO(1)\) при \(|x - \varphi_1(y_0)| \to 0\).

      \footnote{с этого момента звук в записи прерывается, и, как следствие, содержательность
      этих записей тоже}
      Тогда \[
      \begin{aligned}
        \ldots &= \lim_{h \to 0} \frac{1}{h} \left( f(\varphi_1(y_0), y_0) \left( \varphi_1(y_0) -
        \varphi_1(y_0 + h) \right) + \int_{\varphi_1(y_0 + h)}^{\varphi_1(y_0)} \smallO(1) \df{x}
        \right) \\
        &= -f(\varphi_1(y_0), y_0) \varphi_1'(y_0) + 0.
      \end{aligned}
      \]

    \item Аналогично.
  \end{enumerate}
\end{proof}

\begin{consequence}
  Если \(\varphi_1 \equiv a\) и \(\varphi_2 \equiv b\), то \(\left( \int_a^b f(x, y) \df{x}
  \right)'_y = \int_a^b \frac{\partial}{\partial y} f(x, y) \df{x}\).
\end{consequence}

\begin{remark}[из доказательства]
  \(\forall x \quad f(x, y_0 + h) - f(x, y_0) = f'_y(x, y_0 + \Theta_x)h\), где \(\Theta_x\) ---
  непрерывная функция от переменной \(x\).

  Возьмем такой \(x'\), что \(|x - x'| < \delta\). ...
\end{remark}

Всё остается так же: \(f \in C(E)\).

\begin{theorem}
  \[
    \int_a^b \left( \int_c^d f(x, y) \df{y} \right) \df{x} = \int_c^d \left( \int_a^b f(x, y) \df{x}
    \right) \df{y}.
  \]
\end{theorem}
\begin{proof}
  ...
\end{proof}

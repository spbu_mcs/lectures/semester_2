\lecture{12}{25.03.2024, 11:15}{}

...

Пусть \(\mathcal{D}\) открыто в \(\R^2\); \(f\colon \mathcal{D} \to \R\).

\begin{theorem}[о перестановке порядка дифференцирования]
  Если существуют \(\frac{\partial^2 \varphi}{\partial x \partial y}(x, y)\) и \(\frac{\partial^2
  \varphi}{\partial y \partial x}(x, y)\), причем они непрерывны в точке \(\begin{pmatrix} x^0 \\
  y^0 \end{pmatrix} \), то \(\frac{\partial^2 \varphi}{\partial y \partial x}(x^0, y^0) =
  \frac{\partial^2 \varphi}{\partial x \partial y}(x^0, y^0)\).
\end{theorem}
\begin{proof}
  Введем операторы \(\Delta_1^h f(x, y) \coloneqq f(x + h, y) - f(x, y)\) и \(\Delta_2^h f(x, y)
  \coloneqq f(x, y + h) - f(x, y)\) (\(h \in \R\)).

  Рассмотрим точку \(\begin{pmatrix} x^0 \\ y^0 \end{pmatrix} \): \[
  \begin{aligned}
    \Delta_2^h\Delta_1^h f(x^0, y^0) &= \Delta_2^h\left( f(x^0 + h, y^0) - f(x^0, y^0) \right) \\
    &\oversym{=}{\text{т. Л-жа}} h\Delta_2^h \frac{\partial f}{\partial x}\left( x^0 + \Theta_1h,
    y^0 \right) = h\frac{\partial f}{\partial x}\left( x^0 + \Theta_1h, y^0 + h\right) -
    h\frac{\partial f}{\partial x}\left( x^0 + \Theta_1h, y^0 \right) \\
    &\oversym{=}{\text{т. Л-жа}} h^2\frac{\partial^2 f}{\partial x \partial y}\left( x^0 +
    \Theta_1h, y^0 + \Theta_2h \right),
  \end{aligned}
  \] где \(\Theta\), как и до этого, \(\in (0, 1)\).

  Аналогичным образом, \[
    \Delta_1^h\Delta_2^h f(x^0, y^0) = h^2 \frac{\partial^2 f}{\partial y \partial x}\left( x^0 +
    \Theta_4h, y^0 + \Theta_3h \right).
  \]

  Как видно, операторы \(\Delta\) коммутируют.

  Из этих двух полученных равенств, \[
    \frac{\partial^2 f}{\partial x \partial y}\left(x^0 + \Theta_1h, y^0 + \Theta_2h\right) =
    \frac{\partial^2 f}{\partial y \partial x}\left( x^0 + \Theta_3h, y^0 + \Theta_4h \right).
  \]

  В силу непрерывности частных производных, устремляя \(h \to 0\) получается искомый результат.
\end{proof}

Пусть теперь \(\mathcal{D} \subset \R^n\) открыто; \(f\colon \mathcal{D} \to \R\).

\begin{definition}
  \(f \in C^1\left( \mathcal{D} \right) \iff \forall i = 1, \ldots, n \quad D_i f(x)\) непрерывны.
\end{definition}

\begin{definition}
  \(f \in C^2\left( \mathcal{D} \right) \iff \forall i, j = 1, \ldots, n \quad D_{ij}f(x)\)
  существуют и непрерывны.
\end{definition}

\begin{statement}
  Если \(f \in C^2\left( \mathcal{D} \right)\), то \(\forall i = 1, \ldots n \quad D_if \in
  C^1\left( \mathcal{D} \right)\).
\end{statement}

\begin{definition}
  Если \(f \in C^2\left( \mathcal{D} \right)\), то матрица Гессе в точке \(x \in \mathcal{D}\) \(H
  \coloneqq \left\{ D_{ij}^2 f(x) \right\}_{i, j = 1}^n\).
\end{definition}

\begin{statement}
  \(H = H^\top\).
\end{statement}

\begin{definition}
  Оператор Лапласа \(\Delta f \coloneqq \tr{H}\).
\end{definition}

\begin{example}
  Вспомним пример с направляющими косинусами из прошлой лекции: оказывается, что и \[
    \frac{\partial^2 u}{\partial l_1^2} + \frac{\partial^2 u}{\partial l_2^2} + \frac{\partial^2
    u}{\partial l_3^2} = \frac{\partial^2 u}{\partial x^2} + \frac{\partial^2 u}{\partial y^2} +
    \frac{\partial^2 u}{\partial z^2}
  ,\] --- лапласиан не меняется вовсе.

  Доказывается методами линейной алгебры; и, конечно же, это верно и для \(\R^n\).
\end{example}

\begin{definition} \mnote{вторая квадратичная форма}
  Дифференциал второго порядка \(\df^2{f(x, h)} \coloneqq \sum_{i, j = 1}^n D_{ij}^2 f(x)h_ih_j\).
\end{definition}

Мы этот дифференциал определяем для класса \(C^2\left( \mathcal{D} \right)\), хотя его можно
определить и для всех функций --- тогда он перестанет быть симметричным. 

\begin{definition}
  Частные производные \(D_{i_1i_2 \ldots i_r}^r f(x) \coloneqq D_{i_r}D_{i_1i_2 \ldots i_{r - 1}}^{r
  - 1}f(x)\).
\end{definition}

\begin{statement}
  Если \(\left\{ i_1, \ldots, i_r \right\}\) --- перестановка \(\left\{ j_1, \ldots, j_r \right\}\),
  то \(D_{i_1 \ldots i_r}^r f(x) = D_{j_1 \ldots j_r}^r f(x)\) (\(f \in C^r\left( \mathcal{D}
  \right)\)).
\end{statement}

\begin{definition}
  Дифференциал порядка \(m\) --- \(\df^m{f(x, h)} \coloneqq \sum_{i_1 = 1}^n \ldots \sum_{i_m = 1}^n
  D_{i_1 \ldots i_m}^m f(x)h_{i_1}\ldots h_{i_n}\) (\(h \in \R^n\)).
\end{definition}

По-другому говоря, за счет возможности перестановки, \[
\begin{aligned}
  \df^m{f(x, h)} &= \sum_{i_1, \ldots, i_m = 1}^n \frac{\partial^m}{\partial x_{i_1} \ldots \partial
  x_{i_m}} f(x)h_{i_1} \ldots h_{i_m} = \left( \sum_{i = 1}^n h_i \frac{\partial}{\partial x_i}
  \right)^m f(x) \\
  &= \sum_{|k| = m} \frac{m!}{k!} f^{(k)}(x)h^k, \quad k = (k_1, \ldots, k_n).
\end{aligned}
\]

\begin{theorem}[формула Тейлора-Лагранжа]
  Если \(f \in C^{r + 1}\left( \mathcal{D} \right)\), \([x, x + h] \in \mathcal{D}\), то  \[
  \begin{aligned}
    f(x + h) &= \sum_{m = 0}^r \frac{\df^m{f(x, h)}}{m!} + \frac{\df^{r + 1}{f(x + \Theta h, h)}}{(r +
    1)!}
    \\
    &= \sum_{|k| \le r} \frac{f^{(k)}(x)}{k!}h^k + \sum_{|k| = r + 1} \frac{f^{(k)}(x + \Theta
    h)}{k!}h^k,
  \end{aligned}
  \] где \(\Theta \in (0, 1)\).
\end{theorem}

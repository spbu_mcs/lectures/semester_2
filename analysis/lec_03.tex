\lecture{3}{19.02.2024, 11:15}{}

Напоминание:
\begin{definition}
  \(\gamma\) непрерывна в точке \(t_0 \) в \(\R^m\) тогда и только тогда, когда \[
  \forall \varepsilon > 0 \exists \delta\colon |t - t_0| < \delta \implies |\gamma(t) - \gamma(t_0)|
  < \varepsilon
  .\]
\end{definition}

Где \(\gamma = \gamma(t) = \left( \gamma_1(t), \ldots, \gamma_m(t) \right)\), \(|\gamma(t) -
\gamma(t_0)| = \sqrt{\sum_{j = 1}^m(\gamma_j(t) - \gamma_j(t_0))^2}\).

Понятно также, что
\begin{statement}
  \(\gamma\) непрерывна тогда и только тогда, когда непрерывны все ее компоненты.
\end{statement}

\begin{definition}
  Путем в \(\R^m\) называется непрерывное отображение \(\gamma\colon [a, b] \to \R^m\) (\([a, b]
  \subset \R^1\)).
\end{definition}

Обозначать будем так: \(\gamma = \gamma(t)\), \(t \in [a, b]\).

\begin{definition}
  Путь \(\gamma^- \coloneqq \gamma(a + b - t)\) (\(t \in [a, b]\)) называется противоположным
  \(\gamma\).
\end{definition}

Интуитивно, противоположный путь меняет ориентацию пути (``стрелочки'').

\begin{definition}
  Пути \(\gamma(t)\) и \(\tilde{\gamma}(\tau)\) \((t \in [a, b]\), \(\tau \in [c, d]\)) называются
  эквивалентными (\(\gamma \sim \tilde{\gamma}\)), если существует непрерывная возрастающая биекция
  \(t = t(\tau)\colon [c, d] \to [a, b]\) такая, что \(\tilde{\gamma}(\tau) = \gamma(t(\tau))\).
\end{definition}

Если откинуть условие на монотонность, то ориентация не будет сохраняться.

А корректно ли такое определение?

\begin{statement}
  \(\gamma_1 \sim \gamma_2 \implies \gamma_2 \sim \gamma_1\).
\end{statement}

\begin{statement}
  \(\gamma_1 \sim \gamma_2 \sim \gamma_3 \implies \gamma_1 \sim \gamma_3\).
\end{statement}

Доказательства предоставляются читателю.

\begin{definition}
  Кривая --- класс эквивалентностям путей.
\end{definition}

Углубимся в характеристики путей.

\begin{definition}
  Длина пути \(L_\gamma \coloneqq \sup_{\Delta_{[a, b]}} \sum \left| \gamma(t_k) - \gamma(t_{k - 1})
  \right|\).
\end{definition}

Понятно, что длина не обязательно конечна: например, кривая Ван дер Вардена.
\mlink{https://math.stackexchange.com/questions/187402/existence-of-an-infinite-length-path}{Example}

\begin{statement}
  Если \(\gamma \sim \tilde{\gamma}\), то \(L_\gamma = L_{\tilde{\gamma}}\) --- длина пути не
  зависит от параметризации.
\end{statement}

Предлагается подумать, как доказать это самому.

\begin{definition}
  Путь \(\gamma\) называется спрямляемым, если \(L_\gamma < +\infty\).
\end{definition}

\begin{statement}
  Все компоненты пути \(\gamma_j \in V[a, b]\) тогда и только тогда, когда путь \(\gamma\)
  спрямляем.
\end{statement}
\begin{proof}
  Распишем, что такое сумма в длине пути: \[
    \begin{aligned}
      \sum_{k = 1}^n \left| \gamma(t_k) - \gamma(t_{k - 1}) \right| &= \sum_{k = 1}^n \sqrt{\sum_{i =
      1}^m \left| \gamma_i(t_k) - \gamma_i(t_{k - 1}) \right|^2} \\
                                                                    &\le \sum_{k = 1}^n\sum_{i =
                                                                    1}^m \left| \gamma_i(t_k) -
                                                                    \gamma_i(t_{k - 1}) \right| \\
                                                                    &= \sum_{i = 1}^m\sum_{k = 1}^n
                                                                    \left| \gamma_i(t_k) -
                                                                    \gamma_i(t_{k - 1}) \right| 
                                                                    \le \sum_{i = 1}^m
                                                                    V_a^b(\gamma_i).
    \end{aligned}
  \]

  Ну а в обратную сторону тривиально.
\end{proof}

Удобно также рассматривать путь как непрерывную вектор-функцию.

\begin{definition}
  \(L_\gamma(t) \coloneqq V_a^t(\gamma)\) по всем разбиениям \(\Delta_{[a, \symbf{t}]}\).
\end{definition}

Будем считать, что \(\gamma\) --- это путь без задержки: \(L_\gamma(t)\) \textbf{строго} возрастает.

\begin{statement}
  \(L_\gamma(t)\) непрерывна.
\end{statement}

Надо повторить доказательство для \(V_a^x(f)\).

Оказывается, что если кривая спрямляема, то можно ввести натуральный параметр.

\begin{definition}
  Натуральный параметр (кривой) \(s \coloneqq L_\gamma(t)\).
\end{definition}

Такой параметр иногда называется естественным.

\begin{statement}
  \(s \in [0, L]\).
\end{statement}

Как его можно понимать? Рассмотрим \(\tilde{\gamma}(s) \coloneqq  \gamma\left( L_\gamma^{-1}(s)
\right)\) --- это строго возрастающая, непрерывная функция; \(L_{\tilde{\gamma}}(s) = s\), а
\(\tilde{\gamma} \sim \gamma\).

\begin{definition}
  Путь \(\gamma\) называется дифференцируемым, если \(\forall t \in [a, b]\) существует
  \(\gamma'(t)\) (покоординатно).
\end{definition}

``Настоящее'' определение производной будет дано через пару лекций.

\begin{definition}
  Путь \(\gamma\) называется гладким, если \(\gamma' \in C[a, b]\) (\(\iff \forall j \quad \gamma_j'
  \in C[a, b]\)).
\end{definition}

Гладкость не совсем подразумевает ``физическую'' гладкость: для нее надо еще требовать, чтобы
производная в точке не равнялась нулю.

\begin{example}
  Положим, что \[
    z(t) = e^{it} + \frac{e^{i2t}}{2} = (\cos{t} + i\sin{t}) + \frac{\cos{2t} + i\sin{2t}}{2} =
    \left( \cos{t} + \frac{\cos{2t}}{2}, \sin{t} + \frac{\sin{2t}}{2} \right)
  .\]

  По определению такой путь гладкий (триг. функции гладкие), но в точке \(-\frac{1}{2}\) гладкость в
  физическом понимании нарушается.
  \begin{figure}[ht]
    \centering
    \includegraphics[width=.35\textwidth]{non_physically_smooth_path}
    \caption{Гладкий, но не физически гладкий путь}
  \end{figure}
\end{example}

\begin{definition}
  \(v(t) \coloneqq \gamma'(t)\) --- касательный вектор в точке \(\gamma(t)\).
\end{definition}

Считается, что \(\gamma'(t) \neq 0\).

В физическом понимании это скорость.

\begin{definition}
  Касательное пространство в точке \(\gamma(t_0)\) --- вектор \(hv(t_0)\).
\end{definition}

Понятно, что если \(\gamma(t_0) \neq 0\), то такое пространство существует.

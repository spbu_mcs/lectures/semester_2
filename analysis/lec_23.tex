\lecture{23}{06.05.2024, 11:15}{}

Для чего вообще могут использоваться ряды?

\begin{example} \mnote{ОДУ}
  Рассмотрим следующее дифференциальное уравнение: \[
    (1 + x)f'(x) = \alpha f(x)
  ,\] где \(-1 < x < 1\) и \(f(0) = 1\). Такая задача называется \textit{задачей Коши}.

  Будем решать его методом неопределенных коэффициентов: \(f(x) = \sum_{k = 0}^\infty c_k x^k\). Из
  начальных условий следует, что \(c_0 = 1\); подставим весь ряд в уравнение: \[
  \begin{aligned}
    & (1 + x) \sum_{k = 0}^\infty kc_k x^{k - 1} = \alpha\sum_{k = 0}^\infty c_k x^k \\
    \iff & \sum_{k = 0}^\infty kc_k x^{k - 1} + \sum_{k = 0}^\infty kc_k x^k = \alpha \sum_{k =
    0}^\infty c_k x^k \\
    \oversym{\iff}{k \mapsto k + 1} & \sum_{k = 0}^\infty (k + 1)c_{k + 1} x^k + \sum_{k = 0}^\infty
    (k - \alpha) c_k x^k = 0.
  \end{aligned}
  \]

  Методом неопределенных коэффициентов, \[
  \begin{aligned}
    (n + 1) c_{n + 1} &= (\alpha - n)c_n \iff c_n = \frac{(\alpha - n)(\alpha - (n - 1)) \ldots}{(n
    + 1)!}\alpha \\
    &= \frac{\alpha(\alpha - 1) \ldots(\alpha - n + 1)}{n!}
  \end{aligned}
  \]

  Проверим, что ряд сходится: \[
    \left| \frac{c_n}{c_{n + 1}} \right| = \left| \frac{\alpha - n}{n + 1} \right| \xrightarrow{n
    \to \infty} 1
  ,\] --- радиус сходимости \(R = 1\).

  Получается, что \[
    \boxed{f(x) = (1 + x)^\alpha}
  .\]

  Почему не будет других решений? Пусть \(f(x) = (1 + x)^\alpha \varphi(x)\): \[
    (1 + x)(1 + x)^\alpha \varphi'(x) = 0 \iff \varphi(x) = \mathrm{const} = 1
  \] в силу начального условия.
\end{example}

Пусть \(\alpha \in \R\).

\begin{definition}
  \(C_n^\alpha \coloneqq \frac{\alpha(\alpha - 1) \ldots (\alpha - n + 1)}{n!}\).
\end{definition}

\begin{statement}[обобщенный бином Ньютона]
  \((1 + x)^\alpha = \sum_{n = 0}^\infty C_n^\alpha x^n\).
\end{statement}

\begin{example}
  Рассмотрим \(\frac{1}{\sqrt{1 - t}} = (1 - t)^{-\frac{1}{2}}\).

  Биномиальные коэффициенты выглядят как \(\frac{\frac{1}{2}\left(\frac{1}{2} + 1\right) \ldots
  \left( \frac{1}{2} + n - 1 \right)}{n!} = \frac{1 \cdot 3 \cdot \ldots \cdot (2n - 1)}{2^n n!} =
  \frac{(2n - 1)!!}{(2n)!!}\).

  Значит \[
    \frac{1}{\sqrt{1 - t}} = 1 + \sum_{n = 1}^\infty \frac{(2n - 1)!!}{(2n)!!} t^n
  .\]
\end{example}

\begin{example}
  Как известно, \(\arcsin{x} \coloneqq \int_0^x \frac{1}{\sqrt{1 - t^2}} \df{t}\).

  Проинтегрируем равномерно сходящийся ряд из предыдущего примера (при \(|x| < 1\)): \[
  \begin{aligned}
    \arcsin{x} = x + \sum_{n = 1}^\infty \frac{(2n - 1)!!}{(2n)!!} \frac{x^{2n + 1}}{2n + 1},
  \end{aligned}
  \] --- сходится равномерно при \(|x| \le 1\), ведь \(\frac{(2n - 1)!!}{(2n)!!} \sim
  \frac{1}{\sqrt{\pi n}}\).
\end{example}

\begin{remark}
  Всё это верно и для \(z \in \C\), \(|z| \le 1\). Так и определяется комплексный арксинус.
\end{remark}

\subsection{Неклассические способы суммирования}

\begin{definition}
  Ряд \(\sum_{n = 0}^\infty a_n\) называется сходящимся по Абелю, если существует предел \(\lim_{x \to
  1_-} \sum_{n = 0}^\infty a_n x^n\).
\end{definition}

\begin{statement}
  Из классической сходимости следует сходимость по Абелю.
\end{statement}

\begin{example}
  \(1 - 1 + 1 - 1 + \ldots = \frac{1}{1 + x} \xrightarrow{x \to 1} \frac{1}{2}\) по Абелю.
\end{example}

\begin{definition}
  Ряд \(\sum_{n = 0}^\infty a_n\) называется сходящимся по Чезаро порядка \(\alpha\) (\((C,
  \alpha)\)) при \(\alpha \ge 0\), если существует предел \(\lim_{n \to \infty}
  \frac{A_n^\alpha}{B_n^\alpha}\), где \(\sum_{n = 0}^\infty A_n^\alpha x^n = \frac{\sum_{n =
  0}^\infty a_n x^n}{(1 - x)^{1 + \alpha}}\) и \(\sum_{n = 0}^\infty B_n^\alpha x^n = \frac{1}{(1 -
  x)^{1 + \alpha}}\).
\end{definition}

\begin{definition}
  Биномиальный коэффициент \(B_n^\alpha \coloneqq \left| C_{-(1 + \alpha)}^n \right|\).
\end{definition}

\begin{example}
  Метод \((C, 0)\) --- классическая сходимость, ведь \(B_n^0 = 1\) и \(A_n^0\) --- частичная сумма.
\end{example}

Довольно часто встречается \((C, 1)\): зачастую он и имеется в виду, когда говорят ``суммирование по
Чезаро''.

\begin{statement}
  В \((C, 1)\), \(B_n^1 = (n + 1)\) и \(A_n = S_0 + S_1 + \ldots + S_n\), где \(S_k \coloneqq a_0 +
  a_1 + \ldots + a_k\).
\end{statement}

Таким образом можно упростить определение \((C, 1)\): должен существовать предел \(\boxed{\lim_{n
\to \infty} \frac{S_0 + \ldots + S_n}{n}}\).

\begin{example}
  Рассмотрим ряд \(1 + 1 - 1 + 1 - \ldots\): его частичные суммы это \(1\) и \(0\), и по \((C, 1)\)
  он сходится к \(\frac{1}{2}\).
\end{example}

\begin{theorem}
  Если ряд \(\sum_n a_n\) суммируем по \((C, \alpha)\) для \(\alpha \ge 0\), то он сходится по
  Абелю.
\end{theorem}

То есть суммирование по Абелю --- более общий метод. \mnote{еще более общий --- метод аналитического
продолжения}

\begin{proof}
  Раз сходится по \((C, \alpha)\), то \[
  \begin{aligned}
    \forall \varepsilon > 0 \exists N = N(\varepsilon)\colon \forall n \ge N(\varepsilon) \quad
    &\left| \frac{A_n^\alpha}{B_n^\alpha} - S\right| < \varepsilon \\
    &\left| A_n^\alpha - SB_n^\alpha \right| < \varepsilon B_n^\alpha.
  \end{aligned}
  \]

  Рассмотрим ряд для суммированию по Абелю: \[
  \begin{aligned}
    \sum_{n = 0}^\infty a_n x^n &= (1 - x)^{1 + \alpha} \sum_{n = 0}^\infty A_n^\alpha x^n = (1 -
    x)^{1 + \alpha} \sum_{n = 0}^\infty (A_n^\alpha - SB_n^\alpha + SB_n^\alpha) x^n \\
    &= S + (1 - x)^{1 + \alpha} \sum_{n = 0}^\infty (A_n^\alpha - SB_n^\alpha) x^n.
  \end{aligned}
  \]

  Подготовимся к предельному переходу: \[
  \begin{aligned}
    \left| \sum_{n = 0}^\infty a_n x^n - S \right| &\le (1 - x)^{1 + \alpha} \sum_{n =
    0}^{N(\varepsilon)} \left| A_n^\alpha - SB_n^\alpha \right| x^n + \underbrace{(1 - x)^{\alpha +
    1}\sum_{n = N(\varepsilon) + 1}^\infty \varepsilon B_n^\alpha x^n}_{< \varepsilon};
  \end{aligned}
  \]

  Тогда \[
    \limsup_{x \to 1_-} \left| \sum_{n = 0}^\infty  a_nx^n - S\right| \le \varepsilon \implies
    \limsup_{x \to 1_-} \left| \sum_{n = 0}^\infty a_nx^n - S \right| = 0
  .\]
\end{proof}

А можно ли понять из сходимости по Абелю или по Чезаро, сходится ли ряд в классическом смысле?

\begin{theorem}[Таубера] \mnote{на самом деле достаточно, что \(na_n\) огр.}
  Если \(na_n \to 0\) и ряд \(\sum_n a_n\) сходится по Абелю, то он сходится в классическом
  смысле.
\end{theorem}
\begin{proof}
  Положим, что \(S_N \coloneqq \sum_{k = 0}^N a_k\): посмотрим, куда она стремится.

  \[
  \begin{aligned}
    S_N = \sum_{k = 0}^\infty a_kx^k + \sum_{k = 0}^\infty a_k(1 - x^k) - \sum_{k = N + 1}^\infty
    \frac{ka_kx^k}{k}.
  \end{aligned}
  \]

  \(x\) --- произвольная переменная; пусть \(x = 1 - \frac{1}{N}\). Тогда, применяя неравенство
  \((1 - x^k) < k(1 - x)\), \[
  \begin{aligned}
    \left| S_N - \sum_{k = 0}^\infty a_k\left( 1 - \frac{1}{N} \right)^k \right| &\le
    \underbrace{\frac{\sum_{k = 0}^N \left| k a_k \right|}{N}}_{\to 0} + \underbrace{\sum_{k = N +
    1}^\infty \frac{|ka_k| x^k}{N}}_{\frac{x^{N + 1}}{(1 - x)^N}}.
  \end{aligned}
  \]
\end{proof}

Пусть \(R\) --- радиус сходимости ряда \(\sum_{k = 0}^\infty c_k z^k\), и \(\sum_{k = 0}^\infty c_k
A^k\) сходится в круге \(\|A\| < R\), где \(A\colon m \times m\) --- матрица.

\begin{remark}
  Этот ряд не обязательно расходится при \(\|A\| > R\): например, \(A = \begin{pmatrix} 0 & 0 \\ M &
  0\end{pmatrix} \), \(A^2 = 0\).
\end{remark}

\begin{theorem}[признак Коши]
  Если \(r \coloneqq \limsup_{n \to \infty} \sqrt{|c_n|\|A^n\|} < 1\), то ряд сходится, а если \(r >
  1\) --- расходится.
\end{theorem}

\begin{definition}
  Матричная экспонента \(e^A \coloneqq \sum_{n = 0}^\infty \frac{A^k}{k!}\).
\end{definition}

\begin{statement}
  \(\det{e^A} = e^{\tr{A}}\).
\end{statement}
\begin{proof}
  Достаточно воспользоваться ЖНФ.
\end{proof}

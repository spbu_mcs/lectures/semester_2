\lecture{13}{28.03.2024, 15:35}{}

\begin{example}
  Рассмотрим функцию от двух переменных (\(n = 2\)) \(\R^2 \to \R\).

  Тогда \[
    \df^m{f(x, h)} = \sum_{i = 0}^m C_m^i \frac{\partial^m f(x)}{\partial x_1^i \partial x_2^{m -
    i}}h_1^ih_2^{m - i}.
  \]
\end{example}

\begin{proof}[Доказательство формулы Тейлора]
  Пусть \(\varphi(t) = f(x + th)\), где \(t \in [0, 1]\). Раз \(\varphi\colon \R \to \R\), то к ней
  применима одномерная формула Тейлора: \[
  \begin{aligned}
    \varphi(1) &= \varphi(0) + \sum_{m = 1}^r \frac{\varphi^{(m)}(0)}{m!}1^k + \frac{\varphi^{(r +
    1)}(\Theta)}{(r + 1)!}.
  \end{aligned}
  \]

  Этого уже достаточно --- посчитаем дифференциалы.
  \[
  \begin{aligned}
    \varphi(0) &= f(x); \\
    \varphi'(t) &= f'(x + th)h = \inner{\grad{f(x + th)}}{h} = \sum_{i = 1}^n D_if(x + th)h_i =
    \df{f(x + th, h)}; \\
    \varphi''(t) &= \sum_{i = 1}^n \left( D_if(x + th) \right)'_th_i = \sum_{i = 1}^n \sum_{j = 1}^n
    D_{ij}^2f(x + th)h_ih_j = \df^2{f(x + th, h)}; \\
    &\vdots \\
    \varphi^{(m)}(t) &= \df^m{f(x + th, h)}.
  \end{aligned}
  \]
\end{proof}

\begin{theorem}[формула Тейлора с остаточным членом в форме Пеано]
  Если \(f \in C^r\left( \mathcal{D} \right)\), \(x \in \mathcal{D}\), то \[
  \begin{aligned}
    f(x + h) = \sum_{|k| \le r} \frac{f^{(k)}(x)}{k!}h^k + \smallO(|h|^r), \quad |h| \to 0.
  \end{aligned}
  \]
\end{theorem}
\begin{proof}
  Применим формулу Тейлора с остаточным членом в форме Лагранжа для \(r - 1\): \[
  \begin{aligned}
    f(x + h) &= \sum_{|k| \le r - 1} \frac{f^{(k)}(x)}{k!}h^k + \sum_{|k| = r} \frac{f^{(k)}(x +
    \Theta h)}{k!}h^k \\
    &= \sum_{|k| \le r} \frac{f^{(k)}(x)}{k!}h^k + \underbrace{\sum_{|k| = r} \frac{f^{(k)}(x +
    \Theta h) - f^{(k)}(x)}{k!}h^k}_{R(x)}.
  \end{aligned}
  \]

  \[
  \begin{aligned}
    |R(x)| &\le \sum_{|k| = r} \left|f^{(k)}(x + \Theta h) - f^{(k)}(x)\right|\frac{\left| h^k
    \right|}{k!}
    \\
    &\le \sum_{|k| = r} \left| f^{(k)}(x + \Theta h) - f^{(k)}(x) \right| \frac{|h|^r}{k!} = \smallO\left(|h|^{r
    }\right),
  \end{aligned}
  \] ведь \(\left| h^k \right| = \left| h_1^{k_1} \ldots h_m^{k_m} \right| \le |h|^{k_1} \ldots
  |h|^{k_m}\) и производные \(f^{(k)}\) непрерывные, где \(h = \max h_i\).
\end{proof}

Как и до этого, \(f\colon \mathcal{D} \to \R\), \(\mathcal{D} \subset \R^n\).

\begin{definition}
  Точка \(x^0\) называется точкой локального максимума функции \(f\), если \(\exists
  U_\delta(x^0)\colon \forall x \in U(x^0) \quad f(x^0) \ge f(x)\).
\end{definition}

Аналогично определяется локальный минимум.

Пусть \(f\) дифференцируема в точке \(x^0\).

\begin{definition}
  Точка \(x^0\) называется стационарной, если \(\grad{f(x^0)} = 0\).
\end{definition}

\begin{theorem}[необходимое условие экстемума]
  Если \(f\) дифференцируема в точке \(x^0\), и \(x^0\) --- точка локального экстремума, то \(x^0\) 
  --- стационарная точка.
\end{theorem}
\begin{proof}
  \[
  \begin{aligned}
    f(x^0 + h) &= f(x^0) + f'(x^0)h + \smallO(h) = f(x^0) + \inner{\grad{f(x^0)}}{h} + \smallO(h);
  \end{aligned}
  \] взяв \(h = \lambda\grad^\top{f(x^0)}\), \[
    \ldots = f(x^0) + \lambda\left| \grad{f(x^0)} \right|^2 + \smallO(\lambda)
  ,\] --- видно, что \(\grad\) должен занулиться.
\end{proof}

\begin{theorem}[достаточное условие экстремума]
  Если \(f \in C^2\left( \mathcal{D} \right)\), \(x^0 \in \mathcal{D}\), \(f'(x^0) = 0\), а
  \(\forall h \neq 0_n \quad \df^2{f(x^0, h)} > 0\), то \(x^0\) --- точка локального минимума.
\end{theorem}
\begin{proof}
  \[
  \begin{aligned}
    f(x^0 + h) &= f(x^0) + \underbrace{\frac{\df{f(x^0, h)}}{1!}}_0 + \frac{\df^2{f(x^0, h)}}{2!} +
    \smallO(|h|^2).
  \end{aligned}
  \]

  При \(|h| = 1\), \(\df^2{f(x^0, h)} \ge c|h|^2\) (теорема Вейерштрасса после сужения на сферу,
  ведь если бы нашелся \(c = 0\), это противоречило бы определенности \(\df^2\)): \[
    \ldots \ge f(x^0) + c|h|^2 + \smallO(|h|^2)
  .\]

  Тогда, переходя к пределу получается, что в какой-то окрестности \(f(x^0 + h) - f(x^0) > 0\).
\end{proof}

Рассмотрим \(A = \left\{ \frac{\partial^2 f}{\partial x_i \partial x_j}(x^0) \right\}_{i, j =
1}^n\).

\begin{theorem}[критерий Сильвестра]
  Форма \(A\) положительно определена \(\forall h \neq 0\) тогда и только тогда, когда все
  \(\det\left\{  \frac{\partial^2 f}{\partial x_i \partial x_j}(x^0)  \right\}_{i, j = 1}^m > 0\).
\end{theorem}

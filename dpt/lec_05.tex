\lecture{5}{15.03.2024, 13:40}{}

Введем естественную константу \(m \coloneqq \E{X_{n, j}} = \sum_{k = 0}^\infty p_k k\) --- среднее
число потомков.

\begin{statement}
  \(\E{M_n} = m^n\).
\end{statement}
\begin{proof}
  Вспомним, что \(\varphi_{M_{n + 1}}(z) = \varphi_{M_n}(\varphi_X(z))\), а если \(X \sim \varphi\),
  то \(\E{X} = \varphi'(1)\).

  Продифференцируем в \(z = 1\): \[
    \begin{aligned}
      \E{M_{n + 1}} &= \varphi_{M_{n + 1}}'(1) =
      \varphi_{M_n}'(\underbrace{\varphi_X(1)}_1)\varphi_X'(1) = \varphi_{M_n}'(1)\varphi_X'(1) \\
      &= \E{M_n}\E{X} = \E{M_n}m.
    \end{aligned}
  \]

  Но \(\E{M_0} = \E\mathbb{1} = m^0\): получилось, что \(\E{M_n} = m^n\).
\end{proof}

\begin{theorem}
  Процесс не вырождается тогда и только тогда, когда или \(m > 1\), или \(p_1 = 1\) (\(m = 1\)).
\end{theorem}
\begin{proof}
  \(p_1 = 1\) означает, что у каждого будет ровно один потомок, процесс не будет ветвящимся, и,
  понятно, не будет вырождаться.

  Разберем случаи.
  \begin{pfparts}
    \item[\(\boxed{m < 1}\)] Покажем, что процесс вырождается, причем покажем это двумя способами.

      С одной стороны, нарисовав графики (с учетом выпуклости), наименьший корень \(\varphi_X(z) =
      z\) --- \(z = 1\), \(q = 1\), процесс вырождается.

      С другой же, \[
        \P\left( M_n \neq 0 \right) = \sum_{k = 1}^\infty \P\left( M_n = k \right) \le \sum_{k =
        1}^\infty \P\left( M_n = k \right)k = \E{M_n} = m^n,
      \] --- \(m < 1\), а значит, \(q_n \to 1\), ведь \(q_n = 1 - \P\left(M_n \neq 0\right)\).

    \item[\(\boxed{m > 1}\)] Посмотрим на график \(\varphi_X(z)\).

      \[
        \varphi_X(z) = \varphi_X(1) + m(z - 1) + \smallO(z - 1) = 1 + m(z - 1) + \smallO(z - 1)
      .\]

      Перекидывая это, \[
        \varphi_X(z) - z = (1 - z) + m(z - 1) + \smallO(z - 1) = (1 - z)\left( 1 - m + \smallO(1)
        \right) < 0, \quad z \approx 1
      .\]

      По теореме Ролля, \(\varphi_X(z) - z < 0\) в окрестности \(1\), а \(\left(\varphi_X(z) -
      z\right)\big|_{z = 0} = \varphi_X(0) = p_0 \ge 0\), и значит \(\exists z_0\colon \varphi_X(z_0)
      = z_0, z_0 < 1\).

    \item[\(\boxed{m = 1}\)] Вновь посмотрим на график \(\varphi_X(z)\).

      В этот раз есть два случая: \(\forall z \in [0, 1) \quad \varphi_X(z) > z\), а значит, \(q =
      1\); и \(z \approx 1 \implies \varphi_X(z) = z\).

      На самом же деле, второй случай невозможен, ведь если это так, то \(\varphi_X''(1) = 0\) (как
      линейная), но \(\varphi_X''(1) = \E{X(X - 1)}\), а значит, \(k \ge 2 \implies p_k = 0\) и
      \(\varphi_X(z) = p_0 + p_1z\) --- получилось, что \(1 = m = \varphi_X'(1) = p_1 \implies p_1 =
      1\).
  \end{pfparts}
\end{proof}

\section{Предельные теоремы Муавра-Лапласа}

\textit{A. de Moivre}, \textit{P. Laplace}.

Эти теоремы должны дать идею о следующем развитии дискретной теории вероятности после закона больших
чисел.

\textit{Историческая справка}: на самом деле доказал ее только Муавр, а Лапласу она настолько понравилась,
что он добавил ее в свой учебник по теории вероятности. Муавр был французом: он получил хорошее
образование, но впоследствии угодил в тюрьму из-за своих протестантских взглядов, и после выхода из
нее, перебрался в Англию, поняв, что во Франции ему не сыскать своего места. В Англии его потуги
оценили по достоинству: в его круг входили такие люди, как Ньютон и Стирлинг. Он был членом
Английской Академии, написал очень хороший учебник \textit{``Doctrine of Chance''}, в которой
впервые появилась эта теорема и, например, первое упоминание о нормальном распределении, открытом
Муавром. Не смотря на всё это, за всю свою жизнь он не смог устроиться ни на какую работу из-за
клубной культуры Лондона --- всё свою жизнь он давал уроки в разных местах.

Вспомним закон больших чисел Бернулли: \(S_n \approx np + \smallO(n)\), где \(\approx\) в ``больших
кавычках''.

В том же духе, теоремы Муавра-Лапласа можно понимать так: \(S_n \approx np + X\sqrt{n} +
\smallO(\sqrt{n})\), где \(X\) --- некоторая случайная величина, а \(\approx\) все еще в
``кавычках''.

Видно, теоремы Муавра-Лапласа дают более точную оценку.

\subsection{Локальная теорема Муавра-Лапласа}

Схема Бернулли говорит, чему равно \(\P(S_n = k)\), но при больших \(n\) выражение получается
практически не вычисляемым. Хочется какое-то приближенное значение.

\begin{theorem}
  \(\forall \varepsilon_n \to 0\) равномерно по зоне, для \(k \in \left[ np - \varepsilon_n
  n^{\frac{2}{3}}, np + \varepsilon_n n^{\frac{2}{3}} \right]\) верно следующее выражение:
  \[
    \P(S_n = k) = \frac{1}{\sqrt{2\pi n p(1 - p)}}\exp\left( -\frac{(k - np)^2}{2np(1 - p)}
    \right)\left( 1 + \smallO(1) \right)
  .\]
\end{theorem}

Для лучшего понимания положим, \mnote{на протяжении всего времени \(0 < p < 1\) фиксировано} что
\(V(n, k) \coloneqq \frac{1}{\sqrt{2\pi n p(1 - p)}}\exp\left( -\frac{(k - np)^2}{2np(1 - p)}
\right)\); тогда теорема говорит, что \[
  \lim_{n \to \infty} \max_{k \in [\ldots, \ldots]} \frac{\P(S_n = k)}{V(n, k)} = 1 = \lim_{n \to
  \infty} \min_{k \in [\ldots, \ldots]} \frac{\P(S_n = k)}{V(n, k)}
.\]

\begin{proof}
  Запишем схему Бернулли: \[
    \begin{aligned}
      \P(S_n = k) &= C_n^k p^k(1 - p)^{n - k} = \frac{n!}{k!(n - k)!} p^k(1 - p)^{n - k} \\
      &\approx \frac{\sqrt{2\pi n}\left(\frac{n}{e}\right)^n}{\sqrt{2\pi k} \left(\frac{k}{e}\right)^k
      \sqrt{2\pi (n - k)} \left(\frac{n - k}{e}\right)^{n - k}} p^k(1 - p)^{n - k} =
      \sqrt{\frac{n}{2\pi k(n - k}} \frac{n^n p^k(1 - p)^{n - k}}{k^k (n - k)^{n - k}}.
    \end{aligned}
  \] 

  Раз \(k \sim np\) и \((n - k) \sim n(1 - p)\) (за счет того, что \(k \in [\ldots, \ldots]\)), то,
  принимая \(n^n = n^kn^{n - k}\):
  \[
  \begin{aligned}
    \ldots &\approx \frac{1}{\sqrt{2\pi n p(1 - p)}} \left( \frac{np}{k} \right)^k
    \left( \frac{n(1 - p)}{n - k} \right)^{n - k}.
  \end{aligned}
  \]

  Как устроен \(\left( \frac{np}{k} \right)^k\)? \[
  \begin{aligned}
    \left( \frac{np}{k} \right)^k = \exp\left( -k \ln\left( \frac{k}{np} \right) \right).
  \end{aligned}
  \]

  Положим, что \(k = np + v\) (\(v\) --- новая переменная): \[
  \begin{aligned}
    k\ln\left( \frac{k}{np} \right) &= (np + v)ln\left( 1 + \frac{v}{np} \right) = (np + v)\left(
    \frac{v}{np} - \frac{v^2}{2(np)^2} + \bigO\left( \frac{v^3}{n^3} \right) \right),
  \end{aligned}
  \] --- \(p\) константа, опущенная в \(\bigO\).

  Раскроем скобки: \[
  \begin{aligned}
    \ldots &= v + \frac{v^2}{np} - \frac{v^2}{2np} + \bigO\left( \frac{v^3}{n^2} \right).
  \end{aligned}
  \]

  Но \(k \in [\ldots, \ldots] \iff v \in \left[ -\varepsilon_n n^{\frac{2}{3}}, \varepsilon_n
  n^{\frac{2}{3}} \right]\), а значит, \(\bigO\left( \frac{v^3}{n^2} \right) = \bigO\left(
  \varepsilon_n \right) = \smallO(1)\): \[
  \begin{aligned}
    v + \frac{v^2}{2np} + \smallO(1)
  \end{aligned}
  .\]

  Итого: \[
    \left( \frac{np}{k} \right)^k = \exp\left( -v - \frac{v^2}{2np} \right)(1 + \smallO(1))
  .\]

  Абсолютно аналогично можно разделаться и со вторым сомножителем: \[
    \left( \frac{n(1 - p)}{n - k} \right)^{n - k} = \exp\left( +v - \frac{v^2}{2n(1 - p)} \right)(1
    + \smallO(1)
  .\]

  Ну и наконец, перемножая экспоненты: \[
    \ldots \cdot \ldots = \exp\left( -\frac{v^2}{2n}\left( \frac{1}{p} + \frac{1}{1 - p} \right)
    \right)(1 + \smallO(1))
  .\]
\end{proof}

\subsection{Интегральная теорема Муавра-Лапласа}

\begin{theorem}
  \[
    \forall \underbrace{c < d}_{\in \R} \quad \P\left( np + c\sqrt{n} \le S_n \le np + d\sqrt{n}
    \right) \xrightarrow{n \to \infty} \frac{1}{\sqrt{2\pi p(1 - p)}} \int_c^d \exp\left(
    -\frac{u^2}{2p(1 - p)} \right) \df{u}
  .\]
\end{theorem}
\begin{proof}
  Пусть \(k \in \left(np + \sqrt{n}, np + d\sqrt{n}\right)\) --- заметим, что \(\sqrt{n} \ll
  n^{\frac{2}{3}}\). Тогда, по локальной теореме: \[
  \begin{aligned}
    \P\left( S_n = k \right) &= \frac{1 + \smallO(1)}{\sqrt{2\pi n p(1 - p)}} \exp\left( -\frac{(k -
    np)^2}{2n p(1 - p)} \right) = \int_k^{k + 1} \frac{1 + \smallO(1)}{\sqrt{2\pi n p(1 - p)}}
    \exp\left( -\frac{(k - np)^2}{2n p(1 - p)} \right) \df{v} \\
    &= \frac{1 + \smallO(1)}{\sqrt{2\pi n p(1 - p)}} \int_{k}^{k + 1} \exp\left( -\frac{(v -
    np)^2}{2n p(1 - p)} \df{v} \right),
  \end{aligned}
  \] --- замена \((k - np)^2 \mapsto (v - np)^2\) легитимная, ведь \[
    (k - np)^2 - (v - np)^2 =
    \underbrace{(k - v)}_{\bigO(1)}\underbrace{\left( (k - np) + (v - np)
    \right)}_{\bigO\left(\sqrt{n}\right)} = \bigO\left( \sqrt{n} \right),
  \] и отношение в экспоненте --- \(\smallO(1)\).

  Просуммируем по всем \(k\) (можно проверить, что пренебрежение концами в пределе не существенно):
  \[
    \begin{aligned}
      \P\left( np + c\sqrt{n} \le S_n \le np + d\sqrt{n} \right) &\approx \int_{np + c\sqrt{d}}^{np +
    d\sqrt{n}} \frac{1}{2\pi n p(1 - p)} \exp\left( -\frac{(v - np)^2}{2\pi n p(1 - p)} \right)
    \df{v} \\
    \begin{bmatrix} v = np + \sqrt{n}u \end{bmatrix} &= \frac{1}{\sqrt{2\pi p(1 - p)}} \int_c^d
    \exp\left( -\frac{u^2}{2p(1 - p)} \right) \df{u}.
    \end{aligned}
  \]
\end{proof}

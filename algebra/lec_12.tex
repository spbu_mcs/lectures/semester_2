\lecture{12}{02.04.2024, 13:40}{}

\subsection{Ортогонализация Грама-Шмидта}

Пусть \((V, B)\) --- билинейное пространство, а \(B\) анизотропна.

По теореме Лагранжа матрицу Грама \(\Gamma\) можно диагонализировать, но в случае анизотропной формы
можно выбрать \(C\) верхнетреугольной (\(C^\top \Gamma C\)).

Наведем формальностей: пусть \(e_1, \ldots, e_n\) --- базис.

\begin{theorem}
  Существуют \(u_1, \ldots, u_n\) такие, что \(B\) в нем диагональная (\(B(u_i, u_j) = 0\) при  \(i
  \neq j\)), причем \(\forall k \quad \langle e_1, \ldots, e_k \rangle = \langle u_1, \ldots, u_k
  \rangle\).
\end{theorem}

Иначе говоря, матрица перехода верхнетреугольная.

\begin{proof} \mnote{нормировать не получится из-за произвольности поля}
  Возьмем \(u_1 \coloneqq e_1\); \(u_2 \coloneqq \alpha u_1 + e_2\). Наложим условие на \(\alpha\):
  \(B(u_1, u_2) = 0 \iff \alpha B(e_1, e_1) + B(e_1, e_2) = 0 \iff \alpha = -\frac{B(e_1,
  e_2)}{B(e_1, e_1)}\).

  Продолжим: \(u_3 \coloneqq \alpha' u_1 + \beta' u_2 + e_3\). \(B(u_1, u_3) = \alpha' B(u_1, u_1) +
  B(u_1, e_3) \iff \alpha' = \ldots\); \(B(u_2, u_3) = \beta' B(u_2, u_2) + B(u_2, e_3) \iff \beta'
  = \ldots\); аналогично для \(u_i\).
\end{proof}

На будущее, это называется \textit{QR-разложением}.

\subsection{Возможные обобщения}

Всё это время мы работали с симметрическими формами. А что с другими?

\subsubsection{Кососимметрические билинейные формы}

Пусть \(B\colon V \times V \to F\) --- билинейная форма.

\begin{definition}
  \(B\) называется кососимметрической, если \(B(u, v) = -B(v, u)\).
\end{definition}

Определение предполагает, что \(\mathrm{char}{F} \neq 2\); корректнее такую форму задавать как
\(\forall u \in V \quad B(u, u) = 0\).

Эти определения эквивалентны, ведь \(0 = B(u + v, u + v) - B(u, u) - B(u, v) = B(u, v) + B(v, u)\)
(аналогично определителю с двумя одинаковыми строчками).

Такие формы особенно полезны в классической и квантовой механике.

Также, как и для квадратичных форм
\begin{statement}
  \(\left( \qfrac{V}{\Rad{B}}, \bar{B} \right)\) --- невырожденная знакопеременная форма.
\end{statement}

\begin{notation}
  Порой невырожденные знакопеременные формы называют симплестическими.
\end{notation}

Аналогично определяя ортогональную прямую сумму,
\begin{lemma}
  \(\left( V, B \right) \cong_i \left( \qfrac{V}{\Rad{B}}, \bar{B} \right) \boxplus \left( \Rad{B},
  0\right)\).
\end{lemma}

Доказательство почти один-в-один с точностью до появлении знака в каком-то месте.

\begin{lemma}
  Если \(U \leqslant V\) и \(\left(U, B\restriction_U\right)\) невырожденная, то \((V, B) \cong_i
  \left( U, B\restriction_U \right) \boxplus \left( U^{\perp}, B\restriction_{U^\perp} \right)\).
\end{lemma}

Аналогично задаётся и матрица Грама, причем, за счет знакопеременности, \(\Gamma^\top = -\Gamma\).
Более того, если \(\mathrm{char}{F} \neq 2\), то дополнительно требуется, что на главной диагонали
стоят \(0\).

\begin{definition}
  Гиперболическая плоскость \(\mathcal{H}\) --- двухмерное пространство с формой \(\begin{pmatrix} 0
  & 1 \\ -1 & 0 \end{pmatrix} \) в некотором базисе.
\end{definition}

Теорема Лагранжа тут уже не применима, однако \(\Gamma\) всё еще можно привести к более-менее
простому виду:
\begin{theorem}
  \((V, B) \cong_i \underbrace{\mathcal{H} \boxplus \ldots \boxplus \mathcal{H}}_r \boxplus \left(
  \Rad{B}, 0 \right)\), причем \(2r = \rank{\Gamma}\).
\end{theorem}
\begin{proof}
  Применим индукцию по \(\dim{V}\).
  \begin{pfparts}
    \item[База] \(0\)-ое пространство: очевидно.

    \item[Шаг индукции] Рассмотрим несколько случаев.
      \begin{itemize}
        \item Если \(\forall u, v \quad B(u, v) = 0\), то очевидно.

        \item Пусть \(\exists u, v\colon B(u, v) \neq 0\).

          Заменив \(v \mapsto v\alpha\) можно добиться того, что \(B(u, v) = 1\).

          Рассмотрим матрицу Грама \(\langle u, v \rangle\): \(
            \begin{pmatrix}
              0 & 1 \\
              -1 & 0
            \end{pmatrix}
          \) из-за условия на кососимметричность.

          Осталось рассмотреть ортогональное пополнение \(\langle u, v \rangle^\perp\) и применить
          индукционное предположение в купе с леммой.
      \end{itemize}
  \end{pfparts}
\end{proof}

\begin{remark}
  В отличии от квадратичного случая, невырожденная кососимметрическая билинейная форма может
  существовать только на четномерных пространствах.
\end{remark}

\subsubsection{Эрмитовы формы}

Как известно, над \(\C\) нет анизотропных квадратичных форм. А если сильно хочется?

Над \(\R\) работало то, что \(x_1^2 + \ldots + x_n^2 = 0 \implies x_1 = \ldots = x_n = 0\). Над
\(\C\) можно взять модули: \(\conj{z}_1z_1 + \ldots + \conj{z}_nz_n = 0 \implies z_1 = \ldots = z_n
= 0\).

Пусть \(K\) --- некоторое поле.

\begin{definition}
  Изоморфизм \(\conj{\cdot}\colon K \to K\), где \(\conj{\alpha + \beta} = \conj{\alpha} +
  \conj{\beta}\) и \(\conj{\alpha\beta} = \conj{\beta}\conj{\alpha}\), называется инволюцией.
\end{definition}

Порядок инволюции полагается \(2\): \(\conj{\conj{\alpha}} = \alpha\).

Пусть теперь \(V\) --- векторное пространство над \(K\).

\begin{definition}
  \(B\colon V \times V \to K\) называется полуторной формой, если
  \begin{enumerate}
    \item \(B(u + v, w) = B(u, w) + B(v, w)\), \(B(u, v + w) = B(u, v) + B(u, w)\);
    \item \(B(u\alpha, v\beta) = \conj{\alpha}B(u, v)\beta\).
  \end{enumerate}
\end{definition}

Говорится, что такая форма линейна по второму аргументу и полулинейна по первому (можно и наоборот,
для левого векторного пространства). Или же, рассматривая \(B\) как \(V \to V^*\), то \(B(v,
\cdot)\) линейно, а само \(B\colon V \to V^*\) полулинейно.

\begin{definition}
  Полуторная линейная форма называется эрмитовой, если \(B(v, u) = \conj{B(u, v)}\).
\end{definition}

\begin{definition}
  Полуторная линейная форма называется антиэрмитовой, если \(B(v, u) = -\conj{B(u, v)}\).
\end{definition}

Посмотрим, что представляет собою матрица Грама эрмитовых форм: \(B\left( \sum_i e_i \alpha_i,
\sum_j e_j\beta_j \right) = \sum_{i, j} \conj{\alpha}_iB(e_i, e_j)\beta_j\).

\begin{definition}
  Эрмитово сопряженная \(A^* \coloneqq \conj{A^\top}\) (транспонируем и ко всем элементам применяем
  инволюцию).
\end{definition}

Тогда \(\boxed{B(u, v) = u^* \Gamma v}\).

\begin{statement}
  Эрмитовость эквивалентна тому, что \(\Gamma^* = \Gamma\).
\end{statement}

Посмотрим, что сразу применимо для эрмитовых форм из теории симметрических.

Определяется \(\Rad{B}\) (рассматривая как гомоморфизм абелевых групп, на что не влияет
полулинейность), для него всё еще применимы \(2\) леммы.

Теорема Лагранжа работает: \(C^*\Gamma C = \begin{pmatrix} * \\ & \ddots \\ & & * \end{pmatrix} \).
\begin{proof}
  Рассмотрим несколько случаев.
  \begin{enumerate}
    \item Пусть \(\forall u \in V \quad B(u, u) = 0\).

      Тогда \(0 = B(u + v, u + v) - B(u, u) - B(v) = B(u, v) + B(v, u) = B(u, v) + \conj{B(u, v)}\)
      --- над \(\C\) это значит, что \(B(u, v)\) чисто мнимое, но такого быть не может, ведь \(B(u,
      vi) = B(u, v)i\).

      Для произвольного поля будет примерно то же самое: \(\begin{cases}
        B(u, v) + \conj{B(u, v)} = 0 \\
        B(u, v) \alpha + \conj{B(u, v)}\conj{\alpha} = 0
      \end{cases}\).

      Значит вся \(\Gamma\) тождественно нулевая, ведь \(B(u, v)(\alpha - \conj{\alpha}) = 0 \iff
      B(u, v) = 0\) --- порядок инволюции \(> 1\).

    \item Пусть \(\exists u \in V\colon B(u, u) \neq 0\).

      Тогда форма \(B\restriction_{\langle u \rangle}\) невырождена и \((V, B) \cong_i \left(
      \langle u \rangle, B\restriction_{\langle u \rangle} \right) \boxplus \left(
      \langle u \rangle^\perp, B\restriction_{\langle u \rangle^\perp} \right)\) --- осталось
      применить индукцию по размерности.
  \end{enumerate}
\end{proof}

Теоремы Витта о продолжении и сокращении тоже работают, но доказываются сложнее.

Закон инерции Сильвестра тоже работает --- докажем его для эрмитовых форм над \(\C\) без применения
теорем Витта.

Раз \(\Gamma^* = \Gamma\) и \(\Gamma = \begin{pmatrix} \alpha_1 \\ & \ddots \\ & & \alpha_n
\end{pmatrix} \), то \(\alpha_i \in \R\).

Сопрягая матрицами \(\begin{pmatrix} \lambda \\ & \ddots \\ & & \lambda_n \end{pmatrix} \)
(\(\lambda \in \R\)), получится \(\begin{pmatrix} \lambda^2\alpha_1 \\ & \ddots \\ & &
\lambda_n^2\alpha_n \end{pmatrix} \) --- можно считать, что \(\alpha_i = \pm 1\) (или \(0\));
\(B\left( \begin{pmatrix} z_1 \\ \vdots \\ z_n \end{pmatrix} , \begin{pmatrix} w_1 \\ \vdots \\ w_n
\end{pmatrix}  \right) = \conj{z}_1w_1 + \ldots + \conj{z}_pw_p - \conj{z}_{p + 1}w_{p + 1} - ... -
\conj{z}_{p + q}w_{p + q}\).

\begin{theorem}[закон инерции Сильвестра]
  Для эрмитовых форм пара \((p, q)\) (\textit{сигнатура}) определена однозначна.
\end{theorem}

\begin{definition} \mnote{\(B(u, u) \in \R\)}
  \(B\) называется положительно определенной, если \(\forall u \neq 0 \quad B(u, u) > 0\).
\end{definition}

\begin{proof}
  Докажем, что \(p = \underbrace{\max_{U \leqslant V, B\restriction_U \text{ положительно
  определена}}{\dim{U}}}_{p'}\) --- оно не зависит от выбора базиса.

  \begin{pfparts}
    \item[\(\boxed{p' \ge p}\)] Очевидно, ведь \(B\restriction_{\langle e_1, \ldots, e_p\rangle}\)
      положительно определенная.

    \item[\(\boxed{p' \le p}\)] Предположим обратное: пусть \(p' > p\).

      Тогда \(U \cap \langle e_{p + 1}, \ldots, e_{p + q} \rangle \neq \left\{ 0 \right\}\);
      \(\dim{U} + \dim\langle e_{p + 1}, \ldots, e_{p + q} \rangle > \dim{V}\).

      Возьмем \(u \neq 0 \in U \cap \langle e_{p + 1}, \ldots, e_{p + q} \rangle\): тогда \(B(u, u)
      > 0\) и \(B(u, u) < 0\).
  \end{pfparts}
\end{proof}

Пусть \(V\) --- векторное пространство над \(\C\). Как проверить, что форма \(B\) (из \((V, B)\))
положительно определена, если мы принципиально не хотим пользоваться теоремой Лагранжа?

\begin{theorem}[критерий Сильвестра]
  \((V, B)\) положительно определена тогда и только тогда, когда все главные миноры матрицы Грама
  положительные.
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\implies}\)] Воспользуемся ортогонализацией Грама-Шмидта: нашлась
      верхнетреугольная \(C\) такая, что \( \Gamma = C^* \begin{pmatrix} \alpha_1 \\ & \ddots \\ & &
      \alpha_n \end{pmatrix} C\), где \(\alpha_i > 0\) (за счет положительности формы).

      Пусть \(C = \left\{ c_{ij} \right\}_{i, j}\). Посмотрим на первый главный минор:
      \(\conj{c_{11}}\alpha_1c_{11} > 0\), ведь \(a\conj{a} > 0 \quad \forall a \in \C\).

      Посмотрим на второй главный минор. Понятно, что \(\det{A^*} = \conj{\det{A}}\), ведь \(\det{A}
      = \det{A^\top}\), а сопряжение можно наложить, например, на явную формулу дискриминанта через
      перестановки. Тогда второй минор --- \(\det{A}\conj{\det{A}}\alpha_1\alpha_2 > 0\); И так
      далее.

    \item[\(\boxed{\impliedby}\)] Почти также, как и в другую сторону: может лишь получиться так,
      что исходное пространство не анизотропно и ортогонализацию Грама-Шмидта провести нельзя.

      На самом деле нет: \[
      \begin{pmatrix} 
        \alpha & 0 & \ldots & 0 \\
        0 \\
        \vdots \\
        0
      \end{pmatrix} 
      ,\] --- домножая на элементарные трансвекции можно перейти по индукции к следующей матрице,
      получив анизотропность.
  \end{pfparts}
\end{proof}

\begin{remark}
  Для квадратичных форм над \(\R\) это тоже работает.
\end{remark}

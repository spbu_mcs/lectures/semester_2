\lecture{1}{12.02.2024, 11:15}{Функции ограниченной вариации, интеграл Римана-Стилтьеса}

Ключевые вехи курса второго семестра:
\begin{enumerate}
  \item Многомерная формула Тейлора;
  \item Теорема об обратной функции.
\end{enumerate}

Как следствие, теорема о неявной функции, метод Лагранжа, ряды (включая функциональные) и так далее.

\begin{definition}
  Вариация функции \(f\colon [a, b] \to \R\) --- \(V_a^b(f) \coloneqq \sup_{\Delta} \sum_{k = 1}^n
  \left| f(x_k) - f(x_{k - 1}) \right|\) (где \(\Delta\) --- всевозможные разбиения отрезка \([a,
  b]\)).
\end{definition}

\begin{definition}
  Функция \(f\colon [a, b] \to \R\) называется функцией ограниченной вариации, если \(V_a^b(f) <
  +\infty\) .
\end{definition}

Такие функции обозначаются как \(f \in V[a, b]\).

Рассмотрим свойства таких функций.

\begin{statement}
  Если \(f\) монотонна, то \(V_a^b(f) = f(b) - f(a)\).
\end{statement}

\begin{statement}
  \(V_a^b(f) \ge |f(b) - f(a)|\).
\end{statement}

\begin{statement}[аддитивность]
  Если \(c \in (a, b)\), то \(V_a^b(f) = V_a^c(f) + V_c^b(f)\).
\end{statement}

Напомним, что
\begin{notation}
  \(f \in C^1[a, b] \iff f'\) непрерывна на \([a, b]\).
\end{notation}

\begin{statement}
  Если \(f \in C^1[a, b]\), то \(\int_a^b |f'(x)| dx = V_a^b(f)\).
\end{statement}

Необязательно, что область определения функции была замкнутой:
\begin{definition}
  Если \(f\colon (a, b) \to \R\), то \(V_a^b(f) \coloneqq \sup_{[\alpha, \beta] \subset (a, b)}
  V_\alpha^\beta(f)\).
\end{definition}

\begin{statement}
  Если \(f, g \in V[a, b]\), то и \(\alpha f + \beta g \in V[a, b]\).
\end{statement}

\begin{statement}
  Если \(f \in V[a, b]\), то \(f\) ограничена.
\end{statement}

\begin{statement}
  Если \(f, g \in V[a, b]\), то и \(fg \in V[a, b]\).
\end{statement}

Достаточно показать это только для \(f^2\).

\begin{theorem}
  Любая функция ограниченной вариации --- сумма двух монотонных функций:  \(f \in V[a, b] \iff f =
  \varphi + \psi\), где \(\varphi, \psi\) монотонные.
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\implies}\)] Покажем, что \(\varphi(x) = V_a^x(f)\), \(\psi(x) = f(x) -
      V_a^x(f)\).

      Понятно, что \(\varphi\) монотонна; с другой стороны, если \(x \ge y\), то \(\psi(x) - \psi(y)
      = f(x) - f(y) - V_y^x(f) < 0\) --- \(\psi\) тоже монотонна.

    \item[\(\boxed{\impliedby}\)] Очевидно.
  \end{pfparts}
\end{proof}

\begin{theorem}
  Если \(f \in C[a, b] \cap V[a, b]\), то и \(V_a^x(f) \in C[a, b]\).
\end{theorem}
\begin{proof}
  Покажем, например, для непрерывности слева: пусть \(x_0 \in (a, b]\), а \(V_a^x(f)\) по предположению не
  непрерывна слева --- \(\exists c > 0\colon \forall x \in [a, x_0) \quad V_x^{x_0}(f) > c\)
  (вариация монотонна).

  \(\exists \delta > 0\colon |x - x_0| < \delta \implies |f(x) - f(x_0)| < \frac{c}{2}\);
  \(V_x^{x_0}(f) > c \implies \exists \Delta_y\colon \sum_{k = 1}^n |f(y_k) - f(y_{k - 1})| > c\)
  --- в частности, \(|f(y_{n - 1}) - f(x_0)| < \frac{c}{2}\).

  Пусть \(x_2 \coloneqq y_{n - 1}\): \(V_{x_1}^{x_2} \ge \frac{c}{2}\) --- ``вытаскивая'' таким
  образом \(x_3, \ldots, x_n\), получится, что \(V_{x_1}^{x_0}(f) = \sum_{k = 2}^\infty V_{x_{k -
  1}}^{x_k} = +\infty\).
\end{proof}

\begin{exercise}
  Проверить, имеет ли функция Ван дер Вардена ограниченную вариацию.
\end{exercise}

Пусть \(f, g \in [a, b] \to \R\).

\begin{definition}
  Интегральная сумма Римана-Стилтьеса \(S(f, dg, \Delta) \coloneqq \sum_{k = 1}^n f(\xi_k)(g(x_k) -
  g(x_{k - 1}))\).
\end{definition}

\begin{definition}
  Функция \(f\) интегрируема по \(g\) в смысле Римана-Стилтьеса, если \(\exists I \colon \forall
  \varepsilon > 0 \exists \delta\colon d(\Delta) < \delta \implies \left| S(f, dg, \Delta) -
  I\right| < \varepsilon\)
\end{definition}

\begin{notation}
  \(I = \int_a^b f(x) dg(x)\).
\end{notation}

\begin{example}
  Если \(f \equiv 1\), то \(\forall g \quad \int_a^b f(x) dg(x) = g(b) - g(a)\).
\end{example}

\begin{theorem}[критерий Коши]
  \(f\) интегрируема по \(g\) тогда и только тогда, когда \(\forall \varepsilon > 0 \exists \delta >
  0\colon d(\Delta') < \delta, d(\Delta'') < \delta \implies |S(f, dg, \xi', \Delta') - S(f, dg,
  \xi'', \Delta'')| < \varepsilon\).
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\implies}\)] По неравенству треугольника.

    \item[\(\boxed{\impliedby}\)] Взяв равномерное разбиение, где \(d(\Delta_n) = \frac{b -
      a}{2^n}\), последовательность \(\{S(f, dg, \xi_n, \Delta_n)\}_n\) фундаментальная.
  \end{pfparts}
\end{proof}

\begin{remark}
  В отличии от интеграла Римана, в интеграле Римана-Стилтьеса функция \(f\) может быть неограничена.
\end{remark}

\begin{remark}
  \(g\) также не обязана быть ограниченной.
\end{remark}

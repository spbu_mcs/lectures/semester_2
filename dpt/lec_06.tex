\lecture{6}{22.03.2024, 13:40}{}

\section{Нормальное распределение}

Чтобы понять, что конкретно означает интегральная теорема Муавра-Лапласа, перепишем подвероятностное
выражение, вычитая \(np\) и деля на \(\sqrt{n}\): \[
  \P\left( np + c\sqrt{n} \le S_n \le np + d\sqrt{n} \right) = \P\left( c \le \frac{S_n -
  np}{\sqrt{n}} \le d \right) \xrightarrow{n \to \infty} \ldots
.\]

Известно, что \(\E{S_n} = np\), а \(\D{S_n} = np(1 - p)\), а значит \[
\begin{aligned}
  \E\left( \frac{S_n - np}{\sqrt{n}} \right) &= 0 \\
  \D\left( \frac{S_n - np}{\sqrt{n}} \right) &= \D\left( \frac{S_n}{\sqrt{n}} \right) = p(1 - p).
\end{aligned}
\]

Это наводит на следующее фундаментальное определение.
\begin{definition}
  Случайная величина \(X\) имеет нормальное распределение с параметрами \(a \in \R\) и \(\sigma^2 >
  0\), если \[
    \forall c < d \quad \P\left( c \le X \le d \right) =
    \frac{1}{\sqrt{2\pi}\sigma}\int_c^d\exp\left( -\frac{(u - a)^2}{2\sigma^2} \right) \df{u}.
  \]
\end{definition}

\begin{notation}
  \(\mathcal{N}(a, \sigma^2)\) --- нормальное распределение.
\end{notation}

\begin{statement}
  Если \(X \sim \mathcal{N}(a, \sigma^2)\), то \(\E{X} = a\), \(\D{X} = \sigma^2\).
\end{statement}

Но что всё это вообще значит?

Рассмотрим частный случай, когда \(a = 0\): \[
  \P\left( c \le X \le d \right) =
  \frac{1}{\sqrt{2\pi}\sigma}\int_c^d\exp\left( -\frac{u^2}{2\sigma^2} \right) \df{u}.
\]

Тогда \[
  \P\left( c \le \frac{S_n - np}{\sqrt{n}} \le d\right) \xrightarrow{n \to \infty} \P\left( c \le X
  \le d\right),
\] где \(X \sim \mathcal{N}\left(0, p(1 - p)\right)\).

По-другому говоря, \(S_n\) по распределению близко к \(np + \sqrt{n}X\).

\begin{remark}
  В рамках дискретной теории вероятности нормальных случайных величин не существует.
\end{remark}

Пусть \(X_1, X_2, ...\) --- независимые одинаково распределенные случайные величины, \(\E{X_j} =
a\), \(\D{X_j} = \sigma^2 > 0\). Положим, что \(S_n \coloneqq \sum_{j = 1}^n X_j\).

\begin{theorem}[Леви]
  \[
    \forall c < d \quad \P\left( c \le \frac{S_n - na}{\sqrt{n}} \le d \right) \xrightarrow{n \to
    \infty} \frac{1}{\sqrt{2\pi} \sigma}\int_c^d \exp\left( -\frac{u^2}{2\sigma^2} \right) \df{u} =
    \P\left( c \le Y \le d \right),
  \] где \(Y \sim \mathcal{N}(0, \sigma^2)\).
\end{theorem}

Для доказательства нужен математический аппарат серьезнее текущего.

То есть оказывается, что нормальное распределение оказывается предельным для сумм \textit{любых}
н.о.р случайных величин.

Теорему Муавра-Лапласа можно рассматривать как частный случай теоремы Леви: \(S_n = \sum_{j = 1}^n
\mathbb{1}_{A_j}\), где \(A_j\) --- успех на испытании \(j\); \(\E\mathbb{1}_{A_j} = p\),
\(\D\mathbb{1}_{A_j} = p(1 - p)\) --- в качестве параметров достаточно взять \(a = p\), \(\sigma^2
= p(1 - p)\).

\section{Цепи Маркова}

\begin{definition}
  \(\mathcal{X}\) --- фазовое пространство (пространство состояний); \(\left| \mathcal{X} \right|
  \le \left| \N \right|\).
\end{definition}

\begin{definition}
  Время --- целые неотрицательные числа.
\end{definition}

\begin{definition}
  Цепь Маркова --- последовательность \(X_0, X_1, \ldots\), где \(X_j\colon \Omega \to
  \mathcal{X}\) --- случайная величина, причем \(\P\left( X_{n + 1} = x \mid X_0 = x_1, \ldots, X_n
  = x_n\right) = \P\left( X_{n + 1} = x_{n + 1} \mid X_n = x_n \right)\).
\end{definition}

То есть вероятность перехода в точку \(X_{n + 1}\) никак не должна зависеть от попадания в точки до
этого.

\begin{example}
  Вспомним случайное блуждание: \(\mathcal{X} = \N\), \(\P\left( n \mapsto n + 1 \right) = p\),
  \(X_0 = 0\) --- цепь Маркова.
\end{example}

\begin{example}
  Пусть есть некоторый прибор, где \(\mathcal{X} = \left\{ \text{работает}, \text{не работает}
  \right\}\).

  Тогда есть 4 возможных состояния:
  \begin{enumerate}
    \item работает и будет работать;
    \item не работает и его не починят;
    \item работает и сломается;
    \item не работает и его починят.
  \end{enumerate}

  Даже для такого, казалось бы маленького фазового пространства, цепи Маркова имеют применения.
\end{example}

\begin{example}
  Возьмем связных неориентированный граф случайной структуры (\(\mathcal{X} = V\)) и рассмотрим
  случайное блуждание на нем: вероятность перехода из текущей вершины \(v\) в инцидентную ей вершину
  --- \(\frac{1}{\deg{v}}\). Такое блуждание --- тоже цепь Маркова.
\end{example}

\begin{example}
  Вспомним легенду о Сизифе. Расположим на вертикальной оси множество неотрицательных чисел (высота
  от подножия горы). Тогда \(p_n\) --- вероятность затащить камень \(n \mapsto n + 1\); \(1 - p_n\)
  --- вероятность упасть к подножию.
\end{example}

Что нужно для определения цепи Маркова?

Из определения, \(\P\left( X_{n + 1} = y \mid X_n = x \right) = p(x, y, n)\) --- некоторая функция
трех аргументов.

Будем считать, цепь одного момента \(n\) (вероятности каждых последующих переходов одинаковы).

\begin{notation}
  \(p(x \leadsto y)\) --- вероятность перехода из \(x\) в \(y\).
\end{notation}

\begin{statement}
  \(p(x \leadsto y) \ge 0\), \(\sum_y p(x \leadsto y) = 1\).
\end{statement}
\begin{proof}
  \[
  \begin{aligned}
    \sum_y p(x \leadsto y) &= \sum_y \P\left( X_{n + 1} = y \mid X_n = X_n = x \right) = \sum_y
    \frac{\P\left( X_{n + 1} = y, X_n = x \right)}{\P\left( X_n = x \right)} \\
    &= \frac{\P(X_n = x)}{\P(X_n = x)} = 1.
  \end{aligned}
  \]
\end{proof}

\begin{notation}
  \(\pi_0(x) \coloneqq \P(X_0 = x)\) (\(x \in \mathcal{X}\)) --- вероятность начала фазового
  пространства именно с точки \(x\).
\end{notation}

\begin{statement}
  \(\pi_0\colon \mathcal{X} \to [0, 1]\), \(\pi_0(x) \ge 0\), \(\sum_{x \in \mathcal{X}} \pi_0(x) =
  1\).
\end{statement}

\(\left( \left\{ \pi_0(x) \right\}_x, \left\{ p(x \leadsto y) \right\}_{x, y} \right)\) полностью
определяют цепь Маркова: для этого достаточно следующего утверждения.

\begin{statement}
  \(\forall x_0, x_1, \ldots, x_n \quad \P\left( X_0 = x_0, X_1 = x_1, \ldots, X_n = x_n \right) =
  \pi_0(x)p(x_0 \leadsto x_1)\ldots p(x_{n - 1} \leadsto x_n)\).
\end{statement}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[База] Очевидно.

    \item[Шаг индукции] Пусть утверждение верно для \(n\).

      \[
      \begin{aligned}
        p(x_n \leadsto x_{n + 1}) &\coloneqq \P\left( X_{n + 1} = x_{n + 1} \mid X_n = x_n \right) =
        \P\left( X_{n + 1} = x_{n + 1} \mid X_0 = x_0, \ldots, X_n = x_n \right) \\
        &= \frac{\P\left( X_0 = x_0, \ldots, X_{n + 1} = x_{n + 1} \right)}{\P\left( X_0 = x_0, X_1
        = x_1, \ldots, X_n = x_n\right)} = \frac{\P\left( X_0 = x_0, \ldots, X_{n + 1} = x_{n + 1}
        \right)}{\pi_0(x_0)p(x_0 \leadsto x_1) \ldots p(x_{n - 1} \leadsto x_n)}.
      \end{aligned}
      \]
  \end{pfparts}
\end{proof}

А когда такие цепи существуют вовсе?

\begin{theorem}[существования]
  Если \(\pi_0\left( \cdot \right)\) и \(p(\cdot \leadsto \cdot)\) таковы, что \(\pi_0(x) \ge 0\),
  \(\sum_{x \in \mathcal{X}} \pi_0(x) = 1\), \(p(x \leadsto y) \ge 0\) и \(\sum_y p(x \leadsto y) =
  1\), то существует цепь Маркова с такими параметрами.
\end{theorem}

\subsection{Матричный формализм}

Элементы рассматриваемых здесь матриц будут нумероваться не числами, а элементами фазового
пространства; сами матрицы будут или квадратные \(\mathcal{X} \times \mathcal{X}\) (например,
матрица \(p = \left( p(x \leadsto y) \right)\)), или векторо-строчные \(1 \times \mathcal{X}\)
(например, \(\pi_0 = \left( \pi_0(x) \right)\)).

\begin{remark}
  Матрицы могут и будут счетного размера, однако значения всегда будут оставаться конечными.
\end{remark}

\begin{definition} \mnote{формула перемножения матриц}
  \(p^n(x \leadsto y) \coloneqq \sum_{z \in \mathcal{X}} p^{n - 1}(x \leadsto z)p(z \leadsto y)\).
\end{definition}

По ассоциативности, \(p^{n_1}p^{n_2} = p^{n_1 + n_2}\).

\begin{definition}
  \(\pi_n(x) \coloneqq \P(X_n = x)\).
\end{definition}

Матричный формализм заключается в следующих двух утверждениях.

\begin{statement}
  \(\pi_n = \pi_0 p^n\).
\end{statement}
\begin{proof}
  В предположении верности следующего утверждения, \[
  \begin{aligned}
    \pi_0 p^n(y) &= \sum_x \pi_0(x)p^n(x \leadsto y) = \sum_x \P\left( X_0 = x \right)\P\left( X_n =
    y \mid X_0 = x\right) \\
    &= \sum_x \P\left( X_0 = x \right) \frac{\P\left( X_n = y, X_0 = x \right)}{\P\left( X_0 = x
    \right)} = \P\left( X_n = y \right) \\
    &= \pi_n(y).
  \end{aligned}
  \]
\end{proof}

\begin{statement}
  \(p^n(x \leadsto y) = \P\left( X_n = y \mid X_0 = x \right)\).
\end{statement}
\begin{proof}
  \[
  \begin{aligned}
    \P\left( X_n = y \mid X_0 = x \right) &= \frac{\P\left( X_n = y, X_0 = x \right)}{\P\left( X_0 =
    x\right)} = \frac{\sum_z \P\left( X_n = y, X_{n - 1} = z, X_0 = x \right)}{\P\left( X_0 = x
    \right)} \\
    &= \frac{\sum_z \P\left( X_n = y \mid X_{n - 1} = z, X_0 = x \right)\P\left( X_{n - 1} = z, X_0
    = x \right)}{\P\left( X_0 = x \right)},
  \end{aligned}
  \] --- сделаем вид (а чуть позже покажем, что так можно), что \(X_0\) не играет роли; тогда \[
  \begin{aligned}
    \ldots &= \sum_z p(z \leadsto y)\P\left( X_{n - 1} = z \mid X_0 = x \right).
  \end{aligned}
  \]

  Всё это время на самом деле доказательство шло по индукции; применяя индукционное предположение,
  \[
  \begin{aligned}
    \ldots &= \sum_z p(z \leadsto y) p^{n - 1}(x \leadsto z) = p^n(x \leadsto y).
  \end{aligned}
  \]
\end{proof}

Из элементарной теории вероятностей,
\begin{lemma}
  Если \(\forall j \quad \P\left( A \mid B_j \right) = q\), то \(\P\left( A \mid B \right) = q\).
\end{lemma}
\begin{proof}
  \[
  \P\left( A \mid B \right) = \frac{\P\left( AB \right)}{\P(B)} = \frac{\sum \P(AB_j)}{\P(B)} = \sum
  \frac{\P\left( A \mid B_j \right)\P(B_j)}{\P(B)} = q.
  \]
\end{proof}

\begin{statement}
  \(\P\left( X_n = y \mid X_{n - 1} = z, X_0 = x \right) = \P\left( X_n = y \mid X_{n - 1} = z
  \right)\).
\end{statement}
\begin{proof}
  Рассмотрим \(v_1, \ldots, v_{n - 1} \in \mathcal{X}\): \[
  \begin{aligned}
    \P\left( X_n = y \mid X_0 = x, X_1 = v_1, \ldots, X_{n - 2} = v_{n - 2}, X_{n - 1} = z \right)
    &\oversym{=}{\text{def}} \P\left( X_n = y \mid X_{n - 1} = z \right) \\
    &= p(z \leadsto y) = q.
  \end{aligned}
  \]

  Такое \(q\) постоянно для любого из переходов; возьмем объединение всех таких переходов и
  воспользуемся леммой: \[
    \P\left( X_n = y \mid X_0 = x, X_{n - 1} = z \right) = q = p(z \leadsto y)
  .\]
\end{proof}

\lecture{26}{22.05.2024, 13:40}{14:42}

\begin{example}
  Пусть \(G\) --- верхнетреугольные обратимые матрицы; \(G \leqslant \GL_n\).

  Пусть \(H\) --- верхнетреугольные обратимые, но по диагонали обязательно стоят \(1\); \(K
  \leqslant \GL_n\) --- диагональная матрица.

  Тогда \(G = H \rtimes K\).
\end{example}

Зададим теперь внешнее полупрямое произведение: пусть \(H\) и \(K\) --- группы, а \(\varphi\colon K
\to \mathrm{Aut}(H)\) --- гомоморфизм.

\begin{definition}
  Внешнее полупрямое произведение \(H \rtimes_\varphi K \coloneqq H \times K\) как множества, с
  операцией \((hk)(h'k') \coloneqq (h\varphi(k)(h'), kk')\).
\end{definition}

\begin{statement}
  \(H \rtimes_\varphi K\) --- группа.
\end{statement}
\begin{proof}
  Проверим только ассоциативность --- всё остальное не сложно, и можно сделать самому.

  \(\left( (h, k)(h', k') \right)(h'', k'') = (h\varphi(k)(h'), kk')(h'', k'') =
  (h\varphi(k)(h')\varphi(kk')(h''), kk'k'')\); с другой стороны, \((h, k)\left( (h', k')(h'', k'')
  \right) = (h, k)(h'\varphi(k')(k''), k'k'') = (h\varphi(k))(h'\varphi(k')(h''), kk'k'')\). Так как
  \(\varphi\) --- гомоморфизм, то \(\varphi(kk') = \varphi(k) \circ \varphi(k')\) и
  \(\varphi(k)(h')\varphi(kk')(h'') = \varphi(k)(h')\varphi(k)(\varphi(k')(h''))\).
\end{proof}

\begin{statement}
  \(\left\{ (h, 1) \right\} \trianglelefteq H \rtimes_\varphi K\).
\end{statement}
\begin{proof}
  Будем проверять не непосредственно, а придумаем гомоморфизм такой, что \(\left\{ (h, 1) \right\}\)
  --- его ядро: например, \(H \rtimes_\varphi K \xrightarrow{(h, k) \mapsto k} K\) --- гомоморфизм
  групп.
\end{proof}

\begin{statement}
  \(H \cong \left\{ (h, 1) \right\}\).
\end{statement}

\begin{statement}
  \(K \cong \left\{ (1, k) \right\} \leqslant H \rtimes_\varphi K\).
\end{statement}

Заметим, что они пересекаются по \(\left\{ (1, 1) \right\}\):
\begin{statement} \mnote{\((h, k) = (h, 1) \cdot (1, k)\)}
  Внешнее полупрямое произведение является и внутренним полупрямым произведением.
\end{statement}

\begin{theorem}
  Если  \(G = H \rtimes K\), то \(G \cong H \rtimes_\varphi K\), где \(\varphi(k)(h) \coloneqq
  khk^{-1}\).
\end{theorem}
\begin{proof}
  Построим гомоморфизм \(\alpha\colon H \rtimes_\varphi K \xrightarrow{(h, k) \mapsto hk} G\):
  проверяется, что это гомоморфизм (см. ассоциативность).

  \(\Imf{\alpha} = G\), так как любой элемент представляется в виде \(hk\); \(\ker{\alpha} = \left\{
  (1, 1) \right\}\), так как \(hk = 1 \implies h = 1, k = 1\) (\(H \cap K = \left\{ 1 \right\}\))
  --- по теореме о гомоморфизме получается искомое.
\end{proof}

Пусть \(H \trianglelefteq G\); при каком условии \(G \cong H \rtimes K\) для некоторого \(K\)?

Если \(K\) вовсе существует, то \(K \cong \qfrac{G}{H}\).

\begin{statement}
  Если \(H \trianglelefteq G\), то \(G = H \rtimes K\) для некоторого \(K\) тогда и только тогда,
  когда \(\exists s\colon \qfrac{G}{H} \to G\) (гомоморфизм) такая, что \(\pi \circ s = \id\) (где
  \(\pi\colon G \to \qfrac{G}{H}\) --- проекция).
\end{statement}

\begin{remark}
  Для множеств это эквивалентно аксиоме выбора, но в группах она не помогает.
\end{remark}

\begin{proof}[Доказательство утверждения]
  Если \(G = H \rtimes K\), то \(K \xrightarrow{k \mapsto (1, k)} H \rtimes K\) --- гомоморфизм
  групп.

  И наоборот, если есть такое \(s\), то \(G = H \rtimes s\left( \qfrac{G}{H} \right)\).
  \begin{enumerate}
    \item Образ \(s\) --- подгруппа.

    \item Проверим тривиальность пересечения.

      Пусть, вдруг, \(s(x) \in H\): тогда \(\pi \circ s(x) = 1\) и \(x = \pi \circ s(x)\) --- всё
      сходится.

    \item Проверим, что этим всё порождается: \(\forall g \in G \quad g = gs\left( \pi(g)
      \right)^{-1} \cdot \underbrace{s(\pi(g))}_{\in \Imf{s}}\). Первый множитель попадает в \(H\),
      ведь \(\pi\left( gs\left( \pi(g) \right)^{-1} \right) = \pi(g) \cdot \pi \circ s\left(
      \pi(g)^{-1} \right) = \pi(g)\pi(g)^{-1} = 1\).
  \end{enumerate}
\end{proof}

\begin{remark}
  Дополнительно подчеркнем, что это не происходит само по себе.

  \mnote{\(C_{100} \not\cong C_{10} \times C_{10}\) --- при сложении складываются не только цифры,
  но еще происходит перенос через разряд}
  Как известно, \(C_{10} \trianglelefteq C_{100}\), \(\qfrac{C_{100}}{C_{10}} \cong C_{10}\), но
  гомоморфизма \(C_{100} \to C_{10}\) не найдется.
\end{remark}

Напоследок экспромтом попробуем обсудить следующую
\begin{theorem}
  Если \(p < q\) --- простые числа, \(|G| = pq\), то
  \begin{enumerate}
    \item \(G \cong C_q \rtimes_\varphi C_p\);
    \item \(G \cong C_{pq}\) если \(q \not\equiv_p 1\).
  \end{enumerate}
\end{theorem}
\begin{proof}
  Возьмем \(g \in G\); если \(g = 1\) то тривиально, если \(\mathrm{ord}(g) = pq\), то \(G \cong
  C_{pq}\).

  \mnote{с использованием т. Лагранжа}
  Если \(\mathrm{ord}(g) = p\), \(k\) подгрупп \(\cong C_p\); если \(\mathrm{ord}(g) = q\), то \(l\)
  подгрупп \(\cong C_q\); хотим \(l = 1\).

  \(pq = 1 + k(p - 1) + l(q - 1)\), \(p < q\) --- попробуем его решить.

  \(p \equiv_{q - 1} 1 + k(p - 1)\), то есть \((q - 1) \divs (p - 1)(k - 1)\), и, аналогично, \((p -
  1) \divs (q - 1)(l - 1)\).

  Облом --- только этого равенства недостаточно, так как есть контрпримеры.

  Сымитируем доказательство теоремы Силова (из которой эта теорема сразу следует). Пусть \(G\)
  действует сопряжением на множестве подгрупп, изоморфных \(C_q\).

  Орбит может быть несколько; стабилизатор каждой --- подгруппа в \(G\), причем каждый стабилизатор
  может иметь порядок \(pq\), \(p\) или \(q\) (не может быть \(1\), ведь иначе \(l = pq\)).

  Предположим, что \(l \ge p\): тогда \(k = 1\).

  ...Магия...

  Тогда, раз \(l = 1\), \(C_q\) нормальная и \(G \cong C_q \rtimes C_p\), где \(C_p\) произвольная.
\end{proof}

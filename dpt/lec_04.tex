\lecture{4}{12.03.2024, 13:40}{}

\begin{theorem}[Вейерштрасса]
  Если \(f\colon [0, 1] \to \R\) непрерывна, то \(\forall \varepsilon > 0 \exists\) многочлен \(Q\)
  такой, что \(\max_{0 \le x \le 1}\left| f(x) - Q(x) \right| < \varepsilon\).
\end{theorem}
\begin{proof}
  Сделаем, как будет видно в будущем, удобную замену: \(x \mapsto p\).

  Рассмотрим схему Бернулли \((n, p)\) с числом успехов \(S_n\): \(S_n = 0, 1, 2, \ldots, n\), а
  значит, \(\frac{S_n}{n} = 0, \frac{1}{n}, \ldots, 1\).

  Тогда \(\E{f\left( \frac{S_n}{n} \right)} = \sum_{k = 0}^n C_n^k p^k (1 - p)^{n - k} f\left(
  \frac{k}{n} \right)\) --- некоторый многочлен \(Q_n(p)\).

  С другой стороны, \(f\left( \E\frac{S_n}{n} \right) = f(p)\): \[
    \begin{aligned}
      \left| f(p) - Q_n(p) \right| &= \left|f\left( \E\frac{S_n}{n} \right) - \E{f\left( \frac{S_n}{n}
      \right)}\right| \\
      &= \left| \E\left( f\left(\E \frac{S_n}{n} \right) - f\left( \frac{S_n}{n} \right) \right)
      \right| \le \E\left| f(p) - f\left( \frac{S_n}{n} \right)\right|,
    \end{aligned}
  \] --- разделим это математическое ожидания на два случая: \[
    \ldots = \E\left| f(p) - f\left( \frac{S_n}{n} \right) \right| \mathbb{1}_{\left| \frac{S_n}{n}
    - p\right| > \delta} + \E\left| f(p) - f\left( \frac{S_n}{n} \right) \right| \mathbb{1}_{\left|
    \frac{S_n}{n} - p \right| \le \delta}.
  \]

  Введем дополнительные обозначения: \(M \coloneqq \max_{0 \le x \le 1} |f(x)|\), \(\omega_\delta(f)
  \coloneqq \max_{|x - y| = \delta} |f(x) - f(y)|\) --- поскольку \(f\) непрерывна, то
  \(\omega_\delta(f) \xrightarrow{\delta \to 0} 0\). Продолжим оценивать: \[
    \ldots \le 2M\P\left( \left|\frac{S_n}{n} - p\right| > \delta \right) + \omega_\delta(f)
  .\]

  Добивающим ударом станет закон больших чисел вместе с применением свойств дисперсии (все испытания
  равноправны) и оценки максимума \(p(1 - p)\): \[
    \begin{aligned}
      \ldots \le 2M\frac{\D\left( \frac{S_n}{n} \right)}{\delta^2} + \omega_\delta(f) &=
      2M\frac{\D{S_n}}{n^2\delta^2} + \omega_\delta(f) = 2M\frac{p(1 - p)}{n\delta^2} +
      \omega_\delta(f) \\
      &\le \frac{M}{2n\delta^2} + \omega_\delta(f).
    \end{aligned}
  \]

  Выберем \(\delta\) такую, что \(\omega_\delta(f) \le \frac{\varepsilon}{2}\) и \(n\) такое, что
  \(\frac{M}{2n\delta^2} \le \frac{\varepsilon}{2}\): \[
    \ldots \le \varepsilon
  .\]
\end{proof}

\section{Производящие функции и ветвящиеся процессы}

Пусть \(X\) --- случайная величина; \(X \in \left\{ 0, 1, \ldots \right\}\). Каждой такой случайной
величине можно сопоставить функцию \[
  \varphi_X(z) \coloneqq \sum_{k = 0}^\infty \P\left( X = k \right)z^k = \sum_{k = 0}^\infty p_kz^k
.\]

\begin{definition}
  \(\varphi_X(z)\) называется производящей функцией (моментов).
\end{definition}

По-английски, \textit{moment generating function}.

При каких \(z\) такая функция определена? Раз \(\sum p_k = 1\), то
\begin{statement}
  Ряд производящей функции сходится абсолютно при \(|z| \le 1\).
\end{statement}

\begin{statement}
  \(\varphi_X\) выпукла как сумма выпуклых функций и монотонно возрастает.
\end{statement}

\begin{statement}
  \(\varphi_X(z) = \E{z^X}\).
\end{statement}

Рассмотрим, какими вероятностными свойствами обладает эта функция.

\begin{statement}
  Если \(X\) и \(Y\) независимы, то \(\varphi_{X + Y}(z) = \varphi_X(z) \varphi_Y(z)\).
\end{statement}
\begin{proof}
  \(\varphi_{X + Y}(z) = \E{z^{X + Y}} = \E\left( z^X z^Y \right) = \E{z^X}\E{z^Y}\).
\end{proof}

\begin{consequence}
  Если \(X_1, \ldots, X_n\) --- независимые одинаково распределенные случайные величины, а \(S
  \coloneqq \sum X_j\), то \(\varphi_S(z) = \prod \varphi_{X_j}(z) = \varphi_{X_1}^n(z)\).
\end{consequence}

Вернемся к определению и ``грубо продифференцируем'': \[
  \varphi_X'(z) = \sum_{k = 1}^\infty kp_kz^{k - 1}
,\] --- подставляя \(1\), получается, что
\begin{statement}
  \[
    \varphi_X'(1) = \sum_{k = 1}^\infty kp_k = \E{X}
  .\]
\end{statement}

Попробуем сделать так еще раз: \[
  \varphi_X''(z) = \sum_{k = 2}^\infty k(k - 1)p_kz^{k - 2}
,\] --- тогда
\begin{statement}
  \[
    \varphi_X''(1) = \E{X(X - 1)} = \E{X^2} - \E{X}
  .\]
\end{statement}

\begin{consequence}
  \[
    \E{X^2} = \varphi''_X(1) + \E{X} = \varphi_X'(1) + \varphi_X''(1)
  .\]
\end{consequence}

Продолжая в том же духе,
\begin{theorem}
  \[
    \varphi_X^{(l)}(1) = \E\left[ X(X - 1) \ldots (X - (l - 1)) \right]
  .\]
\end{theorem}

\begin{definition}
  Выражение под математическим ожиданием называется факториальным моментом.
\end{definition}

А почему так можно было делать изначально? Покажем это для случая \(l = 1\).
\begin{proof}
  Распишем предел производной в явном виде: \[
    \begin{aligned}
      \frac{\varphi(1) - \varphi(z)}{1 - z} &= \frac{\sum_{k = 0}^\infty p_k - \sum_{k = 0}^\infty
      p_kz^k}{1 - z} = \sum_{k = 0}^\infty p_k\left(1 + z + \ldots + z^{k - 1}\right) \\
      &\oversym{\le}{z \le 1} \sum_{k = 0}^\infty kp_k = \E{X}.
    \end{aligned}
  \]

  С другой стороны, \[
    \begin{aligned}
      \sum_{k = 0}^\infty p_k\left( 1 + z + \ldots + z^{k - 1} \right) &\ge \sum_{k = 0}^M kp_kz^{k
      - 1} \xrightarrow{z \to 1} \sum_{k = 0}^M kp_k \\ &\xrightarrow{M \to \infty} \sum_{k =
      0}^\infty kp_k = \E{X}.
    \end{aligned}
  \]
\end{proof}

А что если количество случайных величин случайно? Пусть \(X_1, X_2, \ldots\) --- независимые и
одинаково распределенные величины \(\in \N_0\); \(N \in \N_0\) --- независящая от всех \(X_j\)
случайная величина.

Положим, что \(S_N = \sum_{j = 1}^N X_j\); сразу оговорим, что если \(N = 0\), то \(S_N \coloneqq
0\).

\[
  \begin{aligned}
    \varphi_{S_N}(z) &= \sum_{k = 0}^\infty \P\left( S_N = k \right)z^k = \sum_{k = 0}^\infty
    \sum_{l = 0}^\infty \P(N = l, \underbrace{S_l}_{\sum_{j = 1}^l X_j} = k)z^k \\
    &\oversym{=}{\text{незав.}} \sum_{k = 0}^\infty \sum_{l = 0}^\infty \P(N = l)\P(S_l = k)z^k =
    \sum_{l = 0}^\infty \P(N = l) \sum_{k = 0}^\infty \P(S_l = k)z^k \\
    &= \sum_{l = 0}^\infty \P(N = l) \varphi_{S_l}(z) = \sum_{l = 0}^\infty \P(N = l)
    \varphi_{X_1}^l(z) \\
    &= \varphi_N\left( \varphi_{X_1}(z) \right) = \left(\varphi_N \circ \varphi_{X_1}\right)(z).
  \end{aligned}
\]

В частности, если \(N \equiv n\), то будет уже раннее полученная формула: \(\varphi_S(z) =
\varphi_{X_1}^n(z)\).

\subsection{Ветвящиеся процессы}

А точнее говоря, мы будем рассматривать только процессы Гальтона-Ватсона, хотя есть и другие.

Гальтон и Ватсон интересовались вероятностыми вопросами в сфере генеалогии аристократических семей,
а через 50 лет оказалось, что эта модель также может использоваться для описания процессов ядерной
бомбы.

Положим, что \(M_0, M_1, \ldots, M_n\) --- число потомков поколения \(n\), а \(p_0, p_1, \ldots\)
--- вероятности иметь \(j\) потомков.

Понятно, что \(M_0 = 1\), \(M_{n + 1} = \sum_{j = 1}^{M_n} X_{n, j}\), где \(X_{n, j}\) ---
независимо одинаково распределенные случайные величины; \(\P\left( X_{n, j} = k \right) = p_k\).

Всё это затевалось для того, чтобы понять вероятность вырождения рода; пусть \(q_n \coloneqq \P(M_n
= 0)\). Видно, что последовательность \(\left\{ q_n \right\}\) неубывает, а значит имеет предел.

\begin{definition}
  \(q \coloneqq \lim_{n \to \infty} q_n\) называется вероятностью вырождения.
\end{definition}

\begin{definition}
  Процесс вырождается, если \(q = 1\).
\end{definition}

\begin{definition}
  Процесс не вырождается, если \(q < 1\).
\end{definition}

``Задача о вырождении'' заключается в определении \(q\) с заранее заданными \(p_0, p_1, \ldots\).

Введем (найдем) производящие функции.

\mnote{\(\varphi(z) = \sum p_kz^k\)}
\[
  \varphi_{M_{n + 1}}(z) = \varphi_{M_n}\left( \varphi(z) \right)
.\]

Расписывая, \[
  \begin{aligned}
    \varphi_{M_0}(z) &= z \\
    \varphi_{M_1}(z) &= \varphi(z) \\
    \vdots \\
    \varphi_{M_n}(z) &= \underbrace{\left(\varphi \circ \varphi \ldots \circ \varphi\right)}_{n}(z).
  \end{aligned}
\]

\begin{theorem}
  Вероятность вырождения \(q\) равна наименьшему корню уравнения \(\varphi(z) = z\).
\end{theorem}
\begin{proof}
  Как минимум \(z = 1\) --- корень; а раз \(\varphi(z)\) непрерывна, то множество корней замкнуто
  \mnote{прообраз \(0\)} и найдется наименьший.

  \(q_n = \P(M_n = 0) = \varphi_{M_n}(0)\), а по выведенному выше, \mnote{степень --- композиция} \[
    q_{n + 1} = \varphi_{M_{n + 1}}(0) = \varphi\left( \varphi^m(0) \right) = \varphi(q_n)
  .\]

  Раз \(\varphi\) непрерывна, то перейдем к пределу: \[
    q = \varphi(q)
  ,\] --- некоторый корень уравнения \(\varphi(z) = z\).

  Осталось показать, что \(q\) не превосходит никакого другого корня. Положим, что \(z_*\) ---
  некоторый корень.

  Тогда \(q_0 \coloneqq 0 \le z_* \implies q_1 = \varphi(q_0) \le \varphi(z_*) = z_*\) ---
  продолжая, \(q_n \le z_* \quad \forall n\), \(q < z_* \quad n \to \infty\).
\end{proof}

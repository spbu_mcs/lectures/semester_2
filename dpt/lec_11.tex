\lecture{11}{26.04.2024, 13:40}{}

\begin{pfparts}
  \item[\(\boxed{p \neq q}\)] Тогда \(m(x) = \frac{1}{p - q}\left( \frac{(B - A)(r^x - r^A)}{r^B -
      r^A} + A - x \right)\), где \(r = \frac{q}{p} \neq 1\); в частности, \(m(0) = \frac{1}{p -
      q}\left( \frac{(B - A)(1 - r^A)}{r^B - r^A} + A \right)\).
\end{pfparts}

Здесь вывод этой формулы не представлен, однако он есть в других источниках и будет чуть позднее в
этой лекции в схожей задаче.

Мы выяснили, какой будет ожидаемая продолжительность игры; изучим теперь вероятности разорения
игроков.

Пусть \(\beta_k(x)\) --- вероятность достичь \(B\) раньше, чем \(A\) не позднее, чем за \(k\) шагов.
Аналогично определим \(\alpha_k(x)\).

\begin{statement}
  \(\alpha_k(x) + \beta_k(x) \le 1\).
\end{statement}

\begin{statement}
  \(\beta_k(B) = 1\), \(\beta_k(A) = 0\) и аналогично для \(\alpha\).
\end{statement}

\begin{statement}
  \(\beta_k(x) = p\beta_{k - 1}(x + 1) + q\beta_{k - 1}(x - 1)\) при \(A < x < B\).
\end{statement}

Заметим, что \(\beta_k(x)\) неубывающая по \(k\); \(\beta_k(x) \xrightarrow{k \to \infty} \beta(x)
\quad \forall x\). Подставляя это в уравнение на \(\beta_k(x)\) получится, что
\begin{statement}
  \(\beta(x) = p\beta(x + 1) + q\beta(x - 1)\), \(\beta(A) = 0\), \(\beta(B) = 1\).
\end{statement}

По сути эта \(\beta\) --- вероятность разорения без ограничение на число шагов.

Заметим, что при предельном переходе
\begin{statement} \mnote{несложное упражнение}
  \(\beta(x) + \alpha(x) = 1\).
\end{statement}

Используя это соотношение достаточно найти уравнение для \(\beta(x)\), ведь \(\alpha(x)\) выразится
через него.

Решим уравнение на \(\beta(x)\). Перепишем его в следующем виде: \[
\begin{aligned}
  &(p + q)\beta(x) = p\beta(x + 1) + q\beta(x - 1) \\
  \iff &q(\beta(x) - \beta(x - 1)) = p(\beta(x + 1) - \beta(x)) \\
  \iff &\beta(x + 1) - \beta(x) = \underbrace{\frac{q}{p}}_r(\beta(x) - \beta(x - 1)).
\end{aligned}
\]

Положим, что \(a \coloneqq \beta(A + 1) - \beta(A)\); тогда \(\beta(A + y) - \beta(A + y - 1) =
ar^{y - 1}\). Так как значения в точках \(A\) и \(B\) известны, то \[
\begin{aligned}
  1 &= \beta(B) - \beta(A) = \sum_{y = 1}^{B - A} \left( \beta(A + y) - \beta(A + y - 1) \right) =
  \sum_{y = 1}^{B - A} ar^{y - 1} \\
  &= \sum_{v = 0}^{B - A - 1} ar^v = a \frac{r^{B - A} - 1}{r - 1}.
\end{aligned}
\]

Найдем из этого соотношения \(a\): \[
\begin{aligned}
  &1 = a\frac{r^{B - A} - 1}{r - 1} \iff a = \frac{r - 1}{r^{B - A} - 1}.
\end{aligned}
\]

Из этого можно выразить \(\beta(x)\): \[
\begin{aligned}
  \beta(x) &= \beta(x) - \beta(A) = \sum_{y = 1}^{x - A} \left( \beta(A + y) - \beta(A + y - 1)
  \right) = \sum_{y = 1}^{x - A} ar^{y - 1} \\
  &= \sum_{v = 0}^{x - A - 1} ar^v = \frac{r^{x - A} - 1}{r^{B - A} - 1} = \boxed{\frac{r^x -
  r^A}{r^B - r^A}}.
\end{aligned}
\]

В частности,
\begin{statement}
  \(\beta(0) = \frac{1 - r^A}{r^B - r^A}\) --- вероятность разорения второго игрока (вероятность
  выхода через \(B\)).
\end{statement}

И, как следствие,
\begin{statement}
  \(\alpha(0) = \frac{r^B - 1}{r^B - r^A}\).
\end{statement}

Всё это работает лишь для \(p \neq q\); посмотрим, что будет при \(p = q = \frac{1}{2}\).

Имеется: \(\beta(x + 1) - \beta(x) = \beta(x) - \beta(x - 1) = a\); \(1 = \beta(B) - \beta(A) = (B -
A)a\) из чего получается, что \(a = \frac{1}{B - A} = \frac{1}{B + |A|}\).

Аналогичным образом, \(\beta(x) = \beta(x) - \beta(A) = (x - A)a = \frac{x + |A|}{B + |A|}\). Как
следствие,
\begin{statement}
  При \(p = q\), \(\beta(0) = \frac{|A|}{B + |A|}\) вероятность выигрыша игрока пропорционален его
  капиталу.
\end{statement}

\subsubsection{Закон арксинуса}

Рассмотрим одномерное случайное блуждание с \(p = q = \frac{1}{2}\). Как много раз на его протяжении
оно больше \(0\), и как много меньше \(0\)?

Пусть \(T_n \coloneqq \sum_{k = 0}^{n - 1} \mathbb{1}_{\{S_k \ge 0, S_{k + 1} \ge 0\}}\). Заметим,
что \(\forall k\) верно либо \(\{ S_k \ge 0, S_{k + 1} \ge 0 \}\), либо \(\{ S_k \le 0, S_{k + 1}
\le 0\}\). Изучим, как ведет себя величина \(\frac{T_n}{n} \in [0, 1]\).

\begin{theorem}[закон арксинуса]
  \(\forall a, b \in [0, 1], a < b \quad \P\left( \frac{T_n}{n} \in [a, b] \right) \xrightarrow{n
  \to \infty} \frac{1}{\pi} \int_a^b \frac{\df{x}}{\sqrt{x(1 - x)}}\).
\end{theorem}

Вычисляя этот интеграл, получится \(\frac{1}{\pi}\arcsin(2x - 1)\Big|_a^b =
\frac{2}{\pi}\arcsin(\sqrt{x})\Big|_a^b\) --- в разных источниках пишут по-разному, а сами по себе
эти выражения равны, ведь оба выражения лежат в \([0, \pi]\) и их косинусы равны.

\begin{proof}
  Сделаем несколько наблюдений.

  \begin{enumerate}
    \item Можно рассматривать только четные \(n\): \(n \mapsto 2n\), ведь если они и отличаются, то
      только на \(1\);
    \item Все \(T_{2n} \equiv_2 0\).

      Ясно, что каждый законченный цикл (время пребывания в одной полуплоскости) имеет четное число
      шагов (количество шагов вверх и вниз одинаковы). Последний же цикл может быть незаконченным,
      но он все равно четный, ведь его длина --- разница двух четных чисел (выход из одной
      полуплоскости и нахождение в другой).
  \end{enumerate}

  Утверждается, что \(\P\left(T_{2n} = 2k\right) = \P\left(S_{2k} = 0\right)\P\left(S_{2(n - k)} =
  0\right)\) \mnote{симметрия блуждания} --- доказательство можно посмотреть в открытых источниках
  или в учебнике.

  Как было получено в доказательстве теоремы Пойя, \(\P\left(S_{2k} = 0\right) \sim
  \frac{1}{\sqrt{\pi k}}\), \(\P\left( S_{2(n - k)} = 0 \right) \sim \frac{1}{\sqrt{\pi(n - k)}}\)
  при \(k \to \infty\) и \(n - k \to \infty\).

  Осталось посчитать: \[
  \begin{aligned}
    \P\left( \frac{T_{2n}}{2n} \in [a, b] \right) &= \P\left( T_{2n} \in [2na, 2nb] \right) =
    \sum_{2na \le 2k \le 2nb} \P\left( T_{2n} = 2k \right) \\
    &\sim \sum_{a < \frac{k}{n} < b} \frac{1}{\pi\sqrt{k(n - k)}} = \sum_{a < \frac{k}{n} < b}
    \frac{1}{\pi}\frac{1}{n}\frac{1}{\sqrt{\frac{k}{n}\left( 1 - \frac{k}{n} \right)}},
  \end{aligned}
  \] --- последнее это в точности риманова сумма для функции \(\frac{1}{\sqrt{x(x - 1)}}\), и в
  пределе получается искомое.
\end{proof}

Посмотрим, что этот результат значит на практике.

Функция \(\frac{1}{x(x - 1)}\) на концах своей области определения стремится к бесконечности:
вероятность попадания в интервал одинаковой длины больше, если интервал ближе к концам.

\begin{example}
  \(\P(T_{20} = 0 \lor T_{20} = 20) \approx 0,35\), а \(\P(T_{20} \le 2 \lor T_{20} \ge 18) \approx
  0,537\) --- вероятности неожиданно большие.
\end{example}

Это также объясняет, например, что даже в справедливой игре один из игроков почти всегда будет
выигрывать.

\section{Случайные графы}

Вся жизнь пронизана графами: графы друзей, графы научных публикаций, графы цитирования и так далее.

Практика показывает, что зачастую вероятностные алгоритмы работают с графами эффективнее обычных.

Есть модели двух типов:
\begin{itemize}
  \item статическая --- граф сразу есть и готов к изучению;
  \item динамическая --- граф строится шаг за шагом.
\end{itemize}

\subsection{Модель Эрдеша-Реньи}

Рассмотрим одну очень классическую модель \textit{Эрдеша-Реньи}, зависящую от параметров \(n\) и
\(p\):
\begin{enumerate}
  \item собираем \(n\) вершин;
  \item проводим каждое из возможных ребер с вероятностью \(p\).
\end{enumerate}

Практичнее всего рассматривать такие графы асимптотически, то есть последовательность \(\left\{ G(n,
p_n)\right\}_n\) при \(p_n \to 0\).

Пусть \(G_n\) --- случайный граф, построенный по модели Эрдеша-Реньи с параметрами \((n, p_n)\).
Пусть событие \(X = \{\text{граф } G \text{ связный}\}\).
\begin{theorem}
  Если \(p_n = \frac{c\ln{n}}{n}\) и
  \begin{itemize}
    \item \(c > 1\), то \(\P{X} \xrightarrow{n \to \infty} 1\);
    \item \(c < 1\), то \(\P{X} \xrightarrow{n \to \infty} 0\);
  \end{itemize}
  а если же \(p_n = \frac{\ln{n} + v}{n}\), то \(\P{X} \xrightarrow{n \to \infty} \exp(-\exp(-v))\).
\end{theorem}

\lecture{3}{01.03.2024, 15:25}{}

Рассмотрим применения этой теоремы.

\begin{example}
  Положим, что \(X = \R^3\). Рассмотрим возможные размерности и преобразования.

  \[
  \begin{array}{cccr}
    X_+ & X_- & \bigoplus_{i = 0, \ldots, m} \Pi_i & \text{тип} \\
    1 & 0 & 2 & \text{поворот отн. оси} \\
    0 & 1 & 2 & \text{поворот + ...} \\
    3 & 0 & 0 & \id \\
    2 & 1 & 0 & \text{симм. отн. плоскости} \\
    1 & 2 & 0 & \text{симм. отн. прямой} \\
    0 & 3 & 0 & -\id. \\
  \end{array}
  \]
\end{example}

\section{Три произведения векторов}

\subsection{Ориентация векторного пространства}

Пусть \(X\) --- конечномерное векторное пространство.

Рассмотрим множество всех базисов \(X\) и введем на нем отношение эквивалентности: базисы
эквивалентны, если определитель матрицы перехода больше \(0\).

\begin{statement}
  Это отношение эквивалентности.
\end{statement}

\begin{statement}
  Существует ровно два класса эквивалентности по такому отношению.
\end{statement}

\begin{definition}
  Ориентация \(X\) --- выбор базиса с точностью до эквивалентности.
\end{definition}

\begin{example}
  В \(\R^n\) со стандартным базисом \(e_i \coloneqq (0, \ldots, 0, \underbrace{1}_i, 0, \ldots,
  0)\), ориентация, заданная \(e_i\) --- положительная.
\end{example}

\subsection{Смешанное произведение}

Пусть \(X\) --- ориентированное конечномерное евклидово (или даже векторное) пространство; \(\dim{X}
= n\).

Рассмотрим векторы \(v_1, \ldots, v_n \in X\).

\begin{definition}
  Смешанным произведением векторов \(v_1, \ldots, v_n\) называется определитель матрицы,
  составленной из координат векторов \(v_i\) в некотором положительном ортонормированном базисе
  пространства \(X\), записанных в столбцы.
\end{definition}

\begin{notation}
  \(\mixed{v_1, \ldots, v_n}\) --- смешанное произведение.
\end{notation}

\begin{remark}
  В случае ориентированного пространства, ориентированным считается задающий ориентацию базис.
\end{remark}

\begin{statement}
  Смешанное произведение не зависит от выбора положительного ортонормированного базиса.
\end{statement}
\begin{proof}
  При замене базиса происходит домножение на матрицу перехода, но матрица перехода ортонормированных
  базисов имеет определитель \(\pm 1\), а базисы положительные --- остался только вариант \(+1\).
\end{proof}

Рассмотрим свойства смешанного произведения --- понятно, что они следуют из свойств определителя.

\begin{statement}
  Смешанное произведение линейно по каждому аргументу.
\end{statement}

\begin{statement}
  Смешанное произведение кососиметрично.
\end{statement}

\begin{statement}
  \(\mixed{v_1, \ldots, v_n} = 0 \iff v_1, \ldots, v_n\) линейно зависимы.
\end{statement}

\begin{statement}
  \(\mixed{v_1, \ldots, v_n} > 0 \iff v_1, \ldots, v_n\) --- положительный базис.
\end{statement}

\subsection{Векторное произведение}

Пусть \(X\) --- евклидово пространство, \(X\) ориентированно, \(\dim{X} = 3\).

\begin{definition}
  Векторное произведение векторов \(u, v \in X\) --- такой вектор \(h \in X\), что \(\inner{h}{x} =
  \mixed{u, v, x} \quad \forall x \in X\).
\end{definition}

\begin{notation}
  \(u \times v\) --- векторное произведение.
\end{notation}

\begin{statement}
  Определение векторного произведения корректно.
\end{statement}
\begin{proof}
  Рассмотрим \(f(x) \coloneqq \mixed{u, v, x}\), \(f\colon X \to \R\), \(f\) линейно.

  Но, по лемме Рисса, \(\exists h \in X\colon \inner{h}{x} = f(x) \quad \forall x \in X\).
\end{proof}

Рассмотрим его свойства.

\begin{statement}
  \(\forall u, v, w \in X \quad \inner{u \times v}{w} = \mixed{u, v, w}\).
\end{statement}

\begin{statement}[кососимметричность]
  \(\forall u, v \in X \quad u \times v = -v \times u\).
\end{statement}
\begin{proof}
  \(\inner{u \times v}{w} = \mixed{u, v, w}\), \(\inner{v \times u}{w} = \mixed{v, u, w} =
  -\mixed{u, v, w}\).
\end{proof}

В частности,
\begin{statement}
  \(\forall u, v, w \in X \quad \inner{u \times v}{w} = \inner{-v \times u}{w}\).
\end{statement}

\begin{statement}[линейность по каждому аргументу]
  \(\forall u, v, w \in X\)
   \begin{enumerate}
    \item \((u + v) \times v= u \times w + v \times w\);
    \item ...
  \end{enumerate}
\end{statement}
\begin{proof}
  \(\inner{(u + v) \times w}{x} = \mixed{u + v, w, x} = \mixed{u, w, x} + \mixed{v, w, x}\);
  осталось рассмотреть  \(\inner{u \times w + v \times w}{x}\).
\end{proof}

\begin{statement}
  \(\forall u, v \in X \quad u \times v = 0 \iff u, v\) линейно зависимы.
\end{statement}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\implies}\)] Предположим обратное: тогда \(\exists w \in X\colon \left\{ u, v, w
      \right\}\) --- базис \(X\).

      Тогда \(\inner{u \times v}{w} = \mixed{u, v, w} \neq 0\), ведь все три вектора образуют базис
      по предположению.

    \item[\(\boxed{\impliedby}\)] \(\inner{u \times v}{u \times v} = \mixed{u, v, u \times v} = 0\)
      по линейной зависимости.
  \end{pfparts}
\end{proof}

\subsubsection{Геометрический смысл векторного произведения}

Пусть \(u\) и \(v\) --- линейно независимые векторы.

\begin{theorem}
  \hfill
  \begin{enumerate}
    \item \(u \times v \perp u, v\);
    \item \(\left\{ u, v, u \times v \right\}\) --- положительный базис;
    \item \(|u \times v|\) --- площадь параллелограмма, натянутого на \(u\) и \(v\).
  \end{enumerate}
\end{theorem}
\begin{proof} % figure 1
  \hfill
  \begin{enumerate}
    \item \(\inner{u \times v}{u} = \mixed{u, v, u} = 0\).

    \item \(\left\{ u, v, u \times v \right\}\) --- положительный базис тогда и только тогда, когда
      \(\mixed{u, v, u \times v} > 0\).

      Но \(\mixed{u, v, u \times v} = |u \times v|^2 > 0\), ведь векторы линейно независимы.

    \item Рассмотрим \(u\) и \(v\) в некоторых ортонормированных базисах \(e_1, e_2\).

      Тогда \(u = ae_1\), \(a > 0\); \(v = be_1 + ce_2\), \(c > 0\). Получается, что \(S = ac\); но
      \(u \times v = (ac_1) \times (be_1 + ce_2) = ac(e_1 \times e_2) = ac (\lambda e_3)\) (дополнив
      \(e_1, e_2\)).

      С другой стороны, \(\inner{e_1 \times e_2}{e_3} = \mixed{e_1, e_2, e_3} = \begin{vmatrix}
      1 & 0 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{vmatrix} = 1 = \inner{\lambda e_3}{e_3}\).
  \end{enumerate}
\end{proof}

Пусть \(e_1, e2, e_3\) --- положительный ортонормированный базис; \(u = x_1e_1 + x_2e_2 + x_3e_3\),
\(v = y_1e_1 + y_2e_2 + y_3e_3\).

\begin{theorem}
  \[
    u \times v = \begin{vmatrix}
      e_1 & e_2 & e_3 \\
      x_1 & x_2 & x_3 \\
      y_1 & y_2 & y_3
    \end{vmatrix}
  .\]
\end{theorem}


\chapter{Аффинные пространства}

\begin{definition}
  Аффинное пространство --- тройка \(\left( X, \vv{X}, + \right)\), где
  \begin{itemize}
    \item \(X \neq \O\) --- множество элементов (\textit{точки});
    \item \(\vv{X}\) --- векторное пространство над \(\R\);
    \item \(+\colon X \times \vv{X} \to X\) --- операция ``откладывания вектора от точки'', где
      \begin{enumerate}
        \item \(\forall x, y \in X \exists! \vv{xy} \coloneqq v \in \vv{X}\colon x + v = y\);
        \item \(\forall x \in X \forall u, v \in \vv{X} \quad (x + u) + v = x + (u + v)\).
      \end{enumerate}
  \end{itemize}
\end{definition}

\begin{example}
  \(X = \R^n\) как множество, \(\vv{X} = \R^n\) как векторное пространство, \(+\) задан
  классически.
\end{example}

Аффинные пространства нужны чтобы ``хорошо'' описывать \(\R^n\) с точки зрения геометрии.

\begin{notation}
  Под ``аффинное пространство \(X\)'' подразумевается \(\left( X, \vv{X}, + \right)\).
\end{notation}

Рассмотрим базовые свойства. Пусть \(X\) --- аффинное пространство; \(\forall x, y, z \in X\),
\(\forall u, v \in \vv{X}\).

\begin{statement}
  \(x + \vv{xy} = y\).
\end{statement}

По-другому говоря, \(x + u = x + v \iff u = v\).

\begin{statement}
  \(\vv{xy} + \vv{yz} = \vv{xz}\).
\end{statement}
\begin{proof}
  \(x + \left( \vv{xy} + \vv{yz} \right) = \left( x + \vv{xy} \right) + \vv{yz} = y + \vv{yz} =
  z = x + \vv{xz}\).
\end{proof}

\begin{statement}
  \(\vv{xx} = 0\).
\end{statement}

\begin{statement}
  \(x + 0 = x\).
\end{statement}

\begin{statement}
  \(x + u = y + u \iff x = y\).
\end{statement}

\begin{statement}
  \(\vv{xy} = 0 \iff x = y\).
\end{statement}

\section{Арифметика точек}

\begin{definition}
  Начало отсчета АП \(X\) --- произвольная фиксированная точка \(o \in X\).
\end{definition}

Пусть \(o\) --- начало отсчета \(X\).

\begin{definition}
  \(\varphi_o\colon X \to \vv{X}\), \(\forall x \in X \quad \varphi_o(x) \coloneqq \vv{ox}\) ---
  отображение векторизации.
\end{definition}

\begin{statement}
  \(\varphi_o\) --- биекция.
\end{statement}
\begin{proof}
  \(\varphi_o(x) = \varphi_o(y) \implies x = y\) по свойству действий с векторами.

  \(\forall x \in \vv{X} \quad \varphi_o(o + v) = x\) --- сюръективность.
\end{proof}

\begin{definition}
  Линейная комбинация \(\sum_{i = 1}^n t_ip_i\) точек \(p_1, \ldots, p_n \in X\) с коэффициентами \(t_1, \ldots, t_n \in \R\)
  относительно начала отсчета \(o\) --- это одно из
  \begin{enumerate}
    \item вектор \(v = \sum_i t_i \vv*{op}{i}\);
    \item точка \(o + v\).
  \end{enumerate}
\end{definition}

\begin{definition}
  Линейная комбинация точек называется барицентрической, если \(\sum_i p_i = 1\).
\end{definition}

\begin{definition}
  Линейная комбинация точек называется сбалансированной, если \(\sum_i p_i = 0\).
\end{definition}

\begin{theorem}
  \hfill
  \begin{enumerate}
    \item Барицентрическая комбинация точек --- точка, не зависящая от начала отсчета;
    \item Сбалансированная комбинация точек --- вектор, не зависящий от начала отсчета.
  \end{enumerate}
\end{theorem}
\begin{proof}
  Пусть \(o\) и \(o'\) --- точки начала отсчета. Распишем векторы \(v\) и \(v'\) с ними: \(v =
  \sum_i t_i \vv*{op}{i}\), \(v' = \sum_i t_i\vv*{o'p}{i}\), где \(p \coloneqq o + v\).

  Перейдем к первичной начальной точке: \(v' = \sum_i t_i\left( \vv{o'o} + \vv*{op}{i} \right) =
  \sum_i t_i\vv{o'o} + \sum_i t_i\vv*{op}{i}\) --- второй пункт получается сразу отсюда.

  Если же комбинация барицентрическая, то \(o' + v' = o' + \vv{o'o} + \sum_i t_i\vv*{op}{i} = o +
  \sum_i t_i\vv*{op}{i} = o + v\).
\end{proof}

\section{Аффинные подпространства}

Пусть \(X\) --- аффинное пространство.

\begin{definition}
  \(p + V \coloneqq \left\{ p + x \mid x \in V \right\} \subseteq X\) (\(p \in X, V \subseteq
  \vv{X}\)).
\end{definition}

\begin{statement}
  Следующие утверждения эквиваленты:
  \begin{enumerate}
    \item \(Y = p + V\);
    \item \(\varphi_p(Y) = V\);
    \item \(q \in Y \iff \vv{pq} \in V\).
  \end{enumerate}
\end{statement}

\begin{definition}
  Множество \(Y \subseteq X\) называется аффинным подпространством, если существует такой векторное
  подпространство \(V \leqslant \vv{X}\) и точка \(p \in Y\), что \(Y = p + V\).
\end{definition}

\begin{definition}
  Такое \(V\) называется направлением \(Y\).
\end{definition}

Пусть \(Y = p + V\) --- аффинное подпространство.
\begin{lemma}
  \hfill
  \begin{enumerate}
    \item \(\forall q \in Y \quad Y = q + V\);
    \item \(Y\) --- аффинное пространство, где \(\vv{Y} = V\);
    \item \(\forall q \in Y \quad \varphi_q(Y) = V\).
  \end{enumerate}
\end{lemma}
\begin{proof}
  \hfill
  \begin{enumerate}
    \item \(q + V = p + \left( \vv{pq} + V \right) = p + V\).

    \item По определению подпространства.

    \item Очевидно, и предоставляется читателю.
  \end{enumerate}
\end{proof}

\begin{definition}
  \(\dim\left( X, \vv{X}, + \right) \coloneqq \dim\vv{X}\).
\end{definition}

В частности, \(\dim{Y} = \dim{V}\), где \(Y = p + V\) --- аффинное подпространство.

\begin{definition}
  Параллельный перенос на \(v \in \vv{X}\) --- отображение \(T_v\colon X \to X\), где \(T_v(x)
  \coloneqq x + v \quad \forall x \in X\).
\end{definition}

Рассмотрим его свойства.

\begin{statement}
  \(T_v\) --- биекция.
\end{statement}

\begin{statement}
  \(T_{u + v} = T_u \circ T_v\).
\end{statement}

\begin{statement}
   \(T_o \equiv \id\).
\end{statement}

\begin{statement}
  \(T_{-v} = T_v^{-1}\).
\end{statement}

Заметим, что эти свойства показывают, что
\begin{statement}
  Все параллельные переносы образуют группу, причем \(T \cong \left( \vv{X}, + \right)\).
\end{statement}

\begin{definition}
  Аффинные подпространства называются параллельными, если их направления совпадают.
\end{definition}

\begin{statement}
  Аффинные подпространства параллельны тогда и только тогда, когда одно переводится в другое
  параллельным переносом.
\end{statement}

\begin{statement}
  Аффинное пространство \(X\) разбивается на афф. подпространства, параллельные \(Y\).
\end{statement}

Предлагается доказать эти утверждения самостоятельно.

Ну и напоследок:

\begin{definition}
  Прямая в аффинном пространстве --- подпространство размерности \(1\).
\end{definition}

\begin{definition}
  Гиперплоскость в аффинном пространстве размерности \(n\) --- подпространство размерности \(n -
  1\).
\end{definition}

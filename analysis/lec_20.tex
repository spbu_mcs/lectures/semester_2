\lecture{20}{25.04.2024, 15:35}{}

Пусть \(f_n\colon X \to \R\) (или \(\C\)).

Напомним ключевой признак.
\begin{theorem}[критерий Коши]
  Функциональный ряд \(\sum_n f_n\) сходится равномерно на множестве \(X\) тогда и только тогда,
  когда \[
    \forall \varepsilon > 0 \exists N = N(\varepsilon)\colon \forall n > N \forall p \in \N \forall
    x \in X \quad \left| \sum_{k = n + 1}^{n + p} f_k(x) \right| < \varepsilon
  \]
\end{theorem}

\begin{statement}[признак сходимости Вейерштрасса]
  Ряд \(\sum_n f_n(x)\) сходится равномерно на \(X\), если \(\sum_n \|f_n\| < +\infty\).
\end{statement}
\begin{proof}
  По критерию Коши для \(\sum_n \|f_n\|\): \[
    \forall \varepsilon > 0 \exists N\colon \forall n > N \forall m > n \quad \sum_{k = n + 1}^m
    \|f_k\| < \varepsilon
  .\]

  Тогда, \[
    \left|\sum_{k = n + 1}^m f_k(x)\right| \le \sum_{k = n + 1}^m |f_k(x)| \le \sum_{k = n + 1}^m
    \|f_k\| < \varepsilon
  ,\] --- раз \(N \neq N(x)\), получилась равномерная сходимость (по признаку Коши).
\end{proof}

\begin{definition}
  \(f_n\) называется равномерной ограниченной на \(X\), если \(\exists M\colon \forall n \forall x
  \in X \quad |f_n(x)| \le M\).
\end{definition}

\begin{theorem}[признак равномерной сходимости Дирихле]
  Если \(s_n(x) = u_1(x) + \ldots + u_n(x)\) равномерно ограничена на \(X\), а \(v_n(x)\) монотонна,
  ограничена (\textit{по модулю}) и \(v_n \unito 0\), то ряд \(\sum_n u_n(x)v_n(x)\) сходится
  равномерно на \(X\).
\end{theorem}
\begin{proof}
  Воспользуемся критерием Коши для равномерной сходимости.

  \[
  \begin{aligned}
    \sum_{k = m}^n u_k(x)v_k(x) &= \sum_{k = m}^n (s_k(x) - s_{k - 1}(x))v_k(x) = \sum_{k = m}^n
    s_k(x)v_k(x) - \sum_{k = m}^n s_{k - 1}(x)v_k(x) \\
    &= \sum_{k = m}^n s_k(x)v_k(x) - \sum_{k = m - 1}^{n - 1} s_k(x)v_{k + 1}(x) \\
    &= \sum_{k = m}^{n - 1} s_k(x)(v_k(x) - v_{k + 1}(x)) + s_n(x)v_n(x) - s_{m - 1}(x)v_m(x).
  \end{aligned}
  \]

  Навесим модули: \[
  \begin{aligned}
    \left| \sum_{k = m}^n u_k(x)v_k(x) \right| &= \left| \sum_{k = m}^{n - 1} s_k(x)(v_k(x) - v_{k +
    1}(x)) + s_n(x)v_n(x) - s_{m - 1}(x)v_m(x) \right| \\
    &\le \sum_{k = m}^{n - 1} \underbrace{|s_k(x)|}_{\le M}|v_k(x) - v_{k + 1}(x)| +
    \underbrace{|s_n(x)|}_{\le M}|v_n(x)| - \underbrace{|s_{m -
    1}(x)|}_{\le M}|v_n(x)| \\
    &\le M|v_n(x) - v_m(x)| + M|v_n(x)| + M|v_m(x)|,
  \end{aligned}
  \] --- каждое из \(v_i \unito 0\), и по признаку Коши получается искомое.
\end{proof}

Для функциональных рядов, в отличии от числовых, б\'{o}льшую значимость имеет
\begin{theorem}[признак равномерной сходимости Абеля]
  Если \(\sum_{n = 1}^\infty u_n(x)\) сходится равномерно на \(X\), \(v_n(x)\) монотонна по \(n\) и
  равномерно ограничена, то ряд \(\sum_n u_n(x)v_n(x)\) сходится равномерно.
\end{theorem}
\begin{proof}[Набросок доказательства]
  Дискретное преобразования Абеля (см. выше) для \(s_n \mapsto s_n - s\).
\end{proof}

\begin{example}
  Рассмотрим степенной ряд \(\sum_{n = 0}^\infty a_n e^{inx}\), где \(a_n \to 0\) монотонно.
  Проверим, что он сходится локально-равномерно на \((0, 2\pi)\).

  Проверим, что сумма \(s_n(x) = 1 + e^{ix} + \ldots + e^{inx}\) ограничена: суммируя прогрессию
  получается \(\frac{e^{i(n + 1)x} - 1}{e^{ix} - 1} = \frac{e^{\frac{i(n +
  1)}{2}x}}{e^{\frac{ix}{2}}}\frac{e^{\frac{i(n + 1)}{2}x} - e^{-\frac{i(n +
  1)}{2}x}}{e^{\frac{ix}{2}} - e^{-\frac{ix}{2}}} = e^{\frac{in}{2}x} \frac{sin{\frac{n +
  1}{2}x}}{\sin{\frac{x}{2}}}\), то есть \(|s_n(x)| \le \left| \frac{1}{\sin{\frac{x}{2}}} \right|\)
  и \(s_n(x)\) локально равномерно ограничена на \((0, 2\pi)\) и этот ряд сходится по признаку
  Дирихле.
\end{example}

\begin{example}
  Рассмотрим ряд Фурье \(\sum_{n = 1}^\infty \frac{sin{nx}}{n}\). Покажем, что он сходится в \(\R\),
  но не равномерно.

  Предположим, что он сходится все же равномерно: \[
  \begin{aligned}
    \forall \varepsilon > 0 \exists N = N(\varepsilon)\colon \forall n \ge N \forall x \quad\left|
    \sum_{k = n}^{2n} \frac{\sin{kx}}{k} \right| < \varepsilon.
  \end{aligned}
  \]

  Возьмем \(x = \frac{1}{4n}\): раз слагаемых \(\approx n\), то \[
  \begin{aligned}
    \left| \frac{\sin{\frac{1}{4}}}{2n}n \right| < \varepsilon,
  \end{aligned}
  \] но это неправда.
\end{example}

Пусть \(X\) --- некоторое метрическое пространство, \(D \subset X\) --- некоторое его открытое
подмножество, а \(f_n\colon X \to \R\).

\begin{theorem}[Стокса-Зейделя]
  Если \(\sum_{n = 1}^\infty f_n(x)\) сходится равномерно (или даже локально равномерно) к \(f\), и
  все \(f_n\) непрерывны на \(D\), то и \(f\) непрерывна.
\end{theorem}

Пусть \(K\) --- компактное пространство.

\begin{definition}
  \(C(K)\) --- пространство непрерывных функций \(f\colon K \to \R\) с нормой \(\|f\|_K \coloneqq
  \sup_{K}|f(x)|\).
\end{definition}

Из теоремы Стокса-Зейделя следует, что
\begin{statement}
  Пространство \(C(K)\) Банахово (полное).
\end{statement}

Пусть \(f_n \in C(K)\).

\begin{theorem}[Дини]
  Если \(\forall x \in K \quad f_n(x) \ge 0\) и \(\sum_{n = 1}^\infty f_n(x) = f(x) \in C(K)\), то
  \(\sum_n f_n \unito f\).
\end{theorem}
\begin{proof}
  Переформулируем на языке последовательностей: \(g_n(x) \in C(K)\), \(g_n \to 0\) (\(g_n(x) = f -
  (f_1(x) + \ldots + f_n(x))\)), \(g_{n + 1}(x) \le g_n(x)\) и надо доказать, что \(g_n \unito 0\).

  Нужно, чтобы \[
    \forall \varepsilon > 0 \exists N = N(\varepsilon)\colon \forall n \ge N \forall x \in K \quad
    |g_n(x)| < \varepsilon
  .\]

  Предположим обратное: \[
    \exists \varepsilon_0 > 0\colon \forall N \exists n \ge N \exists x_n \in K\colon |g_n(x_n)| \ge
    \varepsilon_0
  .\]

  В силу компактности \(K\) вытащим последовательность \(x_n \to x_0\). Получится, что \(g_m(x_0)
  \ge \varepsilon_0\) при \(m \le n\) в силу монотонности --- противоречие с тем, что \(g_n \to 0\).
\end{proof}

\begin{theorem}
  Если \(f_n \in C[a, b]\) и \(f(x) = \sum_{n = 1}^\infty f_n(x)\) равномерно, то \(\int_a^b \left(
  \sum_{n = 1}^\infty f_n(x)\right) \df{x} = \sum_{n = 1}^\infty \int_a^b f_n(x) \df{x}\).
\end{theorem}

Доказать предлагается самому.

\begin{theorem}
  Если \(\sum_{n = 1}^\infty f_n(x) = f(x)\) сходится на \((a, b)\), а \(\sum_{n = 1}^\infty
  f'_n(x)\) сходится локально-равномерно к \(\varphi(x)\), то \(f'(x) = \varphi(x)\).
\end{theorem}

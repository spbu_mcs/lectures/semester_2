\lecture{23}{14.05.2024, 13:40}{}

Вернем один ``должок'' с классификации конечнопорожденных модулей.

Пусть \(R\) --- PID, \(M\) --- конечнопорожденный \(R\)-модуль.

Как уже выяснялось, \(M \cong R^n \oplus \qfrac{R}{(x_1)} \oplus \ldots \oplus \qfrac{R}{(x_k)}\),
причем \(x_1 \divs x_2 \divs \ldots \divs x_k\) (\((x_1) \supset (x_2) \supset \ldots \supset
(x_k)\)).

Покажем, что такое разложение единственно.

\(\qfrac{R}{(x_1)} \oplus \ldots \oplus \qfrac{R}{(x_k)} \eqqcolon \Tors(M)\) определен однознано;
тогда \(R^n = \qfrac{M}{\Tors(M)}\).

\begin{theorem}
  Если \(R\) --- \textit{коммутативное} кольцо, то \(R^n \cong R^m \implies n = m\).
\end{theorem}
\begin{proof}
  Возьмем в \(R\) максимальный (по включению среди тех, которые не совпадают с \(R\), то есть не
  содержат \(1\)) идеал \(I\): такой обязательно найдется по лемме Цорна (здесь нужна
  коммутативность, ибо максимальный односторонний идеал не обязательно найдется).

  \begin{statement}
    Если \(\left\{ I_\alpha \right\}_{\alpha \in J}\) --- частично упорядоченное множество
    собственных идеалов, \(\alpha \le \beta \implies I_\alpha \subseteq I_\beta\), то и
    \(\bigcup_\alpha I_\alpha\) --- тоже собственный идеал.
  \end{statement}

  Умножим \(R^n \cong R^m\) тензорно на \(\qfrac{R}{I}\): получится \(R^n \otimes_R \qfrac{R}{I}
  \cong \left( \qfrac{R}{I} \right)^n\), \(R^m \otimes_R \qfrac{R}{I} \cong \left( \qfrac{R}{I}
  \right)^m\). Но \(\qfrac{R}{I}\) --- поле (и здесь понадобилась коммутативность), а для векторных
  пространств над полем уже известно, что \(n = m\).
\end{proof}

\begin{remark}
  Можно доказать и через то, что \(M \otimes_R \Frac(R) \cong \Frac(R)^n\), а кручение ``умирает''
  (например, \(M \otimes_{\Z} \Q \cong \Q^n\)): здесь не пригодится использованная лемма Кролля.
\end{remark}

Для некомутативных колец существуют контрпримеры (предлагается доказать их самостоятельно).

\begin{example}
  В \(M_n(F)\) единственный двусторонний идеал --- \(0\).
\end{example}

\begin{example} \mnote{воспользоваться тем, что \(F^{(\N)} \cong F^{(\N)} \oplus F^{(\N)}\)}
  Если \(R = \End\left( F^{(\N)} \right)\) (эндоморфизмы счетномерного векторного пространства), то
  \(R \cong R^2\) как левый модуль.
\end{example}

А как восстановить \((x_i)\)?

Будем считать теперь, что \(M \coloneqq \Tors(M)\), ведь \(\Tors\) определен инвариантно.

Как известно, \((x_k) = \Ann(M)\). Для остальных можно попробовать применить какую-нибудь внешнюю
степень, ведь в нормальной форме Смита возникают какие-то миноры.

\(\Lambda^2\left( \qfrac{R}{(x_i)} \right) = 0\), ведь это факторкольцо задается одной образующей, а
\([1] \wedge [1] = 0\) (и \([r] \wedge [s] = rs[1] \wedge [1] = 0\)).

Тогда, раз внешние квадраты ``умирают'', \(\Lambda^2(M) \cong \bigoplus_{i < j} \qfrac{R}{(x_i)}
\otimes \qfrac{R}{(x_j)}\).

\begin{notation}
  \(x^{(n)} \coloneqq \frac{x^n}{n!}\).
\end{notation}

\begin{example}
  \((x + y)^{(n)} = \sum_i x^{(i)} y{(n - i)}\).
\end{example}

\begin{statement} \mnote{см. пример про \(\qfrac{\Z}{n\Z} \otimes \qfrac{\Z}{m\Z}\)}
  \(\qfrac{R}{(x_i)} \otimes \qfrac{R}{(x_j)} \cong \qfrac{R}{\gcd(x_i, x_j)}\), а раз \(x_i \divs
  x_j\), то \(\cong \qfrac{R}{(x_i)}\).
\end{statement}

Сумма еще раз упростилась: \(\Lambda^2(M) \cong \bigoplus_{i < j} \qfrac{R}{(x_i)}\).

\begin{statement}
  \((x_{k - 1}) = \Ann\left( \Lambda^2(M) \right)\).
\end{statement}

Для куба аналогично: \(\Lambda^3(M) \cong \bigoplus_{i < j < l} \qfrac{R}{(x_i)} \otimes
\qfrac{R}{(x_j)} \qfrac{R}{(x_l)} \cong \bigoplus_{i < j < l} \qfrac{R}{(x_i)}\).

\begin{statement}
  \((x_{k - 2}) = \Ann\left( \Lambda^3(M) \right)\).
\end{statement}

И так далее.

\begin{statement}
  Количество этих элементов --- момент обнуления внешней степени.
\end{statement}

\section{Группы}

\subsection{Задание групп образующими и соотношениями}

\begin{definition}
  Группа \(D_n\) --- группа симметрий \(n\)-угольников, где есть \(n\) поворотов (образующих
  нормальную подгруппу \(C_n\)) и симметрии вокруг прямых через центр и одну вершину или центр и
  середину стороны (они подгруппу не образуют).
\end{definition}

Пусть \(r\) --- образующая \(C_n\), \(s\) --- какое-то отражение.

\begin{statement}
  \(r^n = 1\), \(s^2 = 1\).
\end{statement}

\begin{statement}
  \(srs = r^{-1}\).
\end{statement}

Выпишем таблицу умножения, имея повороты \(r^i\) и симметрии \(r^js\).

\begin{statement}
  \(r^i r^j = r^{(i + j) \mod{n}}\).
\end{statement}

\begin{statement}
  \(r^i \cdot r^js = r^{(i + j) \mod{n}} s\).
\end{statement}

\begin{statement}
  \(r^jsr^i = r^isr^iss = r^{(j - i) \mod{n}} s\).
\end{statement}

\begin{statement}
  \(r^js \cdot r^is = r^{(j - i) \mod{n}}\).
\end{statement}

Получается, что \(D_n\) --- ``наибольшая'' группа с образующими \(r\), \(s\), которая подчиняется
соотношениям из первых двух утверждений.

Как уже известно, \(\langle r \rangle\) --- нормальная подгруппа. Если по ней профакторизовать, то
получится \(\langle s \rangle\), где \(s^2 = 1\): значит есть только элементы \(r^i\) и \(r^is\).

\begin{statement}
  Получилась конечная группа размера \(\le 2n\).
\end{statement}

На самом деле \(D_n\) обладает следующим универсальным свойством.

\begin{statement}
  \(\forall G \quad G = \langle r', s' \rangle, (r')^n = 1, (s')^2 = 1, (s'r')^2 = 1 \implies
  \exists! f\colon D_n \to G\) (гомоморфизм), где \(r \mapsto r'\), \(s \mapsto s'\).
\end{statement}
\begin{proof}
  Пусть \(r^i \mapsto (r')^i\), \(r^is \mapsto (r')^is'\) --- это гомоморфизм, ведь и там и там
  можно выписать таблицу умножения.
\end{proof}

Обобщим это наблюдение.

Возьмем буквы \(x_i\) и начнем выписывать слова в алфавите \(\left\{ x_i, x_i^{-1} \right\}_i\),
приравнивая их к \(1\).

\begin{example}
  \(x^2yxy^{-1}zx^2z = 1\).
\end{example}

\begin{statement}
  Существует группа \(G\), где
  \begin{enumerate}
    \item у нее есть такие образующие с такими соотношениями;
    \item \(\forall H\) с образующими \(x_i'\) и такими же соотношениями существует единственный
      гомоморфизм \(G \to H\), где \(x_i \mapsto x_i'\).
  \end{enumerate}
\end{statement}

\begin{remark}
  Получившийся гомоморфизм --- сюръекция.
\end{remark}

\begin{example}
  Образующая \(r\) с \(r^n = 1\) --- циклическая группа \(C_n\).
\end{example}

\begin{example} \mnote{определение будет позже}
  Образующая \(r\) без соотношения --- свободная группа.
\end{example}

\begin{example}
  Пусть \(s_i\) --- элементарная трансвекция. Наберем \(n - 1\) таких со следующим соотношением:
  \(s_i^2 = 1\), \((s_is_j)^2 = 1\) при \(|i - j| \ge 2\), \((s_is_{i + 1})^3 = 1\) --- получилась
  \(S_n\).
\end{example}

Последнее соотношение удобно переписать как \(s_is_{i + 1}s_i = s_{i + 1}s_is_{i + 1}\):

\begin{example}
  Набор образующих из предыдущего примера без соотношения \(s_i^2 = 1\) --- группа кос. Она
  бесконечна.
\end{example}

\begin{definition}
  Соотношение \(s_is_{i + 1}s_i = s_{i + 1}s_is_{i + 1}\) называется соотношением кос.
\end{definition}

\begin{remark}
  Не существует алгоритма, определяющего, будет ли полученная \(G = 1\).
\end{remark}

\begin{remark}
  Если группа конечна, то есть алгоритм, считающий количество элементов.
\end{remark}

Пусть \(X\) --- некоторое множество (не обязательно конечное).

\begin{definition}
  Свободной группой \(F(X)\) называется группа со следующими свойствами:
  \begin{enumerate}
    \item есть отображение множеств \(X \to F(X)\);
    \item существует и единственное отображение групп, делающее следующую диаграмму коммутативной
      (отображение \(X \to G\) --- отображение множеств):
      \[
      \begin{tikzcd}
        X \arrow[r] \arrow[rd] & F(X) \arrow[d, dashed, "\exists!"] \\
        & G.
      \end{tikzcd}
      \]
  \end{enumerate}
\end{definition}

\begin{statement} \mnote{эквивалентно коммутативности диаграммы}
  \(\Hom(X, G) \simeq \Hom(F(X), G)\), где слева --- отображения множеств, а справа --- гомоморфизмы
  групп.
\end{statement}

Из чего состоит свободная группа?

Рассмотрим сначала задачу поуже: из чего состоит свободный моноид? \(X\) --- это алфавит, и
свободный моноид --- слова конечной длины в этом алфавите (включая пустое слово, равное \(1\)).
Операция умножения --- конкатенация.

Для групп это несколько хитрее из-за сокращения: например (умножая слова), \(xyz \cdot z^{-1}xy =
xyxy\), \(xyzz^{-1}y^{-1}x = xx\). Это один из способов определить свободную группу: умножение с
выполнением нужных сокращений.

Будем следовать такому плану:
\begin{enumerate}
  \item определим свободное произведение \(G * H\);
  \item если \(X\) конечно, определим \(F_n \coloneqq \underbrace{C_\infty * \ldots * C_\infty}_n\).
\end{enumerate}

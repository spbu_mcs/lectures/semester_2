\lecture{8}{05.04.2024, 13:40}{}

\subsection{Классификация состояний}

\subsubsection{По достижимости}

\begin{definition}
  Состояние \(y\) называется достижимым из \(x\) (\(x \stl y\)), если \(\exists i\colon p^i(x
  \leadsto y) > 0\) или \(\exists x_0, \ldots, x_n\colon x_0 = x, x_n = y\) и \(p(x_i \leadsto x_{i
  + 1}) > 0\).
\end{definition}

\begin{definition}
  \(x\) сообщается с \(y\) (\(x \stlr y\)), если \(y\) достижимо из \(x\) и наоборот.
\end{definition}

\begin{definition}
  Состояние \(x\) называется существенным, если \(\left[x \stl y \implies x \stlr y\right]\).
\end{definition}

\begin{example}
  Если \(\mathcal{X} = \Z\), то все состояния несущественны.
\end{example}

\begin{statement}
  Если фазовое пространство конечно, то существует существенное состояние.
\end{statement}
\begin{proof}
  Возьмем \(x_0 \in \mathcal{X}\): если оно не существенно, то \(\exists x_1\colon x_0 \stl x_1\),
  но вернуться обратно нельзя.

  Если \(x_1\) не существенно, то \(\exists x_2\colon x_1 \stl x_2\), но вернуться обратно нельзя.

  Раз цепь конечная, то либо на каком-то шаге найдется существенное состояние, либо на каком-то шаге
  получится цикл, что будет противоречить выбору \(x_i\).
\end{proof}

Рассмотрим только существенные состояния. Положим, что \(x \sim y \iff x \stlr y\) --- отношение
эквивалентности.

\begin{definition}
  Эргодические классы --- разбиение всех существенных состояний по данному отношению
  эквивалентности.
\end{definition}

\begin{statement}
  Эргодический класс --- замкнутое множество.
\end{statement}

\begin{definition}
  Цепь называется неприводимой, если всё пространство \(\mathcal{X}\) представляет собою
  один эргодический класс.
\end{definition}

\subsubsection{По периодичности}

Возьмем состояние \(x \in \mathcal{X}\) и положим, что \(I(x) \coloneqq \left\{ i \ge 1 \mid p^i(x
\leadsto x) > 0 \right\}\).

\begin{definition} \mnote{такой \(\gcd\) у бесконечного множества \(I\) найдется за счет того, что
\(\N\) удовлетворяет условию обрыва цепей главных идеалов}
  Период состояния \(d(x) \coloneqq \gcd{I(x)}\).
\end{definition}

\begin{example}
  Рассмотрим блуждание на квадрате, где на каждом шаге можно перейти в любую из двух соседних точек.

  Понятно, что из любой вершины вернуться в нее же можно только за четное число шагов; период каждой
  точки --- 2.
\end{example}

\begin{statement}
  Если \(x \stlr y\), то \(d(x) = d(y)\).
\end{statement}
\begin{proof}
  Раз они сообщаются, то \(\exists a, b\colon p^a(x \leadsto y) > 0, p^b(y \leadsto x) > 0\).

  Заметим, что \(a + b \in I(y)\), ведь \(p^{a + b}(y \leadsto y) \ge p^b(y \leadsto x)p^a(x
  \leadsto y) > 0\) (по ассоциативности).

  Возьмем некоторый \(i \in I(x)\): \(p^i(x \leadsto x) > 0\). Таким же образом \(a + b + i \in
  I(y)\), ведь \(p^{a + b + i}(y \leadsto y) \ge p^b(y \leadsto x)p^i(x \leadsto x)p^a(x \leadsto y)
  > 0\).

  Видно, что \(d(y) \divs (a + b)\); по той же причине, \(d(y) \divs (a + b + i)\), а значит \(d(y)
  \divs i = (a + b + i) - (a + b)\), то есть \(d(y)\) --- общий делитель \(I(x)\), но \(d(x)\) ---
  наибольший из таких делителей, то есть \(d(x) \le d(y)\). Аналогично \(d(y) \le d(x)\).
\end{proof}

\begin{consequence}
  Периоды эргодических классов различны.
\end{consequence}

\begin{notation}
  \(a \oplus b\) --- сложение по модулю \(d\).
\end{notation}

Пусть \(E\) --- эргодический класс с периодом \(d > 1\).

\begin{statement}
  \(E\) можно разбить на \textit{периодические подклассы}: \(E = E_0 \sqcup E_1 \sqcup \ldots \sqcup
  E_{d - 1}\), причем если \(x \in E_j\) и \(p(x \leadsto y) > 0\), то \(y \in E_{j \oplus 1}\).
\end{statement}

\begin{definition}
  Элементы разбиения \(E_i\) называются периодическими подклассами.
\end{definition}

\begin{proof}
  Зафиксируем \(x_0 \in E\) и возьмем другую произвольную точку \(y \in E\). Раз класс эргодический,
  то \(\exists l(x_0, y)\colon p^{l(x_0, y)}(x_0 \leadsto y) > 0\).

  Заметим, что \(y \in E_j \iff l(x_0, y) \equiv_d j\). Непонятно лишь то, что эти подклассы не
  пересекаются.

  Еще раз вспоминая, что класс эргодический, \(m + l_1(x_0, y) \in I(x_0)\) и \(m + l_2(x_0, y) \in
  I(x_0)\), а значит \(l_1(x_0, y) - l_2(x_0, y) = m + l_1 - m + l_2 \divby d\). Получается, что
  \(l_1(x_0, y) \equiv_d l_2(x_0, y)\) --- подклассы и впрямь не пересекаются.

  Осталось доказать последнюю часть. Раз \(y \in E_j\), то \(\exists l\colon p^l(x_0 \leadsto y) >
  0\) и \(l \equiv_d j\). Но тогда \(p^{l + 1}(x_0 \leadsto z) \ge p^l(x_0 \leadsto y)p(y \leadsto
  z) > 0\) и, раз \(l + 1 \equiv_d j \oplus 1\),  \(z \in E_{j \oplus 1}\).
\end{proof}

Пусть \(E\) --- \textit{конечный} эргодический класс с периодом \(1\).

\begin{statement}
  \(\exists R\colon \forall r \ge R \forall x, y \in E \quad p^r(x \leadsto y) > 0\).
\end{statement}
\begin{proof}
  Уже известно, что \(\forall x, y \exists l(x, y)\colon p^{l(x, y)}(x \leadsto y) > 0\) (за счет
  эргодичности класса). Непонятно лишь почему можно взять ``универсальное'' \(l\), годящееся для
  всех \(x\) и \(y\).

  Докажем это в несколько этапов.
  \begin{enumerate}
    \item \(\forall x \exists M(x)\colon \forall m \ge M \quad p^m(x \leadsto x) > 0\).

      \begin{proof}
        Раз \(d(x) = 1\), то \(1 = \gcd(i_1, \ldots, i_k)\), где \(i_1, \ldots, i_k \in I(x)\).

        Из теории чисел известно, что \(\exists l_1, \ldots, k_k \in \Z\colon 1 = \sum_j l_ji_j\).
        Разобьем это на положительные и отрицательные части: \[
        \begin{aligned}
          1 &= \sum_{j = 1}^k l_ji_j = \sum_{j = 1}^k (l_j^+ - l_j^-)i_j = \sum_{j = 1}^k l_j^+i_j -
          \sum_{j = 1}^k l_j^-i_j \eqqcolon A - B.
        \end{aligned}
        \]

        Заметим, что \(A, B \in I(x)\) (\textit{\(I(x)\) --- полугруппа}); также, перегруппировывая, \(A = B + 1\).

        Деля с остатком, при \(k \ge l\) \[
        \begin{aligned}
          m &= kB + l = kB + l(A - B) = \underbrace{(k - l)B}_{\ge 0} + \underbrace{lA}_{\ge 0} \in
          I(x).
        \end{aligned}
        \]

        Тогда можно взять \(M \coloneqq B(B + 1)\).
      \end{proof}

    \item Положим, что \(L \coloneqq \max_{x, y \in E} l(x, y)\): \(L < \infty\) за счет конечности
      класса; \(M \coloneqq \max_{x \in E} M(x)\) (из предыдущего пункта).

      \(r \ge R \coloneqq L + M\); \(p^r(x \leadsto y) \ge p^{r - l(x, y)}(x \leadsto
      x)\underbrace{p^{l(x, y)}(x \leadsto y)}_{> 0} \oversym{>}{?} 0\). Достаточно показать, что
      \(r - l(x, y) \ge M(x)\).

      \begin{proof}
        \(r - l(x, y) \ge R - L = (L + M) - L = M \ge M(x)\).
      \end{proof}
  \end{enumerate}
\end{proof}

Особо внимательные могли заметить, что всё это --- подготовка к усилению предельной теоремы Маркова.

Комбинируя эти утверждения и предельную теоремы Маркова, получится
\begin{theorem}
  Если \(\mathcal{X}\) конечно, и цепь неприводима с периодом \(1\), то \(\exists \pi(y)\colon
  \pi(y) = \lim_{n \to \infty} p^n(x \leadsto y)\) такое, что \(\pi(\cdot)\) --- единственное
  инвариантное распределение и \(\exists c \ge 0, \alpha \in (0, 1)\colon \left| p^n(x \leadsto y) -
  \pi(y)\right| \le c\alpha^n \quad \forall x, y \in \mathcal{X} \forall n\).
\end{theorem}

Можно и лучше.
\begin{theorem}
  Если \(\mathcal{X}\) конечно и существует только один эргодический класс с периодом \(1\), то
  \(\exists \pi(y)\colon \pi(y) = \lim_{n \to \infty} p^n(x \leadsto y)\) такое, что \(\pi(\cdot)\)
  --- единственное инвариантное распределение, \(\exists c > 0, \alpha \in (0, 1)\colon \left| p^n(x
  \leadsto y) - \pi(y)\right| < c\alpha^n \quad \forall x, y \forall n\), \textit{причем}, если
  \(Y\) --- несущественное состояние, то предельного распределения в нем нет (\(\pi(y) = 0\)).
\end{theorem}

По сути это значит, что всё распределение \(\pi\) находится в этом эргодическом классе.

\begin{proof}
  Рассмотрим все возможные случаи (содержательных из которых только \(2\)).

  \begin{pfparts} \mnote{ЭК --- эргодический, ну вы поняли}
    \item[\(\boxed{x, y \in \text{ЭК}}\)] Верно по предельной теореме Маркова.

    \item[\(\boxed{x \in \text{ЭК}, y \notin \text{ЭК}}\)] За счет замкнутости эргодических классов,
      \(p^n(x \leadsto y) = 0\).

    Продолжение следует.
  \end{pfparts}
\end{proof}

\lecture{7}{29.03.2024, 13:40}{}

\subsection{Инвариантные (стационарные) распределения}

\begin{definition}
  Распределение --- это отображение \(\pi\colon \mathcal{X} \to [0, 1]\) такое, что \(\sum_{x \in
  \mathcal{X}} \pi(x) = 1\).
\end{definition}

\begin{definition}
  Распределение называется инвариантным, если \(\pi = \pi p\).
\end{definition}

В сущности это значит, что \(\forall y \in \mathcal{X} \quad \pi(y) = \sum_{x \in \mathcal{X}}
\pi(x)p(x \leadsto y)\) (система линейных уравнений).

\begin{example}
  Рассмотрим случайное блуждание на квадрате, вероятность перехода в вершину которого ---
  \(\frac{1}{2}\). У такой цепи Маркова инвариантое распределение --- \(\pi(x) = \frac{1}{4} \quad
  \forall x \in \mathcal{X}\).
\end{example}

\begin{example}
  Рассмотрим случайное блуждание на \(\Z\) с вероятностью перехода \(\frac{1}{2}\). Найдем его
  инвариантное распределение.

  \(\pi(x) = \pi(x + 1)\frac{1}{2} + \pi(x - 1)\frac{1}{2}\); преобразовывая, \(\pi(x) - \pi(x - 1)
  = \pi(x + 1) - \pi(x)\), то есть \(\pi(x) - \pi(x - 1) = \mathrm{const}\) --- но такого быть не
  может (в случае \(c \neq 0\)), ведь \(\pi\) окажется неограниченной функцией (или \(\sum_x \pi(x)
  \neq 1\) при \(c = 0\)).

  То есть у такой цепи Маркова инвариантного распределения не существует.
\end{example}

\begin{example}
  Рассмотрим случайное блуждание на графе (см. ранние примеры). Найдем его инвариантное
  распределение.

  Пусть \(N(x)\) --- количество ребер, \textit{выходящих} из вершины \(x\). Тогда рассмотрим
  распределение \(\pi(x) \coloneqq \frac{N(x)}{2E}\).

  Это и впрямь окажется инвариантным распределением: \(\sum_x \pi(x)p(x \leadsto y) = \sum_x
  \frac{N(x)}{2E}\frac{1}{N(x)} = \frac{N(y)}{2E}\).
\end{example}

Инвариантное распределение не обязательно единственно:
\begin{example}
  Рассмотрим вершины \(x_1, x_2, x_3\), где \(p(x_1 \leadsto x_2) = \frac{1}{2}\), \(p(x_1 \leadsto
  x_3) = \frac{1}{2}\), а \(p(x_2 \leadsto x_2) = p(x_3 \leadsto x_3) = 1\) (и других переходов
  нет).

  Тогда \(\pi_2(x_2) = 1\), \(\pi_2(x_1) = \pi_2(x_3) = 0\); \(\pi_3(x_3) = 1, \pi_3(x_1 =
  \pi_3(x_2) = 0\) --- два разных инвариантных распределений.
\end{example}

Любопытно заметить, что
\begin{statement}
  Множество инвариантных распределений выпукло.
\end{statement}

\begin{statement}
  Если \(\pi_0\) --- инвариантное распределение, то \(\forall n \ge 0 \quad \pi_n = \pi_0\).
\end{statement}
\begin{proof}
  По индукции.
  \begin{pfparts}
    \item[База] Очень очевидно.

    \item[Шаг индукции] Пусть \(\pi_{n - 1} = \pi_0\).

      Тогда \(\pi_n = \pi_0 p^n = \left( \pi_0p^{n - 1} \right)p = \pi_{n - 1}p = \pi_0p = \pi_0\)
      по индукционному предположению и инвариантности распределения.
  \end{pfparts}
\end{proof}

Это утверждение показывает, почему порой такие распределения называются стационарными.

\begin{statement}
  Алгебраически, инвариантность означает, что \(\pi\) --- собственный вектор собственного числа
  \(1\).
\end{statement}

\begin{statement} \mnote{сходимость в поточечном смысле: \(\forall x \in \mathcal{X} \quad \pi_n(x)
\to \pi(x)\)}
  Если \(\pi_n \xrightarrow{n \to \infty} \pi\) (и \(\pi\) --- распределение), то \(\pi\) --- инвариантное распределение.
\end{statement}
\begin{proof}
  Раз \(\pi_n \to \pi\), то \(\pi_{n + 1} = \pi_np \to \pi p\), то есть \(\pi = \pi p\) ---
  инвариантное распределение.

  Есть один подвох: непонятно, почему верно \(\left[ \pi_n \to \pi \right] \implies \left[ \pi_n p
  \to \pi p \right]\). Проверим это: \(\sum_x \pi_n(x)p(x \leadsto y) \implies \sum_x \pi(x)p(x
  \leadsto y)\)? Сразу видно, что для конечного случая это верно; интерес представляет только
  бесконечный.

  Оценим снизу (сверху предлагается оценить самим).
  \[
  \begin{aligned}
    \liminf\sum_{x \in \mathcal{X}} \pi_n(x)p(x \leadsto y) \ge \liminf \sum_{x \in \mathcal{X}_0}
    \pi_n(x)p(x \leadsto y) = \sum_{x \in \mathcal{X}_0} \pi(x)p(x \leadsto y),
  \end{aligned}
  \] где \(\mathcal{X}_0\) --- конечное подмножество \(\mathcal{X}\).

  Устремим \(\mathcal{X}_0 \to \mathcal{X}\) за счет счетности последнего: \[
    \ldots \xrightarrow{\mathcal{X}_0 \to \mathcal{X}} \sum_{x \in \mathcal{X}} \pi(x)p(x \leadsto
    y)
  ,\] --- конечно за счет того, что \(\pi\) --- распределение.
\end{proof}

\subsection{Предельная теорема Маркова}

Пусть \(\mathcal{X}\) конечно, а \(\delta = \min_{x, y} p(x \leadsto y) > 0\) (из каждого состояние
в каждое можно перейти за один шаг).

\begin{theorem}
  \(\forall x, y \exists \pi(y)\colon \pi(y) = \lim_{n \to \infty} p^n(x \leadsto y)\), причем
  \(\left| \pi(y) - p^n(x \leadsto y) \right| \le (1 - \delta)^n\) (\textit{очень быстро}).
\end{theorem}

Перед не самым коротким доказательством поразмышляем над этой теоремой.

\begin{statement}
  \(\pi\) в пределе является распределением.
\end{statement}
\begin{proof}
  Понятно, что \(\pi(y) \in [0, 1]\); \[
  \begin{aligned}
    \sum_{y \in \mathcal{X}} \pi(y) = \sum_{y \in \mathcal{X}} \lim_{n \to \infty} p^n(x \leadsto y)
    = \lim_{n \to \infty} \sum_{y \in \mathcal{X}} p^n(x \leadsto y) = 1,
  \end{aligned}
  \] --- здесь понадобилась конечность \(\mathcal{X}\).

  Последнее равенство уже пояснялось раннее; повторим его: \[
  \begin{aligned}
    \sum_y p^n(x \leadsto y) &= \sum \P\left( X_n = y \mid X_0 = x \right) = \frac{\sum_y \P\left( X_n =
    y, X_0 = x\right)}{\P\left( X_0 = x \right)} \\
    &= \frac{\P\left( X_0 = x \right)}{\P\left( X_0 = x \right)} = 1.
  \end{aligned}
  \]
\end{proof}

\begin{statement}
  \(\forall y \quad \pi_n(y) \xrightarrow{n \to \infty} \pi(y)\).
\end{statement}
\begin{proof}
  \[
  \begin{aligned}
    \pi_n(y) &= \sum_{x \in \mathcal{X}} \pi_0(x) p^n(x \leadsto y) \xrightarrow{n \to \infty} \sum_{x \in
    \mathcal{X}} \pi_0(x) \pi(y) \\
    &= \pi(y),
  \end{aligned}
  \] --- опять пригодилась конечность \(\mathcal{X}\).
\end{proof}

\begin{statement}
  \(\pi(y)\) --- инвариантное распределение: \(\pi p = \pi\).
\end{statement}
\begin{proof}
  Сразу следует из инвариантности любого предельного распределения.
\end{proof}

\begin{statement}
  \(\pi(y)\) --- \textit{единственное} инвариантное распределение.
\end{statement}
\begin{proof}
  Пусть \(\pi_*\) --- инвариантное распределение.

  Рассмотрим цепь, где \(\pi_0 = \pi_*\), а переходная функция такая же. Из инвариантности, \(\pi_n
  = \pi_*\).

  С другой стороны, \(\pi_n \to \pi\) (свойство о предельности); значит, \(\pi_*(y) \to \pi(y)\) и
  \(\pi_*(y) = \pi(y)\).
\end{proof}

Единственное, что нехорошо в теореме Маркова --- слишком ограничивающее условие на \(\delta\). На
самом деле его можно ослабить.

Пусть \(\exists m \ge 1\colon \delta = \min_{x, y} p^m(x \leadsto y) > 0\).

\begin{statement}
  \(\forall x, y \exists \pi(y)\colon \pi(y) = \lim_{n \to \infty} p^n(x \leadsto y)\) такое, что
  \(\left| \pi(y) - p^n(x \leadsto y) \right|\le C\alpha^n\), где \(C \in (0, +\infty)\), \(\alpha
  \in (0, 1)\).
\end{statement}

Приведем пояснение о скорости сходимости.
\begin{example}
  Рассмотрим блуждание по пятиугольнику. Понятно, что изначальное условие на \(\delta\) не
  выполняется и теорема Маркова не работает.

  С другой стороны, за \(m = 4\) можно попасть из любой вершины в другую.
\end{example}

\begin{example}
  Рассмотрим блуждание по квадрату. Здесь такого \(m\) для обобщения не найдется.
\end{example}

\begin{proof}[Доказательство обобщения]
  Предполагается, что теорема Маркова уже доказана.

  Пусть \(n = km + l\) (деление с остатком): по ассоциативности произведения матриц, \[
  \begin{aligned}
    p^n(x \leadsto y) &= p^{km + l}(x \leadsto y) = \sum_z p^l(x \leadsto z)p^{km}(z \leadsto y);
  \end{aligned}
  \] применяя теорему Маркова для \(p^{m}(z \leadsto y)\), \[
  \begin{aligned}
    \ldots \to \sum_z p^l(x \leadsto z)\pi(y) = \pi(y).
  \end{aligned}
  \]

  Оценим и скорость сходимости. За счет того, что \(\sum_z p^l(x \leadsto z) = 1\),
  \[
  \begin{aligned}
    \left| p^n(x \leadsto y) - \pi(y) \right| &= \left| \sum_z p^l(x \leadsto z)p^{km}(z \leadsto y)
    - \pi(y)\right| \\
    &= \left| \sum_z p^l(x \leadsto z)\left( p^{km}(z \leadsto y) - \pi(y) \right)\right| \\
    &\le \sum_z p^l(x \leadsto z) \left| p^{km}(z \leadsto y) - \pi(y) \right| \le \sum_z p^l(x
    \leadsto z)(1 - \delta)^k = (1 - \delta)^k \\
    &= \left( (1 - \delta)^{km} \right)^{\frac{1}{m}} =
    (\underbrace{-\delta^{\frac{1}{m}}}_{\alpha})^{km} = \alpha^{km} \\
    &= \frac{\alpha^n}{\alpha^l} \le \frac{\alpha^n}{\alpha^{m - 1}} = \frac{1}{\alpha^{m -
    1}}\alpha^n.
  \end{aligned}
  \]
\end{proof}

\begin{proof}[Доказательство теоремы Маркова]
  Зафиксируем \(y\) и рассмотрим все \(p^n(x \leadsto y)\). Положим, что \(m_n \coloneqq \min_x
  p^n(x \leadsto y)\) и \(M_n \coloneqq \max_x p^n(x \leadsto y)\).

  \begin{statement}
    \(m_n\) не убывает, \(M_n\) не возрастает, а \(\left| M_n - m_n \right| \le (1 - \delta)^n\).
  \end{statement}
  \begin{proof}
    \hfill
    \begin{pfparts}
    \item[Монотонность] Перемножая матрицы, \(p^{n + 1}(x \leadsto y) = \sum_z p(x \leadsto z)p^n(z
      \leadsto y) \ge \sum_z p(x \leadsto z)m_n = m_n\), ведь \(\sum_z \ldots = 1\).

      Следовательно, \(m_{n + 1} = \min_x p^{n + 1}(x \leadsto y) \ge m_n\).

      Аналогично (с точностью до исправления знаков) для \(M_n\).

    \item[Оценка] Нужно доказать, что \(M_{n + 1} - m_{n + 1} = \left( 1 - \delta \right)(M_n -
      m_n)\).

      Введем дополнительные обозначения: если \(x \in \R\), то \(x_+ \coloneqq \begin{cases}
        x, &x > 0 \\
        0, &x \le 0
      \end{cases}\), \(x_- \coloneqq \begin{cases}
        0, &x > 0 \\
        |x|, &x \le 0
      \end{cases}\); видно, что \(x = x_+ - x_-\).

      \[
      \begin{aligned}
        M_{n + 1} - m_{n + 1} &= p^{n + 1}(x_1 \leadsto y) - p^{n + 1}(x_2 \leadsto y) \\
        &=\sum_z p(x_1 \leadsto z)p^n(z \leadsto y) - p(x_2 \leadsto z)p^n(z \leadsto y) \\
        &= \sum_z\left( p(x_1 \leadsto z) - p(x_2 \leadsto z) \right)p^n(z \leadsto y) \\
        &\le \sum_z \left( p(x_1 \leadsto z) - p(x_2 \leadsto z) \right)_+M_n - \sum_z \left( p(x_1
        \leadsto z) - p(x_2 \leadsto z)\right)_- m_n \\
        &= \sum_z \left( p(x_1 \leadsto z) - p(x_2 \leadsto z) \right)_+ (M_n - m_n),
      \end{aligned}
      \] ведь вычитая первую сумму из второй получится что-то вроде \(\sum_z \left( (\ldots)_+ -
      (\ldots)_- \right) = \sum_z \left( p(x_1 \leadsto z) - p(x_2 \leadsto z) \right) = \sum
      (\ldots) - \sum (\ldots) = 1 - 1 = 0\).

      Осталось показать, что \(\sum_z \left( p(x_1 \leadsto z) - p(x_2 \leadsto z) \right)_+ \le 1 -
      \delta\). Пусть нашлось единственное такое \(z_0\), что \(p(x_1 \leadsto z_0) - p(x_2 \leadsto
      z_0) > 0\) (если все они равны \(0\) то неравенство тривиально). Тогда \[
      \begin{aligned}
        \left( p(x_1 \leadsto z_0) - p(x_2 \leadsto z_0) \right)_+ = p(x_1 \leadsto z_0) - p(x_2
        \leadsto z_0) \le p(x_1 \leadsto z_0) - \delta.
      \end{aligned}
      \]

      Если же нашлись другие такие \(z \neq z_0\), то \[
        \left( p(x_1 \leadsto z) - p(x_2 \leadsto z) \right)_+ \le p(x_1 \leadsto z ) =
       p(x_1 \leadsto z)
      ,\] и \[
        S \le p(x_1 \leadsto z_0) - \delta + \sum_{z \neq z_0} p(x_1 \leadsto z) = 1 - \delta
      .\]
    \end{pfparts}
  \end{proof}

  Тогда \(m_n \to \pi_-(y)\), \(M_n \to \pi_+(y)\), причем \(m_n \le \pi_-(y) \le \pi_+(y) \le
  M_n\): \[
    \pi_+(y) - \pi_-(y) \le M_n - m_n \le (1 - \delta)^n \implies \pi_+ = \pi_-(y) \eqqcolon \pi(y)
  .\]

  Получается, что \[
    \forall x \in \mathcal{X} \quad m_n \le \frac{p(x \leadsto y)}{\pi(y)} \le M_n \iff \left| p(x
    \leadsto y) - \pi(y) \right| \le M_n - m_n \le \left( 1 - \delta \right)^n
  .\]
\end{proof}

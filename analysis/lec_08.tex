\lecture{8}{11.03.2024, 11:15}{}

\begin{example}
  Как уже было показано раннее, \[
    \frac{(x_1 + \ldots + x_n)^j}{j!} = \sum_{|k| = j} \frac{x^k}{k!}
  .\]

  Дифференцируя это \(j\) раз, \[
    1 = C_k
  .\]
\end{example}

\chapter{Многомерный анализ}

Пусть \(e_1, \ldots, e_n\) --- базис \(\R^n\). Значения --- векторы, и обозначаются как
вектор-столбцы или вектор-строки: \[
  x = \begin{pmatrix} x_1 \\ \vdots \\ x_n \end{pmatrix} = x_1e_1 + \ldots + x_ne_n
.\]

\begin{definition}
  Скалярное произведение \(\inner{x}{y} \coloneqq \sum_{k = 1}^n x_ky_k\).
\end{definition}

\begin{definition}
  Норма \(\|x\| \coloneqq \sqrt{\inner{x}{x}}\).
\end{definition}

Понятно, что с таким определением \(\|x\| = \sqrt{\sum_{k = 1}^n x_k^2}\).

\begin{definition}
  Метрика \(\rho(x, y) \coloneqq \|x - y\|\).
\end{definition}

Ну а метрика порождает и топологию.

Аналогичным образом можно ввести ВП \(\C^n\) --- в нем скалярное произведения антисимметрично:
\(\inner{x}{y} \coloneqq \sum_{k = 1}^n x_k\conj{y_k}\), \(\inner{x}{y} = \conj{\inner{y}{x}}\).

Скалярное произведение можно ввести и для функций: \(\inner{f}{g} \coloneqq
\frac{1}{2\pi}\int_0^{2\pi} f(x)\conj{g(x)} \df{x}\).

Но этим мы сейчас заниматься не будем и рассматривать будем только \(\R^n\).

Для продолжения рекомендуется освежить в памяти такие разделы элементарной топологии, как
\begin{itemize}
  \item последовательность и ее сходимость;
  \item полные и банаховы пространства;
  \item компактность и секвенциальная компактность.
\end{itemize}

Рассмотрим \(\Omega \subseteq \R^n\) и функции \(f\colon \Omega \to \R^m\).

\begin{definition}
  \(\lim_{x \to a} f(x) = A\), если \[
    \forall \varepsilon > 0 \exists \delta\colon 0 < |x - a| <
    \delta, x \in \Omega \implies |f(x) - A| < \varepsilon
  .\]
\end{definition}

В многомерном анализе надо отдельно оговаривать, что подразумевается под \(0\), следя за
размерностью функции.

\begin{definition}
  Предел по направлению \(\lim_{t \to 0_+} f(a + th)\), где \(|h| = 1\).
\end{definition}

Понятно, что такой предел не обязательно существует и совсем не обязательно зависит от существования
``обычного'' предела в точке.

\begin{statement}
  Если \(A = \lim_{x \to a} f(x) = A\), то \(\lim_{t \to 0_+} f(a + th) = A\).
\end{statement}

В обратную сторону это не всегда верно:
\begin{example}
  Рассмотрим конус с вершиной в \((0, 0, 1)\) и основанием \(r = \varphi\) (\(0 < \varphi \le
  2\pi\)) с добавленным полуинтервалом \((0, 2\pi]\) \mnote{\(0\) и впрямь не включается} (всё в
  полярных координатах).

  Формальнее, \(z = f(x, y) = \begin{cases}
    \frac{\varphi - \sqrt{x^2 + y^2}}{\varphi}, &\sqrt{x^2 + y^2} \le \varphi \\
    0, &\text{иначе}.
  \end{cases}\)

  Тогда для \textit{любого} направления \(h\), \(\lim_{t \to 0_+} f(th) = 1\) (сразу можно увидеть
  на проекции), но в классическом понимании предела нет, ведь к нулю можно ``добраться'', например,
  по кривой \(r = \varphi\).
\end{example}

\begin{definition}
  Функция непрерывна на \(\Omega\) в точке \(a\), если \[
  \forall \varepsilon > 0 \exists \delta\colon |x - a| < \delta, x \in \Omega \implies |f(x) - f(a)|
  < \varepsilon
  .\]
\end{definition}

\begin{definition}
  Функция равномерно непрерывна на \(\Omega\), если \[
  \forall \varepsilon > 0 \exists \delta > 0\colon |x - y| < \delta \implies |f(x) - f(y)| <
  \varepsilon
  .\]
\end{definition}

Как уже всем известно,
\begin{statement}
  Функция, непрерывная на компакте,
  \begin{itemize}
    \item ограничена на нем;
    \item равномерно непрерывна на нем;
    \item достигает максимума и минимума на нем, если ее область определения --- компакт.
  \end{itemize}
\end{statement}

\begin{theorem}
  Любая норма в \(\R^n\) эквивалентна евклидовой.
\end{theorem}
\begin{proof}
  Пусть \(\|\cdot\|_1\) --- некоторая норма в \(\R^n\). Тогда \[
    \begin{aligned}
      \|x\|_1 = \|x_1e_1 + \ldots + x_ne_n\|_1 &\le |x_1|\|e_1\|_1 + \ldots + |x_n|\|e_n\|_1 \\
      &\oversym{\le}{\text{КБШ}} \sqrt{|x_1|^2 + \ldots + |x_n|^2}\sqrt{|\|e_1\|_1^2 + \ldots +
      \|e_n\|_1^2},
    \end{aligned}
  \] --- получается, что любая такая норма непрерывна, ведь \(|\|x\|_1 - \|y\|_1| \le \|x - y\|_1\).

  Но тогда существует \(\min_{x \in S} \|x\|_1 > 0\) (\(S\) --- сфера \(\coloneqq \left\{ x \in \R^n
  \mid \|x\| = 1 \right\}\)) как непрерывная функция на компакте --- \(c\|x\| \le \|x\|_1 \le
  C\|x\|\).
\end{proof}

\lecture{4}{22.02.2024, 15:35}{натуральная параметризация пути, интеграл вектор-функции}

Рассмотрим путь \(\gamma \in C^1[a, b]\) в \(\R^m\): \(\gamma = (\gamma_1(t), \ldots,
\gamma_m(t))\).

\begin{theorem}
  Если \(\gamma\) --- гладкий путь в \(\R^m\), то \[
    L_\gamma = \int_a^b |\gamma'(t)| dt = \int_a^b \sqrt{\sum_{j = 1}^m \gamma_j'^2(t)} dt
  .\]
\end{theorem}
\begin{proof}
  Зафиксируем произвольное \(\varepsilon > 0\): \begin{equation}
  \exists \Delta\colon d(\Delta) < \delta, L_\gamma - \sum_{k = 1}^n |\gamma(t_k) - \gamma(t_{k -
  1})| < \frac{\varepsilon}{2}.
  \end{equation}

  Также верно, что \begin{equation}
  \forall j \quad |x - y| < \delta \implies \left| \gamma_j'^2(x) - \gamma_j'^2(y) \right| <
  \frac{\varepsilon^2}{4m}.
  \end{equation}

  Ну и наконец, последнее равенство (применяя теорему о среднем): \begin{equation}
    \int_a^b |\gamma'(t)| dt = \sum_{k = 1}^n \int_{t_{k - 1}}^{t_k} |\gamma'(t)| dt = \sum_{k =
    1}^n \left|\gamma'(\xi_k)\right|(t_k - t_{k - 1}),
  \end{equation} где \(\xi_k \in [t_{k - 1}, t_k]\).

  Понятно, что модуль непрерывный функции тоже непрерывен (в том числе в многомерном случае).

  Осталось собрать всё воедино (из-за многомерности по-нормальному сумму Дарбу не расписать): \[
    \begin{aligned}
      \left|\sum_{k = 1}^m \left| \gamma(t_k) - \gamma(t_{k - 1}) \right| - \int_a^b |\gamma'(t)|
      dt\right| &\oversym{=}{(c)} \left|\sum_{k = 1}^n \sqrt{\sum_{j = 1}^m \left( \gamma_j(t_k) -
      \gamma_j(t_{k - 1}) \right)^2} - \sum_{k = 1}^n \sqrt{\sum_{j = 1}^m \gamma_j'^2(\xi_k)}(t_k -
      t_{k - 1})\right| \\
                &\oversym{\le}{\triangle} \sum_{k = 1}^n \left| \sqrt{\sum_{j = 1}^m (
                \frac{\gamma_j(t_k) - \gamma_j(t_{k - 1})}{t_k - t_{k - 1}}} )^2 -
                \sqrt{\sum_{j = 1}^m \gamma_j'^2(\xi_k)} \right| (t_k - t_{k - 1}) \\
                &\oversym{=}{\text{MVT}} \sum_{k = 1}^n \left| \sqrt{\sum_{j = 1}^m
                \gamma_j'^2(\xi_{kj})} - \sqrt{\sum_{j = 1}^m \gamma_j'^2(\xi_k)} \right|(t_k -
                  t_{k - 1}) \\
                &\le \sum_{k = 1}^n \sqrt{\left|\sum_{j = 1}^m \gamma_j^2(\xi_{kj}) - \sum_{j = 1}^m
                \gamma_j'^2(\xi_k)\right|} (t_k - t_{k - 1}) \\
                &\le \sum_{k = 1}^n \sqrt{\sum_{j = 1}^m \left|\gamma_j'^2(\xi_{kj}) -
                \gamma_j'^2(\xi_k) \right|}(t_k - t_{k - 1}) \\
                &\oversym{\le}{(b)} \sum_{k = 1}^n \sqrt{m \frac{\varepsilon^2}{4m}}(t_k - t_{k -
                1}) = \frac{\varepsilon}{2}(b - a)^2
    \end{aligned}
  \] (после MVT использовалось неравенство  \(|\sqrt{a} - \sqrt{b}| \le \sqrt{|a - b|}\)).

  И итого: \[
    \left| L - \int_a^b |\gamma'(t)| dt \right| \le \frac{\varepsilon}{2} + \frac{\varepsilon}{2}(b
    - a)
  .\]
\end{proof}

Оказывается, натуральная замена может испортить гладкость пути.

Пусть \(\gamma = \gamma(s)\), где \(s\) --- натуральный параметр: понятно, что тогда \(s =
L_\gamma(s) = \int_a^s |\gamma'(t)| dt\); положим, что \(\gamma \in C^1[a, b]\). Получается, что
\(|\gamma'(s)| = 1\).

\begin{definition}
  Кривая называется регулярной, если \(\gamma(s) \in C^1[a, b]\).
\end{definition}

По-другому говоря,
\begin{statement}
  Кривая регулярная тогда и только тогда, когда \(\gamma \in C^1[a, b]\) и \(\gamma' \neq 0\).
\end{statement}

Рассмотрим примеры таких кривых.

\begin{example}
  Пусть \(z \in \C\), \(w\colon z \mapsto z + \frac{z^2}{2}\) (отображение окружности). Рассмотрим
  уже известный физически не гладкий путь (\textit{кардиоида}) \(e^{it} + \frac{e^{i2t}}{2} = (
  \underbrace{\cos{t} + \frac{\cos{2t}}{2}}_{u(t)}, \underbrace{\sin{t} + \frac{\sin{2t}}{2}}_{v(t)}
  )\). Тогда \[
    L(t) = \int_0^t |z'(t)| dt = \int_0^t \left| 1 + e^{i\tau} \right| d\tau = \int_0^t
    \left|e^{i\frac{\tau}{2}}\left( e^{-i\frac{\tau}{2}} + e^{i\frac{\tau}{2}} \right)\right| dt =
    2\int_0^t \left| \cos\frac{\tau}{2} \right| dt
  .\]

  Положим, что \(t \in [0, \pi]\) --- тогда всё упростится: \(L(t) = 4\sin\frac{t}{2}\).

  Теперь, наконец-то, введем натуральный параметр: \[
    s = L(t) = 4\sin{\frac{t}{2}} \iff t = 2\arcsin{\frac{s}{4}}
  .\]

  Или, по-другому, \(\cos{t} = 1 - 2\sin{\frac{t}{2}} = 1 - \frac{s^2}{8}\) --- для \(\sin\)
  аналогично.

  Понятно, что \(|\gamma'(s)| = 1\); \(u'(t) =
  -2\sin{\frac{3}{2}t}\underbrace{\cos{\frac{t}{2}}}_{\ge 0}\) --- как только \(t \ge
  \frac{2\pi}{3}\), \(u'(t)\) возрастает.

  Посмотрим, что будет при \(t \to \pi\): \[
    \frac{dv}{du} = \frac{dv / dt}{du / dt} = \frac{\cos{t} +
  \cos{2t}}{-2\sin{\frac{3}{2}t}\cos{\frac{t}{2}}} =
  \frac{\cos{\frac{3}{2}t}\cos{\frac{t}{2}}}{-\sin{\frac{3}{2}t}\cos{\frac{t}{2}}} \xrightarrow{t
  \to \pi} 0
  .\]

  Теперь рассмотрим натуральный параметр: вектор скорости единичный и ``смотрит'' вправо (в точке
  \(\pi\)). Но, пойдя в другую сторону, вектор будет смотреть ``влево'' (разрыв) --- путь стал
  негладким.
\end{example}

\subsection{Интеграл от вектор-функции}

Пусть \(f(t) = (f_1(t), \ldots, f_m(t))\), \(f\colon [a, b] \to \R^m\).

\begin{definition}
   \[
  \int_a^b f(t) dt = \left( \int_a^b f_1(t) dt, \ldots, \int_a^b f_m(t) dt \right)
  .\]
\end{definition}

Заметим, что формула Ньютона-Лейбница все еще работает.

Хоть формулы Лагранжа нет, но есть полезная оценка:
\begin{statement}
  \[
  \left| \int_a^b f(t) dt \right| \le M(b - a)
  ,\] где \(M = \sup_{x \in [a, b]} |f(x)|\).
\end{statement}
\begin{proof}
  \[
    \begin{aligned}
      \left| \int_a^b f(t) dt \right| &= \sqrt{\sum_{j = 1}^m \left( \int_a^b f_j(t) dt \right)^2}
      \\
                                      &\stackrel{\text{КБШ}}{\le} \sqrt{\sum_{j = 1}^m (b - a)
                                      \int_a^b f_j^2(t) dt} = \sqrt{(b - a)\int_a^b |f(t)|^2 dt} \le
                                      \sqrt{(b - a)^2 M^2} = (b - a)M.
    \end{aligned}
  \]
\end{proof}

\begin{statement}[неравенство Лагранжа]
  Если \(f \in C[a, b]\), то \[
  |f(b) - f(a)| \le |f'(c)||b - a|
  \] для некоторого \(c \in [a, b]\).
\end{statement}

Через пару лекций это неравенство будет доказано в более общем виде (в текущем виде можно доказать
через Ньютона-Лейбница: \(f(b) - f(a) = \int_a^b f'(t) dt\)).

Построим пример, почему обычное равенство Лагранжа в многомерном случае не обязательно неверно:

\begin{example}
  \(f(t) = (\cos{t}, \sin{t})\) (\(t \in [0, 2\pi]\)): \((0, 0) = f(0) - f(2\pi)\), \(|(0, 0)| =
  0\).
\end{example}

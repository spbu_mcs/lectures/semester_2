\lecture{5}{29.02.2024, 13:40}{} \mnote{лекция простая, читать не рекомендуется --- лучше порешайте
алгебру}

\section{Числовые ряды}

\begin{definition}
  \(\sum_{k = 1}^\infty a_k = a_1 + a_2 + \ldots\), \(a_k \in \R\) (или \(\C\)) --- числовой ряд.
\end{definition}

\begin{definition}
  \(S_n \coloneqq \sum_{k = 1}^n a_k\) --- частичные суммы.
\end{definition}

\begin{definition}
  Ряд \(\sum a_k\) называется сходящимся к \(S\), если существует \(\lim_{n \to \infty} S_n = S\).
\end{definition}

\begin{definition}
  Остаток ряда \(R_n \coloneqq S - S_n = \sum_{k = n + 1}^\infty a_k\).
\end{definition}

\begin{statement}
  Если ряд сходится, то \(R_n \to 0\).
\end{statement}

\begin{statement}[линейность]
  Если ряды \(\sum a_k\) и \(\sum b_k\) сходится к \(A\) и \(B\), то ряд \(\sum (\alpha a_k + \beta
  b_k)\) сходится к \(\alpha A + \beta B\).
\end{statement}

\begin{statement}
  Если \(z_k \coloneqq x_k + iy_k\), то \(\sum z_k = \sum x_k + i\sum y_k\).
\end{statement}

Самый ``надежный'' критерий в числовых рядах:
\begin{theorem}[критерий Коши]
  Ряд \(\sum a_k\) сходится тогда и только тогда, когда \[
    \forall \varepsilon > 0 \exists N = N(\varepsilon)\colon \forall p \in \N_0 \quad n >
    N(\varepsilon) \implies \left| \sum_{k = n}^{n + p} a_k \right| < \varepsilon
  .\]
\end{theorem}

Полезным и удобным оказывается необходимое условие критерия Коши:
\begin{statement}
  Если ряд \(\sum a_k\) сходится, то \(\lim_{k \to \infty} a_k = 0\).
\end{statement}

\begin{example}
  \(1 - 1 + 1 - 1 + \ldots\) расходится.
\end{example}
\begin{proof}
  Сразу понятно, что он расходится по необходимому условию.

  Другое объяснение: можно расставить скобки как \((1 - 1) + (1 - 1) + \ldots = 0 + \ldots + 0 +
  \ldots\), или как \(1 + (-1 + 1) + (-1 + 1) + \ldots = 1 + 0 + \ldots + 0 + \ldots\).
\end{proof}

Это наводит на следующее понятие:
\begin{definition}
  В ряде \(\sum a_k\), группировка ряда --- \(\sum A_j\), где \(A_j \coloneqq \sum_{k = n_j +
  1}^{n_{j + 1}} a_k\), а \(n_j\) строго возрастает.
\end{definition}

Проще говоря, группировка ряда --- расставление скобок.

\begin{theorem}[о группировке ряда]
  \hfill
  \begin{enumerate}
    \item Если ряд \(\sum a_k\) сходится к \(S\), то любая его группировка \(\sum A_j\) тоже
      сходится к \(S\);
    \item Если \(a_k \to 0\), \(n_{j + 1} - n_j \le L\) (ограниченное число),\mnote{в группировке
      не очень много слагаемых} а группировка \(\sum A_j\) сходится к \(S\), то \(\sum a_k\) тоже
      сходится к \(S\);
    \item Если в группировке \(\sum A_j\) все слагаемые одного знака и \(\sum A_j\) сходится к
      \(S\), то \(\sum a_k\) тоже сходится к \(S\).
  \end{enumerate}
\end{theorem}
\begin{proof}
  \hfill
  \begin{enumerate}
    \item ``В лоб'': последовательность сходится, а группировка --- просто некоторая
      подпоследовательность \(a_k\): \(A_1 + \ldots + A_m = a_{n_1} + \ldots + a_{n_{m + 1}}\).

    \item Будем применять критерий Коши. Выделим \(m\): \(n_j \le m \le n_{j + 1}\), и выберем к
      нему такой \(N(\varepsilon)\), что \(|a_m| \le \frac{\varepsilon}{4L}\) при \(m \ge
      N(\varepsilon)\).

      Рассмотрим сумму \(|a_m + \ldots + a_{m + p}|\) --- достаточно показать, что она меньше \(
      \varepsilon\) при \(m \ge N(\varepsilon)\).

      Для \(s \le L\), выпишем сумму \(a_m + a_{m + 1} + \ldots + a_{m + s} + A_{j + 1} + A_{j + 2}
      + \ldots + A_{j + t} + a_p + a_{p + 1} + \ldots + a_{p + q}\) \mnote{выделение группировки}
      --- членов \(a_k\) вне выделенной группировки не больше \(2L\), причем все они не превосходят
       \(\frac{\varepsilon}{4L}\) при \(m \ge N(\varepsilon)\).

      С другой стороны, группировка \(\sum A_j\) сходится --- по критерию Коши выберем \(j\) такое,
      что при \(m \ge N(\varepsilon) \quad |A_{j + 1} + \ldots + A_{j + t}| <
      \frac{\varepsilon}{2}\).

      Но это завершает доказательство: \(|a_m + \ldots + a_{m + p}| < \varepsilon\).

    \item Распишем сумму, ``вычленив'' группировку: \mnote{или можно рассмотреть частичные суммы}
      \[
        \begin{aligned}
          |a_m + a_{m + 1} + \ldots + a_{m + p}| &= |a_m + a_{m + 1} + \ldots + a_{m + s} + A_{j +
          1} + \ldots + A_{j + t} + a_r + \ldots + a_{r + p}| \\
                                                 &\le \underbrace{|A_j|}_{\to 0} + |A_{j + 1} +
                                                 \ldots + A_{j + t}| + \underbrace{|A_{j + t +
                                                 1}|}_{\to 0}.
        \end{aligned}
      \]
  \end{enumerate}
\end{proof}

\subsection{Положительные ряды}

Пусть все \(a_k > 0\).

\begin{notation}
  \(\sum a_k < +\infty\) --- ряд \(\sum a_k\) сходится.
\end{notation}

\begin{statement}
  Если \(\varphi\colon \N \to \N\) --- некоторая перестановка (биекция), то \(\sum a_k =
  \sum a_{\varphi(k)}\).
\end{statement}
\begin{proof}
  \(a_{\varphi(1)} + \ldots + a_{\varphi(n)} \le \sum_{k = 1}^\infty a_k\).
\end{proof}

Удобно исследовать сходимость положительных рядов признаками сравнения.

\begin{theorem}
  Если \(a_k = \bigO(b_k)\), а \(\sum b_k < +\infty\), то \(\sum a_k < +\infty\).
\end{theorem}

И в другую сторону:
\begin{consequence}
  Если \(\sum a_k = +\infty\), то \(\sum b_k = +\infty\).
\end{consequence}

\begin{example}
  Пусть \(a_n \ge 0\), \(a_n\) монотонна и \(a_n \to 0\).

  Тогда \(\sum a_n < +\infty \iff \sum 2^n a_{2^n} < +\infty\).
\end{example}
\begin{proof}
  \[
    \sum_{k = 1}^\infty a_k = \sum_{j = 0}^\infty \sum_{k = 2^j}^{2^{j + 1} - 1} a_k \le \sum_{j =
    0}^\infty \sum_{k = 2^j}^{2^{j + 1} - 1} a_{2^j} \le \sum_{j = 0}^\infty 2^j a_{2^j}
  .\]

  Доказательство в обратную сторону --- упражнение.
\end{proof}

\subsection{Признаки сходимости}

Теперь ряд не обязательно положительный.

\begin{theorem}[радикальный признак Коши]
  Если \(q \coloneqq \limsup_{n \to \infty} \sqrt[n]{a_n} > 1\), то ряд \(\sum a_k\) расходится;
  иначе --- сходится.
\end{theorem}
\begin{proof}
  Понятно, что если ряд сходится, то \(q \le 1\), и при \(q > 1\) ряд расходится.

  Если \(\limsup_{n \to \infty} \sqrt[n]{|a_n|} = q < 1\), то \(\exists N = N(\varepsilon)\colon N >
  N(\varepsilon) \implies \sqrt[n]{|a_n|} < (q + \varepsilon)\) --- выберем \(\varepsilon\) такое,
  что \((q + \varepsilon) < 1\).

  Тогда ряд \(\sum (q + \varepsilon)^n\) сходится как геометрическая прогрессия, а по признаку
  сравнения, \(\sum a_n\) тоже сходится (\(|a_n| < (q + \varepsilon)^n\)).
\end{proof}

\begin{theorem}[интегральный признак]
  Если функция \(f(x)\) монотонная, \(f\colon [1, +\infty] \to \R_+\), то ряд \(\sum_{n = 1}^\infty
  f(n)\) сходится тогда и только тогда, когда \(\int_1^\infty f(x) dx\) сходится.
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\impliedby}\)] Самим.

    \item[\(\boxed{\implies}\)] \[
        \int_1^M f(x) dx = \sum_{k = 1}^{[M] - 1} \int_k^{k + 1} f(x) dx + \int_{[M]}^M f(x) dx
      .\]

      Для определенности положим, что \(f\) убывает: тогда \[
        \ldots \le \sum_{k = 1}^{[M] - 1} f(k) + (M - [M])f([M])
      ,\] --- осталось применить признак сравнения, ведь ограниченный интеграл обязательно сходится.
  \end{pfparts}
\end{proof}

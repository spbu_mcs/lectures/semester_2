\lecture{12}{03.05.2024, 13:40}{}

Отметим, что вообще говоря в условии на \(c > 1\) достаточно и \(p_n \ge \ldots\), ведь если он
связен при равенстве, то и с большим количеством дуг он останется связным. Аналогично, в условии на
\(c < 1\) достаточно \(p_n \le \ldots\).

\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{c > 1}\)] Будем показывать, что \(\P(\lnot X) \to 0\).

      То, что граф не связен, значит, что есть некая несвязная с другими компонента.

      Пусть \(V\) --- некоторый непустой набор вершин: \(1 \le |V| \le n - 1\). \(\P\left( V \text{
      изолирован} \right) = (1 - p_n)^{|V|(n - |V|)}\) (количество начал и концов).

      Тогда \(\P(\lnot X) \le \sum_{1 \le |V| < n} \P\left( V \text{ изолирован} \right)\). Разобьем
      последнюю сумму по \(|V|\): \[
      \begin{aligned}
        \ldots &= \sum_{k = 1}^{n - 1} \sum_{|V| = k} \P\left( V \text{ изолирован} \right) =
        \sum_{k = 1}^{n - 1} C_n^k (1 - p_n)^{k(n - k)};
      \end{aligned}
      \] используя неравенство \(1 - x \le e^{-x}\), \[
      \begin{aligned}
        \ldots &\le \sum_{k = 1}^{n - 1} C_n^k \exp\left(-\frac{c\ln{n}}{n} k(n - k)\right).
      \end{aligned}
      \]

      Рассмотрим самое доминирующее слагаемое --- \(k = 1\): \[
        n\exp\left( -\frac{c\ln{n}}{n} (n - 1) \right) \sim_n n n^{-c} = n^{1 - c} \xrightarrow{c >
        1} 0
      .\]

      Не будем пренебрегать точностью и оценим всё сумму --- разобъем ее для всех ``маленьких'' и
      ``больших'' \(k\), заметим симметрию слагаемых относительно \(k \mapsto n - k\):
      \mnote{поэтому сумма до \(\frac{n}{2}\)} \[
      \begin{aligned}
        \sum_{k = 1}^{\frac{n}{2}} \ldots = \sum_{k = 1}^{\varepsilon n} \ldots + \sum_{k =
        \varepsilon n}^{\frac{n}{2}} \ldots.
      \end{aligned}
      \]

      Пусть \(k \in \left[\varepsilon n, \frac{n}{2}\right]\): \(k \ge \varepsilon n\), \(n - k \ge
      \frac{n}{2}\). Тогда \[
      \begin{aligned}
        \exp\left( -\frac{c\ln{n}}{n} k(n - k) \right) \le \exp\left( -\frac{c\ln{n}}{n} \varepsilon
        n \frac{n}{2}\right) = n^{-c \frac{\varepsilon}{2} n}.
      \end{aligned}
      \]

      Используем это для оценки части суммы: \[
      \begin{aligned}
        \sum_{k = \varepsilon n}^{\frac{n}{2}} C_n^k \exp\left( -\frac{c\ln{n}}{n} k(n - k) \right)
        \le \sum_{k = 0}^n C_n^k n^{-c \frac{\varepsilon}{2} n} = 2^n n^{-c \frac{\varepsilon}{2} n}
        \xrightarrow{n \to \infty} 0
      \end{aligned}
      .\]

      Оценим другую часть суммы. Пусть \(1 \le k \le \varepsilon n\). Воспользуемся следующей
      оценкой: \[
        C_n^k \le \frac{n!}{k!(n - k)!} \le \frac{n^k}{k!}
      .\]

      Раз  \(k \ge 1\), то \(n - k \ge n - \varepsilon n = (1 - \varepsilon) n\) и \(\frac{n - k}{n}
      \ge 1 - \varepsilon\).

      Применим это для искомой оценки: \[
      \begin{aligned}
        \sum_{k = 1}^{\varepsilon n} \frac{1}{k!} n^k n^{-ck(1 - \varepsilon)} &\oversym{\le}{k \ge
        1} \sum_{k = 1}^{\infty} \frac{1}{k!} n^{(1 - c(1 - \varepsilon))} = e n^{(1 - c(1 -
        \varepsilon))} \xrightarrow{n \to \infty} 0,
      \end{aligned}
      \] ведь \(1 < c(1 - \varepsilon)\) верно при достаточно малых \(\varepsilon\).

    \item[\(\boxed{c < 1}\)] Докажем более простую вещь: в таком графе с вероятностью, стремящейся к
      \(1\), есть изолированная вершина.

      Введем случайные величины \(Y_i \coloneqq \begin{cases}
        1, &i \text{ изолирована} \\
        0, &\text{иначе}
      \end{cases}\).

      Положим, что \(S \coloneqq \sum_{i = 1}^n Y_i\) --- число изолированных вершин. Будем
      показывать, что \(\P(S = 0) \xrightarrow{n \to \infty} 0\).

      Начнем ``с конца'': пусть известны \(\E{S}\) и \(\D{S}\). Тогда: \[
        \D{S} = \E(S - \E{S})^2 = \sum_{k = 0}^\infty \P(S = k)(k - \E{S})^2 \oversym{\ge}{k = 0}
        \P(S = 0)(\E{S})^2;
      \] получается, что \(\P(S = 0) \le \frac{\D{S}}{(\E{S})^2}\).

      Вычислим мат. ожидание и дисперсию.

      \[
      \begin{aligned}
        \E{S} &= \E \sum_i Y_i = n\E{Y_i} = n\P(Y_1 = 1) = n(1 - p_n)^{n - 1}.
      \end{aligned}
      \]

      Поймем, как устроены подобного рода выражения: \[
      \begin{aligned}
        (1 - p_n)^n &= \exp\left( \ln(1 - p_n)n \right) = \exp\left( (-p_n + \bigO(p_n^2)) n \right)
        \\
        &= \exp\left( -c\ln{n} + \bigO\left( \frac{\ln{n^2}}{n} \right) \right) \to \exp\left(
        -c\ln{n} + \bigO(n) \right) = n^{-c}(1 + \smallO(1)).
      \end{aligned}
      \]

      Тогда, возвращаясь к мат. ожиданию, \[
        \E{S} = n^{1 - c} \xrightarrow{c < 1} \infty
      .\]

      Посчитаем дисперсию. Заметим, что \[
        \E{Y_i^2} = \E{Y_i} = (1 - p_n)^{n - 1}
      .\]

      С другой стороны, при \(i \neq j\), \[
        \E{Y_iY_j} = (1 - p_n)^{2n - 3}
      ,\] --- из первой выходит \(n - 1\) дуга, из второй столько же, но при этом одна дуга
      посчитана дважды.

      \[
      \begin{aligned}
        \E{S^2} &= \E\left( \sum_i Y_i \right)^2 = n\E{Y_1^2} + n(n - 1)\E{Y_1Y_2} \le n\E{Y_1^2} +
        n^2\E{Y_1Y_2}.
      \end{aligned}
      \]

      \[
      \begin{aligned}
        \D{S} &= \E{S^2} - \E^2{S} \le n\E{Y_1^2} + n^2\E{Y_1Y_2} - n^2\E^2{Y_1} \\
        &= n(1 - p_n)^{n - 1} + n^2\left( (1 - p_n)^{2n - 3} - (1 - p_n)^{2n - 2} \right).
      \end{aligned}
      \]

      Посмотрим на каждое слагаемое по отдельности.

      \[
        n(1 - p_n)^{n - 1} \sim_n n^{1 - c}
      .\]

      \[
        n^2(1 - p_n)^{2n - 3}p_n \sim_n n^2 n^{-2c} \frac{c\ln{n}}{n} = c n^{1 - 2c} \ln{n} \ll n^{1
        - c}
      .\]

      И итого: \[
        \D{S} \sim n^{1 - c}
      .\]

      Подставим всё в изначальную оценку и убедимся в искомом: \[
        \frac{\D{S}}{\E^2{S}} \le \frac{n^{1 - c}(1 + \smallO(1))}{n^{2(1 - c)}} \xrightarrow{c < 1}
        0
      .\]

    \item[\(\boxed{p_n = \ldots}\)] Без доказательства.
  \end{pfparts}
\end{proof}

Из интересного еще можно рассмотреть (без доказательства)
\begin{theorem}[о гигантских компонентах]
  \hfill
  \begin{enumerate}
    \item Если \(p_n = \frac{c}{n}\), \(c > 1\), \mnote{по пред. теореме такой графы несвязен} то
      \(\exists \gamma = \gamma(c)\colon \P( G(n, p_n) \text{ имеет КС размера } \ge \gamma n) \to
      1\).
    \item Если \(p_n = \frac{c}{n}\), \(c < 1\), то \(\exists \beta = \beta(c)\colon \P(G(n, p_n)
      \text{ имеет КС размера } > \beta\ln{n}) \to 0\).
  \end{enumerate}
\end{theorem}


\subsection{Модель преимущественного присоединения Барабаши-Альберта}

Очень простая \textit{динамическая} модель обладающая большим количеством интересных свойств.

Алгоритм построения:
\begin{enumerate}
  \item есть \(2\) вершины, соединенные дугой;
  \item добавляется новая вершина и соединяется дугой с произвольной вершиной с вероятностью,
    пропорциональной степени вершин.
  \item повторяется шаг \(2\).
\end{enumerate}

Несколько простых наблюдений после шага \(n\):
\begin{itemize}
  \item есть \(n + 2\) вершины;
  \item есть \(n + 1\) дуг;
  \item суммарная степень вершин \(2(n + 1)\).
\end{itemize}

Таким образом можно сказать, что
\begin{statement}
  \(\P( \text{к вершине } j \text{ на шаге } n + 1 \text{ добавится новая дуга} ) =
  \frac{\deg{j}}{2(n + 1)}\).
\end{statement}

Этой формулы достаточно для изучения всей модели.

\begin{remark}
  Итоговый граф --- дерево, что несколько ограничивает применимость модели.
\end{remark}

\paragraph{Поведение степеней вершин}

Положим, что \(X_n^{(l)} \coloneqq \deg{l}\) после \(n\) шагов. Посмотрим, как ведет себя
математическое ожидание такой величины.

Посмотрим сначала на \(l = 1\).

Ясно, что \(X_0^{(1)} = 1\); дальше пока непонятно.

Рассмотрим следующую величину: \[
\begin{aligned}
  \P\left( X_{n + 1}^{(1)} - X_n^{(1)} = 1 \mid X_n^{(1)} = d \right) = \frac{d}{2n + 2}.
\end{aligned}
\]

Тогда \[
\begin{aligned}
  \E{X_{n + 1}^{(1)}} - \E{X_n^{(1)}} &= \E\left( X_{n + 1}^{(1)} - X_n^{(1)} \right) = \P\left(
  X_{n + 1}^{(1)} - X_n^{(1)} = 1 \right) \\
  &= \sum_{d = 1}^\infty \P\left( X_{n + 1}^{(1)} - X_n^{(1)} = 1 \mid X_n^{(1)} = d \right)\P\left(
  X_n^{(1)} = d \right) \\
  &= \sum_{d = 1}^\infty \frac{d}{2(n + 1)} \P\left( X_n^{(1)} = d \right) =
  \frac{\E{X_n^{(1)}}}{2(n + 1)}.
\end{aligned}
\]

Получилась такая рекуррентная формула: \[
  \E{X_{n + 1}^{(1)}} = \E{X_n^{(1)}}\left( 1 + \frac{1}{2(n + 1)} \right) = \frac{2n + 3}{2n + 2}
  \E{X_n^{(1)}}
.\]

Посмотрим на первые значения: \(\E X_0^{(1)} = 0\), \(\E X_1^{(1)} = \frac{3}{2}\), \(\E X_2^{(1)} =
\frac{3}{2} \frac{5}{4}\), \(\E X_3^{(1)} = \frac{3}{2} \frac{5}{4} \frac{7}{6}\).

Становится ясно, что \[
  \boxed{\E{X_n^{(1)}} = \frac{(2n + 1)!!}{(2n)!!}}.
\]

Посмотрим на асимптотическое поведение этой величины: \[
\begin{aligned}
  \E{X_n^{(1)}} &= \frac{(2n + 1)!}{((2n)!!)^2} = \frac{(2n + 1)!}{2^{2n}(n!)^2} \sim
  \frac{\sqrt{2\pi} \sqrt{2n + 1} \left( \frac{2n + 1}{e} \right)^{2n + 1}}{2^{2n} 2\pi n \left(
  \frac{n}{e} \right)^{2n}} \\
  &= \frac{\sqrt{2n + 1} (2n + 1)}{\sqrt{2\pi} n} \underbrace{\left( \frac{2n + 1}{2n} \right)^{2n}
  e^{-1}}_{\to 1} = \boxed{\frac{2\sqrt{n}}{\sqrt{\pi}}}.
\end{aligned}
\]

Это значит, что первая вершина имеет порядка \(\sqrt{n}\) вершин, однако отношение дуг к вершинам --
константа \(2\), то есть каждая вершина в среднем имеет \(2\) дуги.

Это одно из первых интересных свойств данной модели: вершины, появляющиеся в начале, имеют больше
дуг тем те, которые появляются в конце.

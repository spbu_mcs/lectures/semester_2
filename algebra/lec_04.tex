\lecture{4}{27.02.2024, 13:40}{}

Продолжим исследовать связи между \(F\) и \(F[t]\)-модулями.

Аналогично, \(\Ann(M) = \left\{ r \in F[t] \mid \forall m \in M \quad rm = 0 \right\} \leqslant
F[t]\) --- идеал в \(F[t]\); \(M \cong \qfrac{F[t]}{(f_1)} \oplus \ldots \oplus
\qfrac{F[t]}{(f_k)}\), где \(f_1 \divs f_2 \divs \ldots \divs f_k\).

Заметим также, что \(\Ann(M) = (f_k)\) --- дальше это пригодится.

Что может значить \(f(t) \in \Ann(M)\) с точки зрения векторных пространств? Например, \(\forall v
\in V \quad f(t)v = 0\), но переменная \(t\) действует как оператор \(A\colon V \to V\) (\(V\) --- КМ ВП)
--- тогда \(f(t)v = (a_nA^n + \ldots + a_0)v\). Получается, что
\begin{statement} \mnote{оператор, любой вектор отправляющий в ноль --- нулевой}
  \(f(t) \in \Ann(M) \iff f(A) = 0\).
\end{statement}

В частности, \(f_k(A) = 0\).

\begin{definition}
  \(f_k\) называется минимальным многочленом оператора \(A\).
\end{definition}

Рассуждения выше показывают, что такой многочлен найдется и будет инвариантным.

Положим, что \(e_1, \ldots, e_n\) --- базис \(V\); в частности, это система образующих
\(F[t]\)-модуля: \(F[t]^n \xrightarrow{m_i \mapsto e_i} V\) (\(m_i\) --- базис свободного модуля).

Как с \(R\)-модулями, будем строить цепочку \(F[t]^N \to F[t]^n \surjto{\varphi} V\) (\(N < n\))
(первое отображение совсем не обязательно инъекция).

Рассмотрим, что происходит с ядром: \(\varphi(tm_i) = te_i = \sum_j a_{ji}e_j\) (\(A \leftrightarrow
(a_{ij})\), а переменной \(t\) сопоставляется оператор \(V \to V\)), но, с другой стороны, \(\sum_j
a_{ji}e_j = \varphi\left( \sum_j a_{ji}m_j \right)\) по линейности. Значит, \(\left(tm_i - \sum_j
a_{ji}m_j\right) \in \ker\varphi\).

\begin{statement}
  \(\left\{ tm_i - \sum_j a_{ji}m_j \right\}_{i = 1}^n\) --- система образующих \(\ker\varphi\).
\end{statement}
\begin{proof}
  Применим индукцию по как-либо упорядоченному (например, частичным порядком) набору
  (\textit{кортежу}) \(\{\deg{g_i(t)}\}\)\mnote{недостаточно просто по \(\max\deg\), ведь может быть
    несколько одинаковых степеней} (\(g_i(t)\) --- некоторые коэффициенты-многочлены).

  \begin{pfparts}
    \item[База] Если все \(g_i(t)\) --- константы (\(\deg{g_i} = 0\)), то, так как они в ядре,
      \(\sum_i g_i(0)e_i = 0\) (\(m_i\) перешел в \(e_i\)) --- \(\forall i \quad g_i = 0\), ведь
      \(e_i\) --- базис.

    \item[Шаг индукции] Рассмотрим \(g_i(t)\) с максимальной степенью: \(g_i(t) = b_lt^l + \ldots +
      b_0\). Будем уменьшать его степень, ``подсократив'' старший член.

       Идея заключается в следующем: мы хотим вычесть \(b_lt^{l - 1}\underbrace{\left( tm_i - \sum_j
       a_{ji}m_j \right)}_{\in \ker\varphi} \in \ker\varphi\). Раскроем скобки --- у \(g_i\)
       пропадет старший член, а ко всем \(g_j\) добавятся члены степени \(l - 1\).
  \end{pfparts}
\end{proof}

На данный момент есть \(F[t]^n \xrightarrow{\psi} F[t]^n \surjto{\varphi} V\) (и там, и там именно
\(n\), а инъективность все еще не гарантированна). Положим, что \(\left\{ u_i \right\}\) --- базис
первого \(F[t]^n\); у остальных базисы по прежнему \(\left\{ m_i \right\}\) и \(\left\{ e_i
\right\}\) соответственно.

Посмотрим, как действует \(\psi\): \(u_i \mapsto tm_i - \sum_j a_{ji}m_j\) (по заданию, ведь по
аналогии с модулями хочется точную последовательность: \(\Imf\psi = \ker\varphi\)); рассмотрим
матрицу такого отображения: \(tI - A = \Psi\).

Поймем, что \(f_l\) --- инвариантные факторы \(\Psi\) (из формы Смита); в частности \((f_1 \divs f_2
\divs \ldots \divs f_k\)), \(\det\Psi = f_1 \ldots f_k\).

\begin{definition}
  Такой определитель называется характеристическим многочленом (\(\chi_A(t)\)) матрицы \(A\).
\end{definition}

На самом деле, мы уже почти доказали
\begin{theorem}[Гамильтона-Кэли]
  Минимальный многочлен делит характеристический, а характеристический делит некоторую степень
  минимального, то есть неприводимые делители одинаковы.
\end{theorem}
\begin{proof}
  \(f_k \divs f_1 \ldots f_k \divs f_k^k\).
\end{proof}

В более сжатой формулировке, \(\boxed{\chi_A(A) = 0}\).

В частности, \(f_i\) из \(V \cong \qfrac{F[t]}{(f_1)} \oplus \ldots \oplus \qfrac{F[t]}{(f_k)}\)
можно считать с помощью \(\Psi\).

\begin{definition}
  Корни характеристического многочлена \(\chi_A(t)\) называются собственными числами \(A\).
\end{definition}

\begin{statement}
  \(\lambda\) --- собственное число тогда и только тогда, когда \(\chi_A(\lambda) = 0\), то есть,
  \(\lambda{I} - A\) вырожденная \mnote{по теореме о сумме ранга и ядра} \(\iff \exists v \neq
  0\colon \left( \lambda{I} - A \right)(v) = 0\).
\end{statement}

Продолжая, в самой удобной форме собственное число задавать так:
\begin{statement}
  \(\lambda\) --- собственное число тогда и только тогда, когда \(\exists v \neq 0\colon Av =
  \lambda{v}\).
\end{statement}

Понятно, что таких \(v\) может быть много; в частности,
\begin{definition}
  \(V_\lambda \coloneqq \ker\left( \lambda{I} - A \right)\) --- собственное подпространство,
  отвечающие числу \(\lambda\).
\end{definition}

На самом деле, мы уже вводили такое понятие в лекции 3: \(V_\lambda = M_{t - \lambda} = \left\{ v
\in V \mid (t - \lambda)v = 0 \right\}\).

\begin{statement}
  \((t - \lambda)\) делит минимальный многочлен.
\end{statement}

Также видно, что \(M_{t - \lambda}\) это не просто векторное подпространство, но и
\(F[t]\)-подмодуль; это в том числе значит, что
\begin{statement}
  \(A(V_\lambda) \leqslant V_\lambda\).
\end{statement}

\begin{statement}
  \(A(Av) = \lambda Av\).
\end{statement}

Из того, что уже доказано в лекции 3 также получили, что
\begin{statement}
  Если \(\lambda, \mu\) --- собственные числа, \(\lambda \neq \mu \implies V_\lambda \cap V_\mu =
  \left\{ 0 \right\}\).
\end{statement}
\begin{proof}
  \(\gcd(t - \lambda, t - \mu) = 1\) --- есть разложение во внутреннюю прямую сумму.
\end{proof}

\begin{definition}
  \(V^{(\lambda)} \coloneqq M[t - \lambda]\) \mnote{\(V^\lambda\), \(V(\lambda)\) } --- корневое
  подпространство.
\end{definition}

Что это значит? \(V^{(\lambda)} = \left\{ v \in V \mid \left( \lambda{I} - A \right)^iv = 0
\right\}\).

Если же \(F\) еще и алгебраически замкнуто (например, \(\C\)), то
\begin{statement} \mnote{для собственных подпространств это неверно}
  \(V = \bigoplus_{\lambda \text{ с.ч. } A} V^{(\lambda)}\).
\end{statement}
\begin{proof}
  Возьмем минимальный многочлен \(f(t)\) --- он раскладывается как \(\prod_\lambda (t -
  \lambda)^{i(\lambda)}\).

  С другой стороны, \(M = M[f(t)] = \bigoplus_\lambda M_{(t - \lambda)^{i(\lambda)}} =
  \bigoplus_\lambda M[t - \lambda]\).
\end{proof}

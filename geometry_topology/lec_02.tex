\lecture{2}{26.02.2024, 15:25}{}

\section{Ортогональное дополнение}

Как и раннее, \(X\) --- конечномерное ЕП (над \(\R\)).

\begin{definition}
  Если \(A \subseteq X\), то ортогональным дополнением к \(A\) называется \(A^\perp \coloneqq \left\{ x
  \in X \mid x \perp v \quad \forall v \in A \right\}\).
\end{definition}

\begin{example}
  Если \(A \subseteq \R^3\), \(A = v\), то \(A^\perp\) --- плоскость, перпендикулярная \(v\).
\end{example}

Рассмотрим свойства дополнения.

\begin{statement}
  \(A^\perp\) --- подпространство \(X\).
\end{statement}

\begin{statement}
  \(A \subseteq B \implies B^\perp \subseteq A^\perp\).
\end{statement}

\begin{statement}
  \(A^\perp = \left( \lin{A} \right)^\perp\).
\end{statement}

\begin{remark}
  \(\left( A^\perp \right)^\perp = \lin{A}\) (а не всегда \(A\)).
\end{remark}

\begin{theorem}
  Если \(V \leqslant X\), то:
  \begin{enumerate}
    \item \(X = V \oplus V^\perp\);
    \item \(\left( V^\perp \right)^\perp = V\).
  \end{enumerate}
\end{theorem}
\begin{proof}
  Докажем только пункт 1 (второй обсуждался выше).

  Пусть \(e_1, \ldots, e_k\) --- ортонормированный базис \(V\); дополним его до ортонормированного
  базиса \(X\) --- \(e_1, \ldots, e_k, e_{k + 1}, \ldots, e_n\). Но \(V^\perp = \lin(e_{k + 1},
  \ldots, e_n)\) --- видно, что из этого следует искомый результат.
\end{proof}

\section{Ортогональная проекция}

Пусть \(V \leqslant X\), \(x \in X\).

По предыдущей теореме, \(x = y + z\), где \(y \in V\), \(z \in V^\perp\), причем такое разложение
единственное (сумма прямая).

\begin{definition}
  Вектор \(y\) называется проекцией \(x\) на \(V\): \(x = \Pr_V(x)\).
\end{definition}

По-другому говоря, \(\Pr_V(x)\) --- такой вектор из \(V\), что \(x - \Pr_V(x) \in V^\perp\).

Рассмотрим свойства проекции.

\begin{statement}
  \(\Pr_V\colon X \to X\) --- линейное отображение.
\end{statement}

\begin{statement}
  \(\Pr_V(x)\) есть ближайшая к \(x\) точка из \(V\): \(d(x, V) = d(x, \Pr_V(x))\).
\end{statement}
\begin{proof}
  По определению, \(d(x, \Pr_V(x)) \coloneqq |x - \Pr_V(x)|\).

  Рассмотрим \(\forall w \in V\): \(|x - w|^2 = |\underbrace{(x - \Pr_V(x))}_a +
  \underbrace{(\Pr_V(x) - w)}_b|^2\) --- \(a \in V^\perp\), \(b \in V\), но тогда, по теореме
  Пифагора, \(\ldots = |x - \Pr_V(x)|^2 + |\Pr_V(x) - w|^2 \ge |x - \Pr_V(x)|^2\).
\end{proof}

\section{Гиперплоскость}

\begin{definition}
  \(H \leqslant X\) называется гиперплоскостью, если \(\dim{H} = \dim{X} - 1\).
\end{definition}

\begin{definition}
  Нормаль гиперплоскости \(H\) --- любой ненулевой вектор из \(H^\perp\).
\end{definition}

Рассмотрим свойства гиперплоскости.

\begin{statement}
  У любой гиперплоскости существует нормаль, причем она единственна с точностью до умножения на
  скаляр.
\end{statement}

Очевидно, ведь \(X = H \oplus H^\perp\).

\begin{statement}
  Если \(v\) --- нормаль к \(H\), то \(H = v^\perp\).
\end{statement}

Другими словами, нормаль полностью задаёт гиперплоскость.

\begin{theorem}[конечномерная лемма Рисса]
  Если \(L\colon X \to \R\) --- линейное отображение, то \(\exists! v \in X\colon \forall x \in X
  \quad L(x) = \inner{v}{x}\).
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[Существование] Пусть \(e_1, \ldots, e_n\) --- ортонормированный базис \(X\). Положим, что
      \(\alpha_i = L(e_i)\).

      Но оказывается, \(\alpha_i\) --- координаты искомого \(v\): \(v \sum_i \alpha_i e_i\).

      Проверим, выполняется ли формула: возьмем \(x \in X\) и разложим его по базису --- \(x = \sum_j
      \beta_j e_j\). Тогда \(\inner{v}{x} = \ldots = \sum_i \alpha_i \beta_i = L(x)\).

    \item[Единственность] Предположим обратное: пусть \(\exists v_1, v_2\colon L(x) = \inner{v_1}{x}
      = \inner{v_2}{x} \quad \forall x \in X\).

      Перегруппируем: \(\inner{v_1 - v_2}{x} = 0\), но \(x\) --- любой, в том числе, \(\inner{v_1 -
      v_2}{v_1 - v_2} = 0 \iff v_1 = v_2\).
  \end{pfparts}
\end{proof}

% По сути, все линейные функционалы на \(X\) есть ни что иное, как

\begin{theorem}
  \hfill
  \begin{enumerate}
    \item Любая гиперплоскость в \(X\) имеет вид \(\ker{L}\), где \(L\colon X \to \R\) ---
      (некоторое) линейное отображение;
    \item Такой \(L\) определен однозначно с точностью до умножения на константу.
  \end{enumerate}
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[Существование (1)] Пусть \(H\) --- гиперплоскость в \(X\). Построим нужный \(L\).

      Пусть \(v \in H^\perp\), \(v \neq 0\). Проверим, что именно он задает искомый \(L\): \(L(x) =
      \inner{v}{x} \quad \forall x \in X\).

      Но \(\ker{L} = H\).

    \item[Однозначность (2)] Очевидно (от противного).
  \end{pfparts}
\end{proof}

\subsection{Уравнение гиперплоскости}

Пусть \(X = \R^n\), \(L\colon \R^n \to R\) --- линейное, где \(L(x) = \inner{v, x} = \sum_i a_ix_i\)
(стандартное скалярное произведение, \(v = \sum_i a_ie_i\), \(x = \sum_i x_ie_i\), \(\left\{ e_i
\right\}_i\) --- стандартный базис в \(\R^n\)).

В силу того, что гиперплоскость задается ядром,
\begin{statement}[уравнение гиперплоскости]
  \[
  \sum_i a_ix_i = 0
  .\]
\end{statement}

\subsection{Расстояние до гиперплоскости}

Пусть \(H = v^\perp\) --- гиперплоскость, \(x \in X\).

Раньше обсуждалось, что расстояние достаточно искать до проекции:
\begin{theorem}
  \[
    d(x, H) = \frac{|\inner{v}{x}|}{|v|}
  .\]
\end{theorem}

\section{Ортогональные преобразования, изометричность}

Пусть \(X\), \(Y\) --- конечномерные ЕП.

\begin{definition}
  Линейное отображение \(L\colon X \to Y\), сохраняющее скалярное произведение, называется
  изометрическим.
\end{definition}

Иногда говорят, что \(L\) --- изометрическое вложение \(X\) на \(Y\).

Рассмотрим свойства таких отображений.

\begin{statement}
  Если \(L\) изометрично, то \(L\) --- инъекция.
\end{statement}
\begin{proof}
  Достаточно показать тривиальность ядра: \(L(x) = 0 \implies |x| = 0 \implies x = 0\).
\end{proof}

\begin{statement}
  Если \(L\) изометрично, то \(X \cong L(X)\) как пространства со скалярным произведением.
\end{statement}

\begin{consequence}
  \(\dim{X} \le \dim{Y}\).
\end{consequence}

Рассмотрим теперь, что происходит, если пространство переводится в себя.

\begin{definition}
  Ортогональное преобразование пространства \(X\) --- изометрическое отображение \(X\) в себя.
\end{definition}

\begin{statement}
  Ортогональное преобразование пространства --- биекция.
\end{statement}

Неожиданно,
\begin{lemma}
  Если \(f\colon X \to X\) сохраняет скалярное произведение, то \(f\) линейное.
\end{lemma}
\begin{proof} \mnote{аналогично проверяется \(\inner{f(\lambda u) - \lambda f(u)}{f(\lambda u) -
  \lambda f(u)}\)}
  Посчитаем скалярное произведение: \(\inner{f(u + v) - f(u) - f(v)}{f(u + v) - f(u) - f(v)} \).
  Раскрывая скобки по линейности и пользуюсь тем, что \(f\) сохраняет скалярное произведение,
  получается, что \(\ldots = \inner{u + v - u - v}{u + v - u - v} = 0\).
\end{proof}

Посмотрим на некоторые полезные свойства изометрических отображений.

\begin{statement}
  \(f\) изометрично тогда и только тогда, когда \(f\) сохраняет длины векторов.
\end{statement}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\implies}\)] По определению.

    \item[\(\boxed{\impliedby}\)] По теореме косинусов.
  \end{pfparts}
\end{proof}

\begin{statement}
  \(f\) изометрично тогда и только тогда, когда \(f\) переводит некоторый ортонормированный
  \textit{базис} в ортонормированный \textit{набор}.
\end{statement}

Идея доказательства заключается в применении линейности, ортонормированности и сохраняемости
скалярного произведения.

\subsection{Матрица изометрического отображения}

Пусть \(f\colon X \to Y\) --- линейное отображение, а \(A\) --- матрица \(f\) в некоторых
ортонормированных базисах.

\begin{theorem}
  \(f\) изометрично тогда и только тогда, когда \(A^\top A = I\).
\end{theorem}
\begin{proof}
  Положим, что \(e_1, \ldots, e_n\) --- некоторый ортонормированный базис \(X\); понятно, что
  \(f(e_1), \ldots, f(e_n)\) --- ортонормированный набор в \(Y\).

  Но тогда \(\left(A^\top A\right)_{ij} = \inner{f(e_i)}{f(e_j)}\) --- результат умножения и впрямь
  единичная матрица.
\end{proof}

\begin{consequence}
  Если \(\dim{X} = \dim{Y}\), то \(f\) изометрично тогда и только тогда, когда \(A^{-1} = A^\top\).
\end{consequence}

\begin{definition}
  Матрица называется ортогональной, если \(A^\top A = I\).
\end{definition}

\begin{lemma}
  Если \(A\) ортогональна, то \(\det{A} = \pm 1\).
\end{lemma}
\begin{proof}
  \(\det{A^\top A} = \det{A^\top}\det{A} = \det^2{A} = \det{I} = 1\).
\end{proof}

\subsection{Классификация ортогональных преобразований}

Посмотрим на несколько наводящих примеров ортогональных преобразований с целью понять, как можно
классифицировать все такие преобразования.

``Легкий, простой и понятный'':
\begin{example}
  \(\id\colon X \to X\).
\end{example}

\begin{example}
  \(f\colon x \mapsto -x\).
\end{example}

\begin{example}
  Поворот плоскости (в \(\R^2\)) относительно \(0\).
\end{example}

\begin{example}
  Симметрия относительно подпространства: раз \(X = V \oplus V^\perp\), то \(x = y + z\) (векторы) и
  симметрию можно задать как \(f\colon y + z \mapsto y - z\).
\end{example}

\begin{example}
  Пусть \(X = X_1 \oplus \ldots \oplus X_k\), \(f_i\colon X_i \to X_i\) --- ортогональное
  преобразование. Поймем, как может выглядеть ``общее'' \(f\).

  Например, раз \(u = u_1 + \ldots + u_k\), \mnote{проверить изометричность самостоятельно} то
  \(f(u) \coloneqq f_1(u_1) + \ldots + f_k(u_k)\).
\end{example}

Разобравшись с этим, попробуем на примере плоскости \(\R^2\) классифицировать преобразования.

Основная идея будет заключаться в том, что базис будет ортонормированным и зафиксированным, а на
преобразования мы будем смотреть как на матрицы.

Зафиксируем базис \(\{e_1, e_2\}\) --- после перехода \(e_1\) ортогональным преобразованием
(\(f(e_1)\)), у \(e_2\) есть только \(2\) ``возможности'', куда он может перейти: \[
  \begin{pmatrix}
    \cos\alpha & -\sin\alpha \\
    \sin\alpha & \cos\alpha
  \end{pmatrix}, \begin{pmatrix}
    \cos\alpha & \sin\alpha \\
    \sin\alpha & -\cos\alpha
  \end{pmatrix}
.\]

Тогда понятно, что у плоскости преобразования это: \mnote{других вариантов нет из-за
ортонормированности}
\begin{enumerate}
  \item поворот;
  \item симметрия относительно биссектрисы угла \(\alpha\).
\end{enumerate}

Наконец-то займемся классификацией общего случая.

Пусть \(V \leqslant X\), причем на этот раз \(X\) может быть просто векторным пространством;
\(L\colon X \to X\) --- линейное отображение

\begin{definition}
  \(V\) инвариантно относительно \(L\), если \(L(V) \subseteq V\).
\end{definition}

\begin{remark}
  Если \(L\) ортогонально, то инвариантность подпространства в этом случае равносильна тому, что
  \(L(V) = V\).
\end{remark}

Далее нам пригодится, что
\begin{statement}
  Если \(V\) инвариантно относительно \textit{ортогонального} \(L\), то  \(V^\perp\) тоже
  инвариантно относительно него.
\end{statement}
\begin{proof}
  Уже есть, что \(L(V) = V\); хотим проверить, что \(L\left( V^\perp \right) \subseteq V^\perp\).

  \(\forall x \in V^\perp \forall v \in V \quad v = L(v')\), то есть, \(\inner{L(x)}{v} =
  \inner{L(x)}{L(v')} = \inner{x}{v'} = 0\) --- получилось, что \(L(x) \subseteq V^\perp\):
  \(L\left( V^\perp \right) \subseteq V^\perp\).
\end{proof}

Ну и наконец,
\begin{theorem}
  Если \(f\colon X \to X\) --- ортогональное преобразование, то существует разложение \(X\) в прямую
  сумму попарно ортогональных инвариантных подпространств (\(n \ge 0\)): \[
  X = X_+ \oplus X_- \oplus \Pi_1 \oplus \ldots \oplus \Pi_n
  ,\] причем
  \begin{enumerate}
    \item \(f\restriction_{X_+} = \id\), \(f\restriction_{X_-} = -\id\);
    \item \(\forall k \quad \dim\Pi_k = 2\), а \(f\restriction_{\Pi_k}\) --- поворот.
  \end{enumerate}
\end{theorem}
\begin{proof}
  Зададим \(X_+\), \(X_-\): \[
  \begin{aligned}
    X_+ &\coloneqq \left\{ x \in X \mid f(x) = x \right\} \\
    X_- &\coloneqq \left\{ x \in X \mid f(x) = -x \right\} \subset X_+^\perp,
  \end{aligned}
  \] \mnote{для сохранения углов} --- понятно, что \(X_+, X_- \leqslant X\).

  Пусть \(Z \coloneqq \left( X_+ \oplus X_- \right)^\perp\); примем к сведению, что это инвариантное
  подпространство.

  Займемся \(\Pi_k\)-ыми: по индукции будем выделять из \(Z\) такие инвариантные плоскости.

  Рассмотрим сферу \(S^1 \subseteq Z\) и введем функцию \(\varphi(v) \coloneqq \angle(v, f(v))\).
  Видно, что \(\varphi\colon S^1 \to \R_+\) и \(\varphi\) непрерывна как композиция непрерывных, а
  так как сфера еще и компактна, то \(\varphi\) достигает своего \(\min\): пусть это происходит в
  точке \(v_0\).

  Положим, что \(v_1 \coloneqq f(v_0)\), \(v_2 \coloneqq f(v_1)\). Достаточно показать, что \(v_0\),
  \(v_1\) и \(v_2\) лежат в одной плоскости. Введем еще несколько переменных: \(w_1 := \frac{v_0 +
  v_1}{|v_0 + v_1|}\) (биссектриса \(\angle(v_0, v_1)\) на сфере), \(w_2 \coloneqq f(w_1)\)
  (биссектриса \(\angle(v_1, v_2)\)).

  По неравенству треугольника для углов, \(\angle(w_1, w_2) \le \angle(w_1, v_1) + \angle(v_1, w_2)
  = \frac{\alpha}{2} + \frac{\alpha}{2} = \alpha\). С другой стороны, \(\alpha\) --- минимальный из
  всех возможных углов: \(\angle(w_1, w_2) \ge \alpha \implies \angle(w_1, w_2) = \alpha\).

  Получилось, что \(w_1\) и \(w_2\) лежат вместе с \(v_1\) в одной плоскости на сфере. Но отсюда
  сразу следует то, что \(v_0\), \(v_1\) и \(v_2\) лежат в этой же плоскости.

  Полученная плоскость также и инварианта (ведь будь какие-то векторы коллинеарны, то \(v_0\) попал
  бы в \(X_+\) или \(X_-\)), и совершает поворот, ведь все неподвижные векторы остались в \(X_+\) 
  или \(X_-\).

  Осталось индукцией выделить все такие плоскости.
\end{proof}

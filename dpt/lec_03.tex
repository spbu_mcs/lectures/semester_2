\lecture{3}{01.03.2024, 13:40}{}

Выведем из этого более конкретизированные полезные неравенства.

\begin{theorem}[неравенство Маркова]
  \[
  \forall r > 0 \quad \P(|X| \ge r) \le \frac{\E|X|}{r}
  .\]
\end{theorem}
\begin{proof}
  Сделаем в предыдущем неравенстве замену: \(x \mapsto |x|\), а \(f(x)\) зададим так: \(f(x) \coloneqq \begin{cases}
    x, &x > 0 \\
    0, &x < 0
  \end{cases}\). Тогда \(f(|x|) = |x|\) --- по предыдущему неравенству получается искомое.
\end{proof}

Порой мат. ожидание квадрата считается проще:
\begin{consequence}
   \[
  \forall x \forall r > 0 \quad \P(|x| \ge r) \le \frac{\E|x|^2}{r^2}
  .\]
\end{consequence}
\begin{proof}
  \(\P(|x| \ge r) = \P(|x|^2 \ge r^2)\) --- осталось применить неравенство Маркова.
\end{proof}

\begin{theorem}[экспоненциальное неравенство Чебышева]
  \[
  \forall x \forall \lambda > 0 \forall r \quad \P(x \ge r) \le \frac{\E\exp(\lambda
  x)}{\exp(\lambda r)}
  .\]
\end{theorem}
\begin{proof}
  Сразу следует из самого первого неравенства, ведь \(\exp\colon \R \to \R_+\) не убывает.
\end{proof}

\begin{theorem}[неравенство Йенсена для вероятностной упаковки]
  Если \(\varphi(X)\) --- функция, выпуклая вниз, \(\E{\varphi(X)}\) определено, а у величины \(X\)
  определено \(\E{X}\), то \[
    \varphi\left( \E{X} \right) \le \E{\varphi(X)}
  .\]
\end{theorem}
\begin{proof}
  Если рассмотреть \(x_j\) и \(p_j \coloneqq \P(x = x_j)\), то неравенство переформулируется так: \[
  \varphi\left( \sum_j p_jx_j \right) \le\sum_j p_j \varphi(x_j)
  ,\] --- но это определение выпуклости.
\end{proof}

Вместе с этой теоремой рассмотрим типичный пример.
\begin{example}
  Пусть \(\varphi(X) = X^2\): \(\left( \E{X} \right)^2 \le \E{X^2}\).
\end{example}

\section{Медиана}

Положим, что \(X\) --- некоторая случайная величина.

\begin{definition}
  \(m\) называется медианой (\textit{серединным значением}) для \(X\), если \(\P(X \ge m) \ge
  \frac{1}{2}\) и \(\P(X \le m) \ge \frac{1}{2}\).
\end{definition}

Просто показать, что, в отличии от мат. ожидания,
\begin{statement}
  Любая случайная величина имеет медиану.
\end{statement}

Однако,
\begin{remark}
  Медиана не единственна.
\end{remark}

Еще одно неприятное отличие медианы от мат. ожидания заключается в ее \textit{нелинейности}.

Рассмотрим несколько оптимизационных задач.

\begin{example}
  Рассмотрим \(X \approx r\) --- как минимизировать \(\E(x - r)^2\)?

  Раскроем скобки: \(\E(x - r)^2 = \E{x^2} - 2\E{x}r + r^2\). Но тогда видно, что достаточно
  продифференцировать: \(-2\E{x} + 2r = 0 \iff \boxed{r = \E{x}}\).

  То есть математическое ожидание наилучшим образом приближает случайным величину.
\end{example}

Близость можно задать и по-другому:
\begin{example}
  Рассмотрим \(X \approx r\) --- как минимизировать \(\E|x - r|\)?

  Ответ --- медианой!
\end{example}

Результат предлагается доказать самостоятельно.

\section{Дисперсия}

По-простому говоря, дисперсия --- мера ``разброса''.

\begin{definition} \mnote{по н-ву Йенсена такая величина неотрицательна}
  Дисперсия \(\D{X} \coloneqq \E(X^2) - (\E{X})^2\).
\end{definition}

Можно задать дисперсию по-другому:
\begin{definition}
  Дисперсия  \(\D{X} \coloneqq \E\left( X - \E{X} \right)^2\).
\end{definition}

\begin{notation}
  В зарубежной литературе дисперсия обозначается как \(\mathrm{Var}{X}\).
\end{notation}

\begin{statement}
  Два определения дисперсии эквивалентны.
\end{statement}
\begin{proof}
  \[
    \E\left( X - \E{X} \right)^2 = \E\left( X^2 - 2X\E{X} + \left( \E{X} \right)^2 \right) = \E{X^2}
    - 2\E{X}\E{X} + \left( \E{X} \right)^2 = \E(X^2) - \left( \E{X} \right)^2
  .\]
\end{proof}

Рассмотрим свойства дисперсии.

\begin{statement}
  \(\D(X + c) = \D{X}\).
\end{statement}

\begin{statement}
  \(\D{cX} = c^2\D{X}\).
\end{statement}

\begin{statement}
  \(\D{X} \ge 0\).
\end{statement}

\begin{statement}
  Если \(X\) и \(Y\) независимы, то \(\D(X + Y) = \D{X} + \D{Y}\).
\end{statement}
\begin{proof}
  По первому определению, \(\D(X + Y) = \E(X + Y)^2 - \left( \E{X} + \E{Y} \right)^2 = \E(X^2) +
  \E(Y^2) + 2\E(XY) - \left( \E{X} \right)^2 - \left( \E{Y} \right)^2 - 2\E{X}\E{Y}\) --- видно, что
  мат. ожидания произведений сократятся, а из оставшихся членов формируются дисперсии \(X\) и \(Y\).
\end{proof}

Понятно, что
\begin{consequence}
  Если \(X_1, \ldots, X_n\) независимы, то \(\D\left( \sum_k X_k \right) = \sum_k \D{X_k}\).
\end{consequence}

Заметим, что в доказательстве линейности независимость нужна была только, чтобы получить \(\E(XY) =
\E{X}\E{Y}\). Это наводит на следующее понятие:
\begin{definition}
  Величины \(X\) и \(Y\) называются некоррелированными, если \(\E(XY) = \E{X}\E{Y}\).
\end{definition}

Для нескольких величин достаточно \textit{попарной} некоррелированности.

\begin{example}
  Если \(X\) и \(Y\) независимы, то \(\D(X - Y) = \D(X) + \D(-Y) = \D{X} + \D{Y}\).
\end{example}

\begin{statement}
  Если \(X\) и \(X'\) независимы и одинаковы распределены, то \[
    \D{X} = \frac{1}{2}\E\left( X - X' \right)^2
  .\]
\end{statement}
\begin{proof}
  \[
    \frac{1}{2}\E\left( X - X' \right)^2 = \frac{1}{2}\left( \E{X^2} + \E{(X')^2} - 2\E(XX') \right)
    = \frac{1}{2}\left( \E{X^2} + \E{X^2} - 2\E{X}\E{X} \right).
  \]
\end{proof}

\begin{definition}
  Такие \(X\) и \(X'\) называются независимыми копиями.
\end{definition}

Пусть \(X\) --- случайная величина, а \(f\colon \R \to \R\) сокращающая --- \(\forall x, y \quad
|f(x) - f(y)| \le |x - y|\).

\begin{statement}
  \(\D{f(X)} \le \D{X}\).
\end{statement}
\begin{proof}
  Положим, что \(X'\) --- независимая копия \(X\); тогда \(f(X')\) --- независимая копия \(f(X)\).

  \(\D{f(X)} = \frac{1}{2}\E\left( f(X) - f(X') \right)^2\), но так как \(f\) сокращающая, то
  \(\frac{1}{2}\E\left( f(X) - f(X') \right)^2 \le \frac{1}{2}\E\left(X - X'\right)^2 = \D{X}\).
\end{proof}

В завершении темы дисперсии рассмотрим, пожалуй, самое знаменитое неравенство теории вероятностей.

\begin{theorem}[неравенство Чебышева]
  Если \(X\) --- случайная величина, то \[
    \forall r > 0 \quad \P\left( \left|X - \E{X}\right| \ge r \right) \le \frac{\D{X}}{r^2}
  .\]
\end{theorem}
\begin{proof}
  Ранее было, что \(\P(|X| \ge r) \le \frac{\E(X^2)}{r^2}\) --- заменив \(X\) на \(X - \E{X}\),
  получится искомое.
\end{proof}

\begin{definition}
  \(\sigma(X) \coloneqq \sqrt{\D{X}}\) --- среднеквадратическое отклонение.
\end{definition}

\section{Моменты высоких порядков}

\begin{definition}
  \(\E{X^k}\) --- момент порядка \(k\).
\end{definition}

\begin{definition}
  \(\E|X^k|\) --- абсолютный момент порядка \(k\).
\end{definition}

\begin{definition}
  \(\E\left(X - \E{X}\right)^k\) --- центральный момент порядка \(k\).
\end{definition}

\begin{definition}
  \(\E\left| X - \E{X} \right|^k\) --- абсолютный центральный момент порядка \(k\).
\end{definition}

Тогда мат. ожидание --- момент порядка \(1\), а дисперсия --- либо центральный момент порядка \(2\),
либо абсолютный центральный момент порядка \(2\).

\section{Закон больших чисел}

\begin{theorem}[закон больших чисел Чебышева]
  Пусть \(X_1, X_2, \ldots\) --- независимые случайные величины, и \(\forall i \quad \E{X_i} = a,
  \D{X_i} = \sigma^2\). Положим, что \(S_n \coloneqq \sum_{j = 1}^n X_j\). Тогда \[
    \forall r > 0 \quad \P\left( \left|\frac{S_n}{n} - a\right| > r \right) \xrightarrow{n \to
    \infty} 0
  .\]
\end{theorem}
\begin{proof}
  Заметим, что \(\E\frac{S_n}{n} = \frac{\E\sum_j X_j}{n} = a\), \(\D\frac{S_n}{n} =
  \frac{1}{n^2}\D{S_n} = \frac{1}{n^2}\D(X_1 + \ldots + X_n) = \frac{\sigma^2}{n}\).

  Осталось применить неравенство Чебышева: положим, что \(X = \frac{S_n}{n}\) --- тогда получается
  \(\P\left( \left| \frac{S_n}{n} - a \right| \ge r \right) \le \frac{\sigma^2}{nr^2} \xrightarrow{n
  \to \infty} 0\).
\end{proof}

Посмотрим, как можно обобщить эту теорему.

\begin{remark}
  Достаточно предполагать некоррелируемость величин.
\end{remark}

\begin{remark}
  Достаточно предполагать, что \(\forall j \quad \D{X_j} \le \sigma^2\).
\end{remark}

Положим, что \(S_n\) --- число успехов в схеме Бернулли с параметрами \((n, p)\).
\begin{theorem}[закон больших чисел Бернулли] \mnote{за каждым \(n\) стоит свое вероятностное
  пространство}
  \[
  \forall r > 0 \quad \P\left( \left| \frac{S_n}{n} - p \right| \ge r \right) \xrightarrow{n \to
  \infty} 0
  .\]
\end{theorem}
\begin{proof}
  \(S_n = \sum_{j = 1}^n X_j\), где \(X_j = \mathbb{1}_j\) --- индикатор; \(\E{X_j} = p\), \(\D{X_j}
  = p(1 - p)\), величины независимы --- осталось применить закон Чебышева.
\end{proof}


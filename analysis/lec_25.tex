\lecture{25}{16.05.2024, 15:30}{}

\begin{example}
  Рассмотрим \(x'(t) = Ax(t)\), где  \(x = x(t) = \begin{pmatrix} x_1(t) \\ \vdots \\ x_n(t)
  \end{pmatrix} \) и \(A\) --- некоторая матрица.

  Понятно, что если \(n = 1\), то \(x(t) = e^{tA} \cdot C\). \mnote{``потому что мы решаем систему
  уравнений!''}

  Зная, что \(e^{tA} = \sum_{k = 0}^\infty \frac{t^kA^k}{k!}\), подставим \(x(t) = e^{tA}\): \[
  \begin{aligned}
    & x'(t) = e^{tA} C(t) \\
    \iff & e^{tA}A C(t) + e^{tA} C'(t) = Ae^{tA} C(t) \\
    \iff & e^{tA} C'(T) = 0 \\
    \iff & C'(t) = 0 \iff C = \mathrm{const}.
  \end{aligned}
  \]

  То есть, итого, \[
    x(t) = e^{tA}
  .\]
\end{example}

Пусть функция \(f\colon \R^2 \to \R\).

\begin{definition} \mnote{\(B\) не зависит от \(y \in Y\)}
  Интеграл \(\int_a^{+\infty} f(x, y) \df{x}\) сходится равномерно на \(Y\), если \[
    \forall \varepsilon > 0 \exists B = B(\varepsilon)\colon \forall b > B \forall y \in Y \quad
    \left| \int_b^{+\infty} f(x, y) \df{x}\right| < \varepsilon
  .\]
\end{definition} \mnote{``рубить хвосты''}

Пусть \(f(x, y)\) непрерывна на \([a, +\infty) \times (y_1, y_2)\) и \(I(y) \coloneqq
\int_a^{+\infty} f(x, y) \df{x}\) сходится равномерно на \((y_1, y_2)\).

\begin{theorem}
  \(I(y)\) непрерывна на \((y_1, y_2)\).
\end{theorem}
\begin{proof}
  Наберем последовательность \(I_n(y) \coloneqq \int_a^n f(x, y) \df{x}\): она сходится равномерно к
  \(I(y)\) при \(n \to \infty\) на \((y_1, y_2)\). Ранее было показано, что функции \(I_n(y)\)
  непрерывны на \((y_1, y_2)\), и, за счет равномерности сходимости, \(I(y)\) тоже непрерывна на
  \((y_1, y_2)\).
\end{proof}

\begin{theorem}[критерий Коши]
  \(\int_a^{+\infty} f(x, y) \df{x}\) сходится равномерно на \(Y\) тогда и только тогда, когда \[
    \forall \varepsilon > 0 \exists B = B(\varepsilon)\colon \forall y \in Y \forall b', b'' > B
    \quad \left| \int_{b'}^{b''} f(x, y) \df{x} \right| < \varepsilon
  .\]
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\implies}\)] \(\int_{b'}^{b''} = \int_{b'}^{+\infty} - \int_{b''}^{+\infty} <
      \varepsilon\).

    \item[\(\boxed{\impliedby}\)] Предельный переход \(b'' \to \infty\) (так можно за счет критерия
      Коши для несобственных интегралов).
  \end{pfparts}
\end{proof}

\begin{theorem}[признак равномерной сходимости несобственных интегралов Вейерштрасса]
  Если \(\forall x \in [a, +\infty), y \in Y \quad |f(x, y)| \le F(x)\) и \(\int_a^{+\infty} F(x)
  \df{x} < +\infty\), то \(\int_a^{+\infty} f(x, y) \df{x}\) сходится равномерно на \(Y\).
\end{theorem}
\begin{proof}
  \[
    \left| \int_b^{+\infty} f(x, y) \df{x}  \right| \le \int_b^{+\infty} |f(x, y)| \df{x} \le
    \int_b^{+\infty} F(x) \df{x} < \varepsilon \quad \forall b > B(\varepsilon)
  .\]
\end{proof}

\begin{theorem} \mnote{такой интеграл существует за счет непрерывности \(f\)}
  Если \(f_n(x) \unito f(x)\) локально равномерно на \([a, +\infty)\), \(f_n(x)\) непрерывны,
  \(\forall n, x \in [a, +\infty) \quad |f_n(x)| \le F(x)\) и \(\int_a^{+\infty} F(x) \df{x} <
  +\infty\) то \(\lim_{n \to \infty} \int_a^{+\infty} f_n(x) \df{x} = \int_a^{+\infty} f(x)
  \df{x}\).
\end{theorem}
\begin{proof}
  \[
  \begin{aligned}
    \left| \int_a^{+\infty} f_n(x) \df{x} - \int_a^{+\infty} f(x) \df{x} \right| &= \left| \int_a^b
    f_n(x) - \int_a^b f(x) \df{x} + \int_b^{+\infty} f_n(x) \df{x} - \int_b^{+\infty} f(x)
    \df{x}\right| \\
    &\le \left| \int_a^b \left( f_n(x) - f(x) \right) \df{x} \right| + \varepsilon, \quad b >
    B(\varepsilon).
  \end{aligned}
  \]

  Возьмем \(b = B(\varepsilon) + 1\): тогда \[
    \limsup_{n \to \infty} \left| \int_a^{+\infty} f_n(x) \d{x} - \int_a^{+\infty} f(x) \df{x}
    \right| \le \varepsilon, \quad \forall \varepsilon > 0
  .\]
\end{proof}

\begin{example}[Інтеграл Гауса]
   \[
     \int_0^{+\infty} e^{-x^2} \df{x} = \lim_{n \to \infty} \int_0^{+\infty} \frac{\df{x}}{\left( 1
     + \frac{x^2}{n}\right)^n}
  .\]

  \[
  \begin{aligned}
    \int_0^{+\infty} \frac{\df{x}}{\left( 1 + \frac{x^2}{n}\right)^n} &= \begin{bmatrix} 
      x = \sqrt{n}y, & y = \tan{t} \\
      \df{x} = \sqrt{n} \df{y}
    \end{bmatrix} = \sqrt{n} \int_0^{\frac{\pi}{2}} \cos^{2n - 2}t \df{t} \\
    &= \sqrt{n} \int_0^{\frac{\pi}{2}} \sin^{2(n - 1)}{t} \df{t} = \frac{\pi}{2}\sqrt{n} \frac{(2(n
    - 1) - 1)!!}{(2(n - 1))!!} \\
    &\xrightarrow{n \to \infty} \frac{\pi}{2} \frac{1}{\sqrt{\pi}} = \boxed{\frac{\sqrt{\pi}}{2}}.
  \end{aligned}
  \]
\end{example}

\begin{example}
  Пусть \(\int_a^{+\infty} |f(x)| \df{x} < +\infty\) и \(f(x)\) непрерывна на \([a, +\infty)\).

  Покажем, что \(\lim_{n \to \infty} \int_a^{+\infty} f(x) \sin{nx} \df{x} = 0\).

  По признаку Вейерштрасса интеграл \(\int_a^{+\infty} f(x) \sin{nx} \df{x}\) сходится равномерно,
  ведь \[
    \left| \int_a^{+\infty} f(x) \sin{nx} \df{x} \right| \le \left| \int_a^{B(\varepsilon)} f(x)
    \sin{nx} \right| + \varepsilon
  .\]

  Осталось показать, что \(\lim_{n \to \infty} \int_a^{B(\varepsilon)} f(x) \sin{nx} \df{x} = 0\).

  Разобьем на отрезки: \[
  \begin{aligned}
    \int_{\frac{2\pi k}{n}}^{\frac{2\pi(k + 1)}{n}} f(x)\sin{nx} \df{x} &= \int_{\frac{2\pi
    k}{n}}^{\frac{2\pi(k + 1)}{n}} \left( f(x) - f(x_n) \right) \sin{nx} \df{x} + \int_{\frac{2\pi
    k}{n}}^{\frac{2\pi(k + 1)}{n}} f(x_n) \sin{nx} \df{x} \\
    &< \varepsilon_n + 0
  \end{aligned}
  \] при некотором \(|x - x_n|\).

  Тогда \[
    \left| \int_{0}^{2\pi} f(x) \sin{nx} \df{x} \right| = \left| \sum_{k = 0}^{n - 1}
    \int_{\frac{2\pi k}{n}}^{\frac{2\pi(k + 1)}{n}} f(x)\sin{nx} \df{x} \right| \le \sum_{k = 0}^{n
    - 1} 2\pi\varepsilon_n \to 0
  ,\] и туда же сойдется и исходный предельным переходом к верхнему пределу.
\end{example}

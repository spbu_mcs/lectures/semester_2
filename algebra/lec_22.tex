\lecture{22}{08.05.2024, 13:45}{}

Зафиксируем в матрице \(A\colon n \times n\) первые \(k\) и \(n - k\) столбцов.

\begin{theorem}[Лапласа]
  \(\det{A} = \sum_{|I| = k, I \subseteq \left\{ 1, \ldots, n \right\}} \pm A_{I, \left\{ 1, \ldots,
  k \right\}} A_{\left\{ 1, \ldots, n \right\} \setminus I, \left\{ k, \ldots n \right\}}\).
\end{theorem}
\begin{proof}
  Пусть \(\varphi\) --- линейное отображение, соответствующее \(A\). Чтобы посчитать определитель
  надо вычислить \(\Lambda^n\varphi(e_1 \wedge \ldots \wedge e_n)\).

  \(\Lambda^n\varphi(e_1 \wedge \ldots \wedge e_n) = \varphi(e_1) \wedge \ldots \wedge
  \varphi(e_n) = \left( \Lambda^k\varphi \right)(e_1 \wedge \ldots \wedge e_k) \wedge \left( \Lambda^{n -
  k}\varphi \right)(e_{k + 1} \wedge \ldots \wedge e_n)\); когда-то ранее уже выяснилось, что это
  равно \(\left( \sum_{|I| = k} A_{|I|, \left\{ 1, \ldots, k \right\}} e_I \right) \wedge \left(
  \sum_{|J| = n - k} A_{J, \left\{ k + 1, \ldots, n \right\}} e_J \right)\).

  Раскрывая скобки, \(e_I \wedge e_J = \begin{cases}
    0, &J \neq \left\{ 1, \ldots, n \right\} \setminus I, \\
    e_{\left\{ 1, \ldots, n \right\}} (-1)^{\mathrm{inv}(I, \left\{ 1, \ldots, n \right\} \setminus
    I)}, &\text{иначе}
  \end{cases}\) --- итоговая формула и знак в ней получается отсюда.
\end{proof}

\begin{remark}
  Разложение по первому столбцу --- частный случай при \(k = 1\), а по последнему --- \(k = n - 1\).
\end{remark}

Рассмотрим, какое геометрическое представление имеет внешняя алгебра. Перед этим надо запастись
некоторым объемом определений и теорем.

\begin{theorem}
  \( \Lambda^k\left( V^* \right) \cong \left( \Lambda^k V \right)^*\), где \(f_1 \wedge \ldots
  \wedge f_k \mapsto \left( v_1 \wedge \ldots \wedge v_k \mapsto  \det\left( f_i(v_j) \right)_{i,
  j} \right)\) (матрица).
\end{theorem}
\begin{proof}
  Покажем сначала, что выражение \(v_1 \wedge \ldots \wedge v_k \mapsto \ldots\) определено
  корректно, то есть что оно полилинейно и антисимметрично.

  Если \(v_j = u_j + u_j'\), то столбец \(f_j(v_j)\) --- сумма \(j\)-го столбца, а определитель
  полилинеен. Если же \(v_j = v_l\), то \(j\) столбец \((f_j(v_j))\) совпадает с \(l\)-ым столбцом
  (и определитель --- \(0\)).

  Надо еще проверить полилинейность по \(f\). Если \(f_i = g_i + g_i'\), то \(i\)-ая строка
  \((f_i(v_j))\) --- это сумма \((g_i(v_j))_j + (g'_i(v_j))_j\). Если же \(f_i = f_l\), то \(i\)-ая
  строка совпадает с \(l\)-ой строкой. Всё корректно (понадобились свойства определителя как по
  строкам, так и по столбцам).

  И слева, и справа размерность --- биномиальный коэффициент, поэтому достаточно проверить
  инъективность.

  Пусть \(e_1, \ldots, e_n\) --- базис \(V\), а \(f_1, \ldots, f_n\) --- двойственный базис \(V^*\).
  \(f_I \mapsto \left( e_J \mapsto \delta_{IJ} \right)\), ведь если \(I \neq J\), то есть строчка из
  \(0\), а если \(I = J\), то рассматривается определитель единичной матрицы (так можно, ведь
  мультииндексы \(I, J\) отсортированы). Но матрица такого отображения --- единичная, и исходное
  отображение инъективно.
\end{proof}

Чтобы лишний раз не мучиться, с этого момента рассматриваются только векторные пространства.

Рассмотрим внешнюю степень \(\Lambda^k(V)\) и зададимся следующим вопросом: когда \(v_1 \wedge
\ldots \wedge v_k = 0\)?

\begin{lemma}
  \(v_1 \wedge \ldots \wedge v_k = 0\) тогда и только тогда, когда они линейно зависимы.
\end{lemma}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\implies}\)] Если \(v_1, \ldots, v_k\) линейно независимы, то их можно дополнить
      до базиса векторного пространства \(V\), и в \(\Lambda^k(V)\) один из элементов базиса будет
      \(v_1 \wedge \ldots \wedge v_k\), и он будет ненулевой.

    \item[\(\boxed{\impliedby}\)] \(v_i = \sum_j \alpha_j v_j\) по полилинейности и
      антисимметричности, \(v_1 \wedge  \ldots \wedge \sum_j \alpha_j v_j \wedge \ldots \wedge v_k =
      0\).
  \end{pfparts}
\end{proof}

\begin{lemma}
  \(\linspan{v_1 \wedge \ldots \wedge v_k} = \linspan{u_1 \wedge \ldots \wedge u_k} \iff
  \linspan{v_1 , \ldots, v_k} = \linspan{u_1, \ldots, u_k}\) при условии, что они линейно
  независимы.
\end{lemma}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\implies}\)] Предположим обратное: пусть они порождают разные пространства.

      Положим, что \(\linspan{v_1, \ldots, v_k} = U'\), \(\linspan{u_1, \ldots, u_k} = U\).

      Зафиксируем базис у \(U' \cap U\): \(e_1, \ldots, e_l\). К нему возьмем также относительный
      базис \(e_{l + 1}', \ldots, e_k'\) в \(U'\) по отношению к \(U' \cap U\), и \(e_{l + 1},
      \ldots, e_k\) (тоже относительный базис) в \(U\) по отношению к \(U' \cap U\).

      Можно проверить, что собирая все эти базисы воедино получится линейно независимый набор.
      \mnote{иначе случиться что-то плохое при факторизации}

      Дополним его до базиса всего пространства. Еще немного и получится противоречие с
      предположением о пропорциональности...

    \item[\(\boxed{\impliedby}\)] Запишем матрицу перехода от \(u_1, \ldots, u_k\) к \(v_1, \ldots,
      v_k\): \(u_1 \wedge \ldots \wedge u_k = \det{C} v_1 \wedge \ldots \wedge v_k\) прямо по
      определению определителя.
  \end{pfparts}
\end{proof}

\begin{definition}
  Грассманиан \(\Gr(k, V)\) --- множество \(k\)-мерных векторных подпространств в \(V\).
\end{definition}

\begin{example}
  \(\mathbb{P}(V) = \Gr(1, V)\).
\end{example}

\begin{statement}
  \(\Gr(k, V) \injto{\linspan{v_1, \ldots, v_k} \mapsto \linspan{v_1 \wedge \ldots \wedge v_k}}
  \mathbb{P}\left( \Lambda^k V \right)\).
\end{statement}

\begin{statement}
  \(\Gr(k, V) \simeq \Gr(n - k, V^*)\), где \(U \leftrightarrow U^\perp \coloneqq \left\{ f \in V^*
  \mid f\restriction_U = 0\right\}\) (\(U \leqslant V\)).
\end{statement}

\begin{notation}
  Вложение Плюккера \(\Gr(k, V) \injto{} \mathbb{P}(\Lambda^k V)\).
\end{notation}

``элемент объема''.

Рассмотрим трехмерное пространство:
\begin{enumerate}
  \item точки --- \(\mathbb{P}(V)\), 4 координаты;
  \item прямые --- \(\Gr(2, V) \injto{} \mathbb{P}(\Lambda^2 V)\), 6 координат;
  \item плоскости --- \(\Gr(3, V) \simeq \Gr(1, V^*) = \mathbb{P}(V^*)\), 4 координаты.
\end{enumerate}

То есть чтобы, например, задать точку, надо задать прямую четырехмерного пространства.

\begin{example}
  Как провести прямую через две точки? Координаты прямой, проходящие через две точки, выражаются
  через внешнее произведение.
\end{example}

\begin{example}
  Как пересечь две плоскости? \(\mathbb{P}(\Lambda^2 V^*) \cong \mathbb{P}(\Lambda^2 V)\) ---
  прямая.
\end{example}

\begin{example}
  Как пересечь прямую и плоскость?
\end{example}

Огромное преимущество всего этого --- полное отсутствие деления.

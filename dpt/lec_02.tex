\lecture{2}{26.02.2024, 13:40}{}

Рассмотрим еще 2 возможных применения условной вероятности.

\subsubsection{Полная вероятность}

Разобьем вероятностное пространство на конечное (или счетное) число непересекающихся множеств
\(H_i\), причем \(\P(H_j) > 0\). Понятно, что всевозможные исходы исчерпываются.

\begin{definition}
  Такая система событий называется полной.
\end{definition}

Рассмотрим некоторое событие \(A\) того же вероятностного пространства.

\begin{theorem}[формула полной вероятности]
   \[
  \P(A) = \sum_j \P(H_j)\P(A \mid H_j)
  .\]
\end{theorem}
\begin{proof}
  Разобьем \(A\) на ``куски'' полной системы событий:
  \[
  \begin{aligned}
    \P(A) &= \P\left( \bigcup_j AH_j \right) = \sum_j \P(AH_j) \\
          &= \sum_j \frac{\P(AH_j)}{\P(H_j)}\P(H_j) = \sum_j \P(H_j)\P(A \mid H_j).
  \end{aligned}
  \]
\end{proof}

\subsubsection{Формула Байеса}

Рассмотрим следующую ситуацию: пусть есть полная система событий и уже произошедшее событие \(A\).
Как понять, при каком условии оно произошло --- какое из событий \(H_j\) имело место?

Понятно, что достоверно это восстановить нельзя, но можно вероятностно.

\begin{theorem}[формула Байеса]
  \[
  \P(H_i \mid A) = \frac{\P(A \mid H_i)\P(H_i)}{\sum_j \P(A \mid H_j)\P(H_j)}
  .\]
\end{theorem}
\begin{proof}
  По формуле полной вероятности известно, что знаменатель есть в точности \(\P(A)\). В числителе же
  стоит \(\frac{\P(AH_i)}{\P(H_i)}\P(H_i)\); значит, \(\P(H_i \mid A) = \frac{\P(AH_i)}{\P(A)}\)
  \mnote{\(\P(A) > 0\)} --- определение условной вероятности.
\end{proof}

\section{Случайные величины и их распределение}

Может сложиться ошибочное впечатление, что теория вероятности нужна только для подсчета
вероятностей.

На самом же деле, теория вероятностей изучает распределение случайных величин.

\begin{definition}
  \(X\colon \Omega \to \R\) (\(\left( \Omega, p \right)\) --- вероятностное пространство) ---
  случайная величина на \(\Omega\).
\end{definition}

У такой случайной величины есть \textit{распределение}.

Положим, что \(B \subset \R\) и рассмотрим \(\P(X \in B)\): \[
  \P(X \in B) = \P\left( \omega \in \Omega, X(\omega) \in B\right) = \sum_{\omega\colon X(\omega)
  \in B} p(\omega)
.\]

\begin{definition}
  \(\left( \P(X \in B), B \in \R \right)\) --- распределение \(X\).
\end{definition}

Для определения распределения достаточно определить вероятности на точечных множествах:
\begin{statement}
  \[
    \P(X \in B) = \sum_{x \in B} \P(X = x)
  .\]
\end{statement}

Распределение \(X\) определяется парой \(\left( \P(X = x), x \in B \right)\).

\begin{definition}
  Случайные величины называются одинаково распределенными, если их распределения одинаковы.
\end{definition}

\begin{notation}
  СВ --- случайная(ые) величина(ы).
\end{notation}

Пусть \(X_1, \ldots, X_n\) --- СВ, определенные на одном и том же ВП, а \(A_1, \ldots, A_n \subset
\R\). Рассмотрим события \((X_1 \in A_1), \ldots, (X_n \in A_n) \subseteq \Omega\).

\begin{definition}
  Если события \((X_1 \in A_1), \ldots, (X_n \in A_n)\) независимы для любых \(A_1, \ldots, A_n\),
  то \(X_1, \ldots, X_n\) независимы.
\end{definition}

Рассмотрим стандартные примеры распределений.

\begin{example}[распределение Бернулли]
  Пусть \(0 \le p \le 1\) и \(\P(X = 1) = p\), \(\P(X = 0) = 1 - p\).

  Тогда \(X \sim B(p)\) --- распределение Бернулли с параметром \(p\).
\end{example}

\begin{example}[биномиальное распределение]
  Пусть \(0 \le p \le 1\), \(n \in \N\); \(\P(X = k) = C_n^k p^k(1 - p)^{n - k}\).

  Тогда \(X \sim \mathcal{B}(n, p)\) --- биномиальное распределение.
\end{example}

\begin{example}[распределение Пуассона]
  Пусть \(a > 0\), \(\forall k = 0, 1, 2, \ldots \quad \P(X = k) = e^{-a}\frac{a^k}{k!}\).

  Тогда \(X \sim \mathcal{P}(a)\) --- распределение Пуассона с параметром \(a\).
\end{example}

\begin{example}[геометрическое распределение]
  Пусть \(\forall k = 1, 2, \ldots \quad \P(X = k) = p(1 - p)^{k - 1}\).

  Тогда \(X \sim G(p)\) --- геометрическое распределение с параметром \(p\); интуитивно, это
  распределение времени ожидания какого-то события.
\end{example}

Чтобы все стало окончательно понятно, решим пару задач.

\begin{exercise}
  Пусть \(X_1, \ldots, X_n\) независимы, \(\forall i \quad X_i \sim B(p)\).

  Рассмотрим \(S = X_1 + \ldots + X_n\) --- какое у нее распределение?
\end{exercise}
\begin{proof}[Решение]
  Понятно, что \(X_i \in \left\{ 0, 1 \right\}\) --- значит, \(S \in [0, n]\).

  Пусть \(A_1, \ldots, A_n\) образуют схему Бернулли; введем индикатор \(X_j = \mathbb{1}_A
  \coloneqq \begin{cases}
    1, &\omega \in A_j \\
    0, &\omega \notin A_j
  \end{cases}\); понятно, что \(X_j \sim B(p)\).

  Пусть \(S = \sum_{j = 1}^n X_j\) --- число произошедших событий (успехов). Но тогда видно, что \(S
  \sim \mathcal{B}(n, p)\).
\end{proof}

\begin{exercise}
  Пусть \(X\), \(Y\) --- независимые случайные величины; \(X \sim \mathcal{P}(a)\), \(Y \sim
  \mathcal{P}(b)\). Какое распределение у \(X + Y\)?
\end{exercise}
\begin{proof}[Решение]
  Понятно, что \(X + Y \in [0, \infty]\).

  \(\P(X + Y = k) = \sum_{l = 0}^k \P(X = l, Y = k - l)\) (\(k \ge 0\)); по независимости величин,
  \(\sum_{l = 0}^k \P(X = l)\P(Y = k - l)\), а по распределению Пуассона, \(\sum_{l = 0}^k
  e^{-a}\frac{a^l}{l!}e^{-b}\frac{b^{k - l}}{(k - l)!} = e^{-(a + b)} \sum_{l = 0}^k \frac{k! a^l
  b^{k - l}}{l!(k - l)! k!} = e^{-(a + b)}\frac{(a + b)^k}{k!}\).

  Получилось, что \(\boxed{(X + Y) \sim \mathcal{P}(a + b)}\). \mnote{редкое и полезное свойство}
\end{proof}

\section{Математическое ожидание}

Как можно численно охарактеризовать случайное распределение?

Как раньше, пусть \(X\) --- случайная величина вероятностного пространства \((\Omega, p)\).

\begin{definition}
  Математическое ожидание \(\E{X} \coloneqq \sum_{\omega \in \Omega} X(\omega)p(\omega)\).
\end{definition}

``E'' от фр. \textit{espérance} (англ. \textit{expectation}).

\begin{remark}
  Если \(\Omega\) счетно, то условимся, что \(\E{X}\) определена, если \(\sum_{\omega \in \Omega}
  \left| X(\omega) \right|p(\omega) < \infty\) (\textit{абсолютная сходимость}).
\end{remark}

Изучим элементарные свойства математического ожидания.

\begin{statement}
  \(\E{X} = \sum_{x \in \R} x\P(X = x)\).
\end{statement}
\begin{proof}
  \[
    \E{X} \coloneqq \sum_{\omega \in \Omega} X(\omega)p(\omega) = \sum_{x \in \R} \sum_{\omega\colon
    X(\omega) = x} \underbrace{X(\omega)}_x p(\omega) = \sum_{x \in \R} x \sum_{\omega\colon X(\omega)
    = x} p(\omega) = \sum_{x \in \R} x\P(X = x)
  .\]
\end{proof}

\begin{statement}
  Если \(X \ge 0\), то \(\E{X} \ge 0\).
\end{statement}

\begin{statement}
  Если \(X\), \(Y\) --- СВ на одном и том же ВП и \(\E{X}\), \(\E{Y}\) определены, то \(\E{(X + Y)}
  = \E{X} + \E{Y}\).
\end{statement}
\begin{proof}
  \[
    \E(X + Y) = \sum_{\omega \in \Omega} \left( X(\omega) + Y(\omega) \right)p(\omega) =
    \sum_{\omega \in \Omega} X(\omega)p(\omega) + \sum_{\omega \in \Omega} Y(\omega)p(\omega)
  .\]
\end{proof}

\begin{statement}
  Если \(X \ge Y\) и \(\E{X}\), \(\E{Y}\) определены, то \(\E{X} \ge \E{Y}\).
\end{statement}
\begin{proof}
  \(X = Y + (X - Y)\): \(\E{X} = \E{Y} + \underbrace{\E(X - Y)}_{\ge 0}\).
\end{proof}

\begin{statement}
  \(\E(cX) = c\E{X}\).
\end{statement}

Тривиальные свойства закончились; рассмотрим более интересные.

Уже известно, как выглядит математическое ожидание суммы; а что можно сказать про произведение?

\begin{example}
  Пусть \(X\) такое, что \(\P(X = 1) = \frac{1}{2} = \P(X = -1)\), \(Y = X\).

  Тогда \(\E{X} = 0 = \E{Y}\), но \(\E{XY} = 1\).
\end{example}

\begin{statement}
  Если \(X\), \(Y\) \textit{независимы}, и \(\E{X}\), \(\E{Y}\) определены, то \(\E(XY) = \E{X}\E{Y}\).
\end{statement}
\begin{proof}
  \(\P(XY = z) = \sum_{x, y\colon xy = z} \P(X = x, Y = y)\); по независимости, \(\ldots = \sum_{x,
  y\colon xy = z} \P(X = x)\P(Y = y)\).

  Умножим обе части на \(z\): \(z\P(XY = z) = \sum_{x, y\colon xy = z} xy\P(X = x)\P(Y = y)\).

  Теперь, сложим всё это по всем \(z\): \(\E{XY} = \left(\sum_x x\P(X = x)\right)\left( \sum_y y\P(Y
  = y)\right)\) (за счет определенности \(\E{X}\), \(\E{Y}\)).
\end{proof}

Понятно, что это обобщается и для большего числа событий:
\begin{statement}
  Если \(X_1, \ldots, X_n\) независимы, все мат. ожидания определены, то \(\E(X_1 \ldots X_n) =
  \prod_{i = 1}^n \E{X_i}\).
\end{statement}

\subsection{Неравенства, основанные на математических ожиданиях}

Пусть \(X\) --- случайная величина, \(f\colon \R \to \R_+\) не убывает.

\begin{statement}
  \(\forall r \quad \P(X \ge r) \le \frac{\E{f(x)}}{f(r)}\).
\end{statement}
\begin{proof}
  Построим дополнительную функцию: \(g(x) = \begin{cases}
    0, &x < r \\
    f(r), &x \ge r
  \end{cases}\) (понадобилась как неотрицательность, так и монотонность).

  Тогда \(g(x) \le f(x) \implies \E{g(x)} \le \E{f(x)}\), но \(\E{g(x)} = f(r)\P(X \ge r)\).
\end{proof}

\lecture{10}{26.03.2024, 13:40}{}

\begin{proof}[Доказательство теоремы Лагранжа]
  Рассмотрим несколько случаев.
  \begin{itemize}
    \item \(\forall v \in V \quad Q(v) = 0 \implies (V, Q) \cong \langle 0, \ldots, 0\rangle\),
      очевидно.

    \item \(\exists v \in V\colon Q(v) \neq 0\). Возьмем тогда такое \(v\) и натянем на него
      пространство: \(Q\restriction_{\langle v \rangle}\) --- невырожденная форма.

      По лемме, \((V, Q) \cong_i \underbrace{\left( \langle v \rangle, Q\restriction_{\langle v
      \rangle}, \right)}_{\cong_i \langle Q(v)\rangle} \boxplus \left( \langle v \rangle^\perp,
      Q\restriction_{\langle v \rangle^\perp} \right)\). Но в обоих случаях размерности уменьшились
      --- можно применить индукцию по \(\dim{V}\).
  \end{itemize}
\end{proof}

Стоит отметить, что доказательство конструктивное --- хотя в открытых источниках можно найти
аналогичное доказательство в терминах матриц.

\begin{remark}
  ``Диагонализация'' матрицы Грама определена неоднозначно; над произвольным полем нельзя сказать,
  когда одна форма изометрична другой.
\end{remark}

Однако, что-то сказать можно, если поле ``хорошее''.

В одномерном случае получается \(cac\), то есть \(\langle a \rangle \cong_i \langle c^2a \rangle\).

\begin{exercise}
  Показать, что \(\langle a, b \rangle \cong_i \langle b, a \rangle\).
\end{exercise}

\begin{statement}
  Над алгебраически замкнутым полем (здесь это \(\C\)), \(\langle a_1, \ldots, a_n \rangle \cong_i
  \langle 1, \ldots, 1, 0, \ldots, 0 \rangle\), где количество \(0\) --- размерность радикала (и,
  соответственно, количество \(1\) --- ранг матрицы Грама).
\end{statement}

По-другому говоря, любая форма изометрична сумме квадратов \(x_1^2 + \ldots + x_r^2\), где \(r =
\rank\Gamma\): \[
  \begin{pmatrix} x_1 & \ldots & x_n \end{pmatrix} \Gamma \begin{pmatrix} x_1 \\ \vdots \\ x_n
\end{pmatrix} = \sum_{ij} a_{ij}x_ix_j
.\]

А что будет происходить над \(\R\)?

\begin{statement}
  Над \(\R\) любая форма изометрична \(x_1^2 + \ldots + x_p^2 - x_{p + 1}^2 - \ldots - x_{p +
  q}^2\), где \(p + q = \rank\Gamma\).
\end{statement}

\begin{theorem}[закон инерции Сильвестра]
  Пара \((p, q)\) определена однозначно.
\end{theorem}
\begin{proof} \mnote{дальше это будет доказано, но в другом виде}
  Доказать предлагается самостоятельно; в качестве подсказки, \(p\) --- максимальная размерность
  подпространства \(U\) в \(V\) такого, что \(Q\restriction_U\) положительно определена, то есть
  \(\forall u \neq 0 \in U \quad Q(u) > 0\).
\end{proof}

Над \(\R\) положительную определенность можно задать и для матрицы Грама:
\begin{definition}
  \(\Gamma\) называется положительно определенной, если \(\forall u \neq 0 \quad u^\top\Gamma u >
  0\).
\end{definition}

В качестве еще одного ``хорошего'' поля можно рассмотреть конечные поля.

\begin{definition} \mnote{имеет смысл только для невырожденной формы}
  Дискриминант формы \(\disc{Q} = (-1)^{\frac{n(n - 1)}{2}}\det\Gamma\) с точностью до умножения на
  квадрат (при замене базиса).
\end{definition}

Формальнее, под умножением над квадрат подразумевается, что дискриминант рассматривается в классе
\(\qfrac{F^\times}{\left( F^\times \right)^2}\) (мультипликативная группа поля и подгруппа полных
квадратов).

\begin{exercise}
  Показать, что над конечным полем \(\mathbb{F}_q\) любая невырожденная форма определяется своим
  рангом и дискриминантом.
\end{exercise}

Сейчас это упражнение может показаться слишком сложным --- после теоремы Витта должно стать проще.

Может также потребоваться вспомогательное
\begin{statement}
  Если \(q\) нечетное, то \(\qfrac{\mathbb{F}_q^\times}{\left( \mathbb{F}_q^\times \right)^2} \cong
  C_2\) (циклическая группа).
\end{statement}

Над \(\Q\) всё еще сложнее: есть попытки классифицировать теоремой Минковского-Хассе, которая
(неявно) предлагает алгоритм проверки изометрии диагональных форм, но она очень сложная.

Пусть есть квадратичные пространства \((V, Q)\), \((U_1, Q_1)\) и \((U_2, Q_2)\) (\textit{полностью
произвольные}).

\begin{theorem}[Витта о сокращении]
  Если \((U_1, Q_1) \boxplus (V, Q) \cong_i (U_2, Q_2) \boxplus (V, Q)\), то можно ``сократить'':
  \((U_1, Q_1) \cong_i (U_2, Q_2)\).
\end{theorem}

А теперь пусть \(U_1, U_2 \leqslant V\) и \(Q\restriction_{U_1}\) невырожденная.

\begin{theorem}[Витта о продолжении]
  Если \(Q\restriction_{U_1} \cong_i Q\restriction_{U_2}\) зафиксированной \(\varphi\), то
  существует изометрия всего пространства, продолжающее заданную изометрию двух подпространств:
  \(\tilde{\varphi}\) такое, что \(\tilde{\varphi}\restriction_{U_1} = \varphi\).
\end{theorem}

\begin{example}
  По своей сути это одна из самых первых теорем математики: признак равенства треугольников.

  Рассмотрим в \(\R^3\) \mnote{можно и в \(\R^n\)} векторы \(u_1, u_2\) и \(v_1, v_2\) такие, что
  \(Q(u_1) = Q(v_1)\), \(Q(u_2) = Q(v_2)\), и углы между ними тоже равны: \(B(u_1, u_2) = B(v_1,
  v_2)\) (\(Q\) --- сумма квадратов, \(B\) --- скалярное произведение). Тогда существует изометрия
  \(\R^3 \to \R^3\) (некоторое движение) такое, что \(\tilde{\varphi}(u_1) = v_1\),
  \(\tilde{\varphi}(u_2) = v_2\).
\end{example}

Перед тем как перейти к доказательствам надо запастись достаточным количеством вспомогательных
утверждений.

\begin{definition}
  Ортогональная группа \(O(V, Q) \coloneqq \{ \varphi\colon V \to V \mid \varphi\text{ ---
  линейная биекция}, \forall v \in V \quad Q(\varphi(v)) = Q(v) \}\).
\end{definition}

\begin{statement}
  \(O(V, Q)\) --- группа.
\end{statement}
\begin{proof}
  \hfill
  \begin{itemize}
    \item \(\id \in O(V, Q)\), очевидно.

    \item Возьмем \(\varphi, \psi \in Q(V, Q)\): \(Q(\varphi \circ \psi(v)) = Q(\psi(\varphi(v)) =
      Q(\varphi(v)) = Q(v)\).

    \item Возьмем \(\varphi \in O(V, Q)\): \(Q(\varphi^{-1}(v)) = Q(\varphi(\varphi^{-1}(v))) =
      Q(v)\), так как \(\varphi\) сохраняет \(Q\).
  \end{itemize}
\end{proof}

\begin{statement}
  \(O(V, Q) \leqslant \GL{V}\).
\end{statement}

\begin{definition}
  Матрицы, соответствующие \(O(V, Q)\), называются ортогональными: \(\left\{ C \in \GL{V} \mid
  C^\top \Gamma C = \Gamma \right\}\).
\end{definition}

``По умолчанию'' в качестве \(\Gamma\) берут \(I\) (сумма квадратов) --- тогда ортогональность
матрицы равносильна равенству \(C^\top = C^{-1}\).

\begin{example}
  В \(\R^2\) ортогональными отображениями являются или поворот, или симметрия (отражение)
  относительно прямой.
\end{example}

\begin{definition}
  Специальная ортогональная группа \(\SO_n\) --- подгруппа ортогональных матриц, определитель
  которых \(1\).
\end{definition}

\begin{example}
  В \(\R^2\) специальная ортогональная группа --- поворот.
\end{example}

Рассмотрим гиперплоскость, задаваемую перпендикулярной ей вектором \(u\). Как отразить от нее
некоторый вектор \(v\)?

Необходимые условия --- \(u \neq \Rad{Q}\), и даже более --- \(B(u, u) = Q(u) \neq 0\); \(B(u,
\cdot) = 0\). Тогда искомое отражение ищется из решений уравнения \(B(u, v + tu) = 0\).

\begin{definition}
  Векторы \(u\) такие, что \(Q(u) \neq 0\), называются анизотропными.
\end{definition}

\begin{definition}
  Отражение \(S_u(v) \coloneqq v - \frac{2B(u, v)}{Q(u)}u\).
\end{definition}

\begin{lemma}
  \hfill
  \begin{enumerate}
    \item \(S_u^2 = \id\);
    \item \(S_u \in O(V, Q)\).
  \end{enumerate}
\end{lemma}
\begin{proof}
  \hfill
  \begin{enumerate}
    \item \(S_u(S_u(v)) = S_u(v) - \frac{2B(u, S_u(v))}{Q(u)}u\); \(B(u, S_u(v)) = B(u, v) -
      \frac{2B(u, v)Q(u)}{Q(u)} = -B(u, v)\).

      Тогда \(S_u(S_u(v)) = v - \frac{2B(u, v)}{Q(u)}u + \frac{2B(u, v)}{Q(u)}u = v\).

    \item \(Q(S_u(v)) = B\left(v - \frac{2B(u, v)}{Q(u)}u, v - \frac{2B(u, v)}{Q(u)}u\right) = B(v,
      v) - \frac{2B^2(u, v)}{Q(u)} - \frac{2B^2(u, v)}{Q(u)} + \frac{4B^2(u, v)}{Q^2(u)}Q(u) = B(v,
      v) = Q(v)\).
  \end{enumerate}
\end{proof}

Сформулируем ключевую лемму.

Зафиксируем \(\alpha \neq 0\) и рассмотрим множество \(\left\{ v \in V \mid Q(v) = \alpha
\right\}\).

\begin{definition}
  \(\left\{ v \in V \mid Q(v) = \alpha \right\}\) --- аффинная квадрика.
\end{definition}

\begin{example}
  Если \(Q\) --- сумма квадратов, то такое множество это сфера.
\end{example}

\begin{lemma}
  \(\forall \alpha \neq 0 \quad O(V, Q)\) действует на аффинной квадрике транзитивно.
\end{lemma}

Что это значит? Если есть \(v_1, v_2\) такие, что \(Q(v_1) = \alpha = Q(v_2)\), то найдется
изометрия \(\varphi \in O(V, Q)\) такая, что \(\varphi(v_1) = v_2\).

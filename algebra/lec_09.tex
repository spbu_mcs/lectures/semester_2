\lecture{9}{20.03.2024, 13:40}{}

Пусть есть пространства \(U\) и \(V\) с билинейными формами на них: \(B\colon U \times U \to F\) и
\(B'\colon V \times V \to F\).

\begin{definition} \mnote{при зафиксированных \(B\) и \(B'\)}
  \(\varphi\colon U \to V\) называется изометрией, если \(\forall u, v \in U \quad B'\left(
  \varphi(u), \varphi(v) \right) = B(u, v)\).
\end{definition}

Иногда также на изометрию накладывается условие изоморфизма.

Векторные пространства с билинейными формами изучаются с точностью до биективных изометрией.

Известно, что ВП задается базисом; как задать билинейную форму на базисе?

Пусть \(e_1, \ldots, e_n\) --- базис \(V\): \(B(e_1a_1 + \ldots + e_na_n, e_1b_1 + \ldots +
e_nb_n)\) можно раскрыть по билинейности: \[
  \ldots = \sum_{i, j} a_ib_jB(e_i, e_j)
.\]

\begin{statement}
  Для задания билинейной формы достаточно задать \(\left\{ B(e_i, e_j) \right\}_{i, j}\).
\end{statement}

Посмотрим на квадратную матрицу, где \(a_{ij} = B(e_i, e_j)\).

\begin{definition}
  Такая матрица называется матрицей Грама.
\end{definition}

%Пусть \(\Gamma\) --- матрица Грама: \[
%  \begin{pmatrix} a_1 & \ldots & a_n \end{pmatrix} \Gamma \begin{pmatrix} b_1 \\ \vdots \\ b_n
%  \end{pmatrix}
%.\]

\begin{statement}
  \(B(u, v) = u^\top\Gamma v\), где \(\Gamma\) --- матрица Грама.
\end{statement}

Что происходит с матрицей Грама при замене базиса матрицей перехода \(C\)? \(u = Cu'\), \(v = Cv'\),
а тогда \[
  B(u, v) = \left( Cu' \right)^\top \Gamma Cv' = \left( u' \right)^\top C^\top \Gamma C v'
:\]
\begin{statement}
  При замене базиса \(\Gamma\) переходит в \(C^\top\Gamma C\).
\end{statement}

Отметим сходство с заменой базиса \textit{сопряжением}, хоть это и объект другой от матрицы
линейного оператора природы.

Исходя из этого задача изучения матриц \(\Gamma\) сводится к их изучению с точности до матриц вида
\(C^\top \Gamma C\).

\begin{definition}
  \(B\) называется симметрической билинейной формой, если \(B(u, v) = B(v, u)\).
\end{definition}

\begin{statement}
  Если \(B\) симметричная, то \(u^\top \Gamma v = v^\top \Gamma u = u^\top \Gamma^\top v\).

  То есть \(\Gamma = \Gamma^\top\).
\end{statement}

\begin{definition}
  Такие матрицы \(\Gamma\) называются симметрическими.
\end{definition}

Понятно, что у таких матриц достаточно задать лишь верхнюю (нижнюю) половину относительно диагонали.

\begin{statement}
  Смена базиса матрицы Грама не влияет на симметричность матрицы.
\end{statement}

С этого место предполагается, что \(\boxed{\mathrm{char}{F} \neq 2}\), то есть существует
\(\frac{1}{2}\).

Зачастую вместо билинейных форм рассматривают \(B(u + v, u + v) = B(u, u) + B(v, v) + 2B(u, v) \iff
\boxed{B(u, v) = \frac{1}{2}\left( Q(u + v) - Q(u) - Q(v) \right)}\), где \(Q(w) \coloneqq B(w, w)\)
--- для симметрических форм достаточно задавать значения только на парах векторов \((v, v)\).

\begin{definition}
  Такое \(Q\) называется квадратичной формой.
\end{definition}

\begin{definition}
  Пара \((V, Q)\) называется квадратичным пространством (пространством с квадратичной формой).
\end{definition}

В этом духе можно переформулировать и
\begin{statement}
  \(\varphi\colon U \to V\) --- изометрия, если \(Q'(\varphi(u)) = Q(u) \quad \forall u \in U\), где
  \(Q\) и \(Q'\) --- квадратичные формы \(U\) и \(V\) соответственно.
\end{statement}

\begin{definition}
  Векторы \(u\) и \(v\) называются ортогональными (\(u \perp v\)), если \(B(u, v) = 0\).
\end{definition}

Пусть \((U, Q)\) и \((V, Q')\) --- квадратичные пространства. В том же духе, что и прямая сумма ВП,
\begin{definition}
  Ортогональная сумма \(U \boxplus V \coloneqq U \oplus V\) с формой \(Q((u, v)) = Q(u) + Q'(v)\).
\end{definition}

\begin{notation}
  Иногда обозначают \(U \perp V \coloneqq U \boxplus V\).
\end{notation}

\begin{statement}
  Если \(\Gamma\) и \(\Gamma'\) --- матрицы Грама \(\left( U, B \right)\) и \(\left( V, B'
  \right)\), то матрица Грама \(\left( U, B \right) \boxplus \left( V, B' \right)\) ---
  \(\begin{pmatrix} \Gamma & 0 \\ 0 & \Gamma' \end{pmatrix} \).
\end{statement}

В терминах билинейных (а не квадратичных) форм, \[
  B((u_1, v_1), (u_2, v_2)) = B(u_1, u_2) + B'(u_2,
  v_2).
\]

Посмотрим, что можно узнать о радикалах билинейных форм (\(\Rad{B} \coloneqq \ker{B}\)) ---
\(\Rad{B}\) состоит из всех ортогональных всем векторов.

На самом деле, по каждой форме можно построить невырожденную.

Понятно, что \(B\restriction_{\Rad{B}} = 0\): по универсальному свойству фактор-пространства, строим
\(\bar{B}\colon \qfrac{V}{\Rad{B}} \to V^*\). С другой стороны есть \(\left( \qfrac{V}{\Rad{B}}
\right)^* \leqslant V^*\), состоящие из таких форм \(f\), где \(f\restriction_{\Rad{B}} = 0\).

\begin{statement}
  Если \(B\) симметрическая, то \(\Imf\bar{B} \leqslant \left( \qfrac{V}{\Rad{B}} \right)^*\).
\end{statement}
\begin{proof}
  \(\bar{B}\left( [v] \right)(u) \coloneqq B(v, u) = B(u, v)\), а значит если \(u \in \Rad{B}\), то
  \(B(u, v) = 0\). Тогда, раз \(\bar{B}\) обнуляется на радикале, то образ попадает в \(\left(
  \qfrac{V}{\Rad{B}} \right)\).
\end{proof}

Тогда (в случае симметричной) \(B'\colon \qfrac{V}{\Rad{B}} \to \left( \qfrac{V}{\Rad{B}}
\right)^*\), где \(B'([v])([u]) = B(v, u)\).

\begin{statement}
  \(B'\) уже невырожденная форма.
\end{statement}
\begin{proof}
  Предположим обратное: есть некоторый \([v]\) такой, что \(\forall [u] \quad B'([v], [u]) = 0\).

  Это значит, что \(v \in \Rad{B}\), но тогда \([v] = 0\) из-за факторизации по радикалу.
\end{proof}

Получилось в частном случае ``обезвыродить'' форму.

На самом деле, утверждение можно немного усилить:
\begin{statement}
  \((V, B) \cong_i \left( \qfrac{V}{\Rad{B}}, B' \right) \boxplus \left( \Rad{B}, 0 \right)\)
  (изометрично), причем первое слагаемое --- невырожденная форма, а второе --- тождественно нулевая.
\end{statement}
\begin{proof}
  Есть отображение проекции \(\pi\colon V \to \qfrac{V}{\Rad{B}}\), --- придумаем ему обратное
  отображение \(s\) (\textit{сечение}). Возьмем базис \([e_1], \ldots, [e_k]\) и зададим \(s([e_i])
  = e_i\); получилось линейное отображение, где \(\pi \circ s = \id_{\qfrac{V}{\Rad{B}}}\), но \(s\)
  \textbf{зависит} от выбора представителя.

  Теперь построим искомый изоморфизм, начав ``справа налево'': пусть \(([v], u) \mapsto s([v]) +
  u\).
  \begin{pfparts}
    \item[Изоморфизм] \(s(v) + u = 0 \iff s(v) = -u \in \Rad{B}\), но тогда \(v = \pi \circ s(v) =
      -\pi(u) = 0\) --- ядро тривиально, а размерности совпадают.

    \item[Изометрия] В ортогональной сумме, \(Q([v], u) \coloneqq Q([v]) + Q(u) = Q([v])\) за счет
      тождественной вырожденности второго слагаемого. 

      \(Q(s(v) + u) = Q(s(v)) + \underbrace{Q(u)}_0 + 2\underbrace{B(s(v),
      u)}_0\), а \(Q(s([v])) = Q([v])\), ведь их проекции равны (\(\pi(s([v])) = [v]\)).
  \end{pfparts}
\end{proof}

\begin{remark}
  Раз \(s\) зависит от выбора представителя, эта изометрия неканоническая.
\end{remark}

\begin{definition}
  Ортогональное дополнение \(U^\perp \coloneqq \left\{ v \in V \mid \forall u \in U \quad B(u, v) =
  0\right\}\).
\end{definition}

\begin{consequence}
  \(\Rad{B} = V^\perp\), где \((V, B)\) --- билинейное пространство.
\end{consequence}

\begin{theorem}[Лагранжа]
  Конечномерное квадратичное пространство над полем, чья \(\mathrm{char} \neq 2\), изометрично
  изоморфно ортогональной прямой сумме одномерных (квадратичных пространств).
\end{theorem}

Перед тем, как ее доказать, понадобится вспомогательная
\begin{lemma}
  Если \((V, Q)\) --- квадратичное пространство, \(U \leqslant V\), а \(Q \restriction_U\)
  невырожденная, то \((V, Q) \cong_i \left( U, Q\restriction_U \right) \boxplus \left( U^\perp,
  Q\restriction_{U^\perp} \right)\).
\end{lemma}
\begin{proof}
{\renewcommand\qedsymbol{\(\square\)}
  \begin{statement}
    \(\dim{U^\perp} = \dim{V} - \dim{U}\) (тут важно, что \(Q\restriction_U\) невырожденное).
  \end{statement}
  \begin{proof}
    Рассмотрим \(V \xrightarrow{B} V^* \xrightarrow{i^*} U^*\), ведь есть вложение \(V^* \to U^*\)
    (из-за наличия вложения \(U \xrightarrow{i} V\)):
    \begin{itemize}
      \item \(\ker\left( i^* \circ B \right) \coloneqq \{ v \in V \mid \underbrace{B(v)(u)}_{B(u,
        v)} = 0 \quad \forall u \in U \} = U^\perp\).

      \item По условию (из-за невырожденности), отображение \(U \xrightarrow{i} V \xrightarrow{B}
        V^* \xrightarrow{i^*} U^*\) сюръективно; значит, тем более, \(V \xrightarrow{B} V^*
        \xrightarrow{i^*} U^*\) тоже сюръективно, и \(\Imf = U^*\).
    \end{itemize}

    Тогда по теореме о размерности ядра и образа получается искомое.
  \end{proof}

  Построим искомую изометрию ``справа налево'': \((u, v) \mapsto u + v\).
  \begin{pfparts}
    \item[Изоморфизм] \(U \ni u = -v \implies v \in U\), но \(v \in U^\perp\), а значит,
      \(B\restriction_U(v)(u) = B(v, u) = 0\), а \(B\restriction_U\) невырождено и \(v = 0\) ---
      ядро тривиально; размерности уже посчитаны раннее.

    \item[Изометрия] \(Q(u, v) = Q(u) + Q(v)\), \(Q(u + v) = Q(u) + Q(v) + \underbrace{B(u, v)}_0\) 
      (за счет ортогональности).
  \end{pfparts}
}
\end{proof}

Практически это означает, что для любой \textit{симметрической} матрицы Грама \(\Gamma\) можно найти
\(C\) такое, что \[
C^\top \Gamma C = \begin{pmatrix}
  a_1 \\
  & \ddots \\
  & & a_n
\end{pmatrix}
.\]

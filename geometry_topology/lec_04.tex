\lecture{4}{12.03.2024, 15:25}{}

\section{Аффинная оболочка}

\begin{theorem}
  Пересечение любого набора аффинных подпространств либо пусто, либо является аффинным
  подпространством.
\end{theorem}
\begin{proof}
  Пусть \(Y_i\) --- аффинные подпространства; рассмотрим \(\bigcap Y_i\), которое
  \begin{itemize}
    \item пусто;

    \item \(\exists p\colon p \in \bigcap Y_i\); положим, что \(Y = \bigcap Y_i\).

      Где взять векторное подпространство? \(Y_i = p + V_i\), где \(V_i\) --- векторное
      подпространство; тогда \(\bigcap Y_i = p + \bigcap V_i\).
  \end{itemize}
\end{proof}

\begin{definition}
  Аффинная оболочка \(\Aff(A)\) (\(A \subseteq X\), \(A \neq \O\)) --- пересечение всех аффинных
  подпространств, содержащих \(A\).
\end{definition}

\begin{statement}
  \(\Aff(A)\) --- аффинное подпространство.
\end{statement}

Как можно представить \(\Aff(A)\) более компактно? Возьмем \(p \in A\) и положим, что \(B \coloneqq
\varphi_p(A)\) (векторизация).

\begin{statement}
  \(\varphi_p(\Aff{A}) = \lin{B}\), или, по-другому, \(\Aff{A} = p + \lin{B}\).
\end{statement}

\begin{theorem}
  \(\Aff{A}\) --- множество всех (конечных) барицентрических комбинаций точек из \(A\).
\end{theorem}
\begin{proof}
  Положим, что \(p \in A\), \(B \coloneqq \varphi_p(A)\).

  \begin{pfparts}
    \item[\(\boxed{\implies}\)] Рассмотрим \(\forall x \in \Aff{A}\) --- как ее представить в виде
      нужной комбинации?

      Понятно, что \(\vv{px} \in \lin{B}\), а значит, \(\exists v_1, \ldots, v_k \in B \exists t_1,
      \ldots, t_k \in \R\colon \vv{px} = \sum_i t_iv_i\); пусть \(A \ni p_i \coloneqq p +
      v_i\) --- подставляя в предыдущую линейную комбинацию, \(\vv{px} = \sum_i t_i\vv{pp_i}\).

      Хоть такая комбинация и не барицентрическая, можно добавить нулевые векторы с нужными
      коэффициентами: \[
        \begin{aligned}
          \vv{px} &= \sum_i t_i\vv{pp_i} + \left( 1 - \sum_i t_i \right)\vv{pp} \iff \\
          x &= \sum_i t_ip_i + \left( 1 - \sum_i t_i) \right)p.
      \end{aligned}
      \]

    \item[\(\boxed{\impliedby}\)] Пусть \(x = \sum_i t_ip_i\), где \(\sum_i t_i = 1\) и \(p_i \in
      A\).

      Но тогда \(x = p + \sum_i t_i\vv{pp_i}\) (выбор \(p\) ни на что не влияет из-за
      барицентричности); \(\left(\sum_i t_i\vv{pp_i}\right) \in \lin{B}\), а \(\Aff{A} = p +
      \lin{B}\).
  \end{pfparts}
\end{proof}

\section{Аффинно независимые точки}

\(X\) как всегда --- аффинное пространство.

\begin{definition}
  Точки \(p_1, \ldots, p_k \in X\) аффинно зависимы, если \(\exists t_1, \ldots, t_k \in \R\colon
  t_1^2 + \ldots + t_k^2 \neq 0\) такие, что
  \begin{enumerate}
    \item \(\sum_i t_i = 0\);
    \item \(\sum_i t_ip_i = 0\).
  \end{enumerate}
\end{definition}

Завуалированнее, точки аффинно зависимы, если существует нетривиальная сбалансированная нулевая
линейная комбинация.

\begin{theorem}[о переформулировке аффинной независимости]
  Если \(p_1, \ldots, p_k \in X\), то следующие утверждения эквивалентны:
  \begin{enumerate}
    \item \(p_1, \ldots, p_k\) аффинно независимы;
    \item векторы \(\vv{p_1p_2}, \ldots, \vv{p_1p_2}\) линейно независимы;
    \item \(\dim\Aff\left( p_1, \ldots, p_k \right) = k - 1\);
    \item каждая точка из \(\Aff\left( p_1, \ldots, p_k \right)\) единственным образом
      представляется в виде барицентрической комбинации точек \(p_1, \ldots, p_k\).
  \end{enumerate}
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{1 \iff 2}\)] Будем показывать, что аффинная зависимость \(\iff\) линейная
      зависимость.

      То есть, с левой стороны \(\exists t_1, \ldots, t_k\) не все равные \(0\) такие, что \(\sum_i t_i = 0\),
      \(\sum_i t_ip_i = 0\); с правой же, \(\exists t_1, \ldots, t_k\) не все равные \(0\) такие,
      что \(\sum_i t_i\vv{p_1p_i} = 0\).

      Импликация \(\boxed{2 \implies 1}\) очевидна (\(t_1 = -\sum t_i\)); в обратную же сторону,
      достаточно отбросить \(t_1\) и в качестве начала отсчета взять \(p\).

    \item[\(\boxed{2 \iff 3}\)] \(\Aff\left( p_1, \ldots, p_k \right) = p_1 + \lin\left(
      \vv{p_1p_2}, \ldots, \vv{p_1p_k} \right)\).

    \item[\(\boxed{1 \implies 4}\)] Предположим обратное: \(\exists x \in \Aff\left( p_1, \ldots, p_k
      \right)\colon x = \sum_i t_ip_i = \sum_i s_ip_i\) --- не совпадающие барицентрические
      комбинации.

      Зафиксируем произвольный \(p_i\): \(\vv{px} = \sum_i t_i\vv{pp_i} = \sum_i s_i\vv{pp_i}\);
      \(\sum_i (t_i - s_i) \vv{pp_i} = 0\) --- противоречие, ведь получилась ...

    \item[\(\boxed{1 \impliedby 4}\)] Предположим обратное: пусть \(p_1, \ldots, p_k\) аффинно
      зависимы --- по определению, \(\exists \left\{ p_i \right\}\colon \sum_i t_ip_i = 0, \sum_i
      t_i = 0\).

      Пусть \(x = \sum_i s_ip_i\) --- произвольная барицентрическая комбинация; \(x = \sum_i (t_i +
      s_i)p_i\) --- такие комбинации разные, противоречие утверждению 4.
  \end{pfparts}
\end{proof}

\section{Аффинный базис}

Пусть \(\dim{X} = n\).

\begin{definition}
  Аффинный базис в \(X\) --- любой набор аффинно независимых \(n + 1\) точек.
\end{definition}

По-другому говоря,
\begin{definition}
  Аффинный базис в \(X\) --- точка \(p \in X\) и базис \(e_1, \ldots, e_n\) пространства \(\vv{X}\).
\end{definition}

\begin{statement}
  Если \(p_1, \ldots\) --- аффинный базис \(X\), то \(X = \Aff\left( p_1, \ldots, p_{n + 1}
  \right)\).
\end{statement}

\section{Координаты точек}

\begin{statement}
  Если \(p_1, \ldots, p_{n + 1}\) --- аффинный базис \(X\), то \(\forall x \quad x = \sum_i
  t_ip_i\).
\end{statement}

\begin{definition}
  Такие \(t_i\) называются барицентрическими координатами точек.
\end{definition}

Выберем \(p_1\) в качестве начала отсчета; векторы \(e_i \coloneqq \vv{p_1p_i} \quad \forall i = 2,
\ldots, n + 1\) --- базис \(\vv{X}\).

Тогда \(\forall x \in X \quad x = p_1 + \sum_i x_ie_i\), ведь \(X = p_1 + \vv{X}\).

\begin{definition}
  Такие \(x_i\) называются аффинными координатами точек.
\end{definition}

От барицентрических координат аффинные отличаются как минимум количеством точек.

\section{Аффинные отображения}

Пусть \(\left( X, \vv{X}, + \right)\), \(\left( Y, \vv{Y}, + \right)\) --- аффинные пространства, а
\(F\colon X \to Y\) --- некоторое отображение.

Посмотрим, как можно связать (сопоставить) \(F\) и \(F_p\colon \vv{X} \to \vv{Y}\).

Зафиксируем \(p \in X\); \(\forall v \in \vv{X} \quad q \coloneqq p + v\).

\begin{definition}
  \(\tilde{F}_p(v) \coloneqq \vv{F(p)F(q)}\).
\end{definition}

\begin{definition}
  Отображение \(F\) называется аффинным, если отображение \(\tilde{F}_p\) линейно.
\end{definition}

\begin{exercise}
  Построить нелинейное отображение \(\tilde{F}_p\).
\end{exercise}

Линейность \(\tilde{F}_p\) дает еще одно преимущество:
\begin{lemma}
  Если \(\tilde{F}_p\) линейно, то \(\forall q \in X \quad \tilde{F}_q \equiv \tilde{F}_p\).
\end{lemma}
\begin{proof}
  Для произвольного \(r \in X\) рассмотрим \(\vv{qr}\): \[
  \begin{aligned}
    \tilde{F}_q\left( \vv{qr} \right) &\coloneqq \vv{F(q)F(r)} = \vv{F(q)F(p)} + \vv{F(p)F(r)} =
    -\vv{F(p)F(q)} + \vv{F(p)F(r)} \\
    &\eqqcolon -\tilde{F}_p\left( \vv{pq} \right) + \tilde{F}_p\left( \vv{pr} \right) =
    \tilde{F}_p\left( \vv{pr} - \vv{pq} \right) = \tilde{F}_p\left( \vv{qr} \right).
  \end{aligned}
  \]
\end{proof}

\begin{definition}
  \(\tilde{F}\) называется линейной частью аффинного отображения \(F\).
\end{definition}

Тогда если \(F\) --- аффинное отображение, то \(F\left( p + \vv{pq} \right) = F(p) + \tilde{F}\left(
\vv{pq}\right) \quad \forall p, q \in X\); можно переопределить аффинное отображение:
\begin{definition}
  Отображение \(F\colon X \to Y\) называется аффинным, если \(\exists L\colon X \to Y\) такое, что
  \(\forall p, q \in X \quad \vv{F(p)F(q)} = L\left( \vv{pq} \right)\).
\end{definition}

\subsection{Задание аффинных отображений}

\begin{theorem}
  Если \(L\colon \vv{X} \to \vv{Y}\) --- линейное отображение и \(x \in X\), \(y \in Y\) ---
  выбранные точки, то \(\exists!\) аффинное отображение \(F\colon X \to Y\) такое, что
  \begin{enumerate}
    \item \(\tilde{F} = L\);
    \item \(F(x) = y\).
  \end{enumerate}
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\exists}\)] Возьмем произвольную \(p \in X\) и положим, что \(F(p) = y + L\left(
      \vv{xp}\right)\). Проверить, что \(F\) --- аффинное есть то же самое, что проверка \(\tilde{F}
      = L\).

      Раз \(\tilde{F}\) не зависит от выбора точки, то \(\tilde{F}_x\left( \vv{xp} \right) =
      \vv{F(x)F(p)} = \vv{yF(p)}\).

      Тогда \(\vv{yF(p)} = L\left( \vv{xp} \right)\) по заданию \(F\), --- получилось, что
      \(\tilde{F}\) и впрямь линейно.

    \item[\(\boxed{!}\)] Предположим обратное: пусть есть два аффинных отображения \(F\) и \(G\)
      такие, что \(\tilde{F} = \tilde{G} = L\) и \(F(x) = G(x) = y\).

      Тогда \[
      \forall p \in X \quad
      \begin{aligned}
        F(p) &= F(x) + \tilde{F}(xp) \\
        G(p) &= G(x) + \tilde{G}(xp).
      \end{aligned}
      \]
  \end{pfparts}
\end{proof}

Возьмем аффинные пространства \(X\) и \(Y\) и наберем аффинно независимые комбинации точек \(p_1,
\ldots, p_{n + 1}\) и \(q_1, \ldots, q_{n + 1}\) в каждом из них соответственно.

\begin{theorem}
  Существует аффинное отображение \(F\colon X \to Y\) такое, что \(\forall i \quad F(p_i) = q_i\),
  причем если \(\dim{X} = n\), то это \(F\) еще и единственно.
\end{theorem}
\begin{proof}
  Раз \(p_1, \ldots, p_{n + 1}\) аффинно независимые, то \(\vv{p_1p_2}, \ldots \vv{p_np_{n + 1}}\)
  линейно независимы.

  Как задать линейную часть? Из линейной алгебры известно, что найдется линейное отображение
  \(\Lambda\colon \vv{X} \to \vv{Y}\) такое, что \(L\left( \vv{p_1p_i} \right) = \Lambda\left(
  \vv{q_1q_i} \right)\). На этом этапе сразу можно понять, почему \(\dim{X} = n\) достаточно для
  единственности.

  По предыдущей теореме \(\exists!F\colon \tilde{F} = L, F(p_1) = q_1\). Осталось проверить лишь то,
  что \(\forall i = 2, \ldots, n + 1 \quad F(p_i) = q_i\): \[
    F(p_i) = \underbrace{F(p_1)}_{q_1} + \underbrace{\tilde{F}\left( \vv{p_1p_i} \right)}_{L\left(
    \vv{p_1p_i} \right) = \vv{q_1q_i}} = q_i
  .\]
\end{proof}

\subsection{Свойства аффинных отображений}

\begin{statement}
  Если \(F\) --- аффинное отображение, а \(\left\{ t_i \right\}\) --- барицентрический набор точек
  (\(\sum_i t_i = 1\)) и \(p_i \in X\), то \(F\left( \sum_i t_ip_i \right) = \sum_i t_iF(p_i)\).
\end{statement}
\begin{proof}
  Зафиксируем \(x \in X\): \[
  \begin{aligned}
    F\left( \sum_i t_ip_i \right) &= F\left( x + \sum_i t_i\vv{xp_i} \right) = F(x) + \tilde{F}\left(
    \sum_i t_i\vv{xp_i}\right) \\
    &= F(x) + \sum_i t_i\tilde{F}(xp_i) = \ldots.
  \end{aligned}
  \]
\end{proof}

Короче говоря, аффинные отображения сохраняют барицентрические комбинации.

\begin{statement}
  Композиция аффинных отображений --- аффинное отображение, причем линейная часть этой композиции
  --- композиция линейных частей.
\end{statement}
\begin{proof}
  Пусть  \(X \xrightarrow{F} Y \xrightarrow{G} Z\) --- рассматриваемые аффинные отображения;
  положим, что \(H \coloneqq G(F)\), \(H\colon X \to Z\).

  Чтобы показать аффинность \(H\) достаточно показать \(\tilde{H} = \tilde{G}\left( \tilde{F}
  \right)\): \[
  \begin{aligned}
    \tilde{H}\left( \vv{pq} \right) &= \vv{H(p)H(q)} = \vv{G(F(p))G(F(q))} = \tilde{G}\left(
    \vv{F(p)F(q)} \right) \\
    &= \tilde{G}\left( \tilde{F}\left( \vv{pq} \right) \right).
  \end{aligned}
  \]
\end{proof}

\(F\colon X \to Y\) --- всё еще аффинное отображение:
\begin{theorem}
  \hfill
  \begin{enumerate}
    \item Если \(A \leqslant X\) (аффинное подпространство), то \(F(A) \leqslant Y\);
    \item Если \(B \leqslant Y\), то \(F^{-1}(B) \leqslant X\) или \(= \O\).
  \end{enumerate}
\end{theorem}
\begin{proof}
  \hfill
  \begin{enumerate}
    \item Положим, что \(A = p + \vv{A}\) для \(p \in A\); тогда \(\forall v \in \vv{A} \quad p +
      v\) ``пробегает'' все точки из \(A\).

      \(F(p + v) = F(p) + \tilde{F}(v)\) --- \(\tilde{F}(v)\) ``пробегает'' подпространство
      \(\tilde{F}\left( \vv{A} \right) \leqslant Y\).

      Тогда \(F(A) = F(p) + \tilde{F}(A)\), \(\tilde{F}(A)\) --- векторное подпространство, \(F(A)\)
      --- аффинное.

    \item Выберем \(p \in F^{-1}(B)\) (если оно пусто, то ``ок'').

      Хотим, чтобы \(F^{-1}(B) = p + \tilde{F}^{-1}\left( \vv{B} \right)\): \[
      \begin{aligned}
        \forall x \in F^{-1}(B) &\iff F(x) \in B \iff \vv{F(p)F(x)} \in \vv{B} \iff \tilde{F}\left(
        \vv{px}\right) \in \vv{B} \\
        &\iff \vv{px} \in \tilde{F}^{-1}\left( \vv{B} \right) \iff x = p + \vv{px}.
      \end{aligned}
      \]
  \end{enumerate}
\end{proof}

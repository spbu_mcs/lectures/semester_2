\lecture{9}{12.04.2024, 13:40}{}

Смена курса и уточнение доказываемого.

\begin{theorem}
  Если \(\mathcal{X}\) конечно и существует только один эргодический класс \(C\), то \(\exists K >
  0, \delta \in (0, 1)\colon \forall n \forall z \forall y \notin C \quad p^n(z \leadsto y) \le K(1
  - \delta)^n\).
\end{theorem}
\begin{proof}
  \hfill
  \begin{enumerate}
    \item Покажем, что \(\forall y \notin C \exists n \exists x \in C\colon p^n(y \leadsto x) > 0\).

      Введем множество \(F_y \coloneqq \left\{ x \mid \exists n\colon p^n(y \leadsto x) > 0
      \right\}\) --- сразу видно, что оно замкнуто по заданию.

      С другой стороны вспомним, что в любой цепи Маркова найдется существенное состояние (см.
      раннее): то есть \(\exists x \in F_y\) такое, что \(x\) --- существенное состояние. Раз такой
      класс единственный по условию теоремы, то \(x \in C\).

    \item Покажем, что \(\forall y \notin C \exists n\colon \sum_{x \in C} p^n(y \leadsto x) > 0\).

      Очевидно, ведь по предыдущему утверждению каждой слагаемое положительное.

    \item Заметим, что функция, сопоставляющая \(n \mapsto \sum_{x \in C} p^n(y \leadsto x)\)
      неубывающая, ведь эргодический класс замкнут: попав в него выйти уже нельзя.

      Тогда \(\exists \delta \exists n\colon \forall y \quad \sum_{x \in C} p^n(y \leadsto x) >
      \delta > 0\): за счет конечности \(\mathcal{X}\) можно взять самое большое такое \(n\) среди
      всех \(n\), зависящих от \(y\).

      Переформулируем это: \(\sum_{x \notin C} p^n(y \leadsto x) \le (1 - \delta)\).

    \item Покажем, что \(\forall j \ge 1 \forall y \quad \quad \sum_{x \notin C} p^{nj}(y \leadsto
      x) \le (1 - \delta)^j\): начав из любого состояния вероятность не попасть в эргодический класс
      экспоненциально мала.

      Сделаем это по индукции.
      \begin{pfparts}
        \item[База] Выше.

        \item[Шаг индукции] Рассмотрим \(\sum_{x \notin C} p^{n(j + 1)}(y \leadsto x)\) и разобьем
          ее в зависимости от предыдущего шага: \[
            \ldots = \sum_{x \notin C} \sum_v p^n(y \leadsto v)p^{nj}(v \leadsto x)
          .\]

          Понятно, что если \(v \in C\), то \(p^{nj}(v \leadsto x) = 0\) (\(x \notin C\)): \[
          \begin{aligned}
            \ldots &= \sum_{x \notin C} \sum_{v \notin C} p^n(y \leadsto v) p^{nj}(v \leadsto x) =
            \sum_{v \notin C} p^n(y \leadsto v) \sum_{x \notin C} p^{nj}(v \leadsto x) \\
            &\oversym{\le}{\text{и.п.}} (1 - \delta)^j \sum_{v \notin C} p^n(y \leadsto v) \le (1 -
            \delta)^{j + 1}.
          \end{aligned}
          \]
      \end{pfparts}

    \item Осталось показать, что \(\exists K\colon \forall y \forall m \quad \sum_{x \notin C} p^m(y
      \leadsto x) \le K(1 - \delta)^m\) --- оно уже получено в предыдущем пункте для всех степеней,
      кратных \(n\).

      Но это элементарно. \mnote{скорее всего подобное преобразование проделывалось в прошлых
      лекциях}

    \item Раз вся сумма оценивается искомым выражением, то и \(x \notin C\) оценивается ей же.
  \end{enumerate}
\end{proof}

Рассмотрим неприводимую и \textit{конечную} \mnote{чтобы применить теоремы Маркова} цепь, то есть
цепь, состоящую из одного эргодического класса периода \(d > 1\).

Раннее обсуждалось, что такую цепь можно разбить на периодические подклассы \(E_1, \ldots, E_{d -
1}\): \(\forall x \in E_j \quad p^l(x \leadsto y) = 0\), если \(y \notin E_{j \oplus l}\) (сложение
по модулю \(d\)).

Рассмотрим еще одну, заключающую предельную теорему.

\begin{theorem}
  Существует система распределений \((Q_j)_j\), сосредоточенная на \(E_j\) (то есть \(y \notin E_j
  \implies Q_j(y) = 0\)), и при этом
  \begin{enumerate}
    \item \(Q_jp = Q_{j \oplus 1}\) (инвариантность);
    \item \(\forall x \in E_j \forall l \forall y \in E_{j \oplus l} \quad \lim_{n \to \infty} p^{nd
      + l}(x \leadsto y) = Q_{j \oplus l}(y)\).
  \end{enumerate}
\end{theorem}

По сути, нет сходимости к конкретному распределению, но есть сходимость ``по кругу''.

\begin{proof}
  Зафиксируем класс \(E_j\) и рассмотрим на нем цепь Маркова с матрицей перехода \(p^d\). Раз \(d\)
  шагов ``слились'' в \(1\), то получилась цепь с периодом \(1\), в которой, более того, все
  состояния существенны: по теореме Маркова, \(\exists Q_j\) на \(E_j\colon \forall x, y \in E_j
  \quad p^{nd}(x \leadsto y) \xrightarrow{n \to \infty} Q_j(y)\).

  \begin{pfparts}
    \item[Свойство 2] Распишем: \[
      \begin{aligned}
        p^{nd + l}(x \leadsto y) &= \sum_{z \in E_{j \oplus l}} p^l(x \leadsto
        z)\underbrace{p^{nd}(z \leadsto y)}_{\to Q_{j \oplus l}(y)} \to \left( \sum_{z \in E_{j
        \oplus l}} p^l(x \leadsto z) \right) Q_{j \oplus l}(y) \\
        &= Q_{j \oplus l}(y),
      \end{aligned}
      \] используя раннее доказанное и тот факт, что за \(l\) шагов обязательно случится переход из
      \(E_j\) в \(E_{j \oplus l}\).

    \item[Свойство 1] Возьмем \(x_0 \in E_0\) и рассмотрим распределение \(\pi_0 \coloneqq
      \mathbb{1}_{\{x_0\}}\) (``заставим'' цепь стартовать из \(x_0\)).

      Тогда \(\pi_0p^{nd + j}(y) = p^{nd + j}(x_0 \leadsto y) \to Q_j(y)\), где \(y \in E_j\)
      (другие рассматривать нет смысла).

      Запишем это в матричной форме: \(\pi_0p^{nd + j} \to Q_j\). Аналогично можно сказать, что
      \(\pi_0p^{nd + j + l} \to Q_{j \oplus l}\).

      Домножив на матрицу \(p^l\), \(\pi_0p^{nd + j + l} \to Q_jp^l \equiv Q_{j \oplus l}\), то есть
      \(Q_jp^l = Q_{j \oplus l}\).
  \end{pfparts}
\end{proof}

Вспомним, что все это время на самом деле мы классифицировали Марковские цепи по периодичности; пора
рассмотреть еще одну классификацию.

\subsubsection{По возвратности}

Пусть \(x \in \mathcal{X}\).

\begin{definition}
  \(f_j(x) \coloneqq \P\left( \text{на шаге } j \text{, выйдя из } x \text{, придем в } x \text{
  впервые} \right)\).
\end{definition}

Ясно, что раз такие события не пересекаются, то
\begin{statement}
  \(\sum_{j = 1}^\infty f_j \le 1\).
\end{statement}

\begin{definition}
  \(x\) называется возвратным, если \(\sum_j f_j = 1\).
\end{definition}

\begin{definition}
  Иначе такое \(x\) называется невозвратным.
\end{definition}

\begin{definition}
  \(x\) называется положительно возвратным, если \(\sum_{j = 1}^\infty jf_j(x) < +\infty\).
\end{definition}

По-другому говоря, среднее время возвращения конечно.

\begin{definition}
  \(x\) называется нуль возвратным, если \(\sum_{j = 1}^\infty jf_j(x) = +\infty\).
\end{definition}

Интуитивно понятно, что эти \(f_j\) из себя представляют; вообще неясно, как их считать, ведь пока
все что мы умеем считать --- вероятности перехода. Попробуем выразить \(f_j\) через эти вероятности.

\begin{theorem}[основное уравнение]
  \(\forall k \ge 1 \quad p^k(x \leadsto x) = \sum_{j = 1}^k f_j(x) p^{k - j}(x \leadsto x)\).
\end{theorem}

Ясно, откуда оно берется: чтобы выйти из \(x\) и через \(k\) шагов снова оказаться в \(x\) нужно
сначала на каком-то шаге \(j\) впервые вернуться в \(x\), а потом за \(k - j\) шагов снова как-то
пройти из \(x\) в \(x\).

Выражение справа чем-то напоминает свертку: вспомним производящие функции и попробуем с их помощью
упростить выражение.

Зафиксируем \(x \in \mathcal{X}\).

\begin{definition}
  \(\mathcal{P}(z) \coloneqq \sum_{k = 0}^\infty p^k(x \leadsto x) z^k\), где \(p^0(x \leadsto x)
  \coloneqq 1\).
\end{definition}

\begin{definition}
  \(\mathcal{F}(z) \coloneqq \sum_{j = 1}^\infty f_j(x) z^j\).
\end{definition}

Ясно, что последние два определения имеют смысл лишь при \(0 < z < 1\).

\begin{statement}
  \(\mathcal{P}(z) = 1 + \sum_{k = 1}^\infty p^k(x \leadsto x) z^k = 1 +
  \mathcal{P}(z)\mathcal{F}(z)\).
\end{statement}

Выразим \(\mathcal{P}\) через \(\mathcal{F}\): \(\mathcal{P}(z) = \frac{1}{1 - \mathcal{F}(z)}\).

Все эти рассуждения на самом деле доказали
\begin{theorem}[критерий возвратности]
  \(x\) возвратно тогда и только тогда, когда \(\sum_{k = 1}^\infty p^k(x \leadsto x) = \infty\).
\end{theorem}

\begin{consequence}
  Два сообщающихся состояния одновременно либо возвратны, либо невозвратны.
\end{consequence}
\begin{proof}
  \(p^a(x \leadsto y) > 0\), \(p^b(y \leadsto x) > 0\): тогда \(p^{n + a + b}(x \leadsto x) \ge p^a(
  x \leadsto y)p^n(y \leadsto y)p^b(y \leadsto x)\), то есть \(\sum_n p^{n + a + b}(x \leadsto x)
  \ge p^a(x \leadsto y)p^b(y \leadsto x)\sum_n p^n(y \leadsto y)\).
\end{proof}

\begin{remark}
  Возвратность --- характеристика не отдельного состояние, а его эргодического класса.
\end{remark}

Рассмотрим случайное блуждание в \(\Z^d\), где \(p(x \leadsto y) = \frac{1}{2d}\) (\(x, y \in
\Z^d\), \(\mathcal{X} = \Z^d\)). Заметим, что все состояния сообщаются друг с другом, поэтому они
все сразу или возвратные, или невозвратные.

\begin{theorem}[Пойа о возвратности случайного блуждания в \(\Z^d\)]
  Случайное блуждание в \(\Z^d\) возвратно при \(d \in \left\{ 1, 2 \right\}\) и невозвратно при \(d
  \ge 3\).
\end{theorem}
\begin{proof}
  \hfill
  \begin{pfparts}
  \item[\(\boxed{d = 1}\)] Рассмотрим \(p^{2k}(0 \leadsto 0)\) (для нечетного показателя степени это
    не имеет смысла): надо сделать \(k\) шагов вправо и \(k\) шагов влево.

    То есть \(p^{2k}(0 \leadsto 0) = C_{2k}^k \frac{1}{2^{2k}}\): \[
    \begin{aligned}
      \ldots &= \frac{(2k)!}{k!k!2^{2k}} \approx \frac{\sqrt{2\pi 2k} \left( \frac{2k}{e}
      \right)^{2k}}{\sqrt{2\pi k}\sqrt{2\pi k}\left( \frac{k}{e} \right)^k \left( \frac{k}{e}
      \right)^k 2^{2k}} = \frac{\sqrt{2\pi}\sqrt{2k}}{\sqrt{2\pi k}\sqrt{2\pi k}} \\
      &= \frac{1}{\sqrt{\pi k}}.
    \end{aligned}
    \]

    Сразу видно, что \(\sum_{k = 1}^\infty p^{2k}(0 \leadsto 0) = \infty\).

    \item[\(\boxed{d = 2}\)] Хочется свести все к одномерному случаю: было бы идеально, если
      движения по осям были бы независимы, но это не так.

      Сделаем замену переменных \((x, y) \mapsto (u, v)\), где \(u \coloneqq x + y\) и \(v \coloneqq
      x - y\). Посмотрим, что случилось:
      \begin{enumerate}
        \item возвращение \((0_x, 0_y)\) равносильно возвращению \((0_u, 0_v)\);
        \item блуждание в \((u, v)\) --- это два независимых одномерных случайных блуждания.
      \end{enumerate}

      То есть \(p_{(2)}^{2k}((0, 0) \leadsto (0, 0)) = \left( p_{(1)}^{2k}(0 \leadsto 0) \right)^2
      \approx \frac{1}{\pi k}\) и \(\sum_{k = 1}^\infty p_{(2)}^{2k}((0, 0) \leadsto (0, 0)) =
      \infty\).
  \end{pfparts}

  Продолжение следует\textellipsis
\end{proof}

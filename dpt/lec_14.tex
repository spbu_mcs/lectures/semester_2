\lecture{14}{17.05.2024, 13:40}{}

\begin{statement}[сохранение математического ожидания]
  \(\E\left( \E(X \mid \mathcal{B})\right) = \E{X}\).
\end{statement}
\begin{proof}
  \(\E\left( \E\left( X \mid \mathcal{B} \right) \right) = \E\left( \sum_j \E\left( X \mid B_j
  \right) \mathbb{1}_{B_j} \right) = \sum_j \E\left( X \mid B_j \right) \P(B_j) = \sum_j
  \frac{\P((X = x) \cap B_j) x}{\P(B_j)}\P(B_j) = \sum_x \left( \sum_j \P((X = x) \cap B_j)
  \right)x = \sum_x \P(X = x)x = \E{X}\).
\end{proof}

\begin{statement}[правило вынесения]
  Если \(Y = \sum_j y_j \mathbb{1}_{B_j}\) (принимает постоянные значения на элементах разбиения),
  то \(\E(XY \mid \mathcal{B}) = Y \E\left( X \mid \mathcal{B} \right)\).
\end{statement}
\begin{proof}
  Достаточно рассмотреть \(Y\) как \(\mathbb{1}_{B_i}\): с правой стороны равенства будет
  \[
    Y\E\left( X \mid \mathcal{B} \right) = \mathbb{1}_{B_i} \E\left( X \mid \mathcal{B} \right) =
    \begin{cases}
      \E\left( X \mid B_i \right), &\omega \in B_i, \\
      0, &\text{иначе};
    \end{cases}
  \] с левой же стороны будет \[
    \E\left( XY \mid \mathcal{B} \right) = \E\left( X\mathbb{1}_{B_i} \mid \mathcal{B} \right) =
    \E\left( X \mathbb{1}_{B_i} \mid B_j \right) = \begin{cases}
      \E\left( X \mid B_i \right), &i = j, \\
      0, &\text{иначе}
    \end{cases}
  \] при \(\omega \in B_j\).
\end{proof}

Чтобы лучше понять следующее свойство, рассмотрим
\begin{example}
  Рассмотрим точки \((x_1, x_2, x_3) \in \R^3\): есть плоскость с координатами  \((x_1, x_2, 0)\) и
  прямая \((x_1, 0, 0)\).

  Рассмотрим следующую композицию проекций: \((x_1, x_2, x_3) \xrightarrow{\text{плоскость}} (x_1,
  x_2, 0) \xrightarrow{\text{прямая}} (x_1, 0, 0)\) --- то же самое, что произошло бы при проекции
  сразу на прямую, причем то же самое вышло бы и при другом порядке проекций.
\end{example}

Пусть есть два разбиения вероятностного пространства \(\mathcal{B}\) и \(\mathcal{C}\), причем
\(\mathcal{C}\) --- \textit{измельчение} \(\mathcal{B}\), то есть \(B_j = \bigcup_k C_{jk}\).

\begin{statement}[композиция условных ожиданий]
  \(\E\left( \E\left( X \mid \mathcal{B} \right) \mid \mathcal{C} \right) = \E\left( X \mid
  \mathcal{B} \right)\), и наоборот: \(\E\left( \E\left( X \mid \mathcal{C} \right) \mid
  \mathcal{B} \right) = \E\left( X \mid \mathcal{B} \right)\).
\end{statement}
\begin{proof}
  \hfill
  \begin{pfparts}
    \item[\(\boxed{\mathcal{B} \mid \mathcal{C}}\)] \(\E\left( X \mid \mathcal{B} \right) = \sum_j
      b_j\mathbb{1}_{B_j} = \sum_j b_j \sum_k \mathbb{1}_{C_{kj}} = \sum_{k, j} b_j
      \mathbb{1}_{C_{kj}}\) --- эта величина кусочно-постоянна на элементах разбиения
      \(\mathcal{C}\).

      Тогда \(\E\left( \E\left( X \mid \mathcal{B} \right) \mid \mathcal{C} \right) = \E\left(
      \sum_{k, j} b_j \mathbb{1}_{C_{kj}} \mid \mathcal{C} \right)\); по свойствам проекции, это \(
      = \sum_{j, k} b_j \mathbb{1}_{C_{kj}} = \E\left( X \mid \mathcal{B} \right)\).

    \item[\(\boxed{\mathcal{C} \mid \mathcal{B}}\)] Распишем по определению: \[
    \begin{aligned}
      \E\left( X \mid \mathcal{C} \right)(\omega) \oversym{=}{\omega \in C_{jk}} \frac{\sum_x \P\left( (X =
      x) \cap C_{jk} \right)}{\P(C_{jk})};
    \end{aligned}
    \] если \(\omega \in B_j\), то \[
    \begin{aligned}
      \E\left( \E\left( X \mid \mathcal{C} \right) \mid \mathcal{B} \right) &= \sum_v \frac{\P\left(
      \left(\E\left( X \mid \mathcal{C} = v \right) \cap B_j\right)\right)}{\P(B_j)} \\
      &= \sum_k \frac{\P\left( C_{kj} \right) \sum_x \frac{\P\left( (X = x) \cap C_{jk}
      \right)}{\P(C_{jk})}x}{\P(B_j)} \\
      &= \sum_x \sum_k \frac{\P\left( (X = x) \cap C_{jk} \right) x}{\P(B_j)} = \sum_x
      \frac{\P\left( (X = x) \cap B_j \right)}{\P(B_j)} \\
      &= \E\left( X \mid B_j \right) = \E\left( X \mid \mathcal{B} \right).
    \end{aligned}
    \]
  \end{pfparts}
\end{proof}

\subsection{Разбиения, порожденные случайными величинами}

Пусть \(Y\) --- случайная величина.

Она порождает разбиение \(\mathcal{B}_Y = \left\{ \left\{ Y = y
\right\} \right\}_y\), ведь \(Y\) принимает счетное число значений, которым соответствует счетное
число прообразов.

Тогда можно определить \(\E\left( X \mid \mathcal{B}_Y \right)\), но так никто не пишет.

\begin{notation}
  \(\E\left( X \mid Y \right) \coloneqq \E\left( X \mid \mathcal{B}_Y \right)\).
\end{notation}

\begin{statement}
  \(\E\left( X \mid Y \right) = \varphi(Y)\) (функция), где \(\varphi(y) \coloneqq \E\left( X \mid Y
  = y \right)\).
\end{statement}
\begin{proof}
  \(\E\left( X \mid Y \right) = \sum_y \E\left( X \mid Y = y \right) \mathbb{1}_{Y = y} = \sum_y
  \varphi(y) \mathbb{1}_{Y = y} = \varphi(Y)\).
\end{proof}

Если есть некоторый набор случайных величин \(Y_1, \ldots, Y_m\), то он порождает разбиение
\(\mathcal{B}_{Y_1, \ldots, Y_m} \coloneqq \left\{ (Y_1 = y_1, \ldots, Y_m = y_m) \right\}\).

\begin{notation}
  \(\E\left( X \mid Y_1, \ldots, Y_m \right) \coloneqq \E\left( X \mid \mathcal{B}_{Y_1, \ldots,
  Y_m} \right)\).
\end{notation}

\begin{statement}
  \(\E\left( X \mid Y_1, \ldots, Y_m \right) = \varphi(Y_1, \ldots, Y_m)\), где \(\varphi(y_1,
  \ldots y_m) = \E\left( X \mid Y_1 = y_1, \ldots, Y_m = y_m \right)\).
\end{statement}

\subsection{Мартингалы}

Пусть \(\mathcal{B}\) --- разбиение, а \(X = \sum_j c_j \mathbb{1}_{B_j}\) (случайная величина
постоянна на элементах разбиения).

\begin{definition}
  Такое \(X\) называется измеримым относительно \(\mathcal{B}\).
\end{definition}

Рассмотрим теперь последовательность разбиений \(\mathcal{B_1}, \ldots, \mathcal{B_n}\), причем
каждое следующее является измельчением предыдущего.

Это можно понимать так: идет время (день \(1\), день \(2\), и так далее); \(\mathcal{B}_1\)
описывает события, произошедшие сразу, в первый день; \(\mathcal{B_2}\) описывает события,
произошедшие в день \(1\) и, возможно, день \(2\), и так далее.

Сопоставим каждому \(\mathcal{B}_i\) измеримое относительно него \(X_i\).

\begin{definition}
  Последовательность \(X_1, \ldots, X_n\) называется мартингалом, если
  \begin{enumerate}
    \item \(X_k\) измеримо относительно \(\mathcal{B}_k\);
    \item \(\E\left( X_{k + 1} \mid \mathcal{B}_k \right) = X_k\).
  \end{enumerate}
\end{definition}

То есть каждый следующий элемент последовательность \textit{проектируется} в предыдущий.

\begin{example}
  Пусть \(Y_1, \ldots, Y_n\) --- независимые случайные величины такие, что \(\E{Y_k} = 0\). Построим
  по ним мартингал.

  Положим, что \(X_k \coloneqq \sum_{j = 1}^k Y_i\), а \(\mathcal{B}_k = \mathcal{B}(Y_1, \ldots,
  Y_k)\). Утверждается, что \(X_k\) --- мартингал, то есть мартингалы --- обобщение сумм независимых
  случайных величин.

  Чтобы это показать, проверим оба свойства:
  \begin{enumerate}
    \item \(X_k = \sum_{j = 1}^k Y_j = \sum_{y_1, \ldots, y_k} (y_1 + \ldots + y_k)
      \mathbb{1}_{\left\{ Y_1 = y_1, \ldots, Y_k = y_k \right\}}\).

    \item \(\E\left( X_{k + 1} \mid \mathcal{B}_k \right) = \E\left( X_k + Y_{k + 1} \mid
      \mathcal{B}_k \right) = \E\left( X_k \mid \mathcal{B}_k \right) + \E\left( Y_{k + 1} \mid
      \mathcal{B}_k \right) = X_k + 0\) в силу свойств проекции (для \(X_k\)) и независимости \(Y_{k
      + 1}\) от элементов разбиений \(\mathcal{B}_k\), из-за чего условность пропадает, а \(\E{Y_{k +
      1}} = 0\).
  \end{enumerate}
\end{example}

\begin{example}
  Пусть \(Y_1, \ldots, Y_n\) --- независимые случайные величины такие, что \(\E{Y_k} = 0\);
  аналогично, \(\mathcal{B}_k = \mathcal{B}(Y_1, \ldots, Y_k)\).

  Положим, что теперь \(X_k \coloneqq \sum_{j = 1}^k V_j(Y_1, \ldots, Y_{j - 1}) Y_j\), где
  \(V_j\colon \R^{j - 1} \to \R\). Заметим, что подобные случайные величины уже встречались при
  рассмотрении примеров со ставками в казино.

  Утверждается, что такие \(\left(X_k, \mathcal{B}_k\right)\) тоже являются мартингалом.

  Проверим только второе свойство, ведь первое довольно очевидно.

  \(\E\left( X_{k + 1} \mid \mathcal{B}_k \right) = \E\left( X_k + V_{k + 1}(Y_1, \ldots, Y_k)Y_{k +
  1} \mid \mathcal{B}_k \right) = X_k + \E\left( V_{k + 1}(Y_1, \ldots, Y_k)Y_{k + 1} \mid
  \mathcal{B}_k \right) = X_k + V_{k + 1}(Y_1, \ldots, Y_k) \underbrace{\E\left( Y_{k + 1} \mid
  \mathcal{B}_k \right)}_0\) по правилу вынесения.
\end{example}

Рассмотрим последний пример.

\begin{example}
  Пусть \(\mathcal{B}_1, \ldots, \mathcal{B}_n\) --- произвольная последовательность измельчающих
  разбиений, а \(Y\) --- случайная величина такая, что \(\E{|Y|} < +\infty\).

  Тогда \(X_k \coloneqq \E\left( Y \mid \mathcal{B}_k \right)\) --- мартингал.

  \begin{enumerate}
    \item Очевидно по определению условного мат. ожидания.

    \item \(\E\left( X_{k + 1} \mid \mathcal{B}_k \right) = \E\left( \E\left( Y \mid \mathcal{B}_{k
      + 1}\right) \mid \mathcal{B}_k \right) = \E\left( Y \mid \mathcal{B}_k \right) = X_k\) по
      правилу композиции.
  \end{enumerate}
\end{example}

Ну вот, собственно, и всё.

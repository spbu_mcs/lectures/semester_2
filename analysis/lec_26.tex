\lecture{26}{20.05.2024, 11:15}{}

Пусть \(f(x, y)\) и \(\frac{\partial}{\partial y} f(x, y)\) непрерывны на \([a, +\infty) \times
(y_1, y_2)\).

\begin{theorem}[о дифференцировании несобственных интегралов]
  Если \(\int_a^{+\infty} f(x, y) \df{x}\) сходится на \((y_1, y_2)\) и \(\int_a^{+\infty}
  \frac{\partial}{\partial y} f(x, y) \df{x}\) сходится равномерно на \((y_1, y_2)\), то \[
    \frac{\df{}}{\df{y}} \int_a^{+\infty} f(x, y) \df{x} = \int_a^{+\infty} \frac{\partial}{\partial
    y} f(x, y) \df{x}.
  \]
\end{theorem}
\begin{proof}
  Введем последовательность \(I_n(y) \coloneqq \int_a^n f(x, y) \df{x}\):
  \[
    I_n(y) \xrightarrow{n \to \infty} \int_a^{+\infty} f(x, y) \df{x} \quad \forall y \in (y_1,
    y_2).
  \] \(I_n(y)\) --- собственный интеграл, и, применяя уже доказанную теорему,
  \[
    I_n'(y) = \int_a^n \frac{\partial}{\partial y} f(x, y) \df{x} \unito \int_a^{+\infty}
    \frac{\partial}{\partial y} f(x, y) \df{x}
  \] при \(n \to \infty\).
\end{proof}

Пусть \(f(x, y)\) теперь непрерывна на \([a, +\infty) \times [y_1, y_2]\).

\begin{theorem}[об интегрировании несобственных интегралов]
  Если \(\int_a^{+\infty} f(x, y) \df{x}\) сходится равномерно на \([y_1, y_2]\), то \mnote{формула
  Фурье} \[
    \int_{y_1}^{y_2} \left( \int_a^{+\infty} f(x, y) \df{x} \right) \df{y} = \int_a^{+\infty}
    \left( \int_{y_1}^{y_2} f(x, y) \df{y} \right) \df{x}.
  \]
\end{theorem}
\begin{proof}
  В том же духе предлагается рассмотреть \(I_n(y)\).
\end{proof}

\begin{theorem}[признак Дирихле]
  \(\int_a^{+\infty} f(x, y)g(x, y) \df{x}\) сходится равномерно на \(Y = (y_1, y_2)\), если
  \begin{enumerate}
    \item ``частичные интегралы'' \(\left| \int_a^k f(x, y) \df{x} \right| \le M \quad \forall k \ge
      a\);
    \item \(g(x, y) \unito 0\) при \(x \to +\infty\) на \(Y\);
    \item \(g(x, y)\) монотонна по \(x\) на \(Y\);
    \item существует непрерывная производная \(\frac{\partial}{\partial x} g(x, y)\). \mnote{это
      условие не обязательно, но без него тяжелее доказывать}
  \end{enumerate}
\end{theorem}

Доказательство предоставляется читателю.

Признак Абеля предлагается не только самому доказать, но и самому сформулировать.

\begin{theorem}[признак Абеля] \mnote{для тех, кто не справился сам}
  \(\int_a^{+\infty} f(x, y)g(x, y) \df{x}\) сходится равномерно на \(Y = (y_1, y_2)\), если: \begin{enumerate}
    \item интеграл \(\int_a^{+\infty} f(x, y) \df{x}\) сходится равномерно на \(Y\);
    \item \(\forall y \quad g(x, y)\) монотонна по \(x\) на \([a, +\infty)\);
    \item \(\exists M \in \R\colon \forall x \in [a, +\infty) \forall y \in Y \quad |g(x, y)| < M\).
  \end{enumerate}
\end{theorem}

\begin{example}
  Пусть \(f(a) = \int_0^{+\infty} \frac{1}{x^2 + a} \df{x}\): \(f(a) = \frac{\pi}{2\sqrt{a}}\).

  Тогда с одной стороны \(f'(a) = -\frac{\pi}{4a^{\frac{3}{2}}}\), а с другой ---
  \(-\int_0^{+\infty} \frac{1}{(x^2 + a)^2} \df{x}\).

  \begin{exercise}
    Найти общую формулу для \(\int_0^{+\infty} \frac{1}{(x^2 + a)^n} \df{x}\).
  \end{exercise}
\end{example}

\begin{example} \mnote{``лучше посчитать один интеграл десятью способами, чем десять интегралов
одним''}
  Рассмотрим (еще раз) \(I \coloneqq \int_0^{+\infty} e^{-x^2} \df{x}\).

  Появившийся после замены \(r\) --- Якобиан: \[
  \begin{aligned}
    I^2 &= \int_0^{+\infty} e^{-x^2} \df{x} \int_0^{+\infty} e^{-y^2} \df{y} \\
    &= \int_0^{+\infty} \int_0^{+\infty} e^{-(x^2 + y^2)} \df{x}\df{y} \\
    &= \begin{bmatrix}
      x = r\cos\varphi \\ y = r\sin\varphi \\
    \end{bmatrix} = \int_0^{+\infty} \left( \int_0^{\frac{\pi}{2}} e^{-r^2} r\df{\varphi} \right)
    \df{r} \\
    &= \frac{\pi}{2} \int_0^{+\infty} e^{-r^2} r \df{r} = \boxed{\frac{\pi}{4}}.
  \end{aligned}
  \]
\end{example}

\section{Эйлеровы интегралы}

\begin{definition}
  Гамма функция Эйлера \(\Gamma(a) \coloneqq \int_0^{+\infty} x^{a - 1} e^{-x} \df{x}\), \(a > 0\).
\end{definition}

\begin{statement}
  \(\Gamma(a)\) сходится локально равномерно на \(\R_+\).
\end{statement}

\begin{statement}
  \(\Gamma(a + 1) = a\Gamma(a)\).
\end{statement}
\begin{proof}
  Интегрированием по частям.
\end{proof}

\begin{consequence}
  \(\Gamma(n) = (n - 1)!\) для \(n \in \N_0\).
\end{consequence}

\begin{definition}
  Бета функция Эйлера \(\Beta(a, b) \coloneqq \int_0^1 x^{a - 1} (1 - x)^{b - 1} \df{x}\) для \(a, b
  > 0\).
\end{definition}

\begin{statement}
  Если \(m, n \in \N\), то \(\Beta(m, n) = \int_0^1 x^{m - 1} (1 - x)^{n - 1} \df{x} = \ldots\)
  (много раз интегрировать по частям) \(= \frac{(m - 1)!(n - 1)!}{(m + n - 1)!} =
  \frac{\Gamma(m)\Gamma(n)}{\Gamma(m + n)}\).
\end{statement}

\begin{theorem}
  \(\Beta(a, b) = \frac{\Gamma(a)\Gamma(b)}{\Gamma(a + b)}\) для \(a, b > 0\).
\end{theorem}
\begin{proof}
  Заметим, что \(\Gamma(a)\) при \(x = t^2\) --- \(2\int_0^{+\infty} t^{2a - 1} e^{-t^2} \df{t}\).
  Для удобства переименуем \(t\) обратно в \(x\).

  \[
  \begin{aligned}
    \Gamma(a)\Gamma(b) &= 4\int_0^{+\infty} x^{2a - 1} e^{-x^2} \df{x} \int_0^{+\infty} y^{2b - 1}
    e^{-y^2} \df{y} \\
    &= 4\int_0^{+\infty} \int_0^{+\infty} x^{2a - 1}y^{2b - 1} e^{-(x^2 + y^2)} \df{x}\df{y} \\
    &= \begin{bmatrix}
      x = r\cos\varphi \\ y = r\sin\varphi
    \end{bmatrix} = 4\int_0^{+\infty} \left( \int_0^{\frac{\pi}{2}} r^{2a - 1 + 2b - 1} r e^{-r^2}
    \cos^{2a - 1}\varphi \sin^{2b - 1}\varphi \df{\varphi} \right) \df{r} \\
    &= \Gamma(a + b) 2\int_0^{\frac{\pi}{2}} \cos^{2a - 1}\varphi \sin^{2b - 1}\varphi \df{\varphi}
    \\
    &= \Gamma(a + b) \Beta(a, b),
  \end{aligned}
  \] --- последний переход можно получить заменой \(x = \sin^2\varphi\). \mnote{\(B(a, b) = B(b,
  a)\)}
\end{proof}

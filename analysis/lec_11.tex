\lecture{11}{21.03.2024, 15:35}{}

Пусть \(f\colon \underbrace{\mathcal{D}}_{\subseteq \R^n} \to \R\) дифференцируема в точке \(x\).

\begin{definition}
  Градиент в точке \(x\) --- \(\grad{f} \coloneqq f'(x)\).
\end{definition}

\begin{theorem}
  Если \(f\) дифференцируема в точке \(x\), то \(\grad{f(x)} = \left( \frac{\partial f}{\partial
  x_1}, \frac{\partial f}{\partial x_2}, \ldots, \frac{\partial f}{\partial x_n} \right)\), причем
  \(f'(x)h = \inner{\grad{f}}{h}\).
\end{theorem}
\begin{proof}
  \[
  f(x_1 + th_1, x_2, \ldots, x_n) - f(x_1, \ldots, x_n) = f'(x)\begin{pmatrix} th_1 \\ 0 \\ \vdots
  \\ 0 \end{pmatrix} + \smallO\left( t|h| \right),
  \] --- обозначив \(f'(x) \coloneqq \left( y_1, \ldots, y_n \right)\), \[
  \ldots = y_1th_1 + \smallO\left( t|h| \right)
  .\]

  Для определения производной, поделим всё это на \(t\) и перейдем к пределу (\(h_1 = 1\) как
  производная по направлению): \[
  \lim_{t \to 0} \frac{y_1th_1 + \smallO\left( t|h| \right)}{t} = \frac{\partial f}{\partial x_1}
  .\]
\end{proof}

\begin{theorem}[экстремальное свойство градиента]
  Если \(|h| = 1\), то  \[
    -\left| \grad{f(x)} \right| \le D_hf(x) \le \left| \grad{f(x)} \right|.
  \]
\end{theorem}
\begin{proof}
  \[
  \begin{aligned}
    D_hf(x) &= \inner{\grad{f(x)}}{h} \\
    &\oversym{\le}{\text{КБШ}} \left| \grad{f(x)} \right||h|.
  \end{aligned}
  \]
\end{proof}

\begin{remark}
  Равенство справа достигается для \(h = \frac{\grad{f(x)}}{\left| \grad{f(x)} \right|}\), если
  \(\grad{f} \neq 0\), а слева, если \(h = -\ldots\).
\end{remark}

\begin{example}
  \(z_n = f(x_n, y_n)\), а \(x_{n + 1} = x_n + \lambda_n\frac{\partial f}{x_n}\) и \(y_{n +
  1}\) аналогично. Для нахождения максимума, идти надо идти по градиенту, однако, возможно попадание
  в область лишь локального экстремума.
\end{example}

По-другому говоря, градиент --- область наискорейшего роста: как уже известно, \(f(x + h) - f(x) =
\underbrace{f'(x)th}_{\grad{f(x)}} + \smallO(th)\).

\begin{theorem}[необходимое условие локального экстремума]
  Если \(x\) --- точка локального экстремума \(f\), а \(f\) дифференцируема в этой точке, то
  \(\grad{f(x)} = 0\).
\end{theorem}
\begin{proof}
  Предположим, что \(\grad{f(x)} \neq 0\) и рассмотрим производную по направлению градиента \(h =
  \frac{\grad{f(x)}}{\left| \grad{f(x)} \right|}\): \(D_h(x) = \grad{f(x)} = 0\).

  С другой стороны, \(D_h(x) = \lim_{t \to 0} \frac{f(x + th(x)) - f(x)}{t} > 0 \iff \exists t_0 >
  0\colon \forall t \in (0, t_0) \quad f(x + th(x)) > f(x)\) --- противоречие.
\end{proof}

\begin{example}
  Пусть \(u(x, y, z)\) дифференцируема в некоторой области \(\mathcal{D} \subset \R^3\), а \(l_i =
  \begin{pmatrix}\cos\alpha_i \\ \cos\beta_i \\ \cos\gamma_i\end{pmatrix}\) --- направления
  (\textit{направляющие косинусы}); требуется, чтобы \(\inner{l_i}{l_j} = \delta_{ij}\), и
  чтобы такие \(l_i\) образовали ортонормированный базис.

  Тогда \[
    \left( \frac{\partial u}{\partial l_1} \right)^2 + \left( \frac{\partial u}{\partial l_2}
    \right)^2 + \left( \frac{\partial u}{\partial l_3} \right)^2 = \left|\grad{u}\right|^2 =
    \inner{\grad{u}}{\grad{u}}
  ,\] --- другими словами, модуль градиента не меняется при движении пространства (замене базиса).

  \begin{proof}
    \[
      \frac{\partial u}{\partial l_1} = \frac{\partial u}{\partial x}\cos\alpha_1 + \frac{\partial
      u}{\partial y}\cos\beta_1 + \frac{\partial u}{\partial z}\cos\gamma_1,
    \]
    --- по факту, просто скалярное произведение (производная по направлению).

    Продолжая, \[
    \begin{aligned}
      \frac{\partial u}{\partial l_2} &= \frac{\partial u}{\partial x}\cos\alpha_2 + \frac{\partial
      u}{\partial y}\cos\beta_2 + \frac{\partial u}{\partial z}\cos\gamma_2 \\
      \frac{\partial u}{\partial l_3} &= \frac{\partial u}{\partial x}\cos\alpha_3 + \frac{\partial
      u}{\partial y}\cos\beta_3 + \frac{\partial u}{\partial z}\cos\gamma_3.
    \end{aligned}
    \]

    За счет ортогональности возникнет \(\sum_k^3 \cos^2\alpha_k = 1\), \(\sum_k^3
    \cos\alpha_k\cos\beta_k = 0\) и еще некоторые соотношения --- полностью это расписав, получится
    искомый результат.
  \end{proof}
\end{example}

Посмотрим, какая связь может быть между существованием градиента и частной дифференцируемостью
функции.

\begin{example}
  Пусть \(f(x, y) = \begin{cases}
    1, &y = x^2, x > 0 \\
    0, &\text{иначе}
  \end{cases}\).

  Тогда \(\forall h \quad \frac{\partial f}{\partial h}(0, 0) = 0\), но \(f'(0, 0)\) не существует.
\end{example}

Пусть \(f\colon \mathcal{D} \to \R\).

\begin{theorem}[о дифференцируемости функции с непрерывными частными производными]
  Если существует \(\frac{\partial f}{\partial x_k} \quad \forall k = 1, \ldots, n\) (в
  \(\mathcal{D}\)) и функция непрерывна в точке \(x = x^0\), то \(f\) дифференцируема в точке \(x\).
\end{theorem}
\begin{proof}
  Покажем это для \(\R^3\) --- для \(\R^n\) доказывается аналогично.

  Пусть \(x_0 = \begin{pmatrix} x_1 & x_2 & x_3 \end{pmatrix}^\top\): \[
  \begin{array}{lll}
    f(x_1 + h_1, x_2 + h_2, x_3 + h_3) - f(x_1, x_2, x_3) &= &f(x_1 + h_1, x_2 + h_2, x_3 + h_3) \\
    &&- f(x_1, x_2 + h_2, x_3 + h_3) \\
    &&+ f(x_1, x_2 + h_2, x_3 + h_3) \\
    &&- f(x_1, x_2, x_3 + h_3) \\
    &&+ f(x_1, x_2, x_3 + h_3) \\
    &&- f(x_1, x_2, x_3) \\
    &\oversym{=}{\text{т. Л-жа}}& D_1(x_1 + \Theta_1h_1, x_2 + h_2, x_3 + h_3)h_1 \\
    &&+ D_2(x_1, x_2 + \Theta_2h_2, x_3 + h_3)h_2 \\
    &&+ D_3(x_1, x_2, x_3 + \Theta_3h_3)h_3 \\
    &=& D_1(x_1, x_2, x_3)h_1 \\
    &&+ D_2(x_1, x_2, x_3)h_2 \\
    &&+ D_3(x_1, x_2, x_3)h_3 \\
    &&+ D_1(x_1 + \Theta_1h_1, x_2 + h_2, x_3 + h_3)h_1 \\
    &&- D_1(x_1, x_2, x_3)h_1 \\
    &&+ D_2(x_1, x_2 + \Theta_2h_2, x_3 + h_3)h_2 \\
    &&- D_2(x_1, x_2, x_3)h_2 \\
    &&+ D_3(x_1, x_2, x_3 + \Theta_3h_3)h_3 \\
    &&- D_3(x_1, x_2, x_3)h_3,
  \end{array}
  \] где \(\Theta_i \in (0, 1)\).

  На самом деле, этого уже достаточно: например, \((D_3(x_1, x_2, x_3 + \Theta_3h_3) - D_3(x_1, x_2,
  x_3))h_3 = \smallO(|h|)\) по непрерывности \(D_3\) (ведь \(h_3 = \bigO(h)\)); с остальными
  аналогично.
\end{proof}

Более того, это работает и для \(\R^m \to \R^n\), ведь случай для строки матрицы Якоби уже
доказан выше.
